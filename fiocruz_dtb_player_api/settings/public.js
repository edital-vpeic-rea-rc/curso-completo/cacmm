/**
 * This file contains the settings that can be visualized by anyone in a public domain.
 */

exports.public_settings = {
    PPU_PREFIX: "FIOCRUZ_PE_0001_MOSQUITOS_BASES_VIGILANCIA_CONTROLE_"
}

exports.register_logs_remote_db = true;