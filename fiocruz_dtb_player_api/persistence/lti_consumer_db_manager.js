/**
 * The functions of this file are responsible for managing objects of the class LTIConsumer 
 * on a MySQL database.
 */

const TAG = "lti_consumer_manager";

const MSG_FAILED_INVALID_PARAMS = "Invalid input params";
const MSG_FAILED_UPSERT = "Failed to upsertLTIConsumer";
const MSG_FAILED_GET = "Failed to getLTIConsumer";
const MSG_FAILED_GET_ALL = "Failed to getLTIConsumers";
const MSG_FAILED_DELETE = "Failed to deleteLTIConsumer";

const MSG_CREATED_ITEM = "A LTIConsumer has been created";
const MSG_UPDATED_ITEM = "A LTIConsumer has been updated";
const MSG_GOT_ITEM = "A LTIConsumer has been retrieved";
const MSG_GOT_ITEMS = "LTIConsumers have been retrieved";
const MSG_DELETED = "LTIConsumer has been deleted";

var sqlManager = require("./sql_manager");
var logDBManager = require("./log_db_manager");
var logHelper = require("../helpers/log_helper");
var LTIConsumer = require("../models/lti_consumer");

var upsertLTIConsumer = function(course_name, course_url, course_short_name, external_resource_name,
                                 lti_consumer_tool_name, lti_version, utc, timezone) {
    
    return new Promise(function(resolve, reject) {

        try {
            var ltiConsumer = LTIConsumer.obj.new();
    
            ltiConsumer.course_name = course_name;
            ltiConsumer.course_url = course_url;
            ltiConsumer.course_short_name = course_short_name;
            ltiConsumer.external_resource_name = external_resource_name;
            ltiConsumer.lti_consumer_tool_name = lti_consumer_tool_name;
            ltiConsumer.lti_version = lti_version;
            ltiConsumer.created_at = utc;
            ltiConsumer.updated_at = utc;
            ltiConsumer.timezone = timezone;
    
            if (ltiConsumer.isValid()) {
    
                getLTIConsumer(ltiConsumer.course_name, ltiConsumer.course_url).then(function(dbLtiConsumer) {
    
                    if (dbLtiConsumer == undefined) {
    
                        // Create LTI_Consumer
                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'INSERT INTO LTI_Consumer SET ?', 
                                ltiConsumer, 
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.LTI_CONSUMER, ltiConsumer.course_url, ltiConsumer, false);
                                    logHelper.logMsg(TAG, MSG_CREATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                            reject(err);
                        });
                    } else {
    
                        // Update LTI_Consumer
                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'UPDATE `LTI_Consumer` SET ' +
                                '`course_name` = ?,' +
                                '`course_url` = ?,' +
                                '`course_short_name` = ?,' +
                                '`external_resource_name` = ?,' +
                                '`lti_consumer_tool_name` = ?,' +
                                '`lti_version` = ?,' +
                                '`updated_at` = ?,' +
                                '`timezone` = ? ' +
                                'WHERE `course_name` = ? AND `course_url` = ?',
                                [
                                    ltiConsumer.course_name,
                                    ltiConsumer.course_url,
                                    ltiConsumer.course_short_name,
                                    ltiConsumer.external_resource_name,
                                    ltiConsumer.lti_consumer_tool_name,
                                    ltiConsumer.lti_version,
                                    ltiConsumer.updated_at,
                                    ltiConsumer.timezone,
                                    ltiConsumer.course_name,
                                    ltiConsumer.course_url
                                ], 
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.LTI_CONSUMER, ltiConsumer.course_url, ltiConsumer, false);
                                    logHelper.logMsg(TAG, MSG_UPDATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                            reject(err);
                        });
                    }
                }, function(err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
                    reject(err);
                });
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, MSG_FAILED_INVALID_PARAMS);
                reject(MSG_FAILED_INVALID_PARAMS);
            }
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
            reject(err);
        }
    });
}

var getLTIConsumer = function(course_name, course_url) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `LTI_Consumer` WHERE `course_name` = ? and `course_url` = ?",
                        timeout: 40000,
                    },
                    [
                        course_name,
                        course_url
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                            reject(err);

                        } else if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);
                                resolve(sqlManager.getSingleItemFromResult(results));
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);
                                resolve(undefined);
                            }
                        } else {
                            logHelper.logMsg(TAG, MSG_GOT_ITEM);
                            resolve(undefined);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                reject(err);    
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
            reject(err);
        }
    });
}

var getLTIConsumers = function() {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `LTI_Consumer`",
                        timeout: 40000,
                    },
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                            reject(err);
                        } 
                        
                        if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve(sqlManager.getListItemsFromResult(results));
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve([]);
                            }
                        } else {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, "");
                            reject(MSG_FAILED_GET_ALL);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
            reject(err);
        }
    });
}

var deleteLTIConsumer = function(course_name, course_url) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "DELETE FROM `LTI_Consumer` WHERE `course_name` = ? and `course_url` = ?",
                        timeout: 40000,
                    },
                    [
                        course_name,
                        course_url
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                            reject(err);
                        }

                        logHelper.logMsg(TAG, MSG_DELETED);
                        resolve(true);
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
            reject(err);
        }
    });
}

exports.upsertLTIConsumer = upsertLTIConsumer;
exports.getLTIConsumer = getLTIConsumer;
exports.getLTIConsumers = getLTIConsumers;
exports.deleteLTIConsumer = deleteLTIConsumer;