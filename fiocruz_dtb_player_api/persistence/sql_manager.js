/**
 * The functions of this file are responsible for creating a managing a connection with a MySQL database.
 */

const TAG = "sql_manager";
const MSG_FAILED_GET_CONNECTION = "Failed to getConnection";

var mysql = require('mysql');

var secretSettings = require("../settings/secret");
var logHelper = require("../helpers/log_helper");

var isTesting = false;

/** 
 * General
 */
exports.setIsTesting = function(isTestingVal) {
    isTesting = isTestingVal;
}

exports.getConnection = function() {

    return new Promise(function(resolve, reject) {

        try {
            var connection = undefined

            if (isTesting) {
                connection = mysql.createConnection(secretSettings.relational_db_connection_test);
            } else {
                connection = mysql.createConnection(secretSettings.relational_db_connection);
            }
            
            connection.connect(function(err) {

                if (err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_CONNECTION, err.stack);
                    reject(err);
                }

                resolve(connection);
            });
    
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_CONNECTION, err);
            reject(err);
        }

    });
}

exports.getListItemsFromResult = function(results) {
    try {
        return JSON.parse(JSON.stringify(results));

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed to getListItemsFromResult", err);
    }

    return undefined;
}

exports.getSingleItemFromResult = function(results) {
    try {
        return JSON.parse(JSON.stringify(results[0]));

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed to getSingleItemFromResult", err);
    }

    return undefined;
}