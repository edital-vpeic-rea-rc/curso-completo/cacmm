/**
 * The functions of this file are responsible for creating a managing connection with NoSQL databases such
 * as Redis and Firebase.
 */

const TAG = "nosql_manager";
const MSG_FAILED_SET_SESSION = "Failed to setSessionVariable";
const MSG_FAILED_GET_SESSION = "Failed to getSessionVariable";
const MSG_FAILED_CONNECT_FIREBASE = "Failed to connect to Firebase log";

var frbAdmin = require("firebase-admin");

var logHelper = require("../helpers/log_helper");
var settings = require("../settings/secret");

var sessions = {};
var isConnectedFrb = false;

/** 
 * General
 */
var setSessionVariable = function(sessionID, value) {
    
    return new Promise(function(resolve, reject) {
        try {
            sessions[sessionID] = value;
            resolve(true);
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_SESSION, err);
            reject(MSG_FAILED_SET_SESSION);
        }
    });
}

var getSessionVariable = function(sessionID) {

    return new Promise(function(resolve, reject) {
        try {
            resolve(sessions[sessionID]);
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_SESSION, err);
            reject(MSG_FAILED_GET_SESSION);
        }
    });
}

var connectToFrbDB = function() {
    var success = true;
        
    try {
        frbAdmin.initializeApp({
            credential: frbAdmin.credential.cert(settings.nosql_db_connection),
            databaseURL: settings.nosql_db_connection_meta.frb_proj_db_url,
            storageBucket: settings.nosql_db_connection_meta.frb_proj_bucket,
        });

        isConnectedFirebase = true;

    } catch (err) {
        logHelper.logMsgWithErr(TAG, MSG_FAILED_CONNECT_FIREBASE, err);
        success = false;
        isConnectedFirebase = false;
    }

    return success;
}

var isConnectedFrb = function() {
    return isConnectedFrb;
}

var getFrbAdmin = function() {
    return frbAdmin;
}

exports.setSessionVariable = setSessionVariable;
exports.getSessionVariable = getSessionVariable;
exports.connectToFrbDB = connectToFrbDB;
exports.isConnectedFrb = isConnectedFrb;
exports.getFrbAdmin = getFrbAdmin;