/**
 * The functions of this file are responsible for sending log information to a remote NoSQL database.
 */

const TAG = "log_db_manager";
const MSG_FAILED_REGISTER_LOG = "Failed to registerLog";

var settings = require("../settings/public");
var nosqlManager = require("./nosql_manager");
var logHelper = require("../helpers/log_helper");
var idHelper = require("../helpers/id_helper");
var objHelper = require("../helpers/obj_helper");

var logTypes = {
    LTI_CONSUMER: "LTI_CONSUMER",
    STUDENT: "STUDENT",
    STUDENT_DATA: "STUDENT_DATA"
}

var registerLog = function(logType, subObjId, data, pushNew) {

    return new Promise(function(resolve, reject) {

        try {

            if (!settings.register_logs_remote_db) {
                resolve(true);

            } else if (data.logObj != undefined) {

                if (objHelper.isJson(data)) {
                    var db = nosqlManager.getFrbAdmin().database();
                    var logTypeRef = undefined;
    
                    if (subObjId == undefined) {
                        logTypeRef = db.ref(`${logType}`);
                    } else {
                        subObjId = idHelper.getValidIdForFirebase(subObjId);
                        logTypeRef = db.ref(`${logType}/${subObjId}`);   
                    }
        
                    if (pushNew) {
                        var newLogRef = logTypeRef.push();
                        data.firebaseUID = newLogRef.getKey()
                        newLogRef.set(data.logObj());
                    } else {
                        logTypeRef.set(data.logObj());
                    }
            
                    resolve(true);
            
                } else {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_REGISTER_LOG, "Data object can't be converted into JSON string.");
                    reject(false)
                }
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_REGISTER_LOG, "Data object doesn't have a log representation.");
                reject(false)
            }
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_REGISTER_LOG, err);
            reject(false);
        }
    });
}

exports.registerLog = registerLog;
exports.logTypes = logTypes;