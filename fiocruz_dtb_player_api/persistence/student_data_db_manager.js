/**
 * The functions of this file are responsible for managing objects of the class StudentData 
 * on a MySQL database.
 */

const TAG = "student_data_manager";

const MSG_FAILED_INVALID_PARAMS = "Invalid input params";
const MSG_FAILED_UPSERT = "Failed to upsertStudentData";
const MSG_FAILED_GET = "Failed to getStudentData";
const MSG_FAILED_GET_ALL = "Failed to getAllStudentData";
const MSG_FAILED_DELETE = "Failed to deleteStudentData";

const MSG_CREATED_ITEM = "StudentData has been created";
const MSG_UPDATED_ITEM = "StudentData has been updated";
const MSG_GOT_ITEM = "StudentData has been retrieved";
const MSG_GOT_ITEMS = "All StudentData have been retrieved";
const MSG_DELETED = "StudentData has been deleted";

var sqlManager = require("./sql_manager");
var logDBManager = require("./log_db_manager");
var logHelper = require("../helpers/log_helper");
var objHelper = require("../helpers/obj_helper");
var StudentData = require("../models/student_data");

var upsertStudentData = function(student_id, student_email, data_key, data_value, utc, timezone) {

    return new Promise(function(resolve, reject) {

        try {
            var studentData = StudentData.obj.new();
            var value = undefined;

            if (objHelper.isJson(data_value)) {
                value = JSON.stringify(data_value);
            } else {
                value = `${value}`;
            }

            studentData.student_id = student_id;
            studentData.data_key = data_key;
            studentData.data_value = value;
            studentData.created_at = utc;
            studentData.updated_at = utc;
            studentData.timezone = timezone;
    
            if (studentData.isValid()) {
    
                getStudentData(studentData.student_id, studentData.data_key).then(function(dbStudentData) {
    
                    if (dbStudentData == undefined) {
    
                        // Create StudentData
                        if (logHelper.isDebugEnabled()) {
                            console.log(JSON.stringify(studentData));
                        }

                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'INSERT INTO Student_Data SET ?', 
                                studentData, 
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.STUDENT_DATA, student_email, studentData, true);
                                    logHelper.logMsg(TAG, MSG_CREATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                            reject(err);
                        });
                    } else {
    
                        // Update Student
                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'UPDATE `Student_Data` SET ' +
                                '`student_id` = ?,' +
                                '`data_key` = ?,' +
                                '`data_value` = ?,' +
                                '`updated_at` = ?,' +
                                '`timezone` = ? ' +
                                'WHERE `student_id` = ? AND `data_key` = ?',
                                [
                                    studentData.student_id,
                                    studentData.data_key,
                                    studentData.data_value,
                                    studentData.updated_at,
                                    studentData.timezone,
                                    studentData.student_id,
                                    studentData.data_key,
                                ],
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.STUDENT_DATA, student_email, studentData, true);
                                    logHelper.logMsg(TAG, MSG_UPDATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                            reject(err);
                        });
                    }
                }, function(err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
                    reject(err);
                });
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, MSG_FAILED_INVALID_PARAMS);
                reject(MSG_FAILED_INVALID_PARAMS);
            }
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
            reject(err);
        }
    });
}

var getStudentData = function(student_id, data_key) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `Student_Data` WHERE `student_id` = ? and `data_key` = ?",
                        timeout: 40000,
                    },
                    [
                        student_id,
                        data_key
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                            reject(err);

                        } else if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);

                                var dataItem = sqlManager.getSingleItemFromResult(results);
                                dataItem.Value = "";

                                if (objHelper.isJsonString(dataItem.data_value)) {
                                    dataItem.Value = JSON.parse(dataItem.data_value);
                                    dataItem.data_value = dataItem.Value;
                                }

                                resolve(dataItem);
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);
                                resolve(undefined);
                            }
                        } else {
                            logHelper.logMsg(TAG, MSG_GOT_ITEM);
                            resolve(undefined);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                reject(err);    
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
            reject(err);
        }
    });
}

var getAllStudentData = function(student_id) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `Student_Data` WHERE `student_id` = ?",
                        timeout: 40000,
                    },
                    [
                        student_id
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                            reject(err);
                        } 
                        
                        if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve(sqlManager.getListItemsFromResult(results));
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve([]);
                            }
                        } else {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, "");
                            reject(MSG_FAILED_GET_ALL);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
            reject(err);
        }
    });
}

var deleteStudentData = function(student_id, data_key) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "DELETE FROM `Student_Data` WHERE `student_id` = ? and `data_key` = ?",
                        timeout: 40000,
                    },
                    [
                        student_id,
                        data_key
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                            reject(err);
                        }

                        logHelper.logMsg(TAG, MSG_DELETED);
                        resolve(true);
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
            reject(err);
        }
    });
}

exports.upsertStudentData = upsertStudentData;
exports.getStudentData = getStudentData;
exports.getAllStudentData = getAllStudentData;
exports.deleteStudentData = deleteStudentData;