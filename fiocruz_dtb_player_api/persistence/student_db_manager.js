/**
 * The functions of this file are responsible for managing objects of the class Student 
 * on a MySQL database.
 */

const TAG = "student_manager";

const MSG_FAILED_INVALID_PARAMS = "Invalid input params";
const MSG_FAILED_UPSERT = "Failed to upsertStudent";
const MSG_FAILED_GET = "Failed to getStudent";
const MSG_FAILED_GET_ALL = "Failed to getStudents";
const MSG_FAILED_DELETE = "Failed to deleteStudent";

const MSG_CREATED_ITEM = "A Student has been created";
const MSG_UPDATED_ITEM = "A Student has been updated";
const MSG_GOT_ITEM = "A Student has been retrieved";
const MSG_GOT_ITEMS = "Students have been retrieved";
const MSG_DELETED = "Student has been deleted";

var sqlManager = require("./sql_manager");
var logDBManager = require("./log_db_manager");
var logHelper = require("../helpers/log_helper");
var Student = require("../models/student");

var upsertStudent = function(lti_consumer_id, user_email, user_name, full_name, user_id, role, utc, 
                             timezone) {

    return new Promise(function(resolve, reject) {

        try {
            var student = Student.obj.new();
    
            student.lti_consumer_id = lti_consumer_id;
            student.user_email = user_email;
            student.user_name = user_name;
            student.full_name = full_name;
            student.user_id = user_id;
            student.role = role;
            student.created_at = utc;
            student.updated_at = utc;
            student.timezone = timezone;
    
            if (student.isValid()) {
    
                getStudent(student.lti_consumer_id, student.user_email).then(function(dbStudent) {
    
                    if (dbStudent == undefined) {
    
                        // Create Student
                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'INSERT INTO Student SET ?', 
                                student, 
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.STUDENT, student.user_email, student, false);
                                    logHelper.logMsg(TAG, MSG_CREATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `CREATE ${err}`);
                            reject(err);
                        });
                    } else {
    
                        // Update Student
                        sqlManager.getConnection().then(function(connection) {
                            connection.query(
                                'UPDATE `Student` SET ' +
                                '`user_email` = ?,' +
                                '`user_name` = ?,' +
                                '`full_name` = ?,' +
                                '`user_id` = ?,' +
                                '`role` = ?,' +
                                '`updated_at` = ?,' +
                                '`timezone` = ? ' +
                                'WHERE `user_email` = ? AND `lti_consumer_id` = ?',
                                [
                                    student.user_email,
                                    student.user_name,
                                    student.full_name,
                                    student.user_id,
                                    student.role,
                                    student.updated_at,
                                    student.timezone,
                                    student.user_email,
                                    student.lti_consumer_id
                                ], 
                                function (err, results, fields) {
                                    connection.end();
    
                                    if (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                                        reject(err);
                                    }
    
                                    logDBManager.registerLog(logDBManager.logTypes.STUDENT, student.user_email, student, false);
                                    logHelper.logMsg(TAG, MSG_UPDATED_ITEM);

                                    resolve(true);
                                }
                            );
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, `UPDATE ${err}`);
                            reject(err);
                        });
                    }
                }, function(err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
                    reject(err);
                });
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, MSG_FAILED_INVALID_PARAMS);
                reject(MSG_FAILED_INVALID_PARAMS);
            }
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_UPSERT, err);
            reject(err);
        }
    });
}

var getStudent = function(lti_consumer_id, user_email) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `Student` WHERE `lti_consumer_id` = ? and `user_email` = ?",
                        timeout: 40000,
                    },
                    [
                        lti_consumer_id,
                        user_email
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                            reject(err);

                        } else if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);
                                resolve(sqlManager.getSingleItemFromResult(results));
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEM);
                                resolve(undefined);
                            }
                        } else {
                            logHelper.logMsg(TAG, MSG_GOT_ITEM);
                            resolve(undefined);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
                reject(err);    
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET, err);
            reject(err);
        }
    });
}

var getStudents = function() {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "SELECT * FROM `Student`",
                        timeout: 40000,
                    },
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                            reject(err);
                        } 
                        
                        if (results != undefined) {

                            if (results.length > 0) {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve(sqlManager.getListItemsFromResult(results));
                            } else {
                                logHelper.logMsg(TAG, MSG_GOT_ITEMS);
                                resolve([]);
                            }
                        } else {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, "");
                            reject(MSG_FAILED_GET_ALL);
                        }
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_ALL, err);
            reject(err);
        }
    });
}

var deleteStudent = function(lti_consumer_id, user_email) {

    return new Promise(function(resolve, reject) {

        try {
            sqlManager.getConnection().then(function(connection) {
                connection.query(
                    {
                        sql: "DELETE FROM `Student` WHERE `lti_consumer_id` = ? and `user_email` = ?",
                        timeout: 40000,
                    },
                    [
                        lti_consumer_id,
                        user_email
                    ],
                    function (err, results, fields) {
                        connection.end();
    
                        if (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                            reject(err);
                        }

                        logHelper.logMsg(TAG, MSG_DELETED);
                        resolve(true);
                    }
                );
            }, function(err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
                reject(err);
            });
        } catch (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_DELETE, err);
            reject(err);
        }
    });
}

exports.upsertStudent = upsertStudent;
exports.getStudent = getStudent;
exports.getStudents = getStudents;
exports.deleteStudent = deleteStudent;