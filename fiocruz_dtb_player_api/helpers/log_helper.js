/**
 * The functions of this file help the rest of the application on tracking the data flow inside 
 * the app or even on a remote database.
 */

const TAG = "log_helper";

exports.isDebugEnabled = function(value) {
    return true;
}

exports.logMsgWithErr = function(errorLocation, msg, err) {
    try {
        console.log(`${errorLocation} warning: ${msg} ${err}`);

    } catch (err) {

        if (err != undefined) {
            console.log(`${TAG} Failed to logMsgWithErr: ${err}`);
        } else {
            console.log(`${TAG} Failed to logMsgWithErr`);
        }
    }
}

exports.logMsg = function(msgLocation, msg) {
    try {
        console.log(this.getMsg(msgLocation, msg));

    } catch (err) {

        if (err != undefined) {
            console.log(`${TAG} Failed to logMsg ${err}`);
        } else {
            console.log(`${TAG} Failed to logMsg`);
        }
    }
}

exports.getMsg = function(msgLocation, msg) {
    try {
        return `${msgLocation}: ${msg}`;

    } catch (err) {

        if (err != undefined) {
            return `${TAG} Failed to getMsg ${err}`;
        } else {
            return `${TAG} Failed to getMsg`;
        }
    }
}

exports.getMsgWithErr = function(errorLocation, msg, err) {
    try {
        return `${errorLocation} warning: ${msg} ${err}`;

    } catch (_) {
        return msg;
    }
}