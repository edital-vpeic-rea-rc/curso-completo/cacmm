/**
 * The functions of this file help on creating and validating ids.
 */

var settings = require("../settings/public");
var logHelper = require("./log_helper");

const TAG = "id_helper";

exports.userDataTypes = {
    STATUS: "STATUS",
    ANSWER: "ANSWER",
    NAVIGATION: "NAVIGATION",
    DOWNLOAD: "DOWNLOAD",
    EXTERNAL_LINK: "EXTERNAL_LINK",
    VIDEO: "VIDEO",
    COURSE_INIT_DATE: "COURSE_INIT_DATE",
    HAS_PRESENTED_WELCOME_DIALOG: "HAS_PRESENTED_WELCOME_DIALOG"
}

exports.getValidIdForFirebase = function(email) {
    try {
        return email.replace(/[.#\[\]\/$]/g, "_");

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed to getValidIdForFirebase", err);
    }

    return "";
}

exports.getPPUPrefix = function() {
    return settings.public_settings.PPU_PREFIX;
}

exports.getUserDataKeyForDB = function(rawKey) {
    try {
        return rawKey.replace(this.getPPUPrefix(), "");

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed to getUserDataKey", err);
    }

    return rawKey;
}