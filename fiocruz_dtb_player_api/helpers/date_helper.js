/**
 * The functions of this file help on handling the different conversions that can be executed 
 * on a Date object.
 */

exports.getTimestampNow = function() {
    try {
        return new Date().toISOString();

    } catch (err) {
        return "";
    }
}

exports.getTimezone = function() {
    try {
        return new Date().getTimezoneOffset();

    } catch (err) {
        return 0;
    }
}
