/**
 * This controller is used to validate inputs
 */

const TAG = "validation_helper";

var logHelper = require("./log_helper");

exports.isString = function(value) {
   return typeof value === 'string' || value instanceof String;
}

exports.isNumber = function(value) {
   return typeof value === 'number';
}

exports.hasKeys = function(value) {
   return Object.keys(value).length > 0;
}

exports.isValidText = function(input) {
   var isValid = true;
   
   if (input == undefined || input == null || input == "") {
      isValid = false;
   }

   return isValid;
}

exports.isMoodleCourseStudent = function(listRoles) {

   try {

      if (listRoles != undefined) {

         if (listRoles.length > 0) {

            for (var i = 0; i < listRoles.length; i++) {
               var role = listRoles[i];

               if (role === "Learner") {
                  return true;
               }
            }
         }
      }

   } catch (err) {
      logHelper.logMsgWithErr(TAG, "Failed to isMoodleCourseStudent", err);
   }

   return false;
}