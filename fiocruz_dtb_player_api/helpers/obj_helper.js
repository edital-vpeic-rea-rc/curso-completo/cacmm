/**
 * The functions of this file are used to help on the manipulation of objects 
 */

var TAG = "obj_helper";

var logHelper = require("./log_helper");
var validationHelper = require("./validation_helper");

exports.new = function(sourceObj) {
    try {
        return Object.assign({}, sourceObj);

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed on new", err);
    }

    return sourceObj;
}

exports.isJson = function(obj) {
    try {
        JSON.stringify(obj);
        return true;

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed on isJson", err);
    }

    return false;
}

exports.isJsonString = function(str) {
    try {
        JSON.parse(str);
        return true;

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed on isJsonString", err);
    }

    return false;
}

exports.isString = function(obj) {
    try {
        return validationHelper.isString(obj);

    } catch (err) {
        logHelper.logMsgWithErr(TAG, "Failed on isString", err);
    }

    return false;
}