/*

DB Table: Student_Data

    PK student_data_id  - int
       data_key         - text
       data_value       - json
       created_at (utc) - text
       updated_at (utc) - text
       timezone         - integer
    FK student_id       - int
 
*/

const TAG = "student_data";

var logHelper = require("../helpers/log_helper");
var objHelper = require("../helpers/obj_helper");
var validationHelper = require("../helpers/validation_helper");

exports.obj = {

    student_id: -1,
    data_key: "",
    data_value: {},
    created_at: "",
    updated_at: "",
    timezone: 0,

    isValid: function() {
        var isValid = true;

        try {
            
            if (!validationHelper.isNumber(this.student_id)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.data_key)) {
                isValid = false;
            }

            if (!validationHelper.hasKeys(this.data_value)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.created_at)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.updated_at)) {
                isValid = false;
            }

        } catch (err) {
            logHelper.logMsgWithErr(TAG, "Failed on isValid", err);

            isValid = false;
        }

        return isValid;
    },

    new: function() {
        return objHelper.new(this);
    },

    logObj: function() {
        return {
            student_id: this.student_id,
            data_key: this.data_key,
            data_value: this.data_value,
            created_at: this.created_at,
            updated_at: this.updated_at,
            timezone: this.timezone
        };
    }

}
