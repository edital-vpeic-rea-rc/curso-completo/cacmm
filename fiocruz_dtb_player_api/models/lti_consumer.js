/*
 
DB Table: LTI_Consumer

    PK lti_consumer_id                                      - int
       course_name (context_title)                          - text
       course_url (lis_outcome_service_url)                 - text
       course_short_name (context_label)                    - text
       external_resource_name (resource_link_title)         - text
       lti_consumer_tool_name (tool_consumer_instance_name) - text
       lti_version                                          - text
       created_at (utc)                                     - text
       updated_at (utc)                                     - text
       timezone                                             - integer

*/

const TAG = "lti_consumer";

var logHelper = require("../helpers/log_helper");
var objHelper = require("../helpers/obj_helper");
var validationHelper = require("../helpers/validation_helper");

exports.obj = {

    course_name: "",
    course_url: "",
    course_short_name: "",
    external_resource_name: "",
    lti_consumer_tool_name: "",
    lti_version: "",
    created_at: "",
    updated_at: "",
    timezone: 0,

    isValid: function() {
        var isValid = true;

        try {
            
            if (!validationHelper.isValidText(this.course_name)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.course_url)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.created_at)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.updated_at)) {
                isValid = false;
            }

        } catch (err) {
            logHelper.logMsgWithErr(TAG, "Failed on isValid", err);

            isValid = false;
        }

        return isValid;
    },

    new: function() {
        return objHelper.new(this);
    },

    logObj: function() {
        return {
            course_name: this.course_name,
            course_url: this.course_url,
            course_short_name: this.course_short_name,
            external_resource_name: this.external_resource_name,
            lti_consumer_tool_name: this.lti_consumer_tool_name,
            lti_version: this.lti_version,
            created_at: this.created_at,
            updated_at: this.updated_at,
            timezone: this.timezone,
        };
    }

}