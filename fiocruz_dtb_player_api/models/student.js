/*

DB Table: Student

    PK student_id                                    - int
       user_email (lis_person_contact_email_primary) - text
       user_name (ext_user_username)                 - text
       full_name (lis_person_name_full)              - text
       user_id                                       - text
       role                                          - text
       created_at (utc)                              - text
       updated_at (utc)                              - text
       timezone                                      - integer
    FK lti_consumer_id                               - integer

*/

const TAG = "student";

var logHelper = require("../helpers/log_helper");
var objHelper = require("../helpers/obj_helper");
var validationHelper = require("../helpers/validation_helper");

exports.obj = {

    lti_consumer_id: -1,
    user_email: "",
    user_name: "",
    full_name: "",
    user_id: "",
    role: "",
    created_at: "",
    updated_at: "",
    timezone: 0,

    isValid: function() {
        var isValid = true;

        try {
            
            if (!validationHelper.isNumber(this.lti_consumer_id)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.user_email)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.user_name)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.role)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.created_at)) {
                isValid = false;
            }

            if (!validationHelper.isValidText(this.updated_at)) {
                isValid = false;
            }

        } catch (err) {
            logHelper.logMsgWithErr(TAG, "Failed on isValid", err);

            isValid = false;
        }

        return isValid;
    },

    new: function() {
        return objHelper.new(this);
    },

    logObj: function() {
        return {
            lti_consumer_id: this.lti_consumer_id,
            user_email: this.user_email,
            user_name: this.user_name,
            full_name: this.full_name,
            user_id: this.user_id,
            role: this.role,
            created_at: this.created_at,
            updated_at: this.updated_at,
            timezone: this.timezone
        };
    }

}
