/**
 * gUnasusApiIntegrationUtils provide utility functions that are necessary to create the 
 * wrapper object (UnasusApiIntegration), which optimize the calls to the the UNASUS persistency API.
 */

var gUnasusApiIntegrationUtils = {

    playerUserId: undefined,

    /**
     * The playerUserId variable is used to build the keys of the session storage, this is 
     * necessary in order to support sessions for different users in the same browser.
     * 
     * The playerUserId variable must be loaded on every page in order to ensure that
     * the user information in the session storage is going to be related to the current session.
     */
    getPlayerUserId: function() {
        try {
       
            if (this.playerUserId === undefined || this.playerUserId === null || this.playerUserId === "") {
                this.playerUserId = unasus.pack.getPlayerUserId();
            } 

            return this.playerUserId;
        } catch (err) {
            console.log("Failed to getPlayerUserId " + err);
        }

        return undefined;
    },

    getSessionVarUserPrefix: function() {
        try {
            return "_user_" + gUnasusApiIntegrationUtils.getPlayerUserId() + "_";
        } catch (err) {
            console.log("Failed to getSessionVarUserPrefix " + err);
        }
    },

    /**
     * setSessionVariables is the function responsible for setting the session variables locally
     * @param {*} oldSessionVar key of the old version of the session variable
     * @param {*} curSessionVar key of the new version of the session variable
     * @param {*} newVal  the updated value of the session variable
     */
    setSessionVariables: function(oldSessionVar, curSessionVar, newVal) {
        try {
            var oldUserSessionVar = this.getSessionVarUserPrefix() + oldSessionVar; 
            var curUserSessionVar = this.getSessionVarUserPrefix() + curSessionVar;

            if (newVal === undefined) {
                window.sessionStorage.removeItem(oldUserSessionVar);
                window.sessionStorage.removeItem(curUserSessionVar);
            } else {
                window.sessionStorage.setItem(oldUserSessionVar, JSON.stringify(newVal));
                window.sessionStorage.setItem(curUserSessionVar, JSON.stringify(newVal));
            }
        } catch (err) {
            console.log("Failed to setSessionVariables " + err);
        }
    },

    getSessionVariable: function(sessionVar) {
        try {
            return window.sessionStorage.getItem(this.getSessionVarUserPrefix() + sessionVar);
        } catch(err) {
            console.log("Failed to getSessionVariable " + err);
        }
    },

    /**
     * Compares two objects and return true if all their attributes are equal, otherwise return false
     * @param {*} o1 
     * @param {*} o2 
     */
    deepEquals: function(o1, o2) {
        try {
            
            if (Object.keys(o1).length == Object.keys(o2).length) {
                var o1Str = "";
                var o2Str = "";
                
                if (Object.keys(o1).length > 1) {
                    var o1Sorted = {};
                    var o2Sorted = {};

                    Object.keys(o1).sort().forEach(function (key) {
                        o1Sorted[key] = o1[key];
                    });
                    
                    Object.keys(o2).sort().forEach(function (key) {
                        o2Sorted[key] = o2[key];
                    });

                    o1Str = JSON.stringify(o1Sorted);
                    o2Str = JSON.stringify(o2Sorted);
                } else {
                    o1Str = JSON.stringify(o1);
                    o2Str = JSON.stringify(o2);
                }
                
                if (o1Str === o2Str) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    },

    initPpuApi: function () {
        unasus.pack.initialize();
    }

};
gUnasusApiIntegrationUtils.initPpuApi();


/**
 * UnasusApiIntegration
 * 
 * This script makes use of the functions provided by the UNASUS API, contained within the file se_unasus_pack.js,
 * it is used to store and retrieve information related to the progress of the user in the course in the local persistency and
 * eventually on the persistency layer accessed by the hosting system, being the hosting system an environment like MOODLE.
 * 
 * The UnasusApiIntegration also ensures that there won't be unecessary calls to the read and write function of the UNASUS API,
 * avoiding wasting computacional resourced and consequently money.
 * 
 * This optimization process is done with the use of session variables, which store a copy of the 
 * data stored by the UNASUS API. Therefore, the following behavior takes place:
 * 
 *    - Read requests: if there is a session variable configured and updated representing the value that the user
 *                     wants to read, there won't be a request to the UNASUS API.
 * 
 *    - Write requests: write requests are going to happen only if there is a session variable more updated than the stored
 *                      var, in other words, an update request to the backend only happen if the local storage and the remote storage are
 *                      not in sync.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @license MIT
 * @requires se_unasus_pack.js
 * @requires se_unasus_pack.json
 */

function UnasusApiIntegration() {
    this._initialized = false;
}

UnasusApiIntegration.prototype = {

    // Read & Write session variables
    IDX_SESSION_VAR_STATUS_OLD: "sv_ppu_vetores_old_status",
    IDX_SESSION_VAR_STATUS: "sv_ppu_vetores_init_status",

    IDX_SESSION_VAR_INIT_DATE_OLD: "sv_ppu_vetores_old_init_date",
    IDX_SESSION_VAR_INIT_DATE: "sv_ppu_vetores_init_date",

    IDX_SESSION_VAR_ANSWER_OLD: "sv_ppu_vetores_old_answer",
    IDX_SESSION_VAR_ANSWER: "sv_ppu_vetores_answer",

    IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG_OLD: "sv_ppu_vetores_old_has_presented_welcome_dialog",
    IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG: "sv_ppu_vetores_has_presented_welcome_dialog",

    IDX_SESSION_VAR_VIEWED_CLINICAL_CASES_OLD: "sv_ppu_vetores_old_viewed_clinical_cases",
    IDX_SESSION_VAR_VIEWED_CLINICAL_CASES: "sv_ppu_vetores_viewed_clinical_cases",

    IDX_SESSION_VAR_NAVIGATION_OLD: "sv_ppu_vetores_old_navigation_page",
    IDX_SESSION_VAR_NAVIGATION: "sv_ppu_vetores_navigation_page",

    IDX_SESSION_VAR_DOWNLOAD_OLD: "sv_ppu_vetores_old_download",
    IDX_SESSION_VAR_DOWNLOAD: "sv_ppu_vetores_download",

    IDX_SESSION_VAR_EXTERNAL_URL_OLD: "sv_ppu_vetores_old_external_url",
    IDX_SESSION_VAR_EXTERNAL_URL: "sv_ppu_vetores_external_url",

    IDX_SESSION_VAR_VIDEO_OLD: "sv_ppu_vetores_old_video",
    IDX_SESSION_VAR_VIDEO: "sv_ppu_vetores_video",

    IDX_SESSION_VAR_VIDEO_OLD: "sv_ppu_vetores_old_video",
    IDX_SESSION_VAR_VIDEO: "sv_ppu_vetores_video",

    // Read only session variables
    IDX_SESSION_VAR_PLAYER_VERSION_OLD: "sv_ppu_vetores_old_player_version",
    IDX_SESSION_VAR_PLAYER_VERSION: "sv_ppu_vetores_player_version",

    IDX_SESSION_VAR_PLAYER_NAME_OLD: "sv_ppu_vetores_old_player_name",
    IDX_SESSION_VAR_PLAYER_NAME: "sv_ppu_vetores_player_name",

    IDX_SESSION_VAR_PLAYER_USER_OLD: "sv_ppu_vetores_old_player_user",
    IDX_SESSION_VAR_PLAYER_USER: "sv_ppu_vetores_player_user",

    IDX_SESSION_VAR_PLAYER_USER_REAL_NAME_OLD: "sv_ppu_vetores_old_player_user_real_name",
    IDX_SESSION_VAR_PLAYER_USER_REAL_NAME: "sv_ppu_vetores_player_user_real_name",

    /**
     * Initialization methods:
     * Define the standard value of the variables of the system
     */
    initNavigationVarIfNeeded: function (navigation) {

        if (navigation === undefined || navigation === null || navigation === "") {
            var navigation = new Object();
            navigation.previous = "";
            navigation.current = "";

            unasus.pack.setPersistence('NAVIGATION', navigation);
        }

        return navigation;
    },

    setCourseStatus: function (status) {
        try {
            var oldStatus = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_STATUS_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(status, oldStatus)) {
                unasus.pack.setStatus(status);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_STATUS_OLD, this.IDX_SESSION_VAR_STATUS, status);
            }
        } catch (err) {
            console.log("Failed to setCourseStatus " + err);
        }
    },

    getCourseStatus: function () {
        try {
            var status = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_STATUS));

            if (status === undefined || status === null || status === "") {
                status = unasus.pack.getStatus();
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_STATUS_OLD, this.IDX_SESSION_VAR_STATUS, status);
            }

            return status;
        } catch (err) {
            console.log("Failed to getCourseStatus " + err);
        }

        return undefined;
    },

    setAnswer: function (answer) {
        try {
            var oldAnswer = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_ANSWER_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(answer, oldAnswer)) {
                unasus.pack.setPersistence('ANSWER', answer);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_ANSWER_OLD, this.IDX_SESSION_VAR_ANSWER, answer);
            }
        } catch (err) {
            console.log("Failed to setAnswer " + err);
        }
    },

    getAnswer: function () {
        try {
            var answer = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_ANSWER));

            if (answer === undefined || answer === null) {
                answer = unasus.pack.getPersistence('ANSWER');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_ANSWER_OLD, this.IDX_SESSION_VAR_ANSWER, answer);
            }

            return answer;
        } catch (err) {
            console.log("Failed to getAnswer " + err);
        }

        return undefined;
    },

    /**
     * Getters and Setters for the [Read & Write] variables
     * The functions of this section are used to control the read and write of the values stored by the UNASUS API.
     */
    setDownload: function (filetype, filename) {
        try {
            // Build a new Download object
            var localizacao_atual = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
            var res_atual = localizacao_atual.replace(".html", "");

            var download = new Object();
            download.current = res_atual;
            download.filetype = filetype;
            download.filename = filename;

            // Get the old Download object
            var oldDownload = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_DOWNLOAD_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(download, oldDownload)) {
                unasus.pack.setPersistence('DOWNLOAD', download);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_DOWNLOAD_OLD, this.IDX_SESSION_VAR_DOWNLOAD, download);
            }
        } catch (err) {
            console.log("Failed to setDownload " + err);
        }
    },

    getDownload: function () {
        try {
            var download = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_DOWNLOAD));

            if (download === undefined || download === null) {
                download = unasus.pack.getPersistence('DOWNLOAD');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_DOWNLOAD_OLD, this.IDX_SESSION_VAR_DOWNLOAD, download);
            }

            return download;
        } catch (err) {
            console.log("Failed to getDownload " + err);
        }

        return undefined;
    },

    setExternalLink: function (externalLink) {
        try {
            // Build a new ExternalLink object
            var currentLocationUrl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
            var link = new Object();
            link.current = currentLocationUrl.replace(".html", "");
            link.url = externalLink;

            // Get the old ExternalLink object
            var oldExternalLink = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_EXTERNAL_URL_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(link, oldExternalLink)) {
                unasus.pack.setPersistence('EXTERNAL_LINK', link)
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_EXTERNAL_URL_OLD, this.IDX_SESSION_VAR_EXTERNAL_URL, link);
            }
        } catch (err) {
            console.log("Failed to setExternalLink " + err);
        }
    },
    

    getExternalLink: function () {
        try {
            var externalLink = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_EXTERNAL_URL));

            if (externalLink === undefined || externalLink === null) {
                externalLink = unasus.pack.getPersistence('EXTERNAL_LINK');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_EXTERNAL_URL_OLD, this.IDX_SESSION_VAR_EXTERNAL_URL, externalLink);
            }

            return externalLink;
        } catch (err) {
            console.log("Failed to getExternalLink " + err);
        }

        return undefined;
    },

    setVideo: function (videoUrl, videoResolution) {
        try {
            var oldVideo = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_VIDEO_OLD));

            var newVideo = {};
            newVideo.url = videoUrl;
            newVideo.resolution = videoResolution;

            if (!gUnasusApiIntegrationUtils.deepEquals(newVideo, oldVideo)) {
                unasus.pack.setPersistence('VIDEO', newVideo);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIDEO_OLD, this.IDX_SESSION_VAR_VIDEO, newVideo);
            }
        } catch (err) {
            console.log("Failed to setVideo " + err);
        }
    },

    getVideo: function () {
        try {
            var video = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_VIDEO));

            if (video === undefined || video === null) {
                video = unasus.pack.getPersistence('VIDEO');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIDEO_OLD, this.IDX_SESSION_VAR_VIDEO, video);
            }

            return video;
        } catch (err) {
            console.log("Failed to getVideo " + err);
        }

        return undefined;
    },

    setPreviousPage: function (previousPage) {
        try {
            var navigation = new Object();
            navigation.previous = previousPage;
            navigation.current = this.getCurrentPage();

            if (navigation.previous !== navigation.current) {
                var oldNavigation = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_NAVIGATION_OLD));

                if (!gUnasusApiIntegrationUtils.deepEquals(navigation, oldNavigation)) {
                    unasus.pack.setPersistence('NAVIGATION', navigation);
                    gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_NAVIGATION_OLD, this.IDX_SESSION_VAR_NAVIGATION, navigation);
                }
            }
        } catch (err) {
            console.log("Failed to setPreviousPage " + err);
        }
    },

    getPreviousPage: function () {
        try {
            var navigation = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_NAVIGATION));

            if (navigation === undefined || navigation === null || navigation === "") {
                navigation = unasus.pack.getPersistence('NAVIGATION');
                navigation = this.initNavigationVarIfNeeded(navigation);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_NAVIGATION_OLD, this.IDX_SESSION_VAR_NAVIGATION, navigation);
            }

            return navigation.previous;
        } catch (err) {
            console.log("Failed to getPreviousPage " + err);
        }

        return undefined;
    },

    setCurrentPage: function (currentPage) {
        try {
            var navigation = new Object();
            navigation.previous = this.getPreviousPage();
            navigation.current = currentPage;

            if (navigation.previous !== navigation.current) {
                var oldNavigation = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_NAVIGATION_OLD));

                if (!gUnasusApiIntegrationUtils.deepEquals(navigation, oldNavigation)) {
                    unasus.pack.setPersistence('NAVIGATION', navigation);
                    gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_NAVIGATION_OLD, this.IDX_SESSION_VAR_NAVIGATION, navigation);
                }
            }
        } catch (err) {
            console.log("Failed to setCurrentPage " + err);
        }
    },

    getCurrentPage: function () {
        try {
            var navigation = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_NAVIGATION));

            if (navigation === undefined || navigation === null || navigation === "") {
                navigation = unasus.pack.getPersistence('NAVIGATION');
                navigation = this.initNavigationVarIfNeeded(navigation);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_NAVIGATION_OLD, this.IDX_SESSION_VAR_NAVIGATION, navigation);
            }

            return navigation.current;
        } catch (err) {
            console.log("Failed to getCurrentPage " + err);
        }

        return undefined;
    },

    setViewedClinicalCases: function (viewedClinicalCases) {
        try {
            var oldViewedClinicalCasesPage = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(viewedClinicalCases, oldViewedClinicalCasesPage)) {
                unasus.pack.setPersistence('VIEWED_CLINICAL_CASES', viewedClinicalCases);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES_OLD, this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES, viewedClinicalCases);
            }
        } catch (err) {
            console.log("Failed to setViewedClinicalCasesPage " + err);
        }
    },

    getViewedClinicalCases: function () {
        try {
            var viewedClinicalCases = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES));

            if (viewedClinicalCases === undefined || viewedClinicalCases === null) {
                viewedClinicalCases = unasus.pack.getPersistence('VIEWED_CLINICAL_CASES');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES_OLD, this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES, viewedClinicalCases);
            }

            return viewedClinicalCases;
        } catch (err) {
            console.log("Failed to getViewedClinicalCases " + err);
        }

        return undefined;
    },

    setHasPresentedWelcomeDialog: function (hasPresentedWelcomeDialog) {
        try {
            var oldHasPresentedWelcomeDialog = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(hasPresentedWelcomeDialog, oldHasPresentedWelcomeDialog)) {
                unasus.pack.setPersistence('HAS_PRESENTED_WELCOME_DIALOG', hasPresentedWelcomeDialog);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG_OLD, this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG, hasPresentedWelcomeDialog);
            }
        } catch (err) {
            console.log("Failed to setHasPresentedWelcomeDialog " + err);
        }
    },

    hasPresentedWelcomeDialog: function () {
        try {
            var hasPresentedWelcomeDialog = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG));

            if (hasPresentedWelcomeDialog === undefined || hasPresentedWelcomeDialog === null || hasPresentedWelcomeDialog === "") {
                hasPresentedWelcomeDialog = unasus.pack.getPersistence('HAS_PRESENTED_WELCOME_DIALOG');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG_OLD, this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG, true);

                return false;
            }

            return hasPresentedWelcomeDialog;
        } catch (err) {
            console.log("Failed to getHasPresentedWelcomeDialog " + err);
        }

        return false;
    },

    setCourseInitDate: function (initDate) {
        try {
            var oldInitDate = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_INIT_DATE_OLD));

            if (!gUnasusApiIntegrationUtils.deepEquals(initDate, oldInitDate)) {
                unasus.pack.setPersistence('COURSE_INIT_DATE', initDate);
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_INIT_DATE_OLD, this.IDX_SESSION_VAR_INIT_DATE, initDate);
            }
        } catch (err) {
            console.log("Failed to setCourseInitDate " + err);
        }
    },

    getCourseInitDate: function () {
        try {
            var initDate = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_INIT_DATE));

            if (initDate == undefined || initDate == null || initDate === "") {
                initDate = unasus.pack.getPersistence('COURSE_INIT_DATE');
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_INIT_DATE_OLD, this.IDX_SESSION_VAR_INIT_DATE, initDate);
            }

            return initDate;
        } catch (err) {
            console.log("Failed to getCourseInitDate " + err);
        }

        return undefined;
    },

    /**
     * Getters for the [Read only] variables
     * The functions of this section are used to control the read operations of the values stored by the UNASUS API.
     */
    getPlayerVersion: function () {
        try {
            var playerVersion = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_PLAYER_VERSION));

            if (playerVersion === undefined || playerVersion === null || playerVersion === "") {
                playerVersion = unasus.pack.getPlayerVersion();
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_PLAYER_VERSION_OLD, this.IDX_SESSION_VAR_PLAYER_VERSION, playerVersion);
            }

            return playerVersion;
        } catch (err) {
            console.log("Failed to getPlayerVersion " + err);
        }

        return undefined;
    },

    getPlayerName: function () {
        try {
            var playerName = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_PLAYER_NAME));

            if (playerName === undefined || playerName === null || playerName === "") {
                playerName = unasus.pack.getPlayerName();
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_PLAYER_NAME_OLD, this.IDX_SESSION_VAR_PLAYER_NAME, playerName);
            }

            return playerName;
        } catch (err) {
            console.log("Failed to getPlayerName " + err);
        }

        return undefined;
    },

    getPlayerUser: function () {
        try {
            var playerUser = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_PLAYER_USER));

            if (playerUser === undefined || playerUser === null || playerUser === "") {
                playerUser = unasus.pack.getPlayerUser();
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_PLAYER_USER_OLD, this.IDX_SESSION_VAR_PLAYER_USER, playerUser);
            }

            return playerUser;
        } catch (err) {
            console.log("Failed to getPlayerUser " + err);
        }

        return undefined;
    },

    getPlayerRealName: function () {
        try {
            var playerRealName = JSON.parse(gUnasusApiIntegrationUtils.getSessionVariable(this.IDX_SESSION_VAR_PLAYER_USER_REAL_NAME));

            if (playerRealName === undefined || playerRealName === null || playerRealName === "") {
                playerRealName = unasus.pack.getPlayerUserRealName();
                gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_PLAYER_USER_REAL_NAME_OLD, this.IDX_SESSION_VAR_PLAYER_USER_REAL_NAME, playerRealName);
            }

            return playerRealName;
        } catch (err) {
            console.log("Failed to getPlayerRealName " + err);
        }

        return undefined;
    },

    /**
     * Inicialização
     */
    refreshSessionVariables: function () {
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_INIT_DATE_OLD, this.IDX_SESSION_VAR_INIT_DATE, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_USER_COURSE_OLD, this.IDX_SESSION_VAR_USER_COURSE, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES_OLD, this.IDX_SESSION_VAR_VIEWED_CLINICAL_CASES, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_PREVIOUS_PAGE_OLD, this.IDX_SESSION_VAR_PREVIOUS_PAGE, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_CURRENT_PAGE_OLD, this.IDX_SESSION_VAR_CURRENT_PAGE, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_DOWNLOAD_OLD, this.IDX_SESSION_VAR_DOWNLOAD, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_EXTERNAL_URL_OLD, this.IDX_SESSION_VAR_EXTERNAL_URL, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_VIDEO_OLD, this.IDX_SESSION_VAR_VIDEO, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_STATUS_OLD, this.IDX_SESSION_VAR_STATUS, undefined);
        gUnasusApiIntegrationUtils.setSessionVariables(this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG_OLD, this.IDX_SESSION_VAR_HAS_PRESENTED_WELCOME_DIALOG, undefined);
    },

    initialized: function () {
        return this._initialized;
    },

    init: function () {
        this._initialized = true;
    }

}

var gApiIntegration = new UnasusApiIntegration();
gApiIntegration.init();