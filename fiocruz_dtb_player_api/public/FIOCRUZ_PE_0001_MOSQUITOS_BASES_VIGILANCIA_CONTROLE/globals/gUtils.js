/**
 * gUtils.js is used to define variables and methods that are going to be used accross the app,
 * in order to promote code reusability and consistency.
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires unasus.pack
 * @license MIT
 */

/**
 * JQuery Polyfills
 */
if (typeof Object.assign != 'function') {

    Object.assign = function(target) {
        'use strict';

        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        target = Object(target);

        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];

            if (source != null) {

                for (var key in source) {

                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                        target[key] = source[key];
                    }
                }
            }
        }

        return target;
    };
}

/**
 * Utils
 */
var gUtils = {

    MONTH_NAMES: [
        "Janeiro",
        "Fevereiro",
        "Março",
        "Abril",
        "Maio",
        "Junho",
        "Julho",
        "Agosto",
        "Setembro",
        "Outubro",
        "Novembro",
        "Dezembro"
    ],

    inheritsFrom: function (child, parent) {
        child.base = Object.create(parent.prototype);
    },

    getNumberFromCss2dDimensionAttr: function (object, css2dAttr) {
        return Number($(object).css(css2dAttr).replace("px", ""));
    },

    getUtcTimeNow: function () {
        var now = new Date();
        var utcNow = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

        return utcNow.getTime();
    },

    getPrettyDate: function (date) {
        var day = date.getDate();
        var monthIdx = date.getMonth();
        var year = date.getFullYear();

        return day + " de " + this.MONTH_NAMES[monthIdx] + " de " + year;
    },

    romanize: function (num) {
        if (!+num)
            return false;

        var digits = String(+num).split(""),
            key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
                "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
                "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
            roman = "",
            i = 3;

        while (i--)
            roman = (key[+digits.pop() + (i * 10)] || "") + roman;

        return roman;
    },

    debounce: function(nextFunc, waitTime, immediate) {
        var timeout;

        return function() {
            var ctx = this; 
            var args = arguments; 
            var callNow = immediate && !timeout;

            clearTimeout(timeout);
            
            timeout = setTimeout(function() {
                timeout = null,
                immediate || nextFunc.apply(ctx, args)
            }, waitTime),

            callNow && nextFunc.apply(ctx, args)
        }
    },

    getGlossaryInfo: function() {
        try {
            return  {
                searchPlaceHolder: "Pesquise o termo desejado ...",
                icoSearchInput: "images/ico_search_black.png",
                lblAlphabetSelectors: "Pesquise pela letra",
                lblItemNotFound: "Nenhum item encontrado",
                contentPerLetter: {
                    A: {
                        "Agente patogênico": {
                            name: "Agente patogênico",
                            htmlDescription: 'Organismos capazes de provocar, direta ou indiretamente, doenças infecciosas aos seus hospedeiros. Os agentes patogênicos podem ser bactérias, vírus, protozoários, fungos ou helmintos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Aleloquímicos": {
                            name: "Aleloquímicos",
                            htmlDescription: 'Substâncias químicas voláteis, ou que se evaporam no ambiente, que são produzidas e disseminadas por indivíduos de uma determinada espécie para provocar uma resposta (fisiológica e/ou comportamental) em indivíduos de outras espécies.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Alomônio": {
                            name: "Alomônio",
                            htmlDescription: 'É um tipo de aleloquímico produzido e liberado por indivíduos de uma espécie que afeta o comportamento de outra espécie, causando benefício à espécie que a produz.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Ametábolo": {
                            name: "Ametábolo",
                            htmlDescription: 'Relativo ao inseto cujas formas jovens de desenvolvimento não passam por metamorfose para atingir a fase adulta, portanto, jovens e adultos são semelhantes.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Análogo": {
                            name: "Análogo",
                            htmlDescription: 'Aquele que apresenta função semelhante, mas, possui origem distinta.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Antropofílico": {
                            name: "Antropofílico",
                            htmlDescription: 'Animais que preferem se alimentar de sangue humano.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Aplicação espacial": {
                            name: "Aplicação espacial",
                            htmlDescription: 'Aplicação de inseticida no ambiente aéreo com amplo alcance no espaço e áreas abertas. A aplicação pode ser feita com nebulizadores, bombas costais ou equipamentos acoplados a veículos. Nesta modalidade existe a aplicação de inseticidas em Ultrabaixo Volume (UBV) com um dispositivo específico para esta finalidade.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Aplicação residual": {
                            name: "Aplicação residual",
                            htmlDescription: 'Aplicação de inseticida no interior das casas, especialmente nas superfícies internas como as paredes, em abrigos de animais e outros locais; os quais são impregnados através de diversos equipamentos (bombas e nebulizadores), inclusive  sob a forma de microgotículas ou Ultrabaixo Volume (UBV).',
                            listImgs: [
                                ""
                            ]
                        },
                        "Arbovirose": {
                            name: "Arbovirose",
                            htmlDescription: 'Doença infecciosa causada por arbovírus.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Arbovírus": {
                            name: "Arbovírus",
                            htmlDescription: 'Vírus que é essencialmente transmitido por artrópodes tal como os mosquitos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Autóctone": {
                            name: "Autóctone",
                            htmlDescription: 'Aquele que é natural, ou se origina, da região onde é encontrado, onde se manifesta.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Autodisseminação": {
                            name: "Autodisseminação",
                            htmlDescription: 'Capacidade que um organismo tem de transportar e espalhar algum composto químico ou agente biológico para outros ambientes ou para outros organismos que tenham contato. ',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    B: {},
                    C: {
                        "Capacidade vetorial": {
                            name: "Capacidade vetorial",
                            htmlDescription: 'Conjunto de características fisiológicas e comportamentais próprias de cada espécie de invertebrado que, associadas às condições ambientais, favorecem a transmissão natural de um patógeno.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Ciclo gonotrófico": {
                            name: "Ciclo gonotrófico",
                            htmlDescription: 'Período de produção dos ovos até a realização da postura. Frequentemente ocorre após uma alimentação sanguínea.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Competência vetorial": {
                            name: "Competência vetorial",
                            htmlDescription: 'Susceptibilidade natural de uma espécie de artrópodo a um patógeno e sua capacidade de transmiti-lo a um hospedeiro vertebrado.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Criadouros de mosquitos": {
                            name: "Criadouros de mosquitos",
                            htmlDescription: 'Local ou espaço capaz de permitir a criação ou o desenvolvimento das formas jovens dos mosquitos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Culicídeos": {
                            name: "Culicídeos",
                            htmlDescription: 'Insetos da família Culicidae tais como mosquitos e pernilongos.',
                            listImgs: [
                                ""
                            ]
                        },"Cutícula": {
                            name: "Cutícula",
                            htmlDescription: 'Camada mais externa do exoesqueleto formada de material não celular de estrutura flexível e elástica.',
                            listImgs: [
                                ""
                            ]
                        },"Carboidrato": {
                            name: "Carboidrato",
                            htmlDescription: 'Biomoléculas formadas sobretudo por carbono, hidrogênio e oxigênio. Suas principais funções são servir como nutriente ou alimento como por exemplo os açúcares que são digeridos para liberar moléculas de glicose e outras capazes de fornecer a energia necessária para o funcionamento do organismo; além de funcionar como componente estrutural dando sustentação, elasticidade e proteção aos organismos tal como a quitina que forma o exoesqueleto dos insetos e outros artrópodes.',
                            listImgs: [
                                ""
                            ]
                        },"Controle populacional": {
                            name: "Controle populacional",
                            htmlDescription: 'Redução da densidade de uma população para níveis aceitáveis que não causem danos ao homem.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    D: {
                        "Dessecação": {
                            name: "Dessecação",
                            htmlDescription: 'Ação ou efeito de dessecar, ou seja, fazer com que a umidade seja retirada de forma natural ou artificial.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Diapausa": {
                            name: "Diapausa",
                            htmlDescription: 'Condição ou estado fisiológico de interrupção temporária do desenvolvimento, com redução da atividade metabólica, que pode ser influenciada por condições ambientais desfavoráveis.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Dimorfismo sexual": {
                            name: "Dimorfismo sexual",
                            htmlDescription: 'Diferenças entre tamanho, forma, estruturas anatômicas, coloração ou comportamento entre machos e fêmeas de uma mesma espécie.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    E: {
                        "Eclosão": {
                            name: "Eclosão",
                            htmlDescription: 'O escape ou surgimento da larva a partir do ovo ou do inseto adulto a partir da pupa ou da ninfa de último estádio de desenvolvimento.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Eficácia": {
                            name: "Eficácia",
                            htmlDescription: 'Capacidade de funcionar em testes executados sob condições controladas; pode referir-se à capacidade de um composto funcionar em testes em laboratório.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Efetividade": {
                            name: "Efetividade",
                            htmlDescription: 'Capacidade de um produto funcionar regularmente e de produzir o seu efeito habitual, de forma prolongada, sob a influência de diversos fatores. Além do funcionamento ser obtido com custos e tempo otimizados para sua execução; pode referir-se à atividade de um composto-método ou procedimento em condições de campo ou aplicação final.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Emergência de adulto": {
                            name: "Emergência de adulto",
                            htmlDescription: 'O escape ou surgimento do adulto alado a partir da fase de pupa.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Encefalite": {
                            name: "Encefalite",
                            htmlDescription: 'Infecções eventualmente fatais que afetam o cérebro humano e que podem ser decorrentes da infecção por arbovírus.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Endofilia": {
                            name: "Endofilia",
                            htmlDescription: 'Tendência habitual de algumas espécies de mosquitos de frequentar o ambiente humano, utilizando o intradomicilio como abrigo.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Endossimbiontes": {
                            name: "Endossimbiontes",
                            htmlDescription: 'São organismos (ex. bactérias) que vivem no corpo ou mais especificamente nas células de um organismo e mantêm uma relação de simbiose, ou seja, proporciona vantagens para a sobrevivência do organismo hospedeiro.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Entomopatógeno": {
                            name: "Entomopatógeno",
                            htmlDescription: 'Organismo capaz de produzir doenças em insetos. Os agentes entomopatogênicos podem ser bactérias, vírus, protozoários, fungos ou helmintos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Espécie exótica": {
                            name: "Espécie exótica",
                            htmlDescription: 'Aquela que não pertence naturalmente a um determinado ambiente, mas foi introduzida. Quando esta oferece ameaça às espécies nativas e aos ecossistemas é chamada de espécie exótica invasora.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Estenogâmica": {
                            name: "Estenogâmica",
                            htmlDescription: 'Espécie capaz de realizar a cópula em espaços confinados.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Eurigâmica": {
                            name: "Eurigâmica",
                            htmlDescription: 'Espécie incapaz de realizar a cópula em espaços confinados, necessitando de espaços amplos para formação de enxames e realização da cópula em pleno voo.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Exoesqueleto": {
                            name: "Exoesqueleto",
                            htmlDescription: 'Estrutura de proteção e sustentação das partes moles do corpo (esqueleto) localizada na parte externa do organismo. No caso dos artrópodes o exoesqueleto é formado de quitina, uma substância impermeável e resistente, normalmente recoberta por camadas de cera.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Exofilia": {
                            name: "Exofilia",
                            htmlDescription: 'Preferência para permanecer em áreas externas fora do ambiente humano.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    F: {
                        "Feromônio": {
                            name: "Feromônio",
                            htmlDescription: 'Composto químico produzido e liberado por indivíduos de uma espécie que afeta o comportamento de indivíduos da mesma espécie. Existem diferentes tipos de feromônios, como por exemplo, os sexuais, de oviposição, de agregação, de alarme, entre outros.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Fumacê": {
                            name: "Fumacê",
                            htmlDescription: 'Termo popular que designa a estratégia de aplicação espacial de adulticidas, formando uma cortina de fumaça ou nuvem do produto com amplo alcance e dispersão no espaço. ',
                            listImgs: [
                                ""
                            ]
                        },
                    },
                    G: {
                        "Gene": {
                            name: "Gene",
                            htmlDescription: 'Segmento de DNA que representa unidade física e funcional fundamental da hereditariedade carregando a informação biológica de uma geração para outra.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Genoma": {
                            name: "Genoma",
                            htmlDescription: 'É o conjunto de toda a informação genética ou hereditária contida em um organismo.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    H: {
                        "Habitat": {
                            name: "Habitat",
                            htmlDescription: 'Área ou ambiente que é naturalmente habitada por uma determinada espécie de animal, vegetal ou outro organismo.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Hematofagia": {
                            name: "Hematofagia",
                            htmlDescription: 'Hábito de se alimentar do sangue de outro animal.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Hematófago": {
                            name: "Hematófago",
                            htmlDescription: 'É o nome dado a um grupo de animais ou parasitas que se alimentam de sangue.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Hemimetábolo": {
                            name: "Hemimetábolo",
                            htmlDescription: 'Inseto que passa por metamorfose ou transformação incompleta durante o desenvolvimento da fase jovem para a adulta; inseto que passa por mudanças graduais de tamanho cujas formas jovens são semelhantes às adultas.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Holometábolo": {
                            name: "Holometábolo",
                            htmlDescription: 'Inseto que passa por uma metamorfose ou uma transformação completa durante o seu desenvolvimento e as fases de larva, pupas e adultos são morfologicamente muito diferentes umas das outras.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Hospedeiro": {
                            name: "Hospedeiro",
                            htmlDescription: 'Organismo que fornece nutrientes ou proteção essencial ao desenvolvimento de outro organismo, o qual é frequentemente chamado de parasita.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    I: {
                        "Imaginal": {
                            name: "Imaginal",
                            htmlDescription: 'Relativo a uma condição do inseto adulto ou imago.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Índice entomológico": {
                            name: "Índice entomológico",
                            htmlDescription: 'Indicadores da presença e quantidade de um inseto, num determinado local e em um determinado momento. Essa medida pode ser obtida em diferentes fases de vida de insetos (ovo, larva-pupa, adulto) e com diferentes instrumentos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Infestação": {
                            name: "Infestação",
                            htmlDescription: 'Ato ou efeito de invadir ou de se espalhar em grande número.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Inseticida biológico": {
                            name: "Inseticida biológico",
                            htmlDescription: 'Substância de origem biológica usada para matar os insetos em suas diferentes fases do desenvolvimento.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Inseticida químico": {
                            name: "Inseticida químico",
                            htmlDescription: 'Substância química usada para matar os insetos em suas diferentes fases do desenvolvimento.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    J: {},
                    K: {},
                    L: {
                        "Larvicida": {
                            name: "Larvicida",
                            htmlDescription: 'Nome dado a toda substância tóxica para as formas imaturas, especialmente as larvas, utilizada para controle de insetos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Linhagem": {
                            name: "Linhagem",
                            htmlDescription: 'Um subconjunto populacional de uma espécie, cujas características ou qualidades foram selecionadas.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    M: {
                        "Machos estéreis": {
                            name: "Machos estéreis",
                            htmlDescription: 'Machos incapazes de produzir descendentes férteis/viáveis; esta condição pode ser devida à destruição ou inativação dos espermatozoides pela ação de uma substância química ou agente físico como a radiação ionizante.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Metamorfose": {
                            name: "Metamorfose",
                            htmlDescription: 'É um processo complexo de mudanças morfológicas e fisiológicas pós-embrionárias que ocorrem com os insetos na passagem das formas jovens para as adultas. ',
                            listImgs: [
                                ""
                            ]
                        },
                        "Monitoramento entomológico": {
                            name: "Monitoramento entomológico",
                            htmlDescription: 'Acompanhamento no espaço e no tempo da presença de insetos cujos resultados são utilizados para calcular um ou mais índices entomológicos.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    N: {},
                    O: {
                        "Oviposição": {
                            name: "Oviposição",
                            htmlDescription: 'Ato de colocar ovos realizado por fêmeas de animais invertebrados; o mesmo que postura de ovos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Ovitrampa": {
                            name: "Ovitrampa",
                            htmlDescription: 'Armadilha que tem a capacidade de atrair fêmeas de mosquitos para realizar a oviposição. Especialmente aplicada a coleta de ovos de espécies do gênero <i>Aedes</i>, colonizadoras de criadouros temporários artificiais.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    P: {
                        "Palpos": {
                            name: "Palpos",
                            htmlDescription: 'Estrutura anatômica apresentada em pares, localizados próximo ao aparelho bucal dos artrópodos, envolvidos com a percepção sensorial destes animais.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Patógenos": {
                            name: "Patógenos",
                            htmlDescription: 'Organismos capazes de provocar, direta ou indiretamente, doenças infecciosas aos seus hospedeiros. Os agentes patogênicos (ou patógenos) podem ser bactérias, vírus, protozoários, fungos ou helmintos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Perifocal": {
                            name: "Perifocal",
                            htmlDescription: 'Situado em torno, ou ao redor, de um foco de proliferação do mosquito; ou do foco de disseminação do patógeno.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Período de incubação extrínseca": {
                            name: "Período de incubação extrínseca",
                            htmlDescription: 'Tempo após a ingestão do sangue contaminado pelo artrópodo necessário ao desenvolvimento e/ou multiplicação do patógeno e a sua transmissão para outro hospedeiro vertebrado.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Pré-imaginal": {
                            name: "Pré-imaginal",
                            htmlDescription: 'Relativo a uma condição do inseto jovem, antes de se tornar adulto.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Probóscide": {
                            name: "Probóscide",
                            htmlDescription: 'Estrutura alongada do aparelho bucal do tipo sugador ou picador-sugador, presente em alguns insetos.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    Q: {
                        "Quiescência": {
                            name: "Quiescência",
                            htmlDescription: 'Estado de dormência provocado pela ausência ou insuficiência de um ou mais fatores externos importantes para o desenvolvimento do vetor, particularmente, temperatura e água.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Quitina": {
                            name: "Quitina",
                            htmlDescription: 'Tipo de carboidrato formado por várias unidades, um polissacarídeo que oferece proteção, suporte e sustentação ao corpo dos insetos; a quitina é um dos principais componentes do exoesqueleto (tegumento externo).',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    R: {
                        "Repasto sanguíneo": {
                            name: "Repasto sanguíneo",
                            htmlDescription: 'É o ato de se alimentar de sangue diretamente de um animal, praticado pelo inseto.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Resistência cruzada": {
                            name: "Resistência cruzada",
                            htmlDescription: 'Resistência (perda da susceptibilidade) de uma espécie de inseto para dois ou mais inseticida causada pelo fato deles compartilharem o mesmo mecanismo de ação tóxica.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Resistência múltipla": {
                            name: "Resistência múltipla",
                            htmlDescription: 'Perda da susceptibilidade de uma espécie de inseto a mais de um inseticida.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    S: {
                        "Semioquímicos": {
                            name: "Semioquímicos",
                            htmlDescription: 'São as substâncias químicas envolvidas na comunicação entre os seres vivos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Seleção natural": {
                            name: "Seleção natural",
                            htmlDescription: 'Taxa de reprodução diferencial dos indivíduos de uma população como resultado de suas distintas características fisiológicas, anatômicas e/ou comportamentais.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Sequenciamento de DNA": {
                            name: "Sequenciamento de DNA",
                            htmlDescription: 'Método para identificar a ordem ou sequência das unidades constituintes - formadoras - do DNA.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Sifão respiratório": {
                            name: "Sifão respiratório",
                            htmlDescription: 'Estrutura em forma de tubo presente na larva dos mosquitos com a função de captar o ar atmosférico.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Simbionte": {
                            name: "Simbionte",
                            htmlDescription: 'Organismo que vive em próximo contato com outro organismo - simbiose.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Simbiose": {
                            name: "Simbiose",
                            htmlDescription: 'Associação a longo prazo entre dois organismos de espécies diferentes, cuja relação é, geralmente, benéfica para ambos os indivíduos envolvidos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Sinomônios": {
                            name: "Sinomônios",
                            htmlDescription: 'É um tipo de aleloquímico produzido e liberado por indivíduos de uma espécie que gera benefício para a espécie emissora da mensagem química e para a espécie receptora.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Substância repelente": {
                            name: "Substância repelente",
                            htmlDescription: 'Substância que tem capacidade de afastar os insetos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Substância letal": {
                            name: "Substância letal",
                            htmlDescription: 'Substância que tem capacidade de matar um organismo.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Substância inseticida": {
                            name: "Substância inseticida",
                            htmlDescription: 'Substância que tem capacidade de matar insetos.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Substância adulticida": {
                            name: "Substância adulticida",
                            htmlDescription: 'Substância que tem capacidade de matar os insetos na fase adulta.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Supressão populacional": {
                            name: "Supressão populacional",
                            htmlDescription: 'Eliminação ou redução de uma população.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Susceptibilidade": {
                            name: "Susceptibilidade",
                            htmlDescription: 'Capacidade de um organismo de ser suscetível ou sofrer os efeitos de um composto ou organismo, condição, situação.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Susceptível": {
                            name: "Susceptível",
                            htmlDescription: 'Organismo que é capaz de se submeter, estar sujeito ou apto a sofrer a ação de uma substância, ou de um processo, ou de uma condição imposta. Pode referir-se à capacidade de sofrer ou sucumbir à ação tóxica de uma substância ou à infecção provocada por um patógeno.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    T: {
                        "Transmissão biológica": {
                            name: "Transmissão biológica",
                            htmlDescription: 'Transferência de um patógeno ou parasita entre organismos sejam estes da mesma espécie ou não.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Transmissão horizontal": {
                            name: "Transmissão horizontal",
                            htmlDescription: 'Transferência de um patógeno ou parasita de um hospedeiro para outro, por exemplo, de um inseto para o homem.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Transmissão mecânica": {
                            name: "Transmissão mecânica",
                            htmlDescription: 'Transferência ou passagem casual de um patógeno de um invertebrado para um vertebrado, sem que isso seja essencial para a transmissão deste patógeno.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Transmissão vertical": {
                            name: "Transmissão vertical",
                            htmlDescription: 'Passagem de um patógeno entre membros da geração parental de invertebrados diretamente para seus descendentes ou linhagem filial. Pode ser conhecida também como transmissão transovariana.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    U: {},
                    V: {
                        "Vetor": {
                            name: "Vetor",
                            htmlDescription: 'Artrópodo capaz de transmitir um agente patogênico, de maneira ativa ou passiva, para um hospedeiro vertebrado.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Vetor primário": {
                            name: "Vetor primário",
                            htmlDescription: 'Principal espécie de vetor responsável pela manutenção do ciclo de transmissão de um patógeno e surgimento de novos casos da doença; é a espécie de vetor com grande importância epidemiológica porque apresenta maior competência e capacidade vetorial.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Vetor secundário": {
                            name: "Vetor secundário",
                            htmlDescription: 'Espécie de vetor com menor competência e capacidade vetorial, responsável pela transmissão ocasional do patógeno, não tendo, portanto, a mesma importância epidemiológica do vetor primário.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    W: {},
                    X: {
                        "Xenodiagnóstico": {
                            name: "Xenodiagnóstico",
                            htmlDescription: 'Método diagnóstico para detectar a presença de um patógeno causador de uma doença infecciosa através de sua detecção no vetor responsável por sua transmissão.',
                            listImgs: [
                                ""
                            ]
                        },
                        "Xenomonitoramento": {
                            name: "Xenomonitoramento",
                            htmlDescription: 'Monitoramento ou acompanhamento da circulação de um patógeno, em determinada área, utilizando como material de investigação o xenodiagnóstico.',
                            listImgs: [
                                ""
                            ]
                        }
                    },
                    Y: {},
                    Z: {}
                }
            };

        } catch (err) {
            console.log("gUtils Warning: Failed to getGlossaryInfo " + err);
        }
    }

}

/**
 * UI variables and functions
 */
var gUi = {

    DEBUG_GUI: true,

    // Selectors
    HTML_BODY_SELECTOR: "html, body",
    EDU_RES_TOP_NAVBAR_SELECTOR: " .componentHeaderTop2.navbar.fixed-top.component-header-top-2",
    EDU_RES_PAGE_CONTAINER_SELECTOR: " .page-container",
    COMP_HEADER_TOP_2_SELECTOR: " .componentHeaderTop2",
    COMP_SIDEBAR_SELECTOR: " .componentSidebar",
    SIDEBAR_SELECTOR: ".component-main-container .sidebar",
    SIDEBAR_TOGGLE_SELECTOR: ".component-main-container .sidebar-toggle",
    SIDEBAR_TOGGLE_EDGE_SELECTOR: ".sidebar-toggle-edge",
    MAIN_CONTAINER_PAGE_TITLE_SELECTOR: ".component-main-container .content-container .cmc-container .page-title",
    FIELD_VALIDATION_CONTAINER_SELECTOR: " .field-validation-container",

    // Timeouts
    TIMEOUT_BIND_SCROLLING_EVENT: 1000,
    TIMEOUT_DISMISS_SIDEBAR: 2000,
    TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE: 3000,

    // Message types
    MESSAGE_TYPE_SUCCESS: "msg_success",
    MESSAGE_TYPE_WARNING: "msg_warning",
    MESSAGE_TYPE_ERROR: "msg_error",

    // Component measures
    UI_LIMIT: {
        "MOBILE": 767,
        "TABLET": 992
    },
    INITIAL_APP_CONTAINER_HEIGHT: "100%",

    // Filters
    BLUE_FILTER: "blueish-filter",
    GREEN_FILTER: "greenish-filter",
    PINK_FILTER: "pinkish-filter",
    YELLOW_FILTER: "yellowish-filter",
    PURPLE_FILTER: "purpleish-filter",

    // Classes
    CLASS_SELECTABLE: "selectable",
    CLASS_ERROR: "error",
    CLASS_WARNING: "warning",
    CLASS_SUCCESS: "success",
    CLASS_HIDDEN: "hidden",
    CLASS_FADE_OUT: "fadded-out",
    CLASS_FADE_IN: "fadded-in",
    CLASS_BIGGER: "bigger",
    CLASS_ANIMATED: "animated",
    CLASS_DISABLED: "disabled",
    CLASS_SHOW: "show",

    // Pre-fixes, post-fixed
    PREFIX_FOOTER_IMG: "images/footer_logo_",
    POSTFIX_PNG_IMG: ".png",
    POSTFIX_SVG_IMG: ".svg",
    POSTFIX_GIF: ".gif",

    // Constants and variables related to the HaeaderTop animations
    INITIAL_SCROLL_BAR_POS_VAL: -1000,
    TIMEOUT_TOGGLE_HEADER_TOP_2_MILLIS: 250,
    NO_OFFSET: "0px",
    APP_CONTENT_HEIGHT: "100%",

    isToogleEduResNavbarEnabled: false,
    isAnimatingEduResTopNavbar: false,
    isEduResTopNavbarHidden: false,
    pageLoader: undefined,
    previousUIModeIsMobile : undefined,

    // Getters and Setters
    hasFixedFooter: true,
    tgtPageContainer: undefined,

    setPageContainer: function (target) {
        this.tgtPageContainer = target;
    },

    getPageContainerSelector: function () {
        return this.tgtPageContainer;
    },

    setHasFixedFooter: function (hasFixedFooter) {
        this.hasFixedFooter = hasFixedFooter;
    },

    hasFixedFooter: function (hasFixedFooter) {
        return this.hasFixedFooter;
    },

    isOnMobileViewMode: function () {
        return $(window).width() <= gUi.UI_LIMIT.MOBILE;
    },

    isOnTabletViewMode: function () {
        return $(window).width() >= gUi.UI_LIMIT.MOBILE && $(window).width() < gUi.UI_LIMIT.TABLET;
    },

    isFirefox: function() {
        return window.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    },

    isEdge: function() {
        return window.navigator.userAgent.toLowerCase().indexOf("edge") > -1;
    },

    isIE: function() {
        try {
            var ua = window.navigator.userAgent;
            var msie = window.navigator.userAgent.toLowerCase().indexOf('MSIE ');

            // IE 10 or older, return version number
            if (msie > 0) {
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            } else {

                // IE 11, return version number
                var trident = ua.indexOf('Trident/');

                if (trident > 0) {
                    var rv = ua.indexOf('rv:');
                }

                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }
        } catch(err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to get IE user agent " + err);
            }
        }
    },

    isIPad: function() {
        return window.navigator.userAgent.match(/iPad/i) != null;
    },

    isIPhone: function() {
        return window.navigator.userAgent.match(/iPhone/i) != null;
    },

    setPageLoader: function (selector, loader, mainMsg, supportMsg, callback) {
        var model = {
            img: gImages.GIF_PAGE_LOADER,
            mainMsg: mainMsg,
            supportMsg: supportMsg
        }

        loader.build(selector, undefined, model);
        loader.show();

        window.onload = function () {
            loader.dismiss();

            if (callback != undefined) {
                callback();
            }
        };
    },

    detectChangeLayoutReload: function() {
    
        if (this.previousUIModeIsMobile != undefined && this.previousUIModeIsMobile != this.isOnMobileViewMode()) {
            location.reload();
        }
    },

    getPageLoader: function () {
        return this.pageLoader;
    },

    getIdFromSelector: function(selector) {
        try {
            return selector.replace("#","").replace(".", "").replace(" ", "");
        } catch (err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to getIdFromSelector " + err);
            }
        }
    },

    // Regular methods
    adjustHeaderTop: function () {
        try {
            var ctx = this;
            
            setTimeout(function() {
                $(ctx.HTML_BODY_SELECTOR).animate({scrollTop: 0}, 250);
            }, 1000);
            
        } catch (err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to adjustHeaderTop " + err);
            }
        }
    },

    adjustViewport : gUtils.debounce(function(callback) {

        try {
            this.adjustViewportWidthIpad = false;
            this.adjustViewportHeightIpad = false;

            var currentVw = Number($(this.EDU_RES_PAGE_CONTAINER_SELECTOR).css("width").replace("px", ""));
            var currentVh = Number($(this.EDU_RES_PAGE_CONTAINER_SELECTOR).css("height").replace("px", ""));
            var currentTopNavHeight = Number($(this.COMP_HEADER_TOP_2_SELECTOR).css("height").replace("px", ""));

            if (gUi.isIPad()) {
                this.adjustViewportWidthIpad = true;
                this.adjustViewportHeightIpad = true;

                if (window.orientation == 0 || window.orientation == 180) {
                    var vw = 768 * 0.01;
                    var vh = 1024 * 0.01;
                } else {
                    var vw = 1024 * 0.01;
                    var vh = 768 * 0.01;
                }

            } else if (gUi.isIPhone()) {

                if (window.orientation == 0 || window.orientation == 180) {
                    var vw = 750 * 0.01;
                    var vh = 1334 * 0.01;
                } else {
                    var vw = 1334 * 0.01;
                    var vh = 750 * 0.01;
                }

            } else {
                var vw = window.innerWidth * 0.01;
                var vh = window.innerHeight * 0.01;
            }

            var newVw = Math.round(100 * vw);
            var newVh = 0;

            if (this.adjustViewportDiscountNavbar) {
                newVh = Math.round((100 * vh) - currentTopNavHeight);
            } else {
                newVh = Math.round(100 * vh);
            }

            if (newVw != currentVw && newVh != currentVh) {
                document.documentElement.style.setProperty('vw', vw + "px");
                document.documentElement.style.setProperty('vh', vh + "px");

                if (this.adjustViewportWidthIpad) {
                    $(this.EDU_RES_PAGE_CONTAINER_SELECTOR).css("width", newVw + "px");
                }

                if (this.adjustViewportHeightIpad) {
                    $(this.EDU_RES_PAGE_CONTAINER_SELECTOR).css("max-height", newVh + "px");
                }
            }
            
            if (callback != undefined) {
                callback();
            }

        } catch(err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to adjustViewport " + err);   
            }
        }
    }, 250),

    applyEdgeFixes: function() {
        try {
            
            if (this.isEdge()) {
                $(this.MAIN_CONTAINER_PAGE_TITLE_SELECTOR).css("position", "-ms-position-fixed");
                $(this.SIDEBAR_TOGGLE_SELECTOR).addClass(this.CLASS_HIDDEN);
                $(this.SIDEBAR_TOGGLE_EDGE_SELECTOR).removeClass(this.CLASS_HIDDEN);
            }
        } catch(err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to applyIeEdgeFixes");
            }
        }
    },
    
    finishUiInit: function () {
        try {
            this.isOnInitMode = false;
            this.applyEdgeFixes();
            this.adjustHeaderTop();
        } catch(err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to finishUiInit " + err);
            }
        }
    },

    /**
     * Display a validation message in a targeted container.
     */
    showFieldMessage: function (messageType, targetContainer, targetFieldMsgReceiver, msg) {

        switch (messageType) {

            case this.MESSAGE_TYPE_ERROR:
                $(targetContainer).find(this.FIELD_VALIDATION_CONTAINER_SELECTOR).addClass(this.CLASS_ERROR);
                break;

            case this.MESSAGE_TYPE_WARNING:
                $(targetContainer).find(this.FIELD_VALIDATION_CONTAINER_SELECTOR).addClass(this.CLASS_WARNING);
                break;

            case this.MESSAGE_TYPE_SUCCESS:
                $(targetContainer).find(this.FIELD_VALIDATION_CONTAINER_SELECTOR).addClass(this.CLASS_SUCCESS);
                break;
        }

        $(targetFieldMsgReceiver).html(msg);
    },

    /**
     * Events
     */
    toggleEduResTopNavbar: function(showNavbar) {
        try {
            var ctx = this;

            if (ctx.isToogleEduResNavbarEnabled && ctx.isAnimatingEduResTopNavbar == false) {
                ctx.isAnimatingEduResTopNavbar = true;
                
                // Calculate the offsets of the current position to the future position of the UI elements that are going to move
                var newNavbarTopCssAttr = "";
                var newMainContainerPaddingTopCssAttr = "";
                var newMainContainerHeightOffset = "";
                var newSidebarTopCssAttr = "";
                var newSidebarToggleTopCssAttr = "";
                var newMainContainerTitleTopCssAttr = "";

                if (showNavbar) {
                    newNavbarTopCssAttr = ctx.NO_OFFSET;
                    newMainContainerPaddingTopCssAttr = "70px";
                    newMainContainerHeightOffset = ctx.NO_OFFSET;
                    newSidebarTopCssAttr = "90px";

                    newSidebarToggleTopCssAttr = "90px";
                    newMainContainerTitleTopCssAttr = "-100px";

                    // Fix for Edge, adjust the vertical alignment
                    if (ctx.isEdge()) {
                        newMainContainerTitleTopCssAttr = "-100px"; 
                    }

                } else {
                    newNavbarTopCssAttr = "-90px";
                    newMainContainerPaddingTopCssAttr = ctx.NO_OFFSET;
                    newMainContainerHeightOffset = "70px";
                    newSidebarTopCssAttr = ctx.NO_OFFSET;

                    newSidebarToggleTopCssAttr = ctx.NO_OFFSET;
                    newMainContainerTitleTopCssAttr = "-120px";
                    
                    // Fix for Edge, hide the elements
                    if (ctx.isEdge()) {
                        newMainContainerTitleTopCssAttr = "-250px"; 
                    }
                }

                // Move the page content container
                $(ctx.EDU_RES_PAGE_CONTAINER_SELECTOR).css({
                    "height": "calc(var(--vh, 1vh) * 100)",
                    "padding-top": newMainContainerPaddingTopCssAttr
                });

                // Move the top navbar of the educational resource
                $(ctx.EDU_RES_TOP_NAVBAR_SELECTOR).css({
                    "top": newNavbarTopCssAttr
                });

                $(ctx.SIDEBAR_SELECTOR).css({
                    "top": newSidebarTopCssAttr
                });

                // Fix for Edge, adjust the vertical alignment
                if (!ctx.isEdge()) {
                    $(ctx.SIDEBAR_TOGGLE_SELECTOR).css({
                        "top": newSidebarToggleTopCssAttr
                    });
                }

                $(ctx.MAIN_CONTAINER_PAGE_TITLE_SELECTOR).css({
                    "margin-top": newMainContainerTitleTopCssAttr
                });

                ctx.isAnimatingEduResTopNavbar = false;
            }
        } catch (err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to toggleEduResTopNavbar " + err);
            }
        }
    },

    hideHeaderEduRes: function () {
        try {

            if ($(this.EDU_RES_TOP_NAVBAR_SELECTOR).css("top") === this.NO_OFFSET) {
                this.toggleEduResTopNavbar(false);
            }
            
        } catch (err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to hideHeaderEduRes " + err);
            }
        }
    },

    showHeaderEduRes: function () {
        try {

            if ($(this.EDU_RES_TOP_NAVBAR_SELECTOR).css("top") !== this.NO_OFFSET) {
                this.toggleEduResTopNavbar(true);
            }

        } catch (err) {

            if (this.DEBUG_GUI) {
                console.log("gUi Warning: Failed to showHeaderEduRes " + err);
            }
        }
    },

    initEvents: function () {
        var ctx = this;
        this.previousUIModeIsMobile = this.isOnMobileViewMode();

        if (this.isIPad() || this.isIPhone() || this.isOnTabletViewMode()) {
            this.adjustViewport();
        }
        
        $(window).scroll(function () {

            if (!ctx.isAnimatingEduResTopNavbar) {
                var scrollTop = $(window).scrollTop();

                if (scrollTop > 0) {
                    ctx.hideHeaderEduRes();
                } else {
                    ctx.showHeaderEduRes();
                }
            }
        });

        window.addEventListener("orientationchange", function() {
            
            if (ctx.isIPad() || ctx.isIPhone() || ctx.isOnTabletViewMode()) {
                ctx.adjustViewport();
            }
        }, false);
    }

}

/**
 * Messages
 */
var gMsgs = {

    APP_PAGE_TITLE: "Mosquitos: Bases da Vigilância e Controle",
    APP_PAGE_TITLE_WITH_NEW_LINE: "Mosquitos: Bases da<br>Vigilância e Controle",
    
    DEFAULT_LOADING_MESSAGE: "Carregando ...",
    DEFAULT_LBL_BTN_TRY_AGAIN: "Tentar de novo",
    DEFAULT_LBL_BTN_SUBMIT_ANSWER: "Enviar Resposta",

    MSG_WELCOME_TO_EDUCATIONAL_RESOURCE: "Bem vindo ao curso",
    MSG_PRESS_PLAY_VIEW_CLINICAL_CASES: "Aperte o play para visualizar alguns casos clínicos",
    MSG_CONGRATS: "Parabéns!",
    MSG_FINISHED_UNIT_N: "Você obteve a nota mínima para a finalização da unidade ",
    MSG_UNIT_APPROVAL_SUPPORT: "Você finaliza uma unidade quando \n acerta pelo menos 70% de suas questões.",

    MSG_YOU_FINISHED_EDUCATIONAL_RESOURCE: "Você finalizou todas as unidades deste recurso educacional!",
    MSG_YOU_FINISHED_EDUCATIONAL_RESOURCE_SUPPORT: "Para verificar o seu desempenho em detalhes, clique na aba de acompanhamento",
    MSG_FAILED_LOAD_COURSE_STATUS: "Não foi possível carregar o seu status no curso",
    
    MSG_YOU_WERE_APPROVED: "Parabéns!",
    BROWSER_DOESNT_SUPPORT_THIS_VIDEO: "Seu browser não suporta este vídeo",

    MSG_BEFORE_ACTION_BTNS: "seguir para"

}

/**
 * Pages
 */
var gPages = {

    IDS: {
        "PAGE_INDEX_ID": "index",
        "PAGE_COURSE_PROGRESS_ID": "courseProgress",
        "PAGE_COURSE_GLOSSARY_ID": "glossary",
        "PAGE_COURSE_UNITS_ID": "courseUnits",
        "PAGE_COURSE_UNIT_DETAILS_1_ID": "courseUnitDetails1",
        "PAGE_COURSE_UNIT_DETAILS_2_ID": "courseUnitDetails2",
        "PAGE_COURSE_UNIT_DETAILS_3_ID": "courseUnitDetails3",
        "PAGE_ABOUT_ID": "about"
    },

    PAGE_INDEX: "index.html",
    PAGE_COURSE_UNITS: "courseUnits.html",
    PAGE_COURSE_UNIT_DETAILS_1: "courseUnitDetails1.html",
    PAGE_COURSE_UNIT_DETAILS_2: "courseUnitDetails2.html",
    PAGE_COURSE_UNIT_DETAILS_3: "courseUnitDetails3.html",
    PAGE_COURSE_PROGRESS: "courseProgress.html",
    PAGE_ABOUT: "about.html",
    PAGE_GLOSSARY: "glossary.html",

    PAGE_INDEX_DESCRIPTION: "página inicial",
    PAGE_COURSE_UNITS_DESCRIPTION: "módulos",
    PAGE_COURSE_UNIT_DETAILS_1_DESCRIPTION: "módulo 1",
    PAGE_COURSE_UNIT_DETAILS_2_DESCRIPTION: "módulo 2",
    PAGE_COURSE_UNIT_DETAILS_3_DESCRIPTION: "módulo 3",
    PAGE_COURSE_PROGRESS_DESCRIPTION: "andamento",
    PAGE_ABOUT_DESCRIPTION: "sobre",
    PAGE_GLOSSARY_DESCRIPTION: "glossário",

    pageCount: 0,

    /**
     * Page methods
     */
    incrementPageCount: function () {
        this.pageCount++;
    },

    decrementPageCount: function () {
        this.pageCount--;
    },

    getPageCount: function () {
        return this.pageCount;
    },

    setPreviousPage: function (page) {
        gApiIntegration.setPreviousPage(page);
    },

    getPreviousPage: function () {
        try {
            var previousPage = gApiIntegration.getPreviousPage();
            
            if (previousPage != undefined) {
                return previousPage;
            } else {
                return this.PAGE_INDEX;
            }
        } catch (err) {
            return this.PAGE_INDEX;
        }
    },

    setCurrentPage: function (currentPage) {
        gApiIntegration.setCurrentPage(currentPage);
    },

    getCurrentPage: function () {
        try {
            var currentPage = gApiIntegration.getCurrentPage();
            
            if (currentPage != undefined) {
                return currentPage;
            } else {
                return this.PAGE_INDEX;
            }
        } catch (err) {
            return this.PAGE_INDEX;
        }
    },

    updateCurrentPage: function (nextPage) {

        if (this.getCurrentPageFromUrl() != nextPage) {

            if (this.getCurrentPageFromUrl == this.getPreviousPage()) {
                this.decrementPageCount();
            } else {
                this.incrementPageCount();
            }

            this.setPreviousPage(this.getCurrentPageFromUrl());
            this.setCurrentPage(nextPage);

            window.location.href = nextPage;
        }
    },

    getCurrentPageFromUrl: function () {
        var currentPage = this.PAGE_INDEX;

        try {
            var currentUrl = window.location.href;
            var currentUrlParts = currentUrl.split("/");
            currentPage = currentUrlParts[currentUrlParts.length - 1];
        } catch (err) {
            if (unasus.pack.getDebug()) {
                console.log(err);
            }
        }

        return currentPage;
    }

}

/**
 * Images
 */
var gImages = {

    ICO_PNG_HEADER_TOP_2_BTN_COLLAPSE: "images/nav_hamburguer_ico.png",

    ICO_SVG_COURSE_UNITS: "images/nav_icon_course_units.svg",
    ICO_SVG_COURSE_UNITS_SELECTED: "images/nav_icon_course_units_selected.svg",
    ICO_SVG_COURSE_UNITS_SELECTED_1: "images/nav_icon_course_units_selected_1.svg",
    ICO_SVG_COURSE_UNITS_SELECTED_2: "images/nav_icon_course_units_selected_2.svg",
    ICO_SVG_COURSE_UNITS_SELECTED_3: "images/nav_icon_course_units_selected_3.svg",
    ICO_PNG_COURSE_UNITS: "",
    ICO_PNG_COURSE_UNITS_SELECTED: "",

    ICO_SVG_COURSE_PROGRESS: "images/nav_icon_course_progress.svg",
    ICO_SVG_COURSE_PROGRESS_SELECTED: "images/nav_icon_course_progress_selected.svg",
    ICO_PNG_COURSE_PROGRESS: "",
    ICO_PNG_COURSE_PROGRESS_SELECTED: "",

    ICO_SVG_ABOUT: "images/nav_icon_about.svg",
    ICO_SVG_ABOUT_SELECTED: "images/nav_icon_about_selected.svg",
    ICO_PNG_ABOUT: "",
    ICO_PNG_ABOUT_SELECTED: "",

    ICO_SVG_GLOSSARY: "images/nav_icon_glossary.svg",
    ICO_SVG_GLOSSARY_SELECTED: "images/nav_icon_glossary_selected.svg",
    ICO_PNG_GLOSSARY: "",
    ICO_PNG_GLOSSARY_SELECTED: "",

    ICO_SVG_COURSE_ENDING: "images/ico_svg_final_handshake.svg",
    ICO_PNG_COURSE_ENDING: "images/ico_png_final_handshake.png",

    ICO_SVG_SIDEBAR_SUMMARY: "images/ico_sidebar_menu_summary_selected.svg", 
    ICO_SVG_SIDEBAR_PROBLEM_SITUATION: "images/ico_sidebar_menu_problem_situation_selected.svg",
    ICO_SVG_SIDEBAR_BOOK: "images/ico_sidebar_menu_book_selected.svg",    
    ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS: "images/ico_sidebar_menu_complementary_materials_selected.svg", 
    ICO_SVG_SIDEBAR_EVALUATION: "images/ico_sidebar_menu_evaluation_selected.svg", 

    ICO_PNG_SIDEBAR_SUMMARY: "images/ico_sidebar_menu_summary_selected.png",
    ICO_PNG_SIDEBAR_PROBLEM_SITUATION: "images/ico_sidebar_menu_problem_situation_selected.png",
    ICO_PNG_SIDEBAR_BOOK: "images/ico_sidebar_menu_book_selected.png", 
    ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS: "images/ico_sidebar_menu_complementary_materials_selected.png",     
    ICO_PNG_SIDEBAR_EVALUATION: "images/ico_sidebar_menu_evaluation_selected.png", 

    QUESTION_STATUS_IMAGES: {
        ICO_PNG_WRONG: "images/ico_png_wrong.png",
        ICO_SVG_WRONG: "images/ico_svg_wrong.svg",
        ICO_PNG_ANSWERING: "images/ico_png_attention.png",
        ICO_SVG_ANSWERING: "images/ico_svg_attention.svg",
        ICO_PNG_SUCCESS: "images/ico_png_rigth.png",
        ICO_SVG_SUCCESS: "images/ico_svg_right.svg",
        ICO_PNG_NO_ANSWER: "images/ico_png_no_answer.png",
        ICO_SVG_NO_ANSWER: "images/ico_svg_no_answer.svg",
    },
    QUESTION_STATUS_IMAGES_NO_SIGNS: {
        ICO_PNG_WRONG: "images/ico_png_wrong_empty.png",
        ICO_SVG_WRONG: "images/ico_svg_wrong_empty.svg",
        ICO_PNG_ANSWERING: "images/ico_png_attention_empty.png",
        ICO_SVG_ANSWERING: "images/ico_svg_attention_empty.svg",
        ICO_PNG_SUCCESS: "images/ico_png_rigth_empty.png",
        ICO_SVG_SUCCESS: "images/ico_svg_right_empty.svg",
        ICO_PNG_NO_ANSWER: "images/ico_png_no_answer_empty.png",
        ICO_SVG_NO_ANSWER: "images/ico_svg_no_answer_empty.svg",
    },

    ICO_GIF_UNIT_1_SUCCESS: "images/img_course_progress_unit_1_complete.gif",
    ICO_GIF_UNIT_2_SUCCESS: "images/img_course_progress_unit_2_complete.gif",
    ICO_GIF_UNIT_3_SUCCESS: "images/img_course_progress_unit_3_complete.gif",
    
    ICO_PNG_COURSE_SUCCESS: "images/ico_png_course_success.png",

    ICO_SVG_ARROW_DOWN: "images/ico_arrow_down.svg",
    ICO_PNG_ARROW_DOWN: "images/ico_arrow_down.svg",
    ICO_SVG_ARROW_DOWN_WHITE: "images/ico_arrow_down_white.svg",
    ICO_PNG_ARROW_DOWN_WHITE: "",

    ICO_SVG_PLAY_ARROW_WHITE: "images/ico_svg_arrow_play_white.svg",
    ICO_PNG_PLAY_ARROW_WHITE: "images/ico_png_arrow_play_white.png",
    ICO_SVG_PLAY_ARROW_BLACK: "images/ico_svg_arrow_play_black.svg",
    ICO_PNG_PLAY_ARROW_BLACK: "images/ico_png_arrow_play_black.png",

    ICO_PNG_CLOSE_BLACK: "images/ico_png_close.png",
    ICO_SVG_CLOSE_BLACK: "images/ico_svg_close.svg",

    ICO_PNG_FULL_SCREEN_BLACK: "images/ico_png_full_screen.png",
    ICO_SVG_FULL_SCREEN_BLACK: "images/ico_svg_full_screen.svg",

    ICO_PNG_ARROW_RIGHT_BLACK: "images/sidebar_arrow_right_black.png",
    ICO_SVG_ARROW_RIGHT_BLACK: "images/sidebar_arrow_right_black.svg",

    ICO_PNG_MS_WHITE: "images/ico_png_ms_white_2.png",
    ICO_SVG_MS_WHITE: "images/ico_svg_ms_white.svg",
    ICO_PNG_MS_BLACK: "images/ico_png_ms_black_2.png",
    ICO_SVG_MS_BLACK: "images/ico_svg_ms_black.svg",

    ICO_PNG_IAM_WHITE: "images/ico_png_iam_white_2.png",
    ICO_SVG_IAM_WHITE: "images/ico_svg_iam_white.svg",
    ICO_PNG_IAM_BLACK: "images/ico_png_iam_black_2.png",
    ICO_SVG_IAM_BLACK: "images/ico_svg_iam_black.svg",

    ICO_PNG_EAD_WHITE: "images/ico_png_ead_white_2.png",
    ICO_SVG_EAD_WHITE: "images/ico_svg_ead_white.svg",
    ICO_PNG_EAD_BLACK: "images/ico_png_ead_black_2.png",
    ICO_SVG_EAD_BLACK: "images/ico_svg_ead_black.svg",

    ICO_PNG_IFF_WHITE: "images/ico_png_iff_white_2.png",
    ICO_SVG_IFF_WHITE: "images/ico_svg_iff_white.svg",
    ICO_PNG_IFF_BLACK: "images/ico_png_iff_black_2.png",
    ICO_SVG_IFF_BLACK: "images/ico_svg_iff_black.svg",

    ICO_PNG_I_WHITE: "",
    ICO_SVG_MS_WHITE: "",

    ICO_SVG_READ_WHITE: "images/ico_svg_read_white.svg",
    ICO_PNG_READ_WHITE: "images/ico_png_book_white.png",
    ICO_SVG_NEED_INTERNET: "images/ico_svg_need_internet_white.svg",
    ICO_PNG_NEED_INTERNET: "",

    ICO_SVG_PROJECTOR_WHITE: "images/ico_svg_projector_white.svg",
    ICO_PNG_PROJECTOR_WHITE: "images/ico_png_projector_white.png",
    ICO_SVG_PROJECTOR_BLACK: "images/ico_svg_projector_black.svg",
    ICO_PNG_PROJECTOR_BLACK: "images/ico_png_projector_black.png",

    ICO_SVG_INFOGRAPHIC_WHITE: "images/ico_svg_infographic_white.svg",
    ICO_PNG_INFOGRAPHIC_WHITE: "images/ico_png_infographic_white.png",
    ICO_SVG_INFOGRAPHIC_BLACK: "images/ico_svg_infographic_black.svg",
    ICO_PNG_INFOGRAPHIC_BLACK: "images/ico_png_infographic_black.png",

    ICO_SVG_DOWNLOAD_WHITE: "images/ico_svg_download_white.svg",
    ICO_SVG_DOWNLOAD_BLACK: "images/ico_svg_download_black.svg",
    ICO_PNG_DOWNLOAD_WHITE: "images/ico_png_download_white.png",
    ICO_PNG_DOWNLOAD_BLACK: "images/ico_png_download_black.png",

    BG_IMG_BLOOD_FLOW_1: "images/bg_blood_flow_1.png",
    BG_IMG_BLOOD_FLOW_2: "images/bg_blood_flow_2.png",
    BG_IMG_BLOOD_FLOW_3: "images/bg_blood_flow_3.png",

    IMG_COURSE_LOGO: "images/ico_course_logo.png",
    IMG_COURSE_BRAND: "images/img_course_brand.png",

    GIF_PAGE_LOADER: "images/gif_colorful_page_loader.gif",

    browerSupportsSvg: function() {
        if (typeof SVGRect !== "undefined") {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * Files
 */
var gFiles = {

    FILE_TYPE: {
        TEXT: "text",
        VIDEO: "video",
        AUDIO: "audio",
        IMAGE: "image",
        OTHER: "other"
    },

    // Books 
    PDF_COURSE_BOOK_COVER: {
        TITLE: "Curso de aperfeiçoamento no controle e monitoramento de mosquitos vetores",
        SUB_TITLE: "Fiocruz, 2018",
        URL: "resources/pdfs/livro_curso_cover.pdf",
        EXT_URL: ""
    },
    PDF_COURSE_BOOK: {
        TITLE: "Curso de aperfeiçoamento no controle e monitoramento de mosquitos vetores",
        SUB_TITLE: "Fiocruz, 2018",
        URL: "resources/pdfs/livro_curso.pdf",
        EXT_URL: ""
    },
    PDF_GUIA_VIGILANCIA: {
        TITLE: "Guia de vigilância do <i>Culex quinquefasciatus</i>",
        SUB_TITLE: "Ministério da Saúde, Secretaria de Vigilância em Saúde, Departamento de Vigilância Epidemiológica, 2011",
        URL: "resources/pdfs/ms_guia_vigilancia_culex_quinquefasciatus.pdf",
        EXT_URL: ""
    },
    
    // Articles
    PDF_ART_PLACEHOLDER: {
        TITLE: "<span>AUTOR, I.N.I.C.I.A.I.S.</span> Nome do Artigo Placeholder",
        SUB_TITLE: "Local onde foi publicado, AAAA",
        URL: "resources/pdfs/art_1_moreira_goldani_2010.pdf",
        EXT_URL: ""
    },
    PDF_ART_REVISTA_SAUDE_PUBLICA: {
        TITLE: "<span>Cleide MR de Albuquerque, Maria Alice V Melo-Santos, Mary Ann S Bezerra, Rosângela MR Barbosa, Dílvia F Silva e Edinalda da Silva.</span> Primeiro registro de <i>Aedes albopictus</i> em área da Mata Atlântica, Recife, PE, Brasil.",
        SUB_TITLE: "Universidade de São Paulo - Faculdade de Saúde Pública, 2000",
        URL: "resources/pdfs/mag_saude_publica_2000_albuquerque.pdf",
        EXT_URL: ""
    },
    PDF_ART_PRINCIPAIS_MOSQUITOS_IMPORTANCIA_SANITARIA_BRASIL: {
        TITLE: "<span>Rotraut A. G. B. Consoli, Ricardo Lourenço de Oliveira.</span> Principais Mosquitos de Importância Sanitária no Brasil",
        SUB_TITLE: "Fundação Oswaldo Cruz, Editora FIOCRUZ, 1994",
        URL: "resources/pdfs/ms_book_principais_mosquitos_importancia_sanitaria_brasil.pdf",
        EXT_URL: ""
    },
    PDF_ART_LIST_MOSQUITOS_BRAZILIAN_STATE_PERNAMBUCO: {
        TITLE: "<span><u>Nádia Consuelo Aragão</u>, Gerson Azulim Müller, Valdir Queiroz Balbino, César Raimundo Lima Costa Junior, Carlos Santiago Figueirêdo Júnior, Jerônimo Alencar, Carlos Brisola Marcondes.</span> Lista de espécies de mosquitos do Estado de Pernambuco e primeiro relato de <i>Haemagogus janthinomys</i> (<i>Diptera: Culicidae</i>) vetor de febre amarela silvestre e outras 14 espécies (<i>Diptera: Culicidae</i>)",
        SUB_TITLE: "Revista da Sociedade Brasileira de Medicina Tropical, julho - agosto, 2010",
        URL: "resources/pdfs/art_araggo_et_al_2010_culicidofauna_pe.pdf",
        EXT_URL: ""
    },
    PDF_ART_CULICIDAE_FAUNA: {
        TITLE: "<span><u>Cláudio Júlio da Silva</u>, Sônia Valéria Pereira, Edivaldo José Apolinário, Gilvan Leite dos Santos, Maria Alice Varjal Melo-Santos, Alexandre Freitas da Silva, Gabriel Luz Wallau, and Cláudia Maria Fontes de Oliveira.</span> <i>Culicidae</i> fauna (<i>Diptera: Culicidae</i>) survey in urban, ecotonal and forested areas, from the Moreno municipality - Pernambuco State, Brazil.",
        SUB_TITLE: "Revista da Sociedade Brasileira de Medicina Tropical, julho - agosto, 2018",
        URL: "resources/pdfs/art_rev_soc_bras_med_trop_silva_et_al_2018.pdf",
        EXT_URL: ""
    },
    PDF_ART_VECTOR_BORNE_DISEASES_GUEDES_2010: {
        TITLE: "<span>D.R.D. Guedes, M.T. Cordeiro, M.A.V. Melo-Santos, T. Magalhaes, E. Marques, L. Regis, A.F. Furtado & C.F.J. Ayres.</span> Patient-based dengue virus surveillance in <i>Aedes aegypti</i> from Recife, Brazil",
        SUB_TITLE: "J Vector Borne Dis 47, junho, 2010",
        URL: "resources/pdfs/art_vector_borne_diseases_2010_guedes.pdf",
        EXT_URL: ""
    },
    PDF_ART_PARASITES_VECTORS_2013_ARAUJO: {
        TITLE: "<span>Ana Paula Araújo, Diego Felipe Araujo Diniz, Elisama Helvecio, Rosineide Arruda de Barros, Cláudia Maria Fontes de Oliveira, Constância Flávia Junqueira Ayres, Maria Alice Varjal de Melo-Santos, Lêda Narcisa Regis and Maria Helena Neves Lobo Silva-Filha*.</span> The susceptibility of <i>Aedes aegypti</i> populations displaying <i>temephos</i> resistance to <i>Bacillus thuringiensis israelensis</i>: a basis for management",
        SUB_TITLE: "Parasites & Vectors, 2013",
        URL: "resources/pdfs/art_parasites_vectors_2013_araujo.pdf",
        EXT_URL: ""
    },
    PDF_ART_DEVELOPING_NEW_APPROACHES_PREVENTING_AEDES: {
        TITLE: "<span>Lêda Regis, Antonio Miguel Monteiro, Maria Alice Varjal de Melo-Santos, José Constantino Silveira Jr, André Freire Furtado, Ridelane Veiga Acioli, Gleice Maria Santos, Mitsue Nakazawa, Marilia Sá Carvalho, Paulo Justiniano Ribeiro Jr, Wayner Vieira de Souza.</span> Developing new approaches for detecting and preventing <i>Aedes aegypti</i> population outbreaks: basis for surveillance, alert and control system",
        SUB_TITLE: "Mem. Inst. Oswaldo Cruz, fevereiro, 2008",
        URL: "resources/pdfs/art_developing_new_approaches_preventing_aedes.pdf",
        EXT_URL: ""
    },
    PDF_ART_CHARACTERIZATION_SPACIAL_TEMPORAL_DYNAMICS_DENGUE: {
        TITLE: "<span>Lêda N. Regis *, Ridelane Veiga Acioli, José Constantino Silveira Jr., Maria Alice Varjal de Melo-Santos, Mércia Cristiane Santana da Cunha, Fátima Souza, Carlos Alberto Vieira Batista, Rosângela Maria Rodrigues Barbosa, Cláudia Maria Fontes de Oliveira, Constância Flávia Junqueira Ayres, Antonio Miguel Vieira Monteiro, Wayner Vieira Souza.</span> Characterization of the spatial and temporal dynamics of the dengue vector population established in urban areas of Fernando de Noronha, a Brazilian oceanic island",
        SUB_TITLE: "Acta Tropica, 2014",
        URL: "resources/pdfs/art_characterization_spacial_temporal_dynamics_dengue_2014.pdf",
        EXT_URL: ""
    },
    PDF_ART_DENGUE_BULLETIN_2003_SANTOS: {
        TITLE: "<span>SRA Santos *, MAV Melo-Santos, L Regis and CMR Albuquerque.</span> Field Evaluation of Ovitraps Consociated with Grass Infusion and <i>Bacillus thuringiensis var. israelensis</i> to determine Oviposition Rates of <i>Aedes aegypti</i>",
        SUB_TITLE: "Dengue Bulletin, 2013",
        URL: "resources/pdfs/art_dengue_bulletin_2003_santos.pdf",
        EXT_URL: ""
    },
    PDF_ART_VECTOR_SURVEILLANCE_DENGUE: {
        TITLE: "<span>P. P. Barbosa, D.R.D. Guedes, M.A.V. Melo-Santos, M.T. Cordeiro, R. V. Acioli, C.A.V. Batista, L.S.M. Gonçalves, M.F.M. Souza, Y. V. Araújo, F.J.R. Magalhães, L. Regis, and C.F.J. Ayres.</span> Vector Surveillance for Dengue Virus Detection in the Archipelago of Fernando de Noronha, Brazil",
        SUB_TITLE: "Journal of Medical Entomology, 2016",
        URL: "resources/pdfs/art_vector_surveillance_dengue_virus_detection.pdf",
        EXT_URL: ""
    },

    // Info Graphics
    PDF_INFO_GRAF_PLACEHOLDER: {
        TITLE: "Infográfico Placeholder",
        SUB_TITLE: "Título do Infográfico Placeholder, AAAA",
        URL: "resources/pdfs/info_graf_1_cdcp_sabe_nao_sabe_zika.pdf",
        EXT_URL: ""
    },
    
    // Materials from the Ministry of Health
    PDF_SLIDES_PLACEHOLDER: {
        TITLE: "Instituição Placeholder",
        SUB_TITLE: "Instituição Placeholder, 2017",
        URL: "resources/pdfs/ms_slides_coletiva_zika.pdf",
        EXT_URL: ""
    },
    
    // Videos
    VIDEO_RESOLUTION_MANY: "many",
    LOCAL_VIDEO_PLACEHOLDER: {
        COVER: "images/img_video_cover_placeholder.png",
        TITLE: "Vídeo provisório (exibido até que o conteúdo definitivo seja criado)",
        SUB_TITLE: "Autor Provisório - Instituição Provisória",
        URL: "./videos/placeholder_video_320p.mp4",
        EXT_URL: "",
        RESOLUTION: "320p"
    },
    VIDEO_PLACEHOLDER: {
        COVER: "images/img_video_cover_placeholder.png",
        TITLE: "Vídeo provisório (exibido até que o conteúdo definitivo seja criado)",
        SUB_TITLE: "Autor Provisório - Instituição Provisória",
        URL: "",
        EXT_URL: "https://www.youtube.com/embed/XmvmtbOut6Q",
        RESOLUTION: "720p"
    },
    VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS: {
        COVER: "images/img_video_cover_conhecendo_aedes.png",
        TITLE: "Conhecendo os mosquitos <i>Aedes</i> - Transmissores de arbovírus (2018)",
        SUB_TITLE: "SPTI - IOC/Fiocruz",
        URL: "./videos/conhecendo_aedes_720p.mp4",
        EXT_URL: "",
        RESOLUTION: "720p"
    },
    VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS: {
        COVER: "images/img_video_cover_aedes_aegypti_e_albopictus.png",
        TITLE: "<i>Aedes aegypti</i> e <i>Aedes albopictus</i> – Uma Ameaça nos Trópicos (2015)",
        SUB_TITLE: "SPTI - IOC/Fiocruz",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=St-uVi41qbc",
        RESOLUTION: "720p"
    },
    VIDEO_MUNDO_MACRO_MICRO_AEDES: {
        COVER: "images/img_video_cover_mundo_macro_micro_aedes.png",
        TITLE: "O mundo Macro e Micro do Mosquito <i>Aedes aegypti</i> (2010)",
        SUB_TITLE: "SPTI - IOC/Fiocruz",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=qmzhpbjxYvk&t=3s",
        RESOLUTION: "720p"
    },
    VIDEO_AEDES_SIMPLES_MOSQUITO: {
        COVER: "images/img_video_cover_aedes_simples_mosquito.png",
        TITLE: "<i>Aedes aegypti</i> - Um simples mosquito?",
        SUB_TITLE: "Canal Saúde da Fiocruz",
        URL: "",
        EXT_URL: "https://www.canalsaude.fiocruz.br/canal/videoAberto/Aedes-Aegypti-Um-simples-mosquito-SDC-0331",
        RESOLUTION: "420p"
    },
    VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES: {
        COVER: "images/img_video_cover_desenvolvimento_tec_combate_aedes.png",
        TITLE: "Fiocruz desenvolve tecnologia para combater o <i>Aedes aegypti</i>",
        SUB_TITLE: "Canal do Planalto no Youtube",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=qQXAzPgjJqc",
        RESOLUTION: "720p"
    },
    VIDEO_MICROINJECAO_WOLBACHIA: {
        COVER: "images/img_video_cover_microinjecao_wolbachia.png",
        TITLE: "Microinjeção da <i>Wolbachia</i>",
        SUB_TITLE: "Canal do World Mosquito Program Brasil no Youtube",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=71ZPnEiTyQE",
        RESOLUTION: "720p"
    },
    VIDEO_WMP_BRASIL_COMO_TRABALHAMOS: {
        COVER: "images/img_video_cover_wmp_brasil_como_trabalhamos.png",
        TITLE: "WMP no Brasil - como trabalhamos",
        SUB_TITLE: "Canal do World Mosquito Program Brasil no Youtube",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=XcernUipSf4",
        RESOLUTION: "720p"
    },
    VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO: {
        COVER: "images/img_video_cover_wmp_complexo_alemao.png",
        TITLE: "WMP Brasil no Complexo do Alemão",
        SUB_TITLE: "Canal do World Mosquito Program Brasil no Youtube",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=NoaEaC6u1cc",
        RESOLUTION: "720p"
    },
    VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS: {
        COVER: "images/img_video_cover_g1_noticia_recife_inaugura_centro_mosquitos.png",
        TITLE: "Recife inaugura Centro de Mosquitos Estéreis para combater arboviroses",
        SUB_TITLE: "Portal de notícias G1",
        URL: "",
        EXT_URL: "http://g1.globo.com/pernambuco/ne1/videos/t/edicoes/v/recife-inaugura-centro-de-mosquitos-estereis-para-combater-arboviroses/7241640/",
        RESOLUTION: "720p"
    },
    VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA: {
        COVER: "images/img_video_cover_opiniao_pe_avanco_zika.png",
        TITLE: "Opinião Pernambuco - 22/02/2016 (Avanço do Zika Vírus)",
        SUB_TITLE: "Canal da TVU Recife no Youtube",
        URL: "",
        EXT_URL: "https://www.youtube.com/watch?v=L_3A00gxuBY",
        RESOLUTION: "720p"
    }

}

var gExtResources = {

    EXT_RESOURCE_PLACEHOLDER: {
        TITLE: "Placeholder de link externo",
        SUB_TITLE: "Conteúdo externo, disponível no site do placeholder",
        URL: "http://combateaedes.saude.gov.br/pt/"
    },
    EXT_RESOURCE_PREVENCAO_COMBATE_DENGUE_CHIK_ZIKA_PMS: {
        TITLE: "Prevenção e Combate: Dengue, Chikungunya e Zika",
        SUB_TITLE: "Conteúdo externo, disponível no site do Zika no Portal do Ministério da Saúde",
        URL: "http://combateaedes.saude.gov.br/pt/"
    },
    EXT_RESOURCE_MOSQUITOS_AMEACA_AR: {
        TITLE: "Mosquitos uma ameaça no ar",
        SUB_TITLE: "Conteúdo externo, disponível no canal Discovery Brasil no Youtube",
        URL: "https://www.youtube.com/watch?v=JRqjVJYKNj8"
    },
    EXT_RESOURCE_ANIMAIS_PERIGOSOS: {
        TITLE: "Animais perigosos.",
        SUB_TITLE: "Conteúdo externo, disponível na Netflix. <small>Obs: é necessário ter uma conta de usuário da Netflix para visualizar o episódio que fala sobre os mosquitos.</small>",
        URL: "https://www.netflix.com/watch/80165259?trackId=155573560"
    },
    EXT_RESOURCE_AEDES_VS_CULEX: {
        TITLE: "<i>Aedes</i> X <i>Culex</i>.",
        SUB_TITLE: "Conteúdo externo, disponível no canal Aula Dengue. O pesquisador do Instituto Oswaldo Cruz (IOC/Fiocruz) José Bento Pereira explica diferenças entre o <i>Culex</i> e o <i>Aedes</i>. Este vídeo faz parte do projeto Vídeo-aulas ‘<i>Aedes aegypti</i> – Introdução aos Aspectos Científicos do Vetor’ - Instituto Oswaldo Cruz (IOC/Fiocruz).",
        URL: "https://youtu.be/uG4yIp6ZwDg"
    },
    EXT_RESOURCE_TRANSMISSAO_ZIKA_PERNILONGOS: {
        TITLE: "Transmissão de Zika por pernilongos",
        SUB_TITLE: "Conteúdo externo, disponível no canal Rede Brasil Oficial no Youtube",
        URL: "https://www.youtube.com/watch?v=BzdqO71_TIw"
    },
    EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES: {
        TITLE: "Técnica que esteriliza <i>Aedes aegypti</i> deve ficar pronta em quatro meses",
        SUB_TITLE: "Conteúdo externo, disponível no portal de notícias G1",
        URL: "http://g1.globo.com/pernambuco/noticia/2016/02/tecnica-de-esterilizacao-do-aedes-deve-ficar-pronta-em-quatro-meses.html"
    },
    EXT_RESOURCE_FIOCRUZ_PROJETO_TESTE_CONTROLE_AEDES: {
        TITLE: "Projeto testa controle do <i>Aedes aegypti</i> usando energia nuclear",
        SUB_TITLE: "Conteúdo externo, disponível no portal de notícias da Fiocruz",
        URL: "https://portal.fiocruz.br/noticia/projeto-testa-controle-do-aedes-aegypti-usando-energia-nuclear"
    },
    EXT_RESOURCE_CANAL_SAUDE_UM_SIMPLES_MOSQUITO: {
        TITLE: "<i>Aedes aegypti</i> - Um simples mosquito?",
        SUB_TITLE: "Conteúdo externo, disponível no site da Fiocruz - Canal Saúde",
        URL: "https://www.canalsaude.fiocruz.br/canal/videoAberto/Aedes-Aegypti-Um-simples-mosquito-SDC-0331"
    },

    EXT_RESOURCE_NOTICIAS_RECENTES_WMP_BRASIL_METODO_WOLBACHIA: {
        TITLE: "Notícias mais recentes sobre o World Mosquito Program Brasil e o Método <i>Wolbachia</i>, uma iniciativa inovadora de combate às doenças transmitidas por mosquitos, como dengue, Zika, chikungunya e Febre Amarela urbana.",
        SUB_TITLE: "Conteúdo externo, disponível no site do World Mosquito Program Brasil",
        URL: "http://www.eliminatedengue.com/brasil"
    }

}