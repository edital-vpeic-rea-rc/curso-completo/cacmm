/**
 * This script is used to manage the integration between the course and its data persistency layer
 * @author Diego M. Silva (diegomsilva.com)
 * @requires gImages
 */

var gData = {

    /**
     * Global
     */
    ID_CONNECTOR: "_",

    /**
     * Session variables not stored in the backend
     */
    IDX_SESSION_VAR_SELECTED_QUESTION_LINK: "selected_question_link",

    /**
     * Clinical cases
     */
    CLINICAL_CASES_IDS: {
        CASE_1: "clinicalCase1",
        CASE_2: "clinicalCase2",
        CASE_3: "clinicalCase3"
    },

    /**
     * Course
     */
    COURSE_WORK_LOAD: "30 horas",
    QUESTION_TYPES: {
        SINGLE_ANSWER: "singleAnswer",
        MULTIPLE_ANSWER: "multipleAnswers",
        TRUE_FALSE_ANSWER: "multipleAnswersTrueFalse",
    },
    QUESTION_STATUS: {
        WRONG: "wrong",
        ANSWERING: "answering",
        RIGHT: "right",
        CLEAR: ""
    },
    LANG: {
        PT: "pt",
        EN: "en"
    },
    COURSE_STATUS: {
        ATTENDED: "attended",
        ATTEMPTED: "attempted",
        COMPLETED: "completed",
        PASSED: "passed",
        FAILED: "failed"
    },
    DEFAULT_USER_COURSE_ID: -1,
    DEFAULT_MSG_RIGHT_QUESTION_ANSWER: "Parabéns, você acertou!",
    DEFAULT_MSG_WRONG_QUESTION_ANSWER: "Você errou, siga as dicas e tente novamente.",
    DEFAULT_MSG_EMPTY_ANSWER: "Selecione alguma opção e então envie a resposta.",

    prevModuleInfo: {
        pageId: "",
        pageName: ""
    },
    nextModuleInfo : {
        pageId: "",
        pageName: ""
    },

    /**
     * The course variable is used to feed the UI of the evaluation forms
     */
    course: {
        name: "Mosquitos Bases da Vigilância e Controle",
        units: [
            {
                unitName: "Unidade 1",
                unitId: 1,
                unitQuestions: [
                    {
                        questionId: 1,
                        type: "singleAnswer",
                        enunciate: "Os insetos reúnem o maior número de espécies animais conhecidos. Assim, é o grupo mais diversificado dentre os artrópodes e, consequentemente, dentre todos os animais. A maioria dos insetos é terrestre, embora que algumas espécies tenham se adaptado secundariamente à vida no ambiente de água doce e, mais raramente, na superfície de oceanos e zonas de entre marés. Sobre os insetos, julgue as alternativas abaixo entre <b>VERDADEIRAS</b> e <b>FALSAS</b> e escolha a sequência correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Os insetos são invertebrados pertencentes à Classe <i>Insecta</i>, apresentando um exoesqueleto quitinoso, membros articulados e adaptação ao voo."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Devido às diferentes estratégias alimentares, alguns insetos tornaram-se potenciais vetores de doenças ao realizar alimentação sanguínea em humanos e outros animais. Doenças como malária, filariose linfática, dengue, febre amarela e chikungunya são exemplos de doenças importantes que acometem seres humanos, causando sérios problemas de saúde pública."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A ordem <i>Diptera</i> reúne insetos que apresentam somente um par de asas, diferente dos outros insetos alados que possuem dois pares. Nessa ordem estão reunidas diferentes espécies de mosquitos e moscas."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Os insetos, em geral, têm o corpo divido em cabeça, tórax e abdômen. Na cabeça estão os olhos compostos, as peças bucais e as antenas. No tórax, observamos a inserção das pernas e no abdômen, asas e halteres."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "As antenas são órgãos extremamente importantes para os insetos. Elas se apresentam aos pares, são móveis e possuem minúsculas sensilas (pelos) que auxiliam na captação de estímulos do meio. Em mosquitos, as antenas são órgãos que nos permite identificar o sexo desses animais. As antenas dos machos possuem uma grande quantidade de pelos, já as das fêmeas, a quantidade é bem menor."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, V",
                                alternativeHint: "I - VERDADEIRO. Além dessas características, os insetos apresentam um eficiente e rápido deslocamento, fuga de predadores, buscas por novas fontes de alimento, além de locais com condições mais adequadas à sobrevivência.<br><br>II - VERDADEIRO. Uns importantes números de insetos estão envolvidos na transmissão de patógenos a partir do processo de alimentação sanguínea. Como exemplo podemos citar ainda os piolhos, que transmitem o tifo, as pulgas, envolvidas na transmissão da peste bubônica e os barbeiros, que transmitem o patógeno causar da Doença de Chagas.<br><br>III - VERDADEIRO. Os dípteros, na evolução animal, tiveram o segundo par de asas atrofiado formando estruturas vestigiais chamadas de os halteres ou balancins. Esses órgãos são evidências da existência do segundo par de asas em moscas e mosquitos em passado distante. Alguns autores acreditam que os halteres possuem fundamental importância no equilíbrio ao voo nos dípteros, uma vez que cada halter está posicionado em um lado do tórax do animal.<br><br>IV - FALSO. Muito cuidado! As asas e halteres ficam no tórax e não no abdômen. Não há nenhuma inserção de apêndices articulados no abdômen.<br><br>V - VERDADEIRO. As antenas são órgãos sensoriais fundamentais para auxiliar na compreensão do meio onde estão inseridos. É a partir das antenas que os insetos identificam melhores fontes alimentares, melhores locais para deposição dos ovos, além de afastarem-se de locais que possuem moléculas odoríferas que sinalizam perigo.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, V, F",
                                alternativeHint: "I - VERDADEIRO. Além dessas características, os insetos apresentam um eficiente e rápido deslocamento, fuga de predadores, buscas por novas fontes de alimento, além de locais com condições mais adequadas à sobrevivência.<br><br>II - VERDADEIRO. Uns importantes números de insetos estão envolvidos na transmissão de patógenos a partir do processo de alimentação sanguínea. Como exemplo podemos citar ainda os piolhos, que transmitem o tifo, as pulgas, envolvidas na transmissão da peste bubônica e os barbeiros, que transmitem o patógeno causar da Doença de Chagas.<br><br>III - VERDADEIRO. Os dípteros, na evolução animal, tiveram o segundo par de asas atrofiado formando estruturas vestigiais chamadas de os halteres ou balancins. Esses órgãos são evidências da existência do segundo par de asas em moscas e mosquitos em passado distante. Alguns autores acreditam que os halteres possuem fundamental importância no equilíbrio ao voo nos dípteros, uma vez que cada halter está posicionado em um lado do tórax do animal.<br><br>IV - FALSO. Muito cuidado! As asas e halteres ficam no tórax e não no abdômen. Não há nenhuma inserção de apêndices articulados no abdômen.<br><br>V - VERDADEIRO. As antenas são órgãos sensoriais fundamentais para auxiliar na compreensão do meio onde estão inseridos. É a partir das antenas que os insetos identificam melhores fontes alimentares, melhores locais para deposição dos ovos, além de afastarem-se de locais que possuem moléculas odoríferas que sinalizam perigo.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, F, V",
                                alternativeHint: "I - VERDADEIRO. Além dessas características, os insetos apresentam um eficiente e rápido deslocamento, fuga de predadores, buscas por novas fontes de alimento, além de locais com condições mais adequadas à sobrevivência.<br><br>II - VERDADEIRO. Uns importantes números de insetos estão envolvidos na transmissão de patógenos a partir do processo de alimentação sanguínea. Como exemplo podemos citar ainda os piolhos, que transmitem o tifo, as pulgas, envolvidas na transmissão da peste bubônica e os barbeiros, que transmitem o patógeno causar da Doença de Chagas.<br><br>III - VERDADEIRO. Os dípteros, na evolução animal, tiveram o segundo par de asas atrofiado formando estruturas vestigiais chamadas de os halteres ou balancins. Esses órgãos são evidências da existência do segundo par de asas em moscas e mosquitos em passado distante. Alguns autores acreditam que os halteres possuem fundamental importância no equilíbrio ao voo nos dípteros, uma vez que cada halter está posicionado em um lado do tórax do animal.<br><br>IV - FALSO. Muito cuidado! As asas e halteres ficam no tórax e não no abdômen. Não há nenhuma inserção de apêndices articulados no abdômen.<br><br>V - VERDADEIRO. As antenas são órgãos sensoriais fundamentais para auxiliar na compreensão do meio onde estão inseridos. É a partir das antenas que os insetos identificam melhores fontes alimentares, melhores locais para deposição dos ovos, além de afastarem-se de locais que possuem moléculas odoríferas que sinalizam perigo.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I - VERDADEIRO. Além dessas características, os insetos apresentam um eficiente e rápido deslocamento, fuga de predadores, buscas por novas fontes de alimento, além de locais com condições mais adequadas à sobrevivência.<br><br>II - VERDADEIRO. Uns importantes números de insetos estão envolvidos na transmissão de patógenos a partir do processo de alimentação sanguínea. Como exemplo podemos citar ainda os piolhos, que transmitem o tifo, as pulgas, envolvidas na transmissão da peste bubônica e os barbeiros, que transmitem o patógeno causar da Doença de Chagas.<br><br>III - VERDADEIRO. Os dípteros, na evolução animal, tiveram o segundo par de asas atrofiado formando estruturas vestigiais chamadas de os halteres ou balancins. Esses órgãos são evidências da existência do segundo par de asas em moscas e mosquitos em passado distante. Alguns autores acreditam que os halteres possuem fundamental importância no equilíbrio ao voo nos dípteros, uma vez que cada halter está posicionado em um lado do tórax do animal.<br><br>IV - FALSO. Muito cuidado! As asas e halteres ficam no tórax e não no abdômen. Não há nenhuma inserção de apêndices articulados no abdômen.<br><br>V - VERDADEIRO. As antenas são órgãos sensoriais fundamentais para auxiliar na compreensão do meio onde estão inseridos. É a partir das antenas que os insetos identificam melhores fontes alimentares, melhores locais para deposição dos ovos, além de afastarem-se de locais que possuem moléculas odoríferas que sinalizam perigo.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 2,
                        type: "singleAnswer",
                        enunciate: "Dentro da família <i>Culicidae</i>, duas subfamílias merecem destaque devido à sua grande importância médica, sendo elas as subfamílias <i>Culicinae</i> e <i>Anophelinae</i>. Na subfamília <i>Culicinae</i>, os principais gêneros envolvidos em transmissão de patógenos são os gêneros <i>Aedes</i> e <i>Culex</i>. Sobre o gênero <i>Aedes</i>, assinale a alternativa <b>INCORRETA</b>.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Os mosquitos pertencentes ao gênero <i>Aedes</i> possuem sua origem relacionada ao continente africano, provavelmente da região etiópica",
                                alternativeHint: "CORRETO. Os mosquitos do gênero <i>Aedes</i> ampliaram sua distribuição geográfica a partir da região etiópica para regiões tropicais e subtropicais do globo, através da expansão da navegação industrial do século 19.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Na fase adulta são caracterizados por apresentar corpo com abdômen frequentemente afilado. Na fase larvária, observam-se larvas com sifão respiratório curto, com aspecto cônico, geralmente escurecido.",
                                alternativeHint: "CORRETO. Além do corpo afilado, os mosquitos desses gêneros apresentam os últimos seguimentos abdominais parcialmente unidos uns nos outros, com cerdas salientes. Nas larvas, o sifão é utilizado para captar o ar atmosférico na superfície dos criadouros.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "O mosquito <i>Aedes aegypti</i>, espécie facilmente reconhecida por apresentar coloração marrom-escura, desenho em forma de lira no tórax e pernas com listras claras e escuras, é encontrado nas regiões tropicais e subtropicais do globo, envolvendo-se em ciclos de transmissão de arboviroses e filariose linfática",
                                alternativeHint: "INCORRETO. Parabéns por saber que o mosquito <i>Aedes aegypti</i> não está incriminado como vetor da filariose linfática. <i>Aedes aegypti</i> é atualmente a espécie mais importante nos registros epidemiológicos da febre amarela urbana, dos quatro sorotipos do vírus da dengue, do vírus Chikungunya e do vírus Zika.",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "<i>Aedes aegypti</i> é um mosquito extremamente antropofílico (alimentação preferencial em sangue humano), alimentando-se durante o dia, com maior atividade no amanhecer e no entardecer.",
                                alternativeHint: "CORRETO. <i>Aedes aegypti</i> está intimamente relacionado a aglomerados humanos nas cidades, onde encontra com facilidade fonte de alimentação, abrigo e potenciais criadouros.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "<i>Aedes albopictus</i>, é caracterizado por ser um mosquito enegrecido (bem mais escuro que o <i>Aedes aegypti</i>), com uma única listra de escamas branco-prateada bem marcada no tórax, apresentando pernas com manchas claras e escuras.",
                                alternativeHint: "CORRETO. Lembre-se que <i>Aedes albopictus</i>, no Brasil, possui a sua distribuição mais relacionada à ambientes rurais, semisilvestre e silvestre, o que não exclui a possibilidade da ocorrência desse inseto nas cidades.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 3,
                        type: "singleAnswer",
                        enunciate: "Sobre o gênero <i>Culex</i>, julgue as alternativas abaixo entre <b>VERDADEIRAS</b> e <b>FALSAS</b> e assinale a sequencia correta.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Mosquitos da espécie <i>Culex quinquefasciatus</i> são popularmente conhecidos como muriçocas ou carapanãs em diversas regiões do país. São insetos que estão distribuídos por todo território brasileiro, causando em algumas épocas do ano, grande desconforto devido às altas densidades populacionais."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Algumas espécies do gênero <i>Culex</i> são reconhecidas pela importância médico-veterinária na transmissão de doenças como a filariose bancroftiana, dirofilariose (filariose canina), malária aviária, diversas encefalites, além da febre do Nilo ocidental, Zika (recentemente incriminada), entre outras arboviroses."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Os ovos de <i>Culex</i> são postos de forma individualizada nas paredes dos recipientes, através da estratégia de oviposições aos saltos. Isso garante maiores chances de sobrevivência das larvas ao eclodir."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Os adultos de <i>Culex quinquefasciatus</i> apresentam porte médio e coloração marrom claro. Suas pernas são escuras e com marcações brancas."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Os criadouros preferenciais de <i>Culex quinquefasciatus</i> geralmente são coleções de água paradas, com grande quantidade de matéria orgânica, sombreadas no solo ou em recipientes naturais ou artificiais."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I - VERDADEIRO. Os mosquitos <i>Culex quinquefasciatus</i>, por realizar alimentação sanguínea preferencialmente durante a noite, causam grande desconforto às pessoas especialmente em períodos chuvosos, onde a oferta de criadouros no ambiente é aumentada.<br><br>II - VERDADEIRO. Em alguns países, mosquitos do gênero <i>Culex</i> estão diretamente relacionados a epidemias de importantes patógenos, em especial, vírus causadores de encefalites.<br><br>III - FALSO. Os ovos de <i>Culex</i> são postos em agrupamentos conhecido por jangadas e não de forma individualizada. Além disso, mosquitos do gênero <i>Culex</i> não possuem a estratégia de oviposição aos saltos. Quem realiza esse tipo de estratégia é <i>Aedes aegypti</i>.<br><br>IV - FALSO. <i>Culex quinquefasciatus</i> não possui marcações brancas nas pernas. As pernas dessa espécie são escuras e sem marcações.<br><br>V - VERDADEIRO. Os criadouros preferenciais de <i>Culex quinquefasciatus</i> em geral são fossas, esgotos, calhas e reservatório de água para animais.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, V, F",
                                alternativeHint: "I - VERDADEIRO. Os mosquitos <i>Culex quinquefasciatus</i>, por realizar alimentação sanguínea preferencialmente durante a noite, causam grande desconforto às pessoas especialmente em períodos chuvosos, onde a oferta de criadouros no ambiente é aumentada.<br><br>II - VERDADEIRO. Em alguns países, mosquitos do gênero <i>Culex</i> estão diretamente relacionados a epidemias de importantes patógenos, em especial, vírus causadores de encefalites.<br><br>III - FALSO. Os ovos de <i>Culex</i> são postos em agrupamentos conhecido por jangadas e não de forma individualizada. Além disso, mosquitos do gênero <i>Culex</i> não possuem a estratégia de oviposição aos saltos. Quem realiza esse tipo de estratégia é <i>Aedes aegypti</i>.<br><br>IV - FALSO. <i>Culex quinquefasciatus</i> não possui marcações brancas nas pernas. As pernas dessa espécie são escuras e sem marcações.<br><br>V - VERDADEIRO. Os criadouros preferenciais de <i>Culex quinquefasciatus</i> em geral são fossas, esgotos, calhas e reservatório de água para animais.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, F, V",
                                alternativeHint: "I - VERDADEIRO. Os mosquitos <i>Culex quinquefasciatus</i>, por realizar alimentação sanguínea preferencialmente durante a noite, causam grande desconforto às pessoas especialmente em períodos chuvosos, onde a oferta de criadouros no ambiente é aumentada.<br><br>II - VERDADEIRO. Em alguns países, mosquitos do gênero <i>Culex</i> estão diretamente relacionados a epidemias de importantes patógenos, em especial, vírus causadores de encefalites.<br><br>III - FALSO. Os ovos de <i>Culex</i> são postos em agrupamentos conhecido por jangadas e não de forma individualizada. Além disso, mosquitos do gênero <i>Culex</i> não possuem a estratégia de oviposição aos saltos. Quem realiza esse tipo de estratégia é <i>Aedes aegypti</i>.<br><br>IV - FALSO. <i>Culex quinquefasciatus</i> não possui marcações brancas nas pernas. As pernas dessa espécie são escuras e sem marcações.<br><br>V - VERDADEIRO. Os criadouros preferenciais de <i>Culex quinquefasciatus</i> em geral são fossas, esgotos, calhas e reservatório de água para animais.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I - VERDADEIRO. Os mosquitos <i>Culex quinquefasciatus</i>, por realizar alimentação sanguínea preferencialmente durante a noite, causam grande desconforto às pessoas especialmente em períodos chuvosos, onde a oferta de criadouros no ambiente é aumentada.<br><br>II - VERDADEIRO. Em alguns países, mosquitos do gênero <i>Culex</i> estão diretamente relacionados a epidemias de importantes patógenos, em especial, vírus causadores de encefalites.<br><br>III - FALSO. Os ovos de <i>Culex</i> são postos em agrupamentos conhecido por jangadas e não de forma individualizada. Além disso, mosquitos do gênero <i>Culex</i> não possuem a estratégia de oviposição aos saltos. Quem realiza esse tipo de estratégia é <i>Aedes aegypti</i>.<br><br>IV - FALSO. <i>Culex quinquefasciatus</i> não possui marcações brancas nas pernas. As pernas dessa espécie são escuras e sem marcações.<br><br>V - VERDADEIRO. Os criadouros preferenciais de <i>Culex quinquefasciatus</i> em geral são fossas, esgotos, calhas e reservatório de água para animais.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, F, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 4,
                        type: "singleAnswer",
                        enunciate: "A malária é uma das protozoonoses mais importantes do mundo, acometendo anualmente milhões de pessoas. Sobre o vetor do protozoário causador da malária, julgue as alternativas abaixo entre <b>VERDADEIRAS</b> e <b>FALSAS</b> e assinale a sequencia correta.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Conhecidos como anofelinos, os mosquitos da subfamília <i>Anophelinae</i> ocorrem principalmente nas regiões tropical e subtropical do globo terrestre."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Os anofelinos são mosquitos que, na maioria das suas espécies, repousam de modo quase perpendicular à superfície, semelhante a um prego inclinado."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Uma característica bem marcante dos anofelinos é a presença de palpos maxilares tão longos quanto à probóscide."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "O sifão respiratório das larvas de <i>Anopheles</i> é igual ao das larvas de Aedes, o que dificulta a identificação desses gêneros a partir da morfologia da larva."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Os ovos de <i>Anopheles</i> são depositados na parede do criadouro, semelhantemente aos mosquitos do gênero <i>Aedes</i>."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I – VERDADEIRO. Os anofelinos são mosquitos pertencentes ao gênero <i>Anopheles</i>, com distribuição em regiões quentes do globo. No Brasil, a principal espécie envolvida na transmissão do <i>Plasmodium</i> é a espécie <i>Anopheles darlingi</i>.<br><br>II – VERDADEIRO. Os anofelinos, devido ao fato do seu pouso praticamente perpendicular, são popularmente chamados de mosquitos-prego.<br><br>III - VERDADEIRO. Os palpos são bem longos e facilmente identificáveis lateralmente à probóscide (aparelho picador).<br><br>IV - FALSO. <i>Anopheles</i> não possui sifão respiratório nas larvas, e por esta razão, posicionam-se de modo que o seu corpo fique paralelo à superfície da água e respiram através de uma estrutura rudimentar de espiráculos localizados no 8º segmento abdominal.<br><br>V – FALSO. Ovos de <i>Anopheles</i> são depositados na lâmina d’água e não na parede dos criadouros, como ocorre em <i>Aedes</i>. Além disso, apresentam estruturas laterais que auxiliam na flutuação do ovo (flutuadores), que mantém seus ovos suspensos na água.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, V, F",
                                alternativeHint: "I – VERDADEIRO. Os anofelinos são mosquitos pertencentes ao gênero <i>Anopheles</i>, com distribuição em regiões quentes do globo. No Brasil, a principal espécie envolvida na transmissão do <i>Plasmodium</i> é a espécie <i>Anopheles darlingi</i>.<br><br>II – VERDADEIRO. Os anofelinos, devido ao fato do seu pouso praticamente perpendicular, são popularmente chamados de mosquitos-prego.<br><br>III - VERDADEIRO. Os palpos são bem longos e facilmente identificáveis lateralmente à probóscide (aparelho picador).<br><br>IV - FALSO. <i>Anopheles</i> não possui sifão respiratório nas larvas, e por esta razão, posicionam-se de modo que o seu corpo fique paralelo à superfície da água e respiram através de uma estrutura rudimentar de espiráculos localizados no 8º segmento abdominal.<br><br>V – FALSO. Ovos de <i>Anopheles</i> são depositados na lâmina d’água e não na parede dos criadouros, como ocorre em <i>Aedes</i>. Além disso, apresentam estruturas laterais que auxiliam na flutuação do ovo (flutuadores), que mantém seus ovos suspensos na água.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, F, V",
                                alternativeHint: "I – VERDADEIRO. Os anofelinos são mosquitos pertencentes ao gênero <i>Anopheles</i>, com distribuição em regiões quentes do globo. No Brasil, a principal espécie envolvida na transmissão do <i>Plasmodium</i> é a espécie <i>Anopheles darlingi</i>.<br><br>II – VERDADEIRO. Os anofelinos, devido ao fato do seu pouso praticamente perpendicular, são popularmente chamados de mosquitos-prego.<br><br>III - VERDADEIRO. Os palpos são bem longos e facilmente identificáveis lateralmente à probóscide (aparelho picador).<br><br>IV - FALSO. <i>Anopheles</i> não possui sifão respiratório nas larvas, e por esta razão, posicionam-se de modo que o seu corpo fique paralelo à superfície da água e respiram através de uma estrutura rudimentar de espiráculos localizados no 8º segmento abdominal.<br><br>V – FALSO. Ovos de <i>Anopheles</i> são depositados na lâmina d’água e não na parede dos criadouros, como ocorre em <i>Aedes</i>. Além disso, apresentam estruturas laterais que auxiliam na flutuação do ovo (flutuadores), que mantém seus ovos suspensos na água.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I – VERDADEIRO. Os anofelinos são mosquitos pertencentes ao gênero <i>Anopheles</i>, com distribuição em regiões quentes do globo. No Brasil, a principal espécie envolvida na transmissão do <i>Plasmodium</i> é a espécie <i>Anopheles darlingi</i>.<br><br>II – VERDADEIRO. Os anofelinos, devido ao fato do seu pouso praticamente perpendicular, são popularmente chamados de mosquitos-prego.<br><br>III - VERDADEIRO. Os palpos são bem longos e facilmente identificáveis lateralmente à probóscide (aparelho picador).<br><br>IV - FALSO. <i>Anopheles</i> não possui sifão respiratório nas larvas, e por esta razão, posicionam-se de modo que o seu corpo fique paralelo à superfície da água e respiram através de uma estrutura rudimentar de espiráculos localizados no 8º segmento abdominal.<br><br>V – FALSO. Ovos de <i>Anopheles</i> são depositados na lâmina d’água e não na parede dos criadouros, como ocorre em <i>Aedes</i>. Além disso, apresentam estruturas laterais que auxiliam na flutuação do ovo (flutuadores), que mantém seus ovos suspensos na água.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, F",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 5,
                        type: "singleAnswer",
                        enunciate: "O sucesso de ações de controle vetorial está diretamente ligado à qualidade e compromisso que os agentes de saúde possuem, assim como o envolvimento da comunidade. Sobre esse tema podemos afirmar:",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A limpeza das residências é de responsabilidade única do morador, entretanto, os agentes de saúde podem auxiliar dando orientações para impedir o surgimento de criadouros."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "O compromisso do agente de saúde em realizar seu trabalho de forma correta, pode repercutir na qualidade de vida da população."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A maneira de aplicar os tratamentos com larvicidas não deve seguir protocolos, seguindo apenas a intuição do agente."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Projetos de colaboração entre o serviço de saúde e instituições de ensino e pesquisa tem como objetivo melhorar as estratégias de combate aos mosquitos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "O incentivo à participação da comunidade atrapalha muito as ações, diminuindo as chances de sucesso dos programas de controle."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II e IV",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e IV",
                                alternativeHint: "I - VERDADEIRO. A limpeza das residências é de responsabilidade única do morador, entretanto, os agentes de saúde podem auxiliar dando orientações para impedir o surgimento de criadouros.<br><br>II - VERDADEIRO. O compromisso do agente de saúde em realizar seu trabalho de forma correta, pode repercuti na qualidade de vida da população.<br><br>III - FALSO. É preciso sempre seguir os protocolos recomendados, nunca aplicar produtos de forma aleatória.<br><br>IV - VERDADEIRO. Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>V - FALSO. A participação da comunidade sempre a ajuda  muito as ações, aumentando  as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Apenas I",
                                alternativeHint: "I - VERDADEIRO. A limpeza das residências é de responsabilidade única do morador, entretanto, os agentes de saúde podem auxiliar dando orientações para impedir o surgimento de criadouros.<br><br>II - VERDADEIRO. O compromisso do agente de saúde em realizar seu trabalho de forma correta, pode repercuti na qualidade de vida da população.<br><br>III - FALSO. É preciso sempre seguir os protocolos recomendados, nunca aplicar produtos de forma aleatória.<br><br>IV - VERDADEIRO. Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>V - FALSO. A participação da comunidade sempre a ajuda  muito as ações, aumentando  as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "III e IV",
                                alternativeHint: "I - VERDADEIRO. A limpeza das residências é de responsabilidade única do morador, entretanto, os agentes de saúde podem auxiliar dando orientações para impedir o surgimento de criadouros.<br><br>II - VERDADEIRO. O compromisso do agente de saúde em realizar seu trabalho de forma correta, pode repercuti na qualidade de vida da população.<br><br>III - FALSO. É preciso sempre seguir os protocolos recomendados, nunca aplicar produtos de forma aleatória.<br><br>IV - VERDADEIRO. Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>V - FALSO. A participação da comunidade sempre a ajuda  muito as ações, aumentando  as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Todas as alternativas.",
                                alternativeHint: "I - VERDADEIRO. A limpeza das residências é de responsabilidade única do morador, entretanto, os agentes de saúde podem auxiliar dando orientações para impedir o surgimento de criadouros.<br><br>II - VERDADEIRO. O compromisso do agente de saúde em realizar seu trabalho de forma correta, pode repercuti na qualidade de vida da população.<br><br>III - FALSO. É preciso sempre seguir os protocolos recomendados, nunca aplicar produtos de forma aleatória.<br><br>IV - VERDADEIRO. Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>V - FALSO. A participação da comunidade sempre a ajuda  muito as ações, aumentando  as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 6,
                        type: "singleAnswer",
                        enunciate: "Das opções abaixo quais são <u>“absolutamente necessárias”</u> para uma espécie de inseto ser classificada como vetor biológico de um patógeno ao <u>homem</u>?",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Ser hematófaga"
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Ser fêmea"
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Ser susceptível ao Agente Infeccioso (AI)"
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Repassar o AI para o hospedeiro vertebrado"
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Se alimentar, preferencialmente, durante o dia."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, III e IV",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "III e IV",
                                alternativeHint: "I - Verdadeira. É necessário que exista alguma porta de entrada para os patógenos, a picada para sugar o sangue do hospedeiro é geralmente por onde acontece as transmissões.<br><br>II - Falso. Diversos insetos se alimentam de sangue e assim causam doenças ao homem, no caso dos piolhos, pulgas e barbeiros tanto os machos quanto as fêmeas são hematófagos.<br><br>III - Verdadeiro. Para que o inseto se torne vetor é necessário que exista uma suscetibilidade ao agente infeccioso para que o patógeno infecte o homem.<br><br>IV - Verdadeiro. É preciso que o inseto seja suscetível ao Agente Infeccioso para que este se torne um vetor.<br><br>V - Falso. O hábito alimentar seja ele diurno ou noturno não interfere na transmissão de AI, desde que o inseto seja hematófago.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Apenas V",
                                alternativeHint: "I - Verdadeira. É necessário que exista alguma porta de entrada para os patógenos, a picada para sugar o sangue do hospedeiro é geralmente por onde acontece as transmissões.<br><br>II - Falso. Diversos insetos se alimentam de sangue e assim causam doenças ao homem, no caso dos piolhos, pulgas e barbeiros tanto os machos quanto as fêmeas são hematófagos.<br><br>III - Verdadeiro. Para que o inseto se torne vetor é necessário que exista uma suscetibilidade ao agente infeccioso para que o patógeno infecte o homem.<br><br>IV - Verdadeiro. É preciso que o inseto seja suscetível ao Agente Infeccioso para que este se torne um vetor.<br><br>V - Falso. O hábito alimentar seja ele diurno ou noturno não interfere na transmissão de AI, desde que o inseto seja hematófago.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III IV e V",
                                alternativeHint: "I - Verdadeira. É necessário que exista alguma porta de entrada para os patógenos, a picada para sugar o sangue do hospedeiro é geralmente por onde acontece as transmissões.<br><br>II - Falso. Diversos insetos se alimentam de sangue e assim causam doenças ao homem, no caso dos piolhos, pulgas e barbeiros tanto os machos quanto as fêmeas são hematófagos.<br><br>III - Verdadeiro. Para que o inseto se torne vetor é necessário que exista uma suscetibilidade ao agente infeccioso para que o patógeno infecte o homem.<br><br>IV - Verdadeiro. É preciso que o inseto seja suscetível ao Agente Infeccioso para que este se torne um vetor.<br><br>V - Falso. O hábito alimentar seja ele diurno ou noturno não interfere na transmissão de AI, desde que o inseto seja hematófago.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "I e II",
                                alternativeHint: "I - Verdadeira. É necessário que exista alguma porta de entrada para os patógenos, a picada para sugar o sangue do hospedeiro é geralmente por onde acontece as transmissões.<br><br>II - Falso. Diversos insetos se alimentam de sangue e assim causam doenças ao homem, no caso dos piolhos, pulgas e barbeiros tanto os machos quanto as fêmeas são hematófagos.<br><br>III - Verdadeiro. Para que o inseto se torne vetor é necessário que exista uma suscetibilidade ao agente infeccioso para que o patógeno infecte o homem.<br><br>IV - Verdadeiro. É preciso que o inseto seja suscetível ao Agente Infeccioso para que este se torne um vetor.<br><br>V - Falso. O hábito alimentar seja ele diurno ou noturno não interfere na transmissão de AI, desde que o inseto seja hematófago.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 7,
                        type: "singleAnswer",
                        enunciate: "Sobre os aspectos bioecológicos dos <i>culicídeos</i> assinale a sentença correta:",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "São conhecidos como mosquitos, são insetos grandes, cuja hematofagia ocorre tanto para os machos quanto para as fêmeas.",
                                alternativeHint: "Incorreta - porque os mosquitos não são insetos grandes e os seus machos não são hematófagos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Espécies de <i>culicídeos</i> são as principais responsáveis pela transmissão de arbovírus, causadores de doenças ao homem, como dengue, zika e malária.",
                                alternativeHint: "Incorreta - porque a malária não é causada por um arbovírus e sim por um parasita unicelular protozoário chamado <i>Plasmodium</i>.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "As formas jovens, larvas e pupas, são encontradas em ambientes aquáticos, são filtradoras e respiram o oxigênio dissolvido na água, por isso ficam nadando no fundo dos criadouros.",
                                alternativeHint: "Incorreta - porque as formas jovens, larvas e pupas, não respiram o oxigênio dissolvido na água.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Os criadouros das formas jovens dos mosquitos podem ser permanentes ou temporários, naturais ou artificiais, pois o que mais importa é estarem disponíveis no ambiente, conter água e alimento suficientes para o desenvolvimento das larvas e pupas até a emergência dos machos e fêmeas dos mosquitos.",
                                alternativeHint: "CORRETA - Pois os criadouros das formas jovens dos mosquitos podem ser permanentes ou temporários, naturais ou artificiais, pois o que mais importa é estarem disponíveis no ambiente, conter água e alimento suficientes para o desenvolvimento das larvas e pupas até a emergência dos machos e fêmeas dos mosquitos.",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "As fêmeas, na maioria das espécies de mosquitos, são exclusivamente hematófagas, ou seja, alimentam-se do sangue de outros animais.",
                                alternativeHint: "Incorreta - Pois as fêmeas, na maioria das espécies de mosquitos, são hematófagas, mas não se alimentam exclusivamente de sangue, elas se alimentam também de seivas de planta como fonte de carboidrato.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 8,
                        type: "singleAnswer",
                        enunciate: "Considerando os ciclos de transmissão vetorial dos patógenos de doenças humanas, coloque V ou F nas sentenças abaixo.",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Todas as espécies de insetos hematófagos estão relacionadas com a transmissão de patógenos ao homem e outros animais vertebrados."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Competência e capacidade vetorial significam a mesma coisa."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "As formas jovens dos mosquitos também são vetores de arbovírus, por isso a transmissão vertical é a via mais importante para a manutenção dos ciclos endêmicos de transmissão."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Doenças como malária, dengue e chikungunya se propagam rapidamente nas populações humanas porque seus agentes etiológicos também conseguem se diferenciar e/ou multiplicar rapidamente nos seus vetores e hospedeiros."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "<i>Aedes albopictus</i> pode ser considerado um vetor ponte entre os ciclos urbano e silvestre de transmissão da febre amarela."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, F",
                                alternativeHint: "I - Falsa. Nem todas as espécies de insetos hematófagos estão relacionadas com a transmissão de patógenos ao homem e outros animais vertebrados.<br><br>II - Falsa. Porque a Competência vetorial está relacionada prioritariamente a fatores genéticos, resultantes da seleção natural durante o processo de co-evolução entre patógeno e vetores, enquanto a capacidade vetorial está relacionada a fatores próprios do mosquito como a sua relação com o patógeno e com fatores ambientais do local em ele ocorre.<br><br>III - Falsa. Porque as formas jovens dos mosquitos não são vetores de arbovírus pois são aquáticas e se alimentam por filtração de partículas em suspensão na água, além da transmissão vertical não ser a via mais importante para a manutenção dos ciclos endêmicos de transmissão e sim a transmissão horizontal.<br><br>IV - Verdadeira. Doenças como malária, dengue e chikungunya se propagam rapidamente nas populações humanas porque seus agentes etiológicos também conseguem se diferenciar e/ou multiplicar rapidamente nos seus vetores e hospedeiros.<br><br>V - Verdadeira. <i>Aedes albopictus</i> pode ser considerado um vetor ponte entre os ciclos urbano e silvestre de transmissão da febre amarela.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "V, F, V, F, V",
                                alternativeHint: "I - Falsa. Nem todas as espécies de insetos hematófagos estão relacionadas com a transmissão de patógenos ao homem e outros animais vertebrados.<br><br>II - Falsa. Porque a Competência vetorial está relacionada prioritariamente a fatores genéticos, resultantes da seleção natural durante o processo de co-evolução entre patógeno e vetores, enquanto a capacidade vetorial está relacionada a fatores próprios do mosquito como a sua relação com o patógeno e com fatores ambientais do local em ele ocorre.<br><br>III - Falsa. Porque as formas jovens dos mosquitos não são vetores de arbovírus pois são aquáticas e se alimentam por filtração de partículas em suspensão na água, além da transmissão vertical não ser a via mais importante para a manutenção dos ciclos endêmicos de transmissão e sim a transmissão horizontal.<br><br>IV - Verdadeira. Doenças como malária, dengue e chikungunya se propagam rapidamente nas populações humanas porque seus agentes etiológicos também conseguem se diferenciar e/ou multiplicar rapidamente nos seus vetores e hospedeiros.<br><br>V - Verdadeira. <i>Aedes albopictus</i> pode ser considerado um vetor ponte entre os ciclos urbano e silvestre de transmissão da febre amarela.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, V, V",
                                alternativeHint: "I - Falsa. Nem todas as espécies de insetos hematófagos estão relacionadas com a transmissão de patógenos ao homem e outros animais vertebrados.<br><br>II - Falsa. Porque a Competência vetorial está relacionada prioritariamente a fatores genéticos, resultantes da seleção natural durante o processo de co-evolução entre patógeno e vetores, enquanto a capacidade vetorial está relacionada a fatores próprios do mosquito como a sua relação com o patógeno e com fatores ambientais do local em ele ocorre.<br><br>III - Falsa. Porque as formas jovens dos mosquitos não são vetores de arbovírus pois são aquáticas e se alimentam por filtração de partículas em suspensão na água, além da transmissão vertical não ser a via mais importante para a manutenção dos ciclos endêmicos de transmissão e sim a transmissão horizontal.<br><br>IV - Verdadeira. Doenças como malária, dengue e chikungunya se propagam rapidamente nas populações humanas porque seus agentes etiológicos também conseguem se diferenciar e/ou multiplicar rapidamente nos seus vetores e hospedeiros.<br><br>V - Verdadeira. <i>Aedes albopictus</i> pode ser considerado um vetor ponte entre os ciclos urbano e silvestre de transmissão da febre amarela.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, F, F",
                                alternativeHint: "I - Falsa. Nem todas as espécies de insetos hematófagos estão relacionadas com a transmissão de patógenos ao homem e outros animais vertebrados.<br><br>II - Falsa. Porque a Competência vetorial está relacionada prioritariamente a fatores genéticos, resultantes da seleção natural durante o processo de co-evolução entre patógeno e vetores, enquanto a capacidade vetorial está relacionada a fatores próprios do mosquito como a sua relação com o patógeno e com fatores ambientais do local em ele ocorre.<br><br>III - Falsa. Porque as formas jovens dos mosquitos não são vetores de arbovírus pois são aquáticas e se alimentam por filtração de partículas em suspensão na água, além da transmissão vertical não ser a via mais importante para a manutenção dos ciclos endêmicos de transmissão e sim a transmissão horizontal.<br><br>IV - Verdadeira. Doenças como malária, dengue e chikungunya se propagam rapidamente nas populações humanas porque seus agentes etiológicos também conseguem se diferenciar e/ou multiplicar rapidamente nos seus vetores e hospedeiros.<br><br>V - Verdadeira. <i>Aedes albopictus</i> pode ser considerado um vetor ponte entre os ciclos urbano e silvestre de transmissão da febre amarela.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 9,
                        type: "singleAnswer",
                        enunciate: "Assinale o que é <u>correto</u> afirmar sobre o desenvolvimento das larvas de mosquitos:",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Elas permanecem fixadas a diferentes substratos nos criadouros e por isso não são móveis;",
                                alternativeHint: "Incorreta - Elas não permanecem fixadas a diferentes substratos nos criadouros e por isso são móveis;",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Alimentam-se por filtração de microorganismos e detritos de origem vegetal e animal em suspensão na água;",
                                alternativeHint: "Correta - Alimentam-se por filtração de microorganismos e detritos de origem vegetal e animal em suspensão na água;",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Na medida em que passam de um estádio para o outro vão adquirindo características que permitem diferenciar machos e fêmeas;",
                                alternativeHint: "Incorreta - A variação de estádios não permite diferenciar características machos e fêmeas;",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Todas possuem sifão respiratório, uma vez que não respiram o oxigênio dissolvido na água.",
                                alternativeHint: "Incorreta - Nem todas as larvas de mosquitos possuem sifão respiratório.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Todas são iguais, por isso, a identificação da espécie só pode ser feita com os mosquitos adultos.",
                                alternativeHint: "Incorreto - Nem todas são iguais, por isso, a identificação da espécie só pode ser feita com os mosquitos adultos.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 10,
                        type: "singleAnswer",
                        enunciate: "Assinale a alternativa que apresenta a sequência correta de doenças com ciclos de transmissão vetorial por mosquitos.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Hepatite, Filariose linfática, malária e miíases;",
                                alternativeHint: "Incorreta - Apenas Filariose linfática e malária são transmitidas por mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Chikungunya, pediculose, Febre Amarela, AIDS;",
                                alternativeHint: "Incorreta - Apenas Chikungunya e Febre Amarela são transmitidas por mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Leishmanioses, dengue, malária, Encefalite do Nilo Ocidental;",
                                alternativeHint: "Incorreta - Apenas dengue, malária, Encefalite do Nilo Ocidental.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Peste, Sífilis, Doença do sono Doença de Chagas;",
                                alternativeHint: "Incorreta - Nenhumas dessas doenças são transmitidas por mosquitos (Peste, Sífilis, Doença do sono Doença de Chagas)",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Filariose linfática, malária, Zika e febre amarela.",
                                alternativeHint: "Correta - Todas essas doenças são transmitidas por mosquitos (Filariose linfática, malária, zika e febre amarela).",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 1,
                        type: "singleAnswer",
                        enunciate: "O filo dos <i>Arthropoda</i> reúne uma grande quantidade de animais, como camarões, aranhas, escorpiões, besouros e mosquitos. É um filo bastante diverso e que apresenta importância ecológica fundamental em diversos ecossistemas terrestres, dulcícolas e marinhos. Os insetos, animais pertencentes à Classe <i>Insecta</i>, apresentam grande importância econômica, médica e veterinária. Sobre os animais da Classe <i>Insecta</i>, assinale a sentença <b>INCORRETA</b>:",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Os insetos são animais que apresentam o corpo dividido, na sua grande maioria, em cabeça, tórax e abdômen. Possuem peças bucais, antenas e olhos compostos na região da cabeça. No tórax encontram-se as pernas e asas.",
                                alternativeHint: "CORRETA. Os insetos desenvolveram características anatômicas que possibilitaram a eles a presença de um corpo adaptado para a condição de voo, apresentando asas e corpo leve. Boa parte dos insetos apresentam dois pares de asas. Os mosquito e moscas, pertencentes à Ordem <i>Díptera</i>, apresentam apenas um par de asas, pois o segundo par atrofiou na evolução do grupo, dando origem aos halteres.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Alguns insetos estão envolvidos em ciclos de transmissão de patógenos que acometem humanos, especialmente na região tropical do globo. Podemos citar doenças transmitidas por mosquitos (dengue, Zika, chikungunya e febre amarela, malária, filariose linfática), barbeiros (doença de Chagas) e pulgas (peste bubônica).",
                                alternativeHint: "CORRETA. Devido ao hábito de se alimentar de sangue, alguns insetos tornaram-se vetores de doenças causadas por vírus, nematódeos, protozoários e bactérias. O controle de espécies de insetos que estão envolvidas nos ciclos de transmissão de doenças é fundamental para a saúde pública.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "As antenas, órgãos sensoriais fundamentais para os insetos, possuem minúsculas sensilas (pelos) que auxiliam na captação de moléculas odoríferas e outros estímulos do meio. Em algumas espécies de insetos, como por exemplo, em mosquitos, é possível identificar machos e fêmeas com a observação das antenas. Nos machos, o número de pelos nas antenas é bem maior que nas fêmeas.",
                                alternativeHint: "CORRETA. É a partir das antenas que os insetos identificam melhores fontes alimentares, melhores locais para deposição dos ovos, além de afastarem-se de locais que possuem moléculas odoríferas que sinalizam perigo.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Os insetos utilizam espiráculos para trocas gasosas (respiração). O gás oxigênio entra no corpo do animal e o gás carbônico sai.",
                                alternativeHint: "CORRETA. Os espiráculos são aberturas localizadas lateralmente no tórax e abdômen da maioria dos insetos, apresentando um par por segmento.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Os insetos passam por processos de maturação para saírem do estado larval para adulto, o que pode acontecer de diferentes formas. Os insetos ametábolos chegam à fase adulta com poucas mudanças estruturais, os hemimetábolos passam pelas mudanças de forma gradual pelo estado de ninfa para o adulto, os holometábolos têm mudanças radicais do estado larval para o adulto. Os mosquitos são classificados como insetos com desenvolvimento hemimetábolo, pois poucas mudanças ocorrem no desenvolvimento das fases larval para a fase adulta.",
                                alternativeHint: "INCORRETA. Parabéns! Os mosquitos são insetos que possuem o desenvolvimento holometábolo, ou seja, realizam metamorfose completa. Diversas alterações estruturais ocorrem durante o desenvolvimento dos mosquitos. Do ovo saem larvas L1, minúsculas e que vão realizar ecdise (troca do exoesqueleto) ao desenvolverem-se para larvas L2, L3 e L4. Após chegar ao estádio L4, as larvas sofrem mais uma ecdise e transformam-se em pupa. A pupa sofre intensa metamorfose até que o inseto esteja pronto para emergir, dando origem ao inseto adulto.",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 2,
                        type: "singleAnswer",
                        enunciate: "Sobre os mosquitos <i>Aedes aegypti</i> e <i>Aedes albopictus</i>, julgue as alternativas abaixo entre VERDADEIRAS e FALSAS e assinale a sequencia correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "As espécies <i>Aedes aegypti</i> e <i>Aedes albopictus</i> depositam seus ovos de forma individualizada, geralmente na parede dos criadouros."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A característica de quiescência, presente em ovos de <i>Aedes aegypti</i>, é caracterizada pela interrupção do processo de maturação embrionária devido à redução drástica da umidade relativa. Isso permite que estes insetos possam resistir a importantes momentos de seca nos ambientes."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A dispersão dos ovos de <i>Aedes aegypti</i> é potencializada pelo comportamento de oviposição aos saltos, que consiste na habilidade que as fêmeas possuem em fracionar a deposição do conjunto de ovos de um ciclo reprodutivo em diversos criadouros."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "<i>Aedes albopictus</i> é uma espécie que não depende de grande concentração humana para sua ocorrência como ocorre em <i>Aedes aegypti</i>, uma vez que populações de <i>Aedes albopictus</i> são encontradas em regiões pouco populosas e próximas ao ambiente de mata."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Vários estudos comprovam a susceptibilidade de <i>Aedes albopictus</i> a diferentes arbovírus, entre eles o vírus Dengue, o vírus da Febre amarela e vírus Chikungunya. Surtos de transmissão do vírus Dengue atribuídos a essa espécie foram registrados apenas em regiões do continente asiático."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I - VERDADEIRO. Diferentemente dos mosquitos do gênero <i>Culex</i>, que depositam seus ovos agrupados em jangadas, <i>Aedes aegypti</i> e <i>Aedes albopictus</i> depositam seus ovos individualizados e com uma “substância adesiva”, que fixa os ovos na parede dos criadouros.<br><br>II - VERDADEIRO. Além disso, essa característica permite que ovos quiescentes no ambiente oportunizem o ressurgimento de mosquitos em áreas anteriormente tratadas após períodos de chuvas e eclosão das larvas.<br><br>III - VERDADEIRO. Esse tipo de dispersão, com a deposição de ovos em diversos criadouros, aumenta muito as chances de que as larvas/pupas cheguem à fase adulta, colaborando para a densidade populacional do inseto.<br><br>IV - VERDADEIRO. <i>Aedes albopictus</i> é um mosquito mais relacionado a ambientes rurais, próximo a matas ou cultivos agrícolas com grande quantidade de água, como ocorre em plantações de arroz.  Por estar mais relacionado a esses ambientes, sua densidade populacional é menor quando comparado a <i>Aedes aegypti</i>.<br><br>V - VERDADEIRO. No Brasil, <i>Aedes albopictus</i> ainda não foi diretamente incriminado como vetor de nenhuma das arboviroses existentes, mesmo algumas populações tendo apresentado em laboratório competência vetorial para a transmissão.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "F, V, F, V, F",
                                alternativeHint: "I - VERDADEIRO. Diferentemente dos mosquitos do gênero <i>Culex</i>, que depositam seus ovos agrupados em jangadas, <i>Aedes aegypti</i> e <i>Aedes albopictus</i> depositam seus ovos individualizados e com uma “substância adesiva”, que fixa os ovos na parede dos criadouros.<br><br>II - VERDADEIRO. Além disso, essa característica permite que ovos quiescentes no ambiente oportunizem o ressurgimento de mosquitos em áreas anteriormente tratadas após períodos de chuvas e eclosão das larvas.<br><br>III - VERDADEIRO. Esse tipo de dispersão, com a deposição de ovos em diversos criadouros, aumenta muito as chances de que as larvas/pupas cheguem à fase adulta, colaborando para a densidade populacional do inseto.<br><br>IV - VERDADEIRO. <i>Aedes albopictus</i> é um mosquito mais relacionado a ambientes rurais, próximo a matas ou cultivos agrícolas com grande quantidade de água, como ocorre em plantações de arroz.  Por estar mais relacionado a esses ambientes, sua densidade populacional é menor quando comparado a <i>Aedes aegypti</i>.<br><br>V - VERDADEIRO. No Brasil, <i>Aedes albopictus</i> ainda não foi diretamente incriminado como vetor de nenhuma das arboviroses existentes, mesmo algumas populações tendo apresentado em laboratório competência vetorial para a transmissão.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, F, V",
                                alternativeHint: "I - VERDADEIRO. Diferentemente dos mosquitos do gênero <i>Culex</i>, que depositam seus ovos agrupados em jangadas, <i>Aedes aegypti</i> e <i>Aedes albopictus</i> depositam seus ovos individualizados e com uma “substância adesiva”, que fixa os ovos na parede dos criadouros.<br><br>II - VERDADEIRO. Além disso, essa característica permite que ovos quiescentes no ambiente oportunizem o ressurgimento de mosquitos em áreas anteriormente tratadas após períodos de chuvas e eclosão das larvas.<br><br>III - VERDADEIRO. Esse tipo de dispersão, com a deposição de ovos em diversos criadouros, aumenta muito as chances de que as larvas/pupas cheguem à fase adulta, colaborando para a densidade populacional do inseto.<br><br>IV - VERDADEIRO. <i>Aedes albopictus</i> é um mosquito mais relacionado a ambientes rurais, próximo a matas ou cultivos agrícolas com grande quantidade de água, como ocorre em plantações de arroz.  Por estar mais relacionado a esses ambientes, sua densidade populacional é menor quando comparado a <i>Aedes aegypti</i>.<br><br>V - VERDADEIRO. No Brasil, <i>Aedes albopictus</i> ainda não foi diretamente incriminado como vetor de nenhuma das arboviroses existentes, mesmo algumas populações tendo apresentado em laboratório competência vetorial para a transmissão.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, V, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, V, V",
                                alternativeHint: "I - VERDADEIRO. Diferentemente dos mosquitos do gênero <i>Culex</i>, que depositam seus ovos agrupados em jangadas, <i>Aedes aegypti</i> e <i>Aedes albopictus</i> depositam seus ovos individualizados e com uma “substância adesiva”, que fixa os ovos na parede dos criadouros.<br><br>II - VERDADEIRO. Além disso, essa característica permite que ovos quiescentes no ambiente oportunizem o ressurgimento de mosquitos em áreas anteriormente tratadas após períodos de chuvas e eclosão das larvas.<br><br>III - VERDADEIRO. Esse tipo de dispersão, com a deposição de ovos em diversos criadouros, aumenta muito as chances de que as larvas/pupas cheguem à fase adulta, colaborando para a densidade populacional do inseto.<br><br>IV - VERDADEIRO. <i>Aedes albopictus</i> é um mosquito mais relacionado a ambientes rurais, próximo a matas ou cultivos agrícolas com grande quantidade de água, como ocorre em plantações de arroz.  Por estar mais relacionado a esses ambientes, sua densidade populacional é menor quando comparado a <i>Aedes aegypti</i>.<br><br>V - VERDADEIRO. No Brasil, <i>Aedes albopictus</i> ainda não foi diretamente incriminado como vetor de nenhuma das arboviroses existentes, mesmo algumas populações tendo apresentado em laboratório competência vetorial para a transmissão.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 3,
                        type: "singleAnswer",
                        enunciate: "Mosquitos da espécie <i>Culex quinquefasciatus</i> são popularmente conhecidos como muriçocas ou carapanãs e apresentam importância médico-veterinária como potencial vetor de diversos patógenos. Sobre essa espécie, assinale a alternativa INCORRETA.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "As larvas de <i>Culex quinquefasciatus</i> possuem um sifão longo, respiram diagonalmente na superfície da água e são filtrador-raspadoras.",
                                alternativeHint: "CORRETA. As larvas de <i>Culex</i> e de diversos outros mosquitos utilizam as cerdas orais para alimentação da matéria orgânica disponível nos criadouros.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Dependendo da temperatura do ambiente e disponibilidade de alimento, o ciclo de vida do mosquito <i>Culex quinquefasciatus</i> pode ser curto ou mais longo, durando em média uma semana do ovo a pupa. Após emergir, a fêmea vive mais, podendo chegar a dois meses.",
                                alternativeHint: "CORRETA. A longevidade das fêmeas é maior que a dos machos, uma vez que as fêmeas possuem uma quantidade maior de reservas energéticas e geralmente, são mais robustas.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "No ambiente domiciliar, <i>Culex quinquefasciatus</i> buscam esconderijos ao abrigo da luz, ficando pousados paralelamente em superfícies para repouso durante o dia. A noite, as fêmeas sugam o sangue humano e de outros animais dentro da habitação ou no peridomicílio.",
                                alternativeHint: "CORRETA. Embora se alimente de outros animais, <i>Culex quiquefasciatus</i> possui predileção por exercer a hematofagia em humanos, preferencialmente dentro das residências.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "<i>Culex quinquefasciatus</i> tiram proveito das modificações humanas no ambiente peridomiciliar e encontram condições favoráveis para postura, portanto não necessitam de realizar grandes deslocamentos.",
                                alternativeHint: "CORRETA. Geralmente os mosquitos <i>Culex</i> preferem ficar próximos à fonte de alimentação sanguínea e locais que são potenciais criadouros, não se deslocando tanto. Porém, alguns estudos já registraram deslocamentos que chegam a 10 km.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "<i>Culex quinquefasciatus</i> é uma espécie que possui coloração preta metalizada, com manchas claras nas pernas e poucas cerdas nas asas.",
                                alternativeHint: "INCORRETA. Muito bem! <i>Culex quinquefasciatus</i> é um mosquito de cor marrom e sem manchas claras nas pernas. Suas assas possuem uma quantidade grande de escamas e suas as larvas preferem locais com grande quantidade de matéria orgânica.",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 4,
                        type: "singleAnswer",
                        enunciate: "A região amazônica brasileira continua apresentando importantes índices de infecção por <i>Plasmodium</i>, parasita causador da malária. Essa parasitose tem o mosquito do gênero <i>Anopheles</i> envolvido como vetor na transmissão. Em relação a esse vetor e suas características biológicas e ecológicas, analise as sentenças abaixo e identifique a alternativa <b>INCORRETA</b>.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Considerado o mosquito mais conhecido entre os anofelinos, o gênero <i>Anopheles</i> é o único que possui espécies de importância para a saúde pública por estarem envolvidas nos ciclos de transmissão tanto da malária humana, filariose bancroftiana e algumas arboviroses.",
                                alternativeHint: "CORRETA. Os mosquitos do gênero <i>Anopheles</i> estão envolvidos em uma grande quantidade de ciclos de transmissão de patógenos. As medidas de controle desse inseto estão voltadas basicamente para ações de educação em saúde e medidas de prevenção destacando o controle do vetor. É orientado à população que use telas de proteção em janelas e portas, evite lugares com grande potencial para criadouros no fim da tarde e amanhecer, utilização de camisas de mangas e calças compridas, além do o uso de repelentes.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "<i>Plasmodium falciparum, P. vivax, P. maláriae e P. ovale</i> são os protozoários que causam a malária humana e o fato deles conseguirem se reproduzir sexualmente no corpo do mosquito faz dele não apenas vetor, mas também hospedeiro definitivo destes parasitas.",
                                alternativeHint: "CORRETA. <i>Plasmodium</i>, em humanos, se reproduz de forma assexuada nos eritrócitos (glóbulos vermelhos) e hepatócitos (células do fígado). Diante disso, o hospedeiro humano é o hospedeiro intermediário no ciclo. Já o mosquito <i>Anopheles</i>, por abrigar a fase sexuada do ciclo, é o hospedeiro definitivo.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Os ovos de anofelinos não resistentes à dessecação e são postos diretamente na água. Além disso, como são colocados isolados, os flutuadores são fundamentais para que eles não afundem nos criadouros que podem ser de pequenas poças a grandes cursos d’água como curvas de rios com pouca correnteza.",
                                alternativeHint: "CORRETA. Os ovos de <i>Anopheles</i> são sensíveis e não resistem longos períodos sem água, como ocorre em <i>Aedes</i>.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "<i>Anopheles darlingi</i> é o principal vetor da malária no Brasil e apresenta hábito crepuscular e noturno, são mosquitos de tamanho que varia de pequeno a médio porte, e habitam ambientes silvestres, semissilvestres, rurais e até mesmo regiões próximas ao litoral.",
                                alternativeHint: "CORRETA. Além disso, quando comparamos <i>Anopheles darlingi</i> a outras espécies de anofelinos, essa espécie é uma das espécies que mais se beneficia das modificações causadas pelo homem no ambiente.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Os anofelinos possuem um corportamteno de pouso muito similar aos mosquitos do gênero <i>Aedes</i>. Além disso, as larvas de <i>Anopheles</i> possuem um sifão longo e posicionam-se perpendicularmente à superfície da água para a respiração.",
                                alternativeHint: "INCORRETA. Parabéns! Os anofelinos são popularmente conhecidos por mosquitos-prego, pois repousa de modo quase perpendicular à superfície, semelhante a um prego inclinado. Esse comportamento é bem diferente de mosquitos do gênero <i>Aedes</i>. Outra importante incorreção nessa sentença é que, em larvas de <i>Anopheles</i>, não observamos o sifão respiratório. As larvas respiram horizontalmente à superfície através de uma estrutura rudimentar de espiráculos localizados no 8º segmento abdominal.",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 5,
                        type: "singleAnswer",
                        enunciate: "As ações com o envolvimento da população, junto com os agentes de saúde podem nos dá um retorno bastante significativo no controle de mosquitos vetores.",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A maneira de aplicar os tratamentos com larvicidas deve seguir protocolos, seguindo todas as orientações do produto contidas nos rótulos."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A limpeza das residências não é de responsabilidade única do morador."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A qualidade do trabalho desempenhada pelo agente de saúde em campo não repercute na qualidade de vida da população."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Projetos de colaboração entre o serviço de saúde e instituições de ensino e pesquisa, tem como objetivo melhorar as estratégias de combate aos mosquitos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, IV e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e IV",
                                alternativeHint: "I - VERDADEIRA. A maneira de aplicar os tratamentos com larvicidas deve seguir protocolos, seguindo todas as orientações do produto contidas nos rótulos.<br><br>II - FALSA - A limpeza das residências  é de responsabilidade única do morador.<br><br>III - FALSA A qualidade do trabalho desempenhada pelo agente de saúde em campo, pode repercutir diretamente na qualidade de vida da população.<br><br>IV - VERDADEIRA Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.<br><br>V - VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Apenas I",
                                alternativeHint: "I - VERDADEIRA. A maneira de aplicar os tratamentos com larvicidas deve seguir protocolos, seguindo todas as orientações do produto contidas nos rótulos.<br><br>II - FALSA - A limpeza das residências  é de responsabilidade única do morador.<br><br>III - FALSA A qualidade do trabalho desempenhada pelo agente de saúde em campo, pode repercutir diretamente na qualidade de vida da população.<br><br>IV - VERDADEIRA Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.<br><br>V - VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "III e IV",
                                alternativeHint: "I - VERDADEIRA. A maneira de aplicar os tratamentos com larvicidas deve seguir protocolos, seguindo todas as orientações do produto contidas nos rótulos.<br><br>II - FALSA - A limpeza das residências  é de responsabilidade única do morador.<br><br>III - FALSA A qualidade do trabalho desempenhada pelo agente de saúde em campo, pode repercutir diretamente na qualidade de vida da população.<br><br>IV - VERDADEIRA Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.<br><br>V - VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "II e IV",
                                alternativeHint: "I - VERDADEIRA. A maneira de aplicar os tratamentos com larvicidas deve seguir protocolos, seguindo todas as orientações do produto contidas nos rótulos.<br><br>II - FALSA - A limpeza das residências  é de responsabilidade única do morador.<br><br>III - FALSA A qualidade do trabalho desempenhada pelo agente de saúde em campo, pode repercutir diretamente na qualidade de vida da população.<br><br>IV - VERDADEIRA Projetos de colaboração entre o serviço de saúde e instituições de ensino, tem como objetivo melhorar as estratégias de combate aos mosquitos.<br><br>VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.<br><br>V - VERDADEIRA A participação da comunidade auxilia muito as ações de controle de mosquitos, pois aumenta as chances de sucesso dos programas de controle.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 6,
                        type: "singleAnswer",
                        enunciate: "Das ordens de insetos quais as de maior importância para a saúde pública?",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "<i>Dermaptera</i>, <i>Hymenoptera</i>, <i>Diptera</i> e <i>Isoptera</i>;",
                                alternativeHint: "INCORRETA - Os insetos das ordens: <i>Dermapteras</i> (tesourinhas); <i>Hymenópteras</i> (vespas, abelhas e formigas) e <i>Isopteras</i> (cupins) não se alimentam de sangue, portanto não apresentam importância na saúde pública.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "<i>Diptera</i>, <i>hemíptera</i>, <i>siphonaptera</i> e <i>Odonata</i>;",
                                alternativeHint: "INCORRETA - Insetos da ordem <i>Odonata</i> (libélula) não se alimentam de sangue, portanto não apresentam importância na saúde pública.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "<i>Coleoptera</i>, <i>Lepdoptera</i>, <i>Siphonaptera</i> e <i>Hemiptera</i>;",
                                alternativeHint: "INCORRETA - Insetos das ordens <i>Coleoptera</i> e <i>Lepdoptera</i> não se alimentam de sangue, portanto não apresentam importância na saúde pública.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "<i>Orthoptera</i>, <i>Blattodea</i>, <i>Odonata</i> e <i>Diptera</i>;",
                                alternativeHint: "INCORRETA - Os insetos das ordens <i>Orthoptera</i> (gafanhoto), <i>Odonata</i> (libélila) e <i>Blattodea</i> (barata) não são hematófagas, portanto não são vetoras de AI através da transmissão biológica, apesar de indivíduos da ordem e <i>Blattodea</i> ter cacapidade de realizar a transmissão mecânica de AI.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "<i>Díptera</i>, <i>Hemíptera</i>, <i>Siphonaptera</i>, <i>Psocodea</i>;",
                                alternativeHint: "CORRETA - Os insetos das ordens <i>Diptera</i>, <i>Hemíptera</i>, <i>Siphonaptera</i>, <i>Psocodea</i> apresentam grande importância para a saúde pública, pois atuam como vetores de vários patógenos",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 7,
                        type: "singleAnswer",
                        enunciate: "Em relação ao <i>Culex quinquefasciatus</i> é certo dizer que:",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "É uma espécie de anofelino, domiciliada, presente apenas nas áreas urbanas."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Suas larvas filtradoras são encontradas em criadouros aquáticos ao nível do solo, com elevada carga de matéria orgânica."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Pertence a um complexo de espécies."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Na maioria dos locais em que ocorre prefere se alimentar no homem do que em outros animais domésticos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Após cada alimentação sanguínea colocar um grupo de ovos (jangada)."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Apenas I",
                                alternativeHint: "I - Falsa. Porque o mosquito <i>Culex quinquefasciatus</i> é uma espécie de culicídeo do gênero <i>Culex</i>, e não de anofelino.<br><br>II - Verdadeira. As larvas de <i>Culex quinquefascaitus</i> são filtradoras e encontradas em criadouros aquáticos ao nível do solo, com elevada carga de matéria orgânica.<br><br>III - Verdadeira. Pertence a um complexo de espécies, ou seja, existem várias espécies incluídas nessa mesma definição taxonomica.<br><br>IV - Verdadeira. A preferência alimentar do <i>Culex quinquefasciatus</i> é o homem e não em outros animais domésticos.<br><br>V - Verdadeira. Após cada alimentação sanguínea a fêmea coloca um grupo de ovos (jangada).",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "II, III, IV e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "III e IV",
                                alternativeHint: "I - Falsa. Porque o mosquito <i>Culex quinquefasciatus</i> é uma espécie de culicídeo do gênero <i>Culex</i>, e não de anofelino.<br><br>II - Verdadeira. As larvas de <i>Culex quinquefascaitus</i> são filtradoras e encontradas em criadouros aquáticos ao nível do solo, com elevada carga de matéria orgânica.<br><br>III - Verdadeira. Pertence a um complexo de espécies, ou seja, existem várias espécies incluídas nessa mesma definição taxonomica.<br><br>IV - Verdadeira. A preferência alimentar do <i>Culex quinquefasciatus</i> é o homem e não em outros animais domésticos.<br><br>V - Verdadeira. Após cada alimentação sanguínea a fêmea coloca um grupo de ovos (jangada).",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "I, II e IV",
                                alternativeHint: "I - Falsa. Porque o mosquito <i>Culex quinquefasciatus</i> é uma espécie de culicídeo do gênero <i>Culex</i>, e não de anofelino.<br><br>II - Verdadeira. As larvas de <i>Culex quinquefascaitus</i> são filtradoras e encontradas em criadouros aquáticos ao nível do solo, com elevada carga de matéria orgânica.<br><br>III - Verdadeira. Pertence a um complexo de espécies, ou seja, existem várias espécies incluídas nessa mesma definição taxonomica.<br><br>IV - Verdadeira. A preferência alimentar do <i>Culex quinquefasciatus</i> é o homem e não em outros animais domésticos.<br><br>V - Verdadeira. Após cada alimentação sanguínea a fêmea coloca um grupo de ovos (jangada).",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "IV e V",
                                alternativeHint: "I - Falsa. Porque o mosquito <i>Culex quinquefasciatus</i> é uma espécie de culicídeo do gênero <i>Culex</i>, e não de anofelino.<br><br>II - Verdadeira. As larvas de <i>Culex quinquefascaitus</i> são filtradoras e encontradas em criadouros aquáticos ao nível do solo, com elevada carga de matéria orgânica.<br><br>III - Verdadeira. Pertence a um complexo de espécies, ou seja, existem várias espécies incluídas nessa mesma definição taxonomica.<br><br>IV - Verdadeira. A preferência alimentar do <i>Culex quinquefasciatus</i> é o homem e não em outros animais domésticos.<br><br>V - Verdadeira. Após cada alimentação sanguínea a fêmea coloca um grupo de ovos (jangada).",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 8,
                        type: "singleAnswer",
                        enunciate: "Entre os fatores ambientais e biológicos que contribuem para a transmissão de doenças vetoradas por <i>culicídeos</i> (mosquitos), assinale o que for <u>correto afirmar</u>.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Para espécies invasoras, como <i>Ae. aegypti e Ae. albopictus</i>, a principal forma de dispersão é através de ovos quiescentes levados, passivamente, de um local para outro pelo próprio homem."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Alterações no ambiente podem favorecer a circulação de mais de uma espécie vetora."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Variáveis ambientais, como temperatura e umidade, podem influenciar a duração do período de incubação extrínseco dos arbovírus e a distribuição das espécies vetoras."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Coleções d‘água como canais, córregos, charcos e fossas sépticas geralmente não interferem na densidade populacional de mosquitos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Os parasitos da malária podem ser transmitidos por espécies dos gêneros <i>Anopheles</i>, <i>Aedes</i> e <i>Culex</i>."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III, IV e V",
                                alternativeHint: "I - Correta. Para espécies <i>Ae. aegypti e Ae. albopictus</i>, a principal forma de dispersão é através de ovos quiescentes levados, passivamente, de um local para outro pelo próprio homem.<br><br>II - Correta. Alterações no ambiente, como o aumento da precipitação ou o número de criadouros podem favorecer a circulação de mais de uma espécie vetora.<br><br>III - Correta. Variáveis ambientais, como temperatura e umidade, podem influenciar a duração do período de incubação extrínseco dos arbovírus e a distribuição das espécies vetoras.<br><br>IV - Incorreta. Córregos, charcos e fossas sépticas são criadouros preferenciais do mosquito <i>Cx. quinquefasciatus</i>. E devido ao fato de serem coleções d’água ricas em carga de matéria favorece a proliferação desse mosquito, aumentando assim sua densidade populacional.<br><br>V - Incorreta. O agente causador da malária não é um parasito e sim um protozoário que pode ser transmitido por mosquitos dos gêneros <i>Anopheles, e Culex</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "II e III",
                                alternativeHint: "I - Correta. Para espécies <i>Ae. aegypti e Ae. albopictus</i>, a principal forma de dispersão é através de ovos quiescentes levados, passivamente, de um local para outro pelo próprio homem.<br><br>II - Correta. Alterações no ambiente, como o aumento da precipitação ou o número de criadouros podem favorecer a circulação de mais de uma espécie vetora.<br><br>III - Correta. Variáveis ambientais, como temperatura e umidade, podem influenciar a duração do período de incubação extrínseco dos arbovírus e a distribuição das espécies vetoras.<br><br>IV - Incorreta. Córregos, charcos e fossas sépticas são criadouros preferenciais do mosquito <i>Cx. quinquefasciatus</i>. E devido ao fato de serem coleções d’água ricas em carga de matéria favorece a proliferação desse mosquito, aumentando assim sua densidade populacional.<br><br>V - Incorreta. O agente causador da malária não é um parasito e sim um protozoário que pode ser transmitido por mosquitos dos gêneros <i>Anopheles, e Culex</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "I e IV",
                                alternativeHint: "I - Correta. Para espécies <i>Ae. aegypti e Ae. albopictus</i>, a principal forma de dispersão é através de ovos quiescentes levados, passivamente, de um local para outro pelo próprio homem.<br><br>II - Correta. Alterações no ambiente, como o aumento da precipitação ou o número de criadouros podem favorecer a circulação de mais de uma espécie vetora.<br><br>III - Correta. Variáveis ambientais, como temperatura e umidade, podem influenciar a duração do período de incubação extrínseco dos arbovírus e a distribuição das espécies vetoras.<br><br>IV - Incorreta. Córregos, charcos e fossas sépticas são criadouros preferenciais do mosquito <i>Cx. quinquefasciatus</i>. E devido ao fato de serem coleções d’água ricas em carga de matéria favorece a proliferação desse mosquito, aumentando assim sua densidade populacional.<br><br>V - Incorreta. O agente causador da malária não é um parasito e sim um protozoário que pode ser transmitido por mosquitos dos gêneros <i>Anopheles, e Culex</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "I, II e III",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "apenas IV",
                                alternativeHint: "I - Correta. Para espécies <i>Ae. aegypti e Ae. albopictus</i>, a principal forma de dispersão é através de ovos quiescentes levados, passivamente, de um local para outro pelo próprio homem.<br><br>II - Correta. Alterações no ambiente, como o aumento da precipitação ou o número de criadouros podem favorecer a circulação de mais de uma espécie vetora.<br><br>III - Correta. Variáveis ambientais, como temperatura e umidade, podem influenciar a duração do período de incubação extrínseco dos arbovírus e a distribuição das espécies vetoras.<br><br>IV - Incorreta. Córregos, charcos e fossas sépticas são criadouros preferenciais do mosquito <i>Cx. quinquefasciatus</i>. E devido ao fato de serem coleções d’água ricas em carga de matéria favorece a proliferação desse mosquito, aumentando assim sua densidade populacional.<br><br>V - Incorreta. O agente causador da malária não é um parasito e sim um protozoário que pode ser transmitido por mosquitos dos gêneros <i>Anopheles, e Culex</i>.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 9,
                        type: "singleAnswer",
                        enunciate: "No que se refere ao comportamento de alimentação sanguínea de mosquitos é <u>correto afirmar que</u>:",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Machos e fêmeas se alimentam de sangue;"
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "As fêmeas precisam de sangue para produzir ovos e todas as espécies de mosquitos colocam seus ovos de forma agrupada nos criadouros;"
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A preferência pelo hospedeiro humano ou a antropofilia é comum entre os mosquitos, sobretudo para as espécies que ocorrem no ambiente urbano, como por exemplo, <i>Ae. aegypti e Cx. quinquefasciatus</i>."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "A maioria das espécies de anofelinos é silvestre, e por isso, preferem se alimentar em animais como pequenos macacos, aves e outros, apenas durante o dia."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "É quando a fêmea pica o homem que elimina junto com a saliva as formas infectantes da maioria dos patógenos."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III, IV e V",
                                alternativeHint: "I - Incorreta. Pois apenas fêmeas se alimentam de sangue;<br><br>II - Incorreta. As fêmeas precisam de sangue para produzir ovos, mas nem todas as espécies de mosquitos colocam seus ovos de forma agrupada nos criadouros;<br><br>III - Correta. A preferência pelo hospedeiro humano ou a antropofilia é comum entre os mosquitos, sobretudo para as espécies que ocorrem no ambiente urbano, como por exemplo, <i>Ae. aegypti e Cx. quinquefasciatus</i>.<br><br>IV - Incorreta. Apesar da maioria das espécies de anofelinos ser silvestre a antropofilia é uma característica acentuada entre elas, além do hábito noturno.<br><br>V - Correta. É quando a fêmea pica o homem que elimina junto com a saliva as formas infectantes da maioria dos patógenos.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III",
                                alternativeHint: "I - Incorreta. Pois apenas fêmeas se alimentam de sangue;<br><br>II - Incorreta. As fêmeas precisam de sangue para produzir ovos, mas nem todas as espécies de mosquitos colocam seus ovos de forma agrupada nos criadouros;<br><br>III - Correta. A preferência pelo hospedeiro humano ou a antropofilia é comum entre os mosquitos, sobretudo para as espécies que ocorrem no ambiente urbano, como por exemplo, <i>Ae. aegypti e Cx. quinquefasciatus</i>.<br><br>IV - Incorreta. Apesar da maioria das espécies de anofelinos ser silvestre a antropofilia é uma característica acentuada entre elas, além do hábito noturno.<br><br>V - Correta. É quando a fêmea pica o homem que elimina junto com a saliva as formas infectantes da maioria dos patógenos.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "III e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "II e IV",
                                alternativeHint: "I - Incorreta. Pois apenas fêmeas se alimentam de sangue;<br><br>II - Incorreta. As fêmeas precisam de sangue para produzir ovos, mas nem todas as espécies de mosquitos colocam seus ovos de forma agrupada nos criadouros;<br><br>III - Correta. A preferência pelo hospedeiro humano ou a antropofilia é comum entre os mosquitos, sobretudo para as espécies que ocorrem no ambiente urbano, como por exemplo, <i>Ae. aegypti e Cx. quinquefasciatus</i>.<br><br>IV - Incorreta. Apesar da maioria das espécies de anofelinos ser silvestre a antropofilia é uma característica acentuada entre elas, além do hábito noturno.<br><br>V - Correta. É quando a fêmea pica o homem que elimina junto com a saliva as formas infectantes da maioria dos patógenos.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Apenas V",
                                alternativeHint: "I - Incorreta. Pois apenas fêmeas se alimentam de sangue;<br><br>II - Incorreta. As fêmeas precisam de sangue para produzir ovos, mas nem todas as espécies de mosquitos colocam seus ovos de forma agrupada nos criadouros;<br><br>III - Correta. A preferência pelo hospedeiro humano ou a antropofilia é comum entre os mosquitos, sobretudo para as espécies que ocorrem no ambiente urbano, como por exemplo, <i>Ae. aegypti e Cx. quinquefasciatus</i>.<br><br>IV - Incorreta. Apesar da maioria das espécies de anofelinos ser silvestre a antropofilia é uma característica acentuada entre elas, além do hábito noturno.<br><br>V - Correta. É quando a fêmea pica o homem que elimina junto com a saliva as formas infectantes da maioria dos patógenos.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 10,
                        type: "singleAnswer",
                        enunciate: "Dos ovos, após a eclosão, saem formas completamente diferentes dos adultos, elas mudam de pele várias vezes e passam por uma metamorfose completa. A descrição se aplica aos insetos:",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Hemimetábolos.",
                                alternativeHint: "Incorreta - Pois os insetos passam por metamorfose completa não podendo ser insetos Hemimetábolos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Anisometábolos.",
                                alternativeHint: "Incorreta - Pois os insetos passam por metamorfose completa não podendo ser insetos Anisometábolos.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Holometábolos.",
                                alternativeHint: "Correto - Pois os insetos passam por metamorfose completa sendo assim insetos Holometábolos.",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Ametábolos.",
                                alternativeHint: "Incorreta - Pois os insetos passam por metamorfose completa não podendo ser insetos Ametábolos.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Isometábolos.",
                                alternativeHint: "Incorreta - Pois os insetos passam por metamorfose completa não podendo ser insetos isometábolos.",
                                hintClass: "warning"
                            }
                        ]
                    },
                ]
            },
            {
                unitName: "Unidade 2",
                unitId: 2,
                unitQuestions: [
                    {
                        questionId: 11,
                        type: "singleAnswer",
                        enunciate: "Algumas espécies de mosquitos podem causar prejuízos à saúde humana. Sobre este tema assinale as sentenças verdadeiras, falsas e escolha a sequência correta.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Todos os mosquitos picam os seres humanos, pois, a alimentação sanguínea é essencial para a sua reprodução."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Em áreas onde as picadas de mosquitos causam alergia nas pessoas, mas não transmitem patógenos, não deve haver ações de vigilância e controle."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Em áreas onde ocorre a transmissão de patógenos por mosquitos, mas estes mosquitos não são percebidos pela população, deve haver ações de vigilância e controle."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Em áreas onde ocorrem mosquitos, mas não há detecção de picadas em humanos ou casos de doença, deve haver ações de vigilância."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Em áreas onde os mosquitos picam preferencialmente animais, e humanos em menor proporção, deve haver ações de vigilância, e ações de controle."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I. FALSO. Atenção, pois, apenas uma parte das espécies de mosquitos destas é hematófaga, ou seja, se alimenta de sangue. Nestes casos deve ser lembrado  que, apenas as fêmeas se alimentam de sangue, pois, este é necessário para a maturação dos ovos; os machos se alimentam de carbohidratos (açúcares) de plantas e fêmeas também fazem este segundo tipo de alimentação. Além disso, deve ser lembrado que dentre os mosquitos hematófagos há aqueles que preferem picar humanos (antropofílicos) e outros que têm preferência em picar animais (zoofílicos).<br><br>II. FALSO. Quaisquer danos à saúde causados por mosquitos devem ser evitados, assim cabem ações de vigilância e controle. Além disso, os mosquitos hematófagos na área podem também atuar como vetores, caso seja introduzido algum patógeno humano.<br><br>III. VERDADEIRO. É necessária ação de vigilância para caracterizar o problema entomológico da área (densidade e infestação de mosquitos) e em seguida realizar ações de controle. Neste caso o nível de infestação pode ser baixo e não perceptível pelas pessoas, mas, a detecção de casos da doença comprova que os vetores estão presentes. Para esta situação são recomendados índices entomológicos sensíveis para detectar sua presença em baixa densidade.<br><br>IV. VERDADEIRO. É necessária vigilância para caracterizar o problema entomológico da área e determinar se ocorrem mosquitos hematófagos. Se positivo cabe vigilância e controle, em caso negativo nenhumas destas ações são cabíveis, pois, possivelmente as espécies de mosquitos que não são hematófagas não costumam causar danos à saúde humana.<br><br>V. VERDADEIRO. Quaisquer danos à saúde humana ou de animais, causados por mosquitos, devem ser evitados, cabendo ações de vigilância e controle. Além disso, o comportamento zoofílico dos mosquitos (preferência alimentar por animais) pode sofrer uma adaptação e passar a ser antropofílico (preferência alimentar por humanos) aumentado o seu potencial papel como vetor.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, F",
                                alternativeHint: "I. FALSO. Atenção, pois, apenas uma parte das espécies de mosquitos destas é hematófaga, ou seja, se alimenta de sangue. Nestes casos deve ser lembrado  que, apenas as fêmeas se alimentam de sangue, pois, este é necessário para a maturação dos ovos; os machos se alimentam de carbohidratos (açúcares) de plantas e fêmeas também fazem este segundo tipo de alimentação. Além disso, deve ser lembrado que dentre os mosquitos hematófagos há aqueles que preferem picar humanos (antropofílicos) e outros que têm preferência em picar animais (zoofílicos).<br><br>II. FALSO. Quaisquer danos à saúde causados por mosquitos devem ser evitados, assim cabem ações de vigilância e controle. Além disso, os mosquitos hematófagos na área podem também atuar como vetores, caso seja introduzido algum patógeno humano.<br><br>III. VERDADEIRO. É necessária ação de vigilância para caracterizar o problema entomológico da área (densidade e infestação de mosquitos) e em seguida realizar ações de controle. Neste caso o nível de infestação pode ser baixo e não perceptível pelas pessoas, mas, a detecção de casos da doença comprova que os vetores estão presentes. Para esta situação são recomendados índices entomológicos sensíveis para detectar sua presença em baixa densidade.<br><br>IV. VERDADEIRO. É necessária vigilância para caracterizar o problema entomológico da área e determinar se ocorrem mosquitos hematófagos. Se positivo cabe vigilância e controle, em caso negativo nenhumas destas ações são cabíveis, pois, possivelmente as espécies de mosquitos que não são hematófagas não costumam causar danos à saúde humana.<br><br>V. VERDADEIRO. Quaisquer danos à saúde humana ou de animais, causados por mosquitos, devem ser evitados, cabendo ações de vigilância e controle. Além disso, o comportamento zoofílico dos mosquitos (preferência alimentar por animais) pode sofrer uma adaptação e passar a ser antropofílico (preferência alimentar por humanos) aumentado o seu potencial papel como vetor.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, V, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, V",
                                alternativeHint: "I. FALSO. Atenção, pois, apenas uma parte das espécies de mosquitos destas é hematófaga, ou seja, se alimenta de sangue. Nestes casos deve ser lembrado  que, apenas as fêmeas se alimentam de sangue, pois, este é necessário para a maturação dos ovos; os machos se alimentam de carbohidratos (açúcares) de plantas e fêmeas também fazem este segundo tipo de alimentação. Além disso, deve ser lembrado que dentre os mosquitos hematófagos há aqueles que preferem picar humanos (antropofílicos) e outros que têm preferência em picar animais (zoofílicos).<br><br>II. FALSO. Quaisquer danos à saúde causados por mosquitos devem ser evitados, assim cabem ações de vigilância e controle. Além disso, os mosquitos hematófagos na área podem também atuar como vetores, caso seja introduzido algum patógeno humano.<br><br>III. VERDADEIRO. É necessária ação de vigilância para caracterizar o problema entomológico da área (densidade e infestação de mosquitos) e em seguida realizar ações de controle. Neste caso o nível de infestação pode ser baixo e não perceptível pelas pessoas, mas, a detecção de casos da doença comprova que os vetores estão presentes. Para esta situação são recomendados índices entomológicos sensíveis para detectar sua presença em baixa densidade.<br><br>IV. VERDADEIRO. É necessária vigilância para caracterizar o problema entomológico da área e determinar se ocorrem mosquitos hematófagos. Se positivo cabe vigilância e controle, em caso negativo nenhumas destas ações são cabíveis, pois, possivelmente as espécies de mosquitos que não são hematófagas não costumam causar danos à saúde humana.<br><br>V. VERDADEIRO. Quaisquer danos à saúde humana ou de animais, causados por mosquitos, devem ser evitados, cabendo ações de vigilância e controle. Além disso, o comportamento zoofílico dos mosquitos (preferência alimentar por animais) pode sofrer uma adaptação e passar a ser antropofílico (preferência alimentar por humanos) aumentado o seu potencial papel como vetor.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, V",
                                alternativeHint: "I. FALSO. Atenção, pois, apenas uma parte das espécies de mosquitos destas é hematófaga, ou seja, se alimenta de sangue. Nestes casos deve ser lembrado  que, apenas as fêmeas se alimentam de sangue, pois, este é necessário para a maturação dos ovos; os machos se alimentam de carbohidratos (açúcares) de plantas e fêmeas também fazem este segundo tipo de alimentação. Além disso, deve ser lembrado que dentre os mosquitos hematófagos há aqueles que preferem picar humanos (antropofílicos) e outros que têm preferência em picar animais (zoofílicos).<br><br>II. FALSO. Quaisquer danos à saúde causados por mosquitos devem ser evitados, assim cabem ações de vigilância e controle. Além disso, os mosquitos hematófagos na área podem também atuar como vetores, caso seja introduzido algum patógeno humano.<br><br>III. VERDADEIRO. É necessária ação de vigilância para caracterizar o problema entomológico da área (densidade e infestação de mosquitos) e em seguida realizar ações de controle. Neste caso o nível de infestação pode ser baixo e não perceptível pelas pessoas, mas, a detecção de casos da doença comprova que os vetores estão presentes. Para esta situação são recomendados índices entomológicos sensíveis para detectar sua presença em baixa densidade.<br><br>IV. VERDADEIRO. É necessária vigilância para caracterizar o problema entomológico da área e determinar se ocorrem mosquitos hematófagos. Se positivo cabe vigilância e controle, em caso negativo nenhumas destas ações são cabíveis, pois, possivelmente as espécies de mosquitos que não são hematófagas não costumam causar danos à saúde humana.<br><br>V. VERDADEIRO. Quaisquer danos à saúde humana ou de animais, causados por mosquitos, devem ser evitados, cabendo ações de vigilância e controle. Além disso, o comportamento zoofílico dos mosquitos (preferência alimentar por animais) pode sofrer uma adaptação e passar a ser antropofílico (preferência alimentar por humanos) aumentado o seu potencial papel como vetor.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 12,
                        type: "singleAnswer",
                        enunciate: "Existem vários compostos que podem ser utilizados no controle de mosquitos. Sobre estes agentes assinale a alternativa correta.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Os inseticidas organofosforados quando usados para o controle de <i>Aedes</i> nos criadouros não são nocivos para outros organismos que habitam estes criadouros.",
                                alternativeHint: "FALSO. Um dos principais problemas relacionados com o uso de organofoforados é que eles são muito tóxicos para outros organismos inclusive muitos que também habitam criadouros de mosquitos; em certas doses e níveis de exposição podem ser muito tóxicos para humanos. Estes compostos são altamente tóxicos e agem no sistema nervoso.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Os inseticidas químicos organofosforados são mais seguros do que os larvicidas biológicos à base de bactéria como Bti porque agem por contato, e não por via oral.",
                                alternativeHint: "FALSO. Como dito acima os organofosforados são agentes muito tóxicos enquanto que o Bti é considerado o mais seguro dentre os produtos hoje disponíveis. A via de ação (por contato ou oral) é um fator importante, mas não o principal fator para determinar a classe toxicológica do produto. De qualquer forma o Bti só age por ingestão o que também aumenta a sua segurança. A ação por contato pode atingir mais facilmente e rapidamente vários organismos.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Os agentes de controle da classe dos reguladores de crescimento de insetos (IGR), como diflubenzuron e pyriproxyfen, agem por contato e matam as larvas em algumas horas.",
                                alternativeHint: "FALSO. Os IGR têm ação na fisiologia dos insetos, mas especificamente no sistema endócrino, causando distúrbios no desenvolvimento que impedem que as larvas e pupas passem para a fase adulta. Apesar de muito eficaz o modo de ação é lento e pode levar até 15 dias.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Os produtos larvicidas causam mortalidade das larvas tratadas em até 24 h após a aplicação.",
                                alternativeHint: "FALSO. Conforme descrito acima há variações. Os inseticidas químicos convencionais causam mortalidade mais rapidamente em algumas horas; os biológicos entre 24 e 48h; os IGR e larvicidas a base de fungos podem levar até 15 dias para causar a morte dos indivíduos. ",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "A aplicação espacial de adulticidas deve ser realizada de forma focal e apenas em áreas de surtos epidêmicos ou áreas estratégicas (ex. ferro-velho, borracharia, cemitério);",
                                alternativeHint: "VERDADEIRO. Atualmente o uso de adulticidas é mais recomendado em áreas de surtos epidêmicos, ações de bloqueio de transmissão ou em áreas ou pontos estratégicos. Deve ser, portanto, lembrado que cada vez mais o seu uso como uma ação de rotina tem sido desaconselhado, pois, a sua ação é limitada, pouco atinge os mosquitos abrigados no intradomicílio. Hoje o uso deste método tornou-se mais restrito e contextualizado em situações críticas ou estratégicas, como àquelas citadas acima. ",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 13,
                        type: "singleAnswer",
                        enunciate: "Sobre os agentes de controle de mosquito assinale a alternativa correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Organofosforados, piretróides, Bti e espinosad agem no sistema nervoso dos insetos.",
                                alternativeHint: "FALSO. Os larvicidas a base de Bti possuem toxinas inseticidas que agem por via oral no intestino das larvas. É ainda importante dizer que os demais compostos citados agem no sistema nervoso, embora a ação de cada um ocorra em moléculas diferentes (organofosforados na enzima acetilcolinesterase, piretróides na proteína canal de sódio e espinosad nos receptores nicotídicos da acetilcolinesterase).",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Os organofosforados são utilizados unicamente como agentes larvicida.",
                                alternativeHint: "FALSO. Há vários compostos da classe química dos organofosfoforados usados no controle de mosquitos como larvicidas (ex. temephos) e adulticidas (ex. malathion). Os organofosforados também são utilizados como ingrediente ativo de inseticidas para outros grupos de insetos.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "O regulador de crescimento piretróide é utilizado para o controle de adultos.",
                                alternativeHint: "FALSO. Os piretróides são agentes químicos, porém, não são da classe de IGR e não agem interferindo no crescimento dos insetos. Eles agem ligando-se a proteína do canal de sódio, que é uma molécula do sistema nervoso essencial para o equilíbrio de entrada e saída de íons nas células e, consequentemente, para a transmissão dos impulsos nervos. Quando o piretróide liga-se a proteína canal de sódio, por exemplo, impede o seu correto funcionamento e causa vários distúrbios que resultam na morte dos insetos.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Larvicidas à base de bactérias como Bti e <i>Lysinibacillus sphaericus</i> são uma opção eficaz e segura pois agem apenas em algumas espécies de mosquitos.",
                                alternativeHint: "VERDADEIRO. Estes larvicidas biológicos a base de bactérias são considerados, hoje, os mais seguros pois agem apenas em algumas espécies de mosquitos e borrachudos (<i>Simulium</i>), e não causam danos para outras espécies.",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Peixes como “barrigudinho” podem ser utilizados para o controle em quaisquer criadouros de mosquitos.",
                                alternativeHint: "FALSO. Eles podem ser importantes aliados pois são excelentes predadores de larvas de mosquitos, no entanto, eles devem ser usados em criadouros com água limpa como tanques, cisternas e tonéis. Há ambientes incompatíveis como fossas, pois, estas possuem alta poluição orgânica, pouco oxigênio na água e não permitem a sobrevivência de muitos predadores. Os peixes são predadores mais utilizados pois possuem baixo custo, a população já conhece e eles podem se manter por muito tempo nos criadouros.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 14,
                        type: "singleAnswer",
                        enunciate: "Sobre programas de controle de insetos assinale a resposta correta.",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Um programa de controle de vetores necessita, inicialmente, identificar as espécies a serem controladas. Em uma determinada área deve haver apenas uma espécie responsável pela transmissão de um agente etiológico.",
                                alternativeHint: "FALSO. A primeira sentença está correta pois identificar a espécie a ser controlada é o primeiro passo das ações de controle. No entanto, numa determinada área, mais de uma espécie de mosquitos pode ser responsável por causar problemas de saúde. Um exemplo são áreas urbanas onde espécies de <i>Aedes aegypti</i>, <i>Aedes albopictus</i> e <i>Culex quinquefasciatus</i> ocorrem simultâneamente e podem transmitir patógenos; outro exemplo são áreas onde mais de uma espécie de anofelino pode estar sendo responsável pela transmissão de plasmódios. Deve ser lembrado que pode haver também as situações em que apenas uma espécie está envolvida.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Deve-se usar produtos larvicidas com diferentes modos de ação, em rotação, durante o programa de controle de vetores, para prevenir a seleção de resistência.",
                                alternativeHint: "VERDADEIRO. O uso de um único larvicida por período prolongado pode levar à seleção de indivíduos resistentes. Por outro lado, o uso de produtos com modos de ação diferentes em rotação evita que os insetos sejam selecionados para resistência. Pode-se, por exemplo, iniciar usando larvicidas a base do inseticida químico temephos químico que age no sistema nervoso, em seguida substituir pelo agente biológico Bti com ação no intestino e em seguida substituir por um IGR-pyriproxyfen que age no sistema endócrino. O tempo de uso para cada produto deve ser estabelecido pelo supervisor do programa, de acordo com as estratégias, métodos escolhidos e recursos disponíveis.",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Para executar um programa de controle de vetores é essencial avaliar a infestação e densidade dos insetos antes da intervenção, porém a avaliação durante o tratamento não é necessária.",
                                alternativeHint: "FALSO. A avaliação da infestação e densidade deve ser feita antes e durante todo o programa a fim de verificar se as ações estão provocando os resultados desejados para reduzir as populações dos mosquitos de acordo com a meta que foi estabelecida. Executar um programa de controle sem avaliar a sua eficácia é um erro bastante comum e que não deve acontecer, afinal de contas todas as ações e recursos utilizados devem atingir um objetivo. Caso a redução não esteja sendo àquela esperada a avaliação ser então para apontar que as estratégias devem ser re-avaliadas e erros operacionais podem estar ocorrendo, para não atingir o objetivo. A avaliação indireta através do número de casos de notificação de casos humanos na área é importante, mas, não garante que o resultado entomológico, ou seja, a redução de mosquitos esteja sendo atingida.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Um programa de controle deve ser executado utilizando uma única estratégia ou método que tenha comprovada eficácia e possa ser adaptada a locais com diferentes condições entomológicas, ambientais e sócio-econômicas.",
                                alternativeHint: "FALSO. Hoje considera-se essencial que o combate aos insetos vetores seja feito através de diferentes métodos que, integrados, possam provocar uma redução populacional importante e também estejam adaptados às características ambientais e sócio-econômicas da área. É ainda essencial associar a ação comunitária com as ações de controle executados pelos órgãos responsáveis. Atualmente observa-se que o uso de única estratégia frequentemente não é suficiente para reduzir os vetores. Além disso, uma determinada estratégia de controle pode funcionar em uma área, porém, pode não se adaptar a outras que tenham características diferentes. Cada programa de controle deve ser construído e adaptado à realidade de cada local.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Equipes técnicas preparadas e recursos materiais são fatores essenciais para os programas de controle, enquanto que  a conscientização e mobilização comunitária são um complemento das ações.",
                                alternativeHint: "FALSO. Na atualidade as ações de conscientização e mobilização comunitária são consideradas indispensáveis para o sucesso de programas de controle. Hoje a população deve ser parte central e não apenas um complemento das ações. Atividades de: informação, formação, discussão, e participação efetiva da comunidade nas ações devem ser contempladas como parte central do programa. Este é um fator que vem sendo negligenciado e que também é responsável pelo fracasso de vários programas que centralizam as ações nos agentes de controle, sem envolver a população como parte essencial dele.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 15,
                        type: "singleAnswer",
                        enunciate: "O que são armadilhas e como são utilizadas? Julgue as afirmativas abaixo em verdadeiras ou falsas e assinale a alternativa correta.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "São reservatórios de água para atrair os mosquitos e servem como criadouros."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "São ferramentas desenhadas para retirar do ambiente formas imaturas e/ou adultas dos mosquitos para estimar a densidade populacional do inseto no ambiente."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "São criadouros utilizados para monitorar a população do inseto no ambiente."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "São vasos de plantas utilizados como criadouros pelos mosquitos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "São ferramentas desenhadas para coletar formas adultas dos mosquitos, também utilizadas para monitoramento da circulação de arbovírus em populações de mosquitos do gênero <i>Aedes</i> e <i>Culex</i>."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I - Incorreta. As armadilhas são ferramentas para a coleta de mosquito, embora algumas utilizem água no seu interior para atrair as fêmeas do inseto, não são reservatórios. Quando permanecem em campo por mais de uma semana devem ser tratadas com larvicidas, biológico ou químico, para evitar que se transformem em criadouros reais para o mosquito.<br><br>II - Correta. Muito bem!<br><br>III - Incorreta. Armadilhas não são criadouros, são ferramentas para atrair e eliminar formas imaturas e/ou adultas dos mosquitos e são utilizadas para estimar a densidade populacional do inseto no ambiente.<br><br>IV - Incorreta. As armadilhas do tipo ovitrampas podem ser confeccionadas a partir de vasos semelhantes aos de cultivo de plantas, embora seu interior seja preenchido com água e possua um suporte para a deposição de ovos de mosquitos. Em campo, estas ferramentas são utilizadas como para estimar a densidade populacional do inseto no ambiente<br><br>V - Correto. Armadilhas são ferramentas desenhadas para coletar diferentes espécies de mosquitos adultos dos ambientes. Alguns modelos se baseiam na atração luminosa e são mais eficientes para <i>Culex spp.</i> e outras com atração química para as fêmeas de <i>Aedes spp.</i> Assim, as armadilhas podem coletar amostras de fêmeas em uma localidade para a detecção de arbovírus circulantes.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, F",
                                alternativeHint: "I - Incorreta. As armadilhas são ferramentas para a coleta de mosquito, embora algumas utilizem água no seu interior para atrair as fêmeas do inseto, não são reservatórios. Quando permanecem em campo por mais de uma semana devem ser tratadas com larvicidas, biológico ou químico, para evitar que se transformem em criadouros reais para o mosquito.<br><br>II - Correta. Muito bem!<br><br>III - Incorreta. Armadilhas não são criadouros, são ferramentas para atrair e eliminar formas imaturas e/ou adultas dos mosquitos e são utilizadas para estimar a densidade populacional do inseto no ambiente.<br><br>IV - Incorreta. As armadilhas do tipo ovitrampas podem ser confeccionadas a partir de vasos semelhantes aos de cultivo de plantas, embora seu interior seja preenchido com água e possua um suporte para a deposição de ovos de mosquitos. Em campo, estas ferramentas são utilizadas como para estimar a densidade populacional do inseto no ambiente<br><br>V - Correto. Armadilhas são ferramentas desenhadas para coletar diferentes espécies de mosquitos adultos dos ambientes. Alguns modelos se baseiam na atração luminosa e são mais eficientes para <i>Culex spp.</i> e outras com atração química para as fêmeas de <i>Aedes spp.</i> Assim, as armadilhas podem coletar amostras de fêmeas em uma localidade para a detecção de arbovírus circulantes.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, F, F, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, V",
                                alternativeHint: "I - Incorreta. As armadilhas são ferramentas para a coleta de mosquito, embora algumas utilizem água no seu interior para atrair as fêmeas do inseto, não são reservatórios. Quando permanecem em campo por mais de uma semana devem ser tratadas com larvicidas, biológico ou químico, para evitar que se transformem em criadouros reais para o mosquito.<br><br>II - Correta. Muito bem!<br><br>III - Incorreta. Armadilhas não são criadouros, são ferramentas para atrair e eliminar formas imaturas e/ou adultas dos mosquitos e são utilizadas para estimar a densidade populacional do inseto no ambiente.<br><br>IV - Incorreta. As armadilhas do tipo ovitrampas podem ser confeccionadas a partir de vasos semelhantes aos de cultivo de plantas, embora seu interior seja preenchido com água e possua um suporte para a deposição de ovos de mosquitos. Em campo, estas ferramentas são utilizadas como para estimar a densidade populacional do inseto no ambiente<br><br>V - Correto. Armadilhas são ferramentas desenhadas para coletar diferentes espécies de mosquitos adultos dos ambientes. Alguns modelos se baseiam na atração luminosa e são mais eficientes para <i>Culex spp.</i> e outras com atração química para as fêmeas de <i>Aedes spp.</i> Assim, as armadilhas podem coletar amostras de fêmeas em uma localidade para a detecção de arbovírus circulantes.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, V",
                                alternativeHint: "I - Incorreta. As armadilhas são ferramentas para a coleta de mosquito, embora algumas utilizem água no seu interior para atrair as fêmeas do inseto, não são reservatórios. Quando permanecem em campo por mais de uma semana devem ser tratadas com larvicidas, biológico ou químico, para evitar que se transformem em criadouros reais para o mosquito.<br><br>II - Correta. Muito bem!<br><br>III - Incorreta. Armadilhas não são criadouros, são ferramentas para atrair e eliminar formas imaturas e/ou adultas dos mosquitos e são utilizadas para estimar a densidade populacional do inseto no ambiente.<br><br>IV - Incorreta. As armadilhas do tipo ovitrampas podem ser confeccionadas a partir de vasos semelhantes aos de cultivo de plantas, embora seu interior seja preenchido com água e possua um suporte para a deposição de ovos de mosquitos. Em campo, estas ferramentas são utilizadas como para estimar a densidade populacional do inseto no ambiente<br><br>V - Correto. Armadilhas são ferramentas desenhadas para coletar diferentes espécies de mosquitos adultos dos ambientes. Alguns modelos se baseiam na atração luminosa e são mais eficientes para <i>Culex spp.</i> e outras com atração química para as fêmeas de <i>Aedes spp.</i> Assim, as armadilhas podem coletar amostras de fêmeas em uma localidade para a detecção de arbovírus circulantes.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 16,
                        type: "singleAnswer",
                        enunciate: "Assinale a alternativa que indica os instrumentos corretos para a captura ativa de mosquitos adultos, em seus locais de repouso, no intra ou no peridomicilio.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Ovitrampa e BR-OVT    ",
                                alternativeHint: "Incorreta: A ovitrampa e a BR-OVT são armadilhas para coleta passiva de ovos de <i>Aedes spp.</i> e <i>Culex quinquefasciatus</i>.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Armadilha tipo CDC e New Jersey",
                                alternativeHint: "Incorreta: As armadilhas tipo CDC e New Jersey são utilizadas para a coleta passiva de várias espécies de mosquitos adultos.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Aspirador e capturador de castro",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Aspirador e Ovitrampas",
                                alternativeHint: "Incorreta: A ovitrampa é uma armadilha para a coleta passiva de ovos de <i>Aedes spp</i>.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Box Gravid Trap e BG-Sentinel",
                                alternativeHint: "Incorreta: A Box Gravid Trap e BG-Sentinel são armadilhas para a coleta passiva de fêmeas grávidas de <i>Culex spp.</i> e de mosquitos adultos de outras espécies, respectivamente.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 17,
                        type: "singleAnswer",
                        enunciate: "Entre as alternativas abaixo, assinale a que descreve a estação disseminadora (ED) de inseticida.",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "É uma combinação entre uma armadilha de oviposição e qualquer inseticida, líquido ou sólido, usada para o controle de mosquitos adultos, que deve ser instalada apenas no interior das residências.",
                                alternativeHint: "Incorreta. O inseticida usado na estação disseminadora deve ser em pó (fino) para ser levado em pequenas quantidades, pelos próprios mosquitos aos seus criadouros para matar larvas e pupas. Deve ser instalada no peridomicílio.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "É uma combinação de água, açúcar e um ingrediente ativo (ex. ácido bórico, malathion, deltametrina, spinosad, ivermectina, pyriproxyfen, dinotefuran, eugenol, óleo de alho) usado para matar os mosquitos em sua fase adulta. Deve ser instalada na área externa das residências.",
                                alternativeHint: "Incorreta. Esta afirmativa descreve a composição da isca tóxica de açúcar e não da estação disseminadora de inseticida, pois essa última necessita que o mosquito permaneça vivo para dispersar o produto para os criadouros.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "É uma armadilha composta por um vaso contendo água tratada com um inseticida e um substrato para a coleta de ovos de mosquitos. Deve ser instalada no peridomicílio.  ",
                                alternativeHint: "Incorreta. Esta afirmativa descreve a composição da ovitrampa e não da estação disseminadora de inseticida, pois essa não se destina a coleta de ovos.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "É uma armadilha adesiva utilizada para atrair e aprisionar mosquitos adultos em seu interior.  Deve ser instalada no intradomicílio.",
                                alternativeHint: " Incorreta. Esta afirmativa descreve a composição de outros modelos de armadilhas para a coleta de mosquitos adultos, como AedesTrap e Mosquitrap. Além disso, a estação disseminadora de inseticida permite o acesso livre dos mosquitos adultos para que ocorra a dispersão do produto para os criadouros.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "É uma armadilha composta por um vaso contendo água e um tecido impregnado com um potente inseticida, como o pyriproxyfen. Serve para atrair os mosquitos, especialmente as fêmeas, tornando-os veículos disseminadores de partículas do inseticida aderidas ao seu corpo, para criadouros. Deve ser usada no peridomicílio.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 18,
                        type: "singleAnswer",
                        enunciate: "A vigilância entomológica é um elemento essencial nos programas de controle de doenças como a malária, filariose e arboviroses. Assinale as alternativas que explicam esta afirmação.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A principal via de transmissão destas doenças é através dos mosquitos vetores."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A vigilância entomológica é responsável pela identificação da(s) espécie(s) vetora(s), monitoramento da sua densidade populacional, classificação das áreas infestadas, acompanhamento das ações de controle e a avaliação da sua efetividade."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Não existem vacinas para todas estas doenças e a forma de reduzir o número de casos é através do controle dos vetores."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "A vigilância entomológica é quem notifica o número de casos destas doenças e, com base nisso, os locais de maior risco para a transmissão."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "A vigilância monitora os indicadores de risco entomológico e ambiental e acompanha ações para o controle dos mosquitos."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II, IV e V",
                                alternativeHint: "I - CORRETA. A principal via de transmissão dos agentes patogênicos causadores destas doenças é horizontal e se dá entre os mosquitos e o homem.<br><br>II - CORRETA. A Vigilância Entomológica é responsável por todas estas atividades nos Programas de controle de vetores.<br><br>III - CORRETA. Até 2019, existem vacinas apenas para algumas arboviroses, como febre amarela e dengue.<br><br>IV - INCORRETA. A vigilância epidemiológica é a responsável pela notificação de casos destas doenças e não a vigilância entomológica.<br><br>V - CORRETA. A vigilância entomológica monitora as espécies vetoras, bem como as condições do ambiente que podem aumentar suas densidades, para indicar quando as ações de controle devem ser intensificadas.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e IV",
                                alternativeHint: "I - CORRETA. A principal via de transmissão dos agentes patogênicos causadores destas doenças é horizontal e se dá entre os mosquitos e o homem.<br><br>II - CORRETA. A Vigilância Entomológica é responsável por todas estas atividades nos Programas de controle de vetores.<br><br>III - CORRETA. Até 2019, existem vacinas apenas para algumas arboviroses, como febre amarela e dengue.<br><br>IV - INCORRETA. A vigilância epidemiológica é a responsável pela notificação de casos destas doenças e não a vigilância entomológica.<br><br>V - CORRETA. A vigilância entomológica monitora as espécies vetoras, bem como as condições do ambiente que podem aumentar suas densidades, para indicar quando as ações de controle devem ser intensificadas.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "I, III, IV e V",
                                alternativeHint: "I - CORRETA. A principal via de transmissão dos agentes patogênicos causadores destas doenças é horizontal e se dá entre os mosquitos e o homem.<br><br>II - CORRETA. A Vigilância Entomológica é responsável por todas estas atividades nos Programas de controle de vetores.<br><br>III - CORRETA. Até 2019, existem vacinas apenas para algumas arboviroses, como febre amarela e dengue.<br><br>IV - INCORRETA. A vigilância epidemiológica é a responsável pela notificação de casos destas doenças e não a vigilância entomológica.<br><br>V - CORRETA. A vigilância entomológica monitora as espécies vetoras, bem como as condições do ambiente que podem aumentar suas densidades, para indicar quando as ações de controle devem ser intensificadas.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Todas estão corretas",
                                alternativeHint: "I - CORRETA. A principal via de transmissão dos agentes patogênicos causadores destas doenças é horizontal e se dá entre os mosquitos e o homem.<br><br>II - CORRETA. A Vigilância Entomológica é responsável por todas estas atividades nos Programas de controle de vetores.<br><br>III - CORRETA. Até 2019, existem vacinas apenas para algumas arboviroses, como febre amarela e dengue.<br><br>IV - INCORRETA. A vigilância epidemiológica é a responsável pela notificação de casos destas doenças e não a vigilância entomológica.<br><br>V - CORRETA. A vigilância entomológica monitora as espécies vetoras, bem como as condições do ambiente que podem aumentar suas densidades, para indicar quando as ações de controle devem ser intensificadas.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 19,
                        type: "singleAnswer",
                        enunciate: "Sobre os índices entomológicos empregados para o mosquito <i>Aedes aegypti</i>, qual sentença abaixo está CORRETA:",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "O Levantamento Rápido do Índice de Infestação por <i>Aedes aegypti</i> (LIRAa) é estimado pela pesquisa larvária e traz informações precisas sobre a densidade da espécie nas diferentes localidades.",
                                alternativeHint: "INCORRETO. O LIRAa informa apenas o percentual de imóveis de uma localidade em que foi encontrado pelo menos um criadouro positivo para larvas de <i>Ae. aegypti</i>, independente da sua quantidade.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "O Índice de Densidade de Ovos (IDO) indica a presença de fêmeas reprodutivamente ativas na área investigada, embora não seja capaz de dizer sua quantidade.",
                                alternativeHint: "CORRETA. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "A partir da obtenção do número médio de ovos de <i>Ae. aegypti</i> de uma área, é possível indicar, exatamente, a distribuição da infestação nessa área;",
                                alternativeHint: "INCORRETA. O número médio de ovos indica a intensidade da infestação por <i>Aedes aegypti</i>, sua distribuição é estimada pela localização de ovitrampas positivas em uma determinada área.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "A densidade populacional dos mosquitos adultos (DA) é calculada, apenas, a partir do número de fêmeas coletadas por armadilha CDC e/ou aspirador de insetos.",
                                alternativeHint: "INCORRETA. A densidade populacional dos mosquitos adultos (DA) é calculada a partir do número total de indivíduos coletados da espécie alvo, tanto machos quanto fêmeas.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "A densidade de larvas e pupas (DLP) é o indicador mais utilizado para estimar a densidade de <i>Ae. aegypti</i>.",
                                alternativeHint: "INCORRETA. A DLP é o indicador mais utilizado para estimar a densidade de <i>Cx. quinquefasciatus</i>. Não é indicado para <i>Ae. aegypti</i>.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 20,
                        type: "singleAnswer",
                        enunciate: "Com relação aos instrumentos utilizados para coleta ou captura de mosquitos é correto afirmar que:",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A ovitrampa é a ferramenta mais sensível para detectar a presença e a abundância de <i>Ae. aegypti</i>;"
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "O aspirador pode coletar formas jovens e adultas dos mosquitos, por isso são utilizados para o monitoramento de <i>Cx. quinquefasciatus</i>;"
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A concha é um instrumento de coleta igualmente eficiente para obter amostras de <i>Ae. aegypti</i>, <i>Cx. quinquefasciatus</i> e <i>An. darlingi</i>."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Mosquitos da espécie <i>Cx. quinquefasciatus</i> podem ser coletados de forma passiva através de armadilha CDC luminosa, à noite, e por busca ativa por meio do aspirador."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "A coleta de fêmeas de <i>An. darlingi</i> pode ser feita por armadilhas CDC, embora a isca humana continue sendo o método mais sensível e utilizado nos programas de controle da malária."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II, IV e V",
                                alternativeHint: "I - CORRETA. A ovitrampa é um instrumento extremamente sensível para detectar a presença e a abundância de <i>Ae. aegypti</i> no ambiente.<br><br>II - INCORRETA. O aspirador é um instrumento de coleta utilizado apenas para obter mosquitos adultos.<br><br>III - INCORRETA. A concha é o instrumento utilizado para coletar formas jovens de mosquitos <i>Cx. quinquefasciatus</i> e <i>An. darlingi</i>, porém não é eficiente para <i>Ae. aegypti</i>.<br><br>IV - CORRETA. O mosquito <i>Cx. quinquefasciatus</i> tem hábito alimentar noturno, portanto é fortemente atraído pela luz. Assim, armadilhas luminosas são eficientes para a coleta passiva dessa espécie e, com o aspirador, a busca ativa dos mosquitos poderá ser feita em qualquer horário.<br><br>V - CORRETA. A isca humana é sem dúvida o método mais eficiente para a amostragem desta espécie, devido ao seu elevado grau de antropofilia. Embora, por questões éticas, as fêmeas deste anofelino sejam capturadas, por aspiração, assim que pousam na pele do hospedeiro, evitando a picada. É sempre indicado o uso de uma dupla de operadores durante a captura por isca humana.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e IV",
                                alternativeHint: "I - CORRETA. A ovitrampa é um instrumento extremamente sensível para detectar a presença e a abundância de <i>Ae. aegypti</i> no ambiente.<br><br>II - INCORRETA. O aspirador é um instrumento de coleta utilizado apenas para obter mosquitos adultos.<br><br>III - INCORRETA. A concha é o instrumento utilizado para coletar formas jovens de mosquitos <i>Cx. quinquefasciatus</i> e <i>An. darlingi</i>, porém não é eficiente para <i>Ae. aegypti</i>.<br><br>IV - CORRETA. O mosquito <i>Cx. quinquefasciatus</i> tem hábito alimentar noturno, portanto é fortemente atraído pela luz. Assim, armadilhas luminosas são eficientes para a coleta passiva dessa espécie e, com o aspirador, a busca ativa dos mosquitos poderá ser feita em qualquer horário.<br><br>V - CORRETA. A isca humana é sem dúvida o método mais eficiente para a amostragem desta espécie, devido ao seu elevado grau de antropofilia. Embora, por questões éticas, as fêmeas deste anofelino sejam capturadas, por aspiração, assim que pousam na pele do hospedeiro, evitando a picada. É sempre indicado o uso de uma dupla de operadores durante a captura por isca humana.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e V",
                                alternativeHint: "I - CORRETA. A ovitrampa é um instrumento extremamente sensível para detectar a presença e a abundância de <i>Ae. aegypti</i> no ambiente.<br><br>II - INCORRETA. O aspirador é um instrumento de coleta utilizado apenas para obter mosquitos adultos.<br><br>III - INCORRETA. A concha é o instrumento utilizado para coletar formas jovens de mosquitos <i>Cx. quinquefasciatus</i> e <i>An. darlingi</i>, porém não é eficiente para <i>Ae. aegypti</i>.<br><br>IV - CORRETA. O mosquito <i>Cx. quinquefasciatus</i> tem hábito alimentar noturno, portanto é fortemente atraído pela luz. Assim, armadilhas luminosas são eficientes para a coleta passiva dessa espécie e, com o aspirador, a busca ativa dos mosquitos poderá ser feita em qualquer horário.<br><br>V - CORRETA. A isca humana é sem dúvida o método mais eficiente para a amostragem desta espécie, devido ao seu elevado grau de antropofilia. Embora, por questões éticas, as fêmeas deste anofelino sejam capturadas, por aspiração, assim que pousam na pele do hospedeiro, evitando a picada. É sempre indicado o uso de uma dupla de operadores durante a captura por isca humana.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "I, IV e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Todas estão corretas",
                                alternativeHint: "I - CORRETA. A ovitrampa é um instrumento extremamente sensível para detectar a presença e a abundância de <i>Ae. aegypti</i> no ambiente.<br><br>II - INCORRETA. O aspirador é um instrumento de coleta utilizado apenas para obter mosquitos adultos.<br><br>III - INCORRETA. A concha é o instrumento utilizado para coletar formas jovens de mosquitos <i>Cx. quinquefasciatus</i> e <i>An. darlingi</i>, porém não é eficiente para <i>Ae. aegypti</i>.<br><br>IV - CORRETA. O mosquito <i>Cx. quinquefasciatus</i> tem hábito alimentar noturno, portanto é fortemente atraído pela luz. Assim, armadilhas luminosas são eficientes para a coleta passiva dessa espécie e, com o aspirador, a busca ativa dos mosquitos poderá ser feita em qualquer horário.<br><br>V - CORRETA. A isca humana é sem dúvida o método mais eficiente para a amostragem desta espécie, devido ao seu elevado grau de antropofilia. Embora, por questões éticas, as fêmeas deste anofelino sejam capturadas, por aspiração, assim que pousam na pele do hospedeiro, evitando a picada. É sempre indicado o uso de uma dupla de operadores durante a captura por isca humana.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 11,
                        type: "singleAnswer",
                        enunciate: "Sobre a presença de mosquitos no ambiente, seu contato com o homem e seu impacto na saúde, defina as sentenças como verdadeiras ou falsas e escolha a sequência correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Os mosquitos são atraídos para as casas para buscar alimentação e abrigo; a eliminação e proteção de locais de criação de mosquitos no intradomicílio são ações essenciais para evitar a transmissão de doenças."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Locais onde há lixo e acúmulo de água de chuvas podem tornar-se importantes locais de criação de mosquito."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "As principais fontes de mosquito no ambiente são locais que possuem, ou que acumulam, água pois, esta é necessária para o desenvolvimento das formas jovens dos mosquitos."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "O interior das casas, ou o intradomicílio, não oferece condições favoráveis para abrigar os mosquitos adultos."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Escolas, locais de trabalho, de lazer e hospitais também são locais que não oferecem um risco de proliferação de mosquitos."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I - VERDADEIRO. O intradomicílio pode ser um ambiente muito adequado para o abrigo servindo de “residência” de mosquitos adultos, sobretudo para as fêmeas que encontram locais seguros e sem predadores (exceto o homem). No interior das casas os adultos podem repousar e ter alimentação disponível (humanos, animais), sem necessidade de deslocamento. Em algumas áreas urbanas já foi detectado que muitos mosquitos passam a sua vida no interior das casas, ou seja são “residentes”, não sendo portanto atingidos pelas ações de controle feitas no peridomicílio. Assim a faxina das casas, eliminação de mosquitos com raquetes ou com o uso de iscas açucaradas tóxicas, são armas eficazes para seu combate.<br><br>II - VERDADEIRO. O lixo, em si, é sólido e não é um criadouro de mosquitos; porém quando recebe água de chuva, são formados inúmeros microcriadouros no interior de recipientes e entulhos. Os restos de alimentos em apodrecimento na água (matéria orgânica em decomposição) podem atrair as fêmeas para postura de ovos além de servir de alimentação das larvas.<br><br>III - VERDADEIRO. A água, em grande ou pequena quantidade, pobre (limpa) ou rica em matéria orgânica (suja), é uma condição obrigatória para que haja a postura ovos e desenvolvimento de mosquitos.<br><br>IV - FALSO. Conforme o item I.<br><br>V - FALSO. Estes locais oferecem várias possibilidades de criadouros e outros locais que acumulam água e tornam-se criadouros temporários. Além disso, estes locais agrupam um grande número de pessoas que, por si, já é um atrativo para os mosquitos e favorece a disseminação da doença, sobretudo em hospitais, onde muitas pessoas estão infectadas com arbovírus, por exemplo. Devemos sempre considerar se o local da transmissão foi no domicílio ou foi em outros locais onde a pessoa costuma passar um bom período de tempo, como escola-creche ou local de trabalho.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, F",
                                alternativeHint: "I - VERDADEIRO. O intradomicílio pode ser um ambiente muito adequado para o abrigo servindo de “residência” de mosquitos adultos, sobretudo para as fêmeas que encontram locais seguros e sem predadores (exceto o homem). No interior das casas os adultos podem repousar e ter alimentação disponível (humanos, animais), sem necessidade de deslocamento. Em algumas áreas urbanas já foi detectado que muitos mosquitos passam a sua vida no interior das casas, ou seja são “residentes”, não sendo portanto atingidos pelas ações de controle feitas no peridomicílio. Assim a faxina das casas, eliminação de mosquitos com raquetes ou com o uso de iscas açucaradas tóxicas, são armas eficazes para seu combate.<br><br>II - VERDADEIRO. O lixo, em si, é sólido e não é um criadouro de mosquitos; porém quando recebe água de chuva, são formados inúmeros microcriadouros no interior de recipientes e entulhos. Os restos de alimentos em apodrecimento na água (matéria orgânica em decomposição) podem atrair as fêmeas para postura de ovos além de servir de alimentação das larvas.<br><br>III - VERDADEIRO. A água, em grande ou pequena quantidade, pobre (limpa) ou rica em matéria orgânica (suja), é uma condição obrigatória para que haja a postura ovos e desenvolvimento de mosquitos.<br><br>IV - FALSO. Conforme o item I.<br><br>V - FALSO. Estes locais oferecem várias possibilidades de criadouros e outros locais que acumulam água e tornam-se criadouros temporários. Além disso, estes locais agrupam um grande número de pessoas que, por si, já é um atrativo para os mosquitos e favorece a disseminação da doença, sobretudo em hospitais, onde muitas pessoas estão infectadas com arbovírus, por exemplo. Devemos sempre considerar se o local da transmissão foi no domicílio ou foi em outros locais onde a pessoa costuma passar um bom período de tempo, como escola-creche ou local de trabalho.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, F, F",
                                alternativeHint: "I - VERDADEIRO. O intradomicílio pode ser um ambiente muito adequado para o abrigo servindo de “residência” de mosquitos adultos, sobretudo para as fêmeas que encontram locais seguros e sem predadores (exceto o homem). No interior das casas os adultos podem repousar e ter alimentação disponível (humanos, animais), sem necessidade de deslocamento. Em algumas áreas urbanas já foi detectado que muitos mosquitos passam a sua vida no interior das casas, ou seja são “residentes”, não sendo portanto atingidos pelas ações de controle feitas no peridomicílio. Assim a faxina das casas, eliminação de mosquitos com raquetes ou com o uso de iscas açucaradas tóxicas, são armas eficazes para seu combate.<br><br>II - VERDADEIRO. O lixo, em si, é sólido e não é um criadouro de mosquitos; porém quando recebe água de chuva, são formados inúmeros microcriadouros no interior de recipientes e entulhos. Os restos de alimentos em apodrecimento na água (matéria orgânica em decomposição) podem atrair as fêmeas para postura de ovos além de servir de alimentação das larvas.<br><br>III - VERDADEIRO. A água, em grande ou pequena quantidade, pobre (limpa) ou rica em matéria orgânica (suja), é uma condição obrigatória para que haja a postura ovos e desenvolvimento de mosquitos.<br><br>IV - FALSO. Conforme o item I.<br><br>V - FALSO. Estes locais oferecem várias possibilidades de criadouros e outros locais que acumulam água e tornam-se criadouros temporários. Além disso, estes locais agrupam um grande número de pessoas que, por si, já é um atrativo para os mosquitos e favorece a disseminação da doença, sobretudo em hospitais, onde muitas pessoas estão infectadas com arbovírus, por exemplo. Devemos sempre considerar se o local da transmissão foi no domicílio ou foi em outros locais onde a pessoa costuma passar um bom período de tempo, como escola-creche ou local de trabalho.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, F",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, F, V, F, F",
                                alternativeHint: "I - VERDADEIRO. O intradomicílio pode ser um ambiente muito adequado para o abrigo servindo de “residência” de mosquitos adultos, sobretudo para as fêmeas que encontram locais seguros e sem predadores (exceto o homem). No interior das casas os adultos podem repousar e ter alimentação disponível (humanos, animais), sem necessidade de deslocamento. Em algumas áreas urbanas já foi detectado que muitos mosquitos passam a sua vida no interior das casas, ou seja são “residentes”, não sendo portanto atingidos pelas ações de controle feitas no peridomicílio. Assim a faxina das casas, eliminação de mosquitos com raquetes ou com o uso de iscas açucaradas tóxicas, são armas eficazes para seu combate.<br><br>II - VERDADEIRO. O lixo, em si, é sólido e não é um criadouro de mosquitos; porém quando recebe água de chuva, são formados inúmeros microcriadouros no interior de recipientes e entulhos. Os restos de alimentos em apodrecimento na água (matéria orgânica em decomposição) podem atrair as fêmeas para postura de ovos além de servir de alimentação das larvas.<br><br>III - VERDADEIRO. A água, em grande ou pequena quantidade, pobre (limpa) ou rica em matéria orgânica (suja), é uma condição obrigatória para que haja a postura ovos e desenvolvimento de mosquitos.<br><br>IV - FALSO. Conforme o item I.<br><br>V - FALSO. Estes locais oferecem várias possibilidades de criadouros e outros locais que acumulam água e tornam-se criadouros temporários. Além disso, estes locais agrupam um grande número de pessoas que, por si, já é um atrativo para os mosquitos e favorece a disseminação da doença, sobretudo em hospitais, onde muitas pessoas estão infectadas com arbovírus, por exemplo. Devemos sempre considerar se o local da transmissão foi no domicílio ou foi em outros locais onde a pessoa costuma passar um bom período de tempo, como escola-creche ou local de trabalho.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 12,
                        type: "singleAnswer",
                        enunciate: "Sobre a aplicação de diferentes métodos para o controle de mosquitos de insetos assinale a alternativa correta.",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "A limpeza de canais com dragagem e limpeza da vegetação das margens são métodos mecânicos e que têm pequeno efeito para a redução da proliferação de mosquitos.",
                                alternativeHint: "FALSO. Estes métodos classificados como mecânicos, físicos ou ambientais podem ter um grande impacto sobre a populações de mosquitos pois modificam o ambiente deixando-o desfavorável para o estabelecimento de mosquitos. Neste caso, por exemplo, a dragagem e limpeza de lixo e vegetação aquática promovem o fluxo de água impedindo a água represada nas margens que serve de criadouro para as larvas. A remoção da vegetação das margens desfavorece o abrigo de adultos e fonte de alimentação para os adultos. Em conclusão é um método que tem grande impacto para reduzir os mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "As ações de um programa integrado incluem ações-mecânicas, melhoria ambiental além de tratamento com larvicidas naqueles locais que não podem ser eliminados ou protegidos de mosquitos.",
                                alternativeHint: "VERDADEIRO. Hoje é altamente recomendável o uso de várias ações possíveis para combater os mosquitos. Deve ser levado em consideração que eliminar a fonte de criação de mosquito é a missão número 1, pois assim, o problema é eliminado na raiz. O tratamento de criadouros só deve ser mantido por longos períodos quando o local de criação não pode ser eliminado ou protegido. O efeito das ações ambientais e físicas é muito mais duradouro, tem baixo custo, evita os gastos com larvicidas e evita seu uso em água potável que, apesar de autorizado para alguns produtos, poderia ser evitado deixando a água com melhor qualidade e pureza.",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "A eficácia de larvicidas não depende do tratamento completo de criadouros na área, e a aplicação em pelo menos 50% dos criadouros é suficiente para causar uma grande redução dos mosquitos.",
                                alternativeHint: "FALSO. Uma condição essencial para que a ação com produtos larvicidas tenha sucesso é a cobertura total, ou mais próximo possível do total de criadouros que possam ser mapeados na área. Deixar parte dos criadouros não tratados é suficiente para ainda manter uma grande densidade populacional de mosquitos na área.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "A utilização de mosquiteiros e medidas de proteção pessoal com roupas/calcados não reduz transmissão dos patógenos pelos mosquitos.",
                                alternativeHint: "FALSO. Estas medidas são muito importantes e apesar de não eliminarem os mosquitos, impedem as picadas e, portanto, reduzem as chances de transmissão. Em locais onde não houve tempo, ou condições suficientes, para executar ações de controle de mosquitos, esta é a única alternativa viável a curto prazo para evitar a transmissão.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "A limpeza-ordenação (“Dez minutos contra a dengue”) do intradomicilio (interior) é mais importante do que a proteção-eliminação de criadouros do peridomicílio (quintal) das casas.",
                                alternativeHint: "FALSO. As ações nos dois ambientes (intra e peridomicílio) são igualmente importantes. Além dos domicílios todos os espaços devem receber ações de controle e vigilância, sobretudo espaços coletivos, como escolas, hospitais etc... A tolerância deve ser zero para as condições que permitem que os mosquitos se instalem, proliferem e piquem as pessoas.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 13,
                        type: "singleAnswer",
                        enunciate: "Sobre os agentes de controle de mosquito assinale a alternativa correta.",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "O pyriproxyfen pode ser usado nas armadilhas para atrair fêmeas e a postura de ovos neste local.",
                                alternativeHint: "FALSO. O pyriproxyfen não é um composto atraente de mosquitos e sim um agente de controle de mosquito da classe de IGR que age na fase de larvas interferindo em seu equilíbrio hormonal e impedindo que estas se desenvolvam até a fase adulta. A sua ação é muito eficaz, porém lenta. A sua seletividade é considerada moderada, pois, pode causar age em vários organismos que se desenvolvem fazendo mudas, tais como outros insetos e crustáceos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "As armadilhas tipo ovitrampas devem ser tratadas com larvicidas para evitar a eclosão das larvas.",
                                alternativeHint: "VERDADEIRO. Sim é essencial tratar as ovitrampas com larvicidas (Bti, temephos, pyriproxyfen) para evitar que elas se tornem criadouros. Atenção, pois se a armadilha for colocada em campo e recolhida em até sete dias, o tratamento com larvicida pode ser dispensado pois não haverá tempo para o desenvolvimento dos ovos até a fase adulta.",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Os larvicidas Bti, temephos e pyriproxyfen possuem o mesmo modo de ação nas larvas.",
                                alternativeHint: "FALSO. Todos os compostos citados atuam na fase jovem mas possuem modos de ação diferentes. O Bti é um agente biológico e suas toxinas devem ser ingeridas pelas larvas e sua ação ocorre no intestino; o temephos é um inseticida químico que age por contato e sua ação ocorre no sistema nervoso; o IGR-pyriproxyfen é um composto químico que age também por contato e atua no sistema endócrino das larvas e pupas não permitindo que ocorra muda para a fase adulta.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Produtos a base de diflubenzuron e Bti causam a morte das larvas cerca de 15 dias após a aplicação.",
                                alternativeHint: "FALSO. Estes são agentes de classes e modos de ação diferentes. O diflubenzuron é um IGR que age por contato ou ingestão, com ação no sistema endócrino do inseto impedindo a muda para a fase adulta. Sua ação é lenta, pois, desencadeia um desequilíbrio hormonal e os insetos permanecem nas fases de larvas ou de pupas e morrem em até 15 dias, sem atingir a fase adulta. A ação destes produtos é mensurada a partir de uma concentração que inibe 50 ou 90% da emergência para a fase adulta, chamada de concentração inibitória da emergência de adultos ao longo de um período de 15 dias. Já o Bti é um agente biológico, suas toxinas devem ser ingeridas, para então agirem no intestino das larvas e a mortalidade ocorre em até 24h. Para este composto, ou outros que causam a mortalidade direta dos indivíduos, a ação é mensurada através da concentração do produto capaz de matar 50 ou 90% dos indivíduos expostos.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Os produtos adulticidas são sempre aplicados em veículos, na forma de UBV, também conhecido como fumacê.",
                                alternativeHint: "FALSO. Estes produtos podem ser aplicados de várias maneiras  que incluem equipamentos como bombas costais para aplicação apenas em torno dos criadouros (perifocal), aplicação residual em paredes e ambiente das casas ou até sprays manuais em pequenos ambientes que são vendido para o público em geral. Ingredientes que têm ação adulticida também podem servir para preparar o ingrediente de iscas letais. Estas iscas são compostas de açúcar misturada com adulticidas. Elas são colocadas no ambiente e podem ser procuradas pelos adultos para servir de alimentação. A necessidade do uso de adulticidas deve ser avaliada com cuidado pois os adultos podem estar abrigados em locais de difícil acesso, portanto a aplicação espacial pode não atingí-los.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 14,
                        type: "singleAnswer",
                        enunciate: "Sobre métodos de controle de insetos que podem ser adotados a programas, assinale a resposta correta.",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "As estratégias de manejo ambiental em áreas urbanas são muito importantes para o controle populacional de <i>Aedes aegypti</i> e <i>Culex quinquefasciatus</i>.",
                                alternativeHint: "VERDADEIRO. Sim, as estratégias de manejo ambientais incluem a eliminação de criadouros, proteção de ambientes que possam se tornar criadouros, eliminando, portanto, as fontes de criação de mosquitos. Estas ações são consideradas essenciais pois vão eliminar o problema na “raiz”. Para <i>C. quinquefasciatus</i> estas ações podem ter um impacto muito forte pois os criadouros desta espécie podem ser mais facilmente localizados e geralmente concentram um grande número de larvas e pupas. A vedação correta de uma fossa séptica, por exemplo, pode eliminar grande parte da produção de <i>C. quinquefasciatus</i> de uma casa. Já as fêmeas de <i>Ae. aegypti</i> e <i>Aedes albopictus</i> têm uma preferência em fazer a postura de forma muito dispersa ou espalhada em vários locais no ambiente, incluindo os chamados “microcriadouros”. Estes podem ser objetos pequenos ou qualquer local que acumula água (ex. copos, garrafas, ralos e até badejas do motor de geladeiras...). Este comportamento de postura das fêmeas de <i>Aedes</i> espalha muito mais os ovos em diversos locais o que dificulta localizar, eliminar ou proteger todos eles. Ainda assim, deve-se fazer o máximo para eliminar ou proteger aqueles locais que podem servir como criadouros.",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "A densidade populacional de todos os mosquitos deve ser preferencialmente avaliada através da coleta de larvas e pupas.",
                                alternativeHint: "FALSO. O índice e método devem ser escolhidos de acordo com as espécies e as características da área. O índice a partir da coleta de larvas e pupas de <i>Aedes</i>, por exemplo, apesar de ser muito usado, hoje não é considerado um método adequado devido às razões descritas na questão anterior. Portanto para <i>Aedes</i>, a detecção de sua presença no ambiente através de armadilhas para ovos (ovitrampas) pode ser muito mais sensível. O comportamento de larvas de <i>Aedes</i> nos criadouros (elas podem passar muito tempo no fundo dos criadouros), dificulta a sua detecção por meio das conchadas (“dipping”) que é feita mais próxima da superfície da água. Em alguns programas de controle de <i>Aedes</i>, a coleta de larvas e pupas pode ser feita com equipamentos especiais, tipo aspiradores para água ou sifões que são usados para fazer a coleta no fundo dos recipientes. Mas, de uma forma geral, as armadilhas de ovos são consideradas os instrumentos mais sensíveis para detectar esta espécie no ambiente.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "O estabelecimento de um programa de controle deve ter início independente da densidade da população do vetor antes da intervenção.",
                                alternativeHint: "FALSO. Recomenda-se fazer um acompanhamento da densidade populacional antes da intervenção, por 12 meses se possível, para identificar as variações ao longo do ano. Isto porquê os programas devem ter início em um momento do ano em que a densidade populacional está baixa, para ter melhores resultados das ações. Iniciar as ações em momentos de alta densidade é uma situação mais crítica e que levará mais esforço para atingir e manter a redução desejada.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "O monitoramento da densidade populacional através qualquer dos índices entomológicos pode mostrar de forma correta e real as variações de densidade populacional e direcionar as ações de controle.",
                                alternativeHint: "FALSO. Apesar de ser possível usar diversos índices para a mesma espécie, há índices que podem ser mais sensíveis do que outros. Portanto é necessário avaliar as diferentes ferramentas para escolher aquela mais adequada para a espécie e o local onde estão sendo feitas as intervenções. O importante é que o índice consiga detectar a espécie da forma mais sensível através de formas jovens (ovo, larva, pupa) ou de adultos.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "A armadilha para captura de adulto é considerada a ferramenta ideal para coleta de mosquitos dos gêneros <i>Aedes</i>, <i>Culex</i> e <i>Anopheles</i>.",
                                alternativeHint: "FALSO. Tais armadilhas podem ser muito eficazes para coleta de <i>Culex quinquefasciatus</i>, porém, muitos trabalhos mostram limitações na coleta de <i>Aedes aegypti</i> cuja presença é muita mais facilmente detectada através de armadilhas para coleta de ovos. Para adultos de ambas as espécies também é possível fazer a coleta pelo método de aspiração ativa, que pode dar bons resultados. O uso de armadilhas para adultos é empregado em vários trabalhos de vigilância e controle de <i>Aedes</i>, porém, a capacidade de coleta pode ser limitada. Testes para a comparação do uso de armadilha de ovos e de adultos na mesma área podem demonstrar as diferentes capacidades destes instrumentos em detectar <i>Aedes</i> no ambiente.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 15,
                        type: "singleAnswer",
                        enunciate: "Armadilhas são ferramentas para atrair e coletar formas imaturas e/ou adultas dos mosquitos e são utilizadas para estimar a densidade populacional do inseto no ambiente. Dentre os diferentes modelos existentes, quais são as utilizadas para coletar mosquitos machos e fêmeas? Julgue as alternativas abaixo assinalando verdadeiro ou falso e assinale a alternativa correta.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "Armadilha tipo CDC; New Jersey; BR-OVT."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Aspirador; Capturador de castro; Ovitrampa."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "BG-Sentinel; New Jersey; CDC luminosa."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Capturador de castro; Ovitrampa; Armadilha tipo CDC."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Ovitrampa; BR-OVT; Box Gravid Trap."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, V",
                                alternativeHint: "I - Incorreta: A armadilha de oviposição BR-OVT foi desenvolvida para coletar ovos de <i>Culex quinquefasciatus</i>.<br><br>II - Incorreta: A armadilha ovitrampa foi desenvolvida para coletar ovos de <i>Aedes spp</i>.<br><br>III - Correta: Muito bem!<br><br>IV - Incorreta: O capturador de castro não é uma armadilha, é um instrumento utilizado na busca ATIVA de mosquitos adultos, portanto precisa ser manuseado por um operador. A ovitrampa é uma armadilha para coletar ovos de <i>Aedes spp.</i><br><br>V - Incorreta: A ovitrampa e a BR-OVT são armadilhas para coletar ovos de <i>Aedes spp.</i> e <i>Culex quinquefasciatus</i>. Enquanto que a armadilha Box Gravid Trap foi desenvolvida para coletar fêmeas grávidas de <i>Culex spp</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, F",
                                alternativeHint: "I - Incorreta: A armadilha de oviposição BR-OVT foi desenvolvida para coletar ovos de <i>Culex quinquefasciatus</i>.<br><br>II - Incorreta: A armadilha ovitrampa foi desenvolvida para coletar ovos de <i>Aedes spp</i>.<br><br>III - Correta: Muito bem!<br><br>IV - Incorreta: O capturador de castro não é uma armadilha, é um instrumento utilizado na busca ATIVA de mosquitos adultos, portanto precisa ser manuseado por um operador. A ovitrampa é uma armadilha para coletar ovos de <i>Aedes spp.</i><br><br>V - Incorreta: A ovitrampa e a BR-OVT são armadilhas para coletar ovos de <i>Aedes spp.</i> e <i>Culex quinquefasciatus</i>. Enquanto que a armadilha Box Gravid Trap foi desenvolvida para coletar fêmeas grávidas de <i>Culex spp</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, F",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, V, V, V, V",
                                alternativeHint: "I - Incorreta: A armadilha de oviposição BR-OVT foi desenvolvida para coletar ovos de <i>Culex quinquefasciatus</i>.<br><br>II - Incorreta: A armadilha ovitrampa foi desenvolvida para coletar ovos de <i>Aedes spp</i>.<br><br>III - Correta: Muito bem!<br><br>IV - Incorreta: O capturador de castro não é uma armadilha, é um instrumento utilizado na busca ATIVA de mosquitos adultos, portanto precisa ser manuseado por um operador. A ovitrampa é uma armadilha para coletar ovos de <i>Aedes spp.</i><br><br>V - Incorreta: A ovitrampa e a BR-OVT são armadilhas para coletar ovos de <i>Aedes spp.</i> e <i>Culex quinquefasciatus</i>. Enquanto que a armadilha Box Gravid Trap foi desenvolvida para coletar fêmeas grávidas de <i>Culex spp</i>.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, V",
                                alternativeHint: "I - Incorreta: A armadilha de oviposição BR-OVT foi desenvolvida para coletar ovos de <i>Culex quinquefasciatus</i>.<br><br>II - Incorreta: A armadilha ovitrampa foi desenvolvida para coletar ovos de <i>Aedes spp</i>.<br><br>III - Correta: Muito bem!<br><br>IV - Incorreta: O capturador de castro não é uma armadilha, é um instrumento utilizado na busca ATIVA de mosquitos adultos, portanto precisa ser manuseado por um operador. A ovitrampa é uma armadilha para coletar ovos de <i>Aedes spp.</i><br><br>V - Incorreta: A ovitrampa e a BR-OVT são armadilhas para coletar ovos de <i>Aedes spp.</i> e <i>Culex quinquefasciatus</i>. Enquanto que a armadilha Box Gravid Trap foi desenvolvida para coletar fêmeas grávidas de <i>Culex spp</i>.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 16,
                        type: "singleAnswer",
                        enunciate: "Com relação ao Aspirador e ao Capturador de Castro utilizados para a amostragem de insetos, é correto afirmar que:",
                        rightAlternativeId: 4,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "São considerados armadilhas para a coleta de fêmeas grávidas de <i>Culex spp.</i> e <i>Aedes spp.</i> e para ambos a atratividade está baseada em fatores físicos."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "São considerados armadilhas para a coleta de ovos de <i>Aedes spp</i>."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "São instrumentos utilizados para a busca ativa e captura de mosquitos adultos, por sucção, em repouso ou em atividade."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "São considerados armadilhas para a captura de várias espécies de mosquitos adultos, cujo princípio de atratividade é uma fonte luminosa."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "São instrumentos utilizados apenas para a busca passiva e coleta de mosquitos adultos, por sucção, em seus locais de repouso."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II e III estão corretas",
                                alternativeHint: "I - Incorreta: Aspirador e Capturador de Castro não são armadilhas, são instrumentos utilizados na busca ativa e captura de mosquitos adultos por sucção.<br><br>II - Incorreta: Armadilhas para coleta de ovos são, por exemplo, as ovitrampas e BR-OVT.<br><br>III - Correta. Muito bem!<br><br>IV - Incorreta: Armadilhas de atração luminosa, como CDC e New Jersey, coletam mosquitos adultos, especialmente fêmeas, com hábito alimentar noturno.<br><br>V - Incorreta: São instrumentos utilizados para a busca e captura ATIVA de mosquitos adultos, por sucção, tanto os que estão em atividade de voo quanto os que estão em locais de repouso.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "III está incorreta",
                                alternativeHint: "I - Incorreta: Aspirador e Capturador de Castro não são armadilhas, são instrumentos utilizados na busca ativa e captura de mosquitos adultos por sucção.<br><br>II - Incorreta: Armadilhas para coleta de ovos são, por exemplo, as ovitrampas e BR-OVT.<br><br>III - Correta. Muito bem!<br><br>IV - Incorreta: Armadilhas de atração luminosa, como CDC e New Jersey, coletam mosquitos adultos, especialmente fêmeas, com hábito alimentar noturno.<br><br>V - Incorreta: São instrumentos utilizados para a busca e captura ATIVA de mosquitos adultos, por sucção, tanto os que estão em atividade de voo quanto os que estão em locais de repouso.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "I, II, IV e V estão corretas",
                                alternativeHint: "I - Incorreta: Aspirador e Capturador de Castro não são armadilhas, são instrumentos utilizados na busca ativa e captura de mosquitos adultos por sucção.<br><br>II - Incorreta: Armadilhas para coleta de ovos são, por exemplo, as ovitrampas e BR-OVT.<br><br>III - Correta. Muito bem!<br><br>IV - Incorreta: Armadilhas de atração luminosa, como CDC e New Jersey, coletam mosquitos adultos, especialmente fêmeas, com hábito alimentar noturno.<br><br>V - Incorreta: São instrumentos utilizados para a busca e captura ATIVA de mosquitos adultos, por sucção, tanto os que estão em atividade de voo quanto os que estão em locais de repouso.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V está correta",
                                alternativeHint: "I - Incorreta: Aspirador e Capturador de Castro não são armadilhas, são instrumentos utilizados na busca ativa e captura de mosquitos adultos por sucção.<br><br>II - Incorreta: Armadilhas para coleta de ovos são, por exemplo, as ovitrampas e BR-OVT.<br><br>III - Correta. Muito bem!<br><br>IV - Incorreta: Armadilhas de atração luminosa, como CDC e New Jersey, coletam mosquitos adultos, especialmente fêmeas, com hábito alimentar noturno.<br><br>V - Incorreta: São instrumentos utilizados para a busca e captura ATIVA de mosquitos adultos, por sucção, tanto os que estão em atividade de voo quanto os que estão em locais de repouso.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "I, III, IV e V estão incorretas",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            }
                        ]
                    },
                    {
                        questionId: 17,
                        type: "singleAnswer",
                        enunciate: "Entre as alternativas abaixo, assinale aquela que descreve como deve ser utilizada uma estação disseminadora (ED) de inseticida para o controle de <i>Aedes aegypti</i> e <i>Culex quinquefasciatus.</i>",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Uma ED deve ser instalada no intradomicílio para matar larvas e pupas dos mosquitos.",
                                alternativeHint: "Incorreta. A ED deve ser instalada no peridomicílio para que ocorra a dispersão do produto inseticida, pelos próprios mosquitos para seus criadouros.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Uma ED deve ser instalada no peridomicílio pois, conta com a participação ativa dos mosquitos para a dispersão de partículas de um potente inseticida, como o pyrirpoxyfen, para o controle de larvas e pupas em seus criadouros.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "A ED deve ser utilizada em associação com o aspirador, pois os mosquitos adultos precisam ser capturados no intra e no peridomicílio.",
                                alternativeHint: "Incorreta. A ED não deve ser utilizada em associação com o aspirador na área peridomiciliar, pois esse instrumento captura os mosquitos adultos do ambiente, o que poderia reduzir a dispersão do inseticida para os criadouros. A efetividade da ED depende do comportamento de oviposição das diferentes espécies de mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "A ED deve ser utilizada, casa a casa, para a coleta e destruição de uma grande quantidade de ovos para promover o controle populacional dos mosquitos.",
                                alternativeHint: "Incorreta. O uso da ED casa a casa seria desnecessário, visto que os mosquitos podem se deslocar ativamente para casas vizinhas carregando o inseticida para outros criadouros. Portanto, sua instalação, mesmo em aglomerados urbanos, deve ser espaçada. Além disso, sua função é favorecer a disseminação de inseticidas pelos mosquitos para ampliar a cobertura do tratamento de criadouros e não para recolher ovos de mosquito do ambiente, tal como as ovitrampas-controle.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "ED é utilizada para o monitoramento de larvas e pupas de mosquitos, quando instaladas no peridomicílio.",
                                alternativeHint: "Incorreta. A ED é uma ferramenta utilizada para o controle de mosquitos e não para o acompanhamento de suas flutuações populacionais.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 18,
                        type: "singleAnswer",
                        enunciate: "A vigilância entomológica faz o acompanhamento contínuo de uma espécie em função da sua relação com a transmissão de doenças ou agravos ao homem. Com isso em mente, assinale abaixo o que for correto.",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "<i>Ae. aegypti</i> é uma espécie de grande importância no Brasil, porque está presente na maioria dos municípios, especialmente naqueles com casos de dengue, Zika e chikungunya."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Os fatores ambientais, ecológicos e entomológicos que aumentam o risco de transmissão das arboviroses, malária e filariose são os mesmos em todos os lugares. Logo só podemos diminuir os casos destas doenças através de vacinas."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "No Brasil, algumas espécies de anofelinos podem ter papel na transmissão da malária, embora <i>An. darlingi</i> seja a mais importante delas na região amazônica."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Embora o mosquito <i>Cx. quinquefasciatus</i> esteja presente na maioria dos municípios brasileiros, não existe um Programa nacional para seu controle, diferente do mosquito <i>Ae. aegypti</i>."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "<i>Cx. quinquefasciatus</i> está presente em grande quantidade nos ambientes urbanos, especialmente nas áreas com condições precárias de saneamento básico, inclusive naquelas onde ocorre a transmissão da filariose ou elefantíase."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "I, II, IV e V ",
                                alternativeHint: "I - CORRETA. O mosquito <i>Aedes aegypti</i> está presente no Brasil desde a década de 1970.<br><br>II - INCORRETA. Tais fatores podem variar bastante de uma área para outra, embora alguns como a sazonalidade possam influenciar a densidade populacional da maioria das espécies de mosquitos. Além disso, não existem vacinas para a maioria destas doenças.<br><br>III - CORRETA. Na região amazônica, o mosquito <i>Anopheles darlingi</i> é o vetor primário dos protozoários causadores das malárias no Brasil.<br><br>IV - CORRETA. A vigilância e o controle regular do mosquito <i>Culex quinquefasciatus</i> ocorrem em poucos municípios brasileiros, com destaque para alguns da Região Metropolitana do Recife/PE, onde essa espécie está envolvida com a transmissão da filariose linfática.<br><br>V - CORRETA. O mosquito <i>Culex quinquefasciatus</i> se desenvolve, principalmente, em criadouros com elevada quantidade de matéria orgânica, como fossas, esgoto a céu aberto, córregos e  sistemas de esgotamento sanitário sem manutenção.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "I, III, IV e V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "I, II, III e V",
                                alternativeHint: "I - CORRETA. O mosquito <i>Aedes aegypti</i> está presente no Brasil desde a década de 1970.<br><br>II - INCORRETA. Tais fatores podem variar bastante de uma área para outra, embora alguns como a sazonalidade possam influenciar a densidade populacional da maioria das espécies de mosquitos. Além disso, não existem vacinas para a maioria destas doenças.<br><br>III - CORRETA. Na região amazônica, o mosquito <i>Anopheles darlingi</i> é o vetor primário dos protozoários causadores das malárias no Brasil.<br><br>IV - CORRETA. A vigilância e o controle regular do mosquito <i>Culex quinquefasciatus</i> ocorrem em poucos municípios brasileiros, com destaque para alguns da Região Metropolitana do Recife/PE, onde essa espécie está envolvida com a transmissão da filariose linfática.<br><br>V - CORRETA. O mosquito <i>Culex quinquefasciatus</i> se desenvolve, principalmente, em criadouros com elevada quantidade de matéria orgânica, como fossas, esgoto a céu aberto, córregos e  sistemas de esgotamento sanitário sem manutenção.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "II, III, IV e V",
                                alternativeHint: "I - CORRETA. O mosquito <i>Aedes aegypti</i> está presente no Brasil desde a década de 1970.<br><br>II - INCORRETA. Tais fatores podem variar bastante de uma área para outra, embora alguns como a sazonalidade possam influenciar a densidade populacional da maioria das espécies de mosquitos. Além disso, não existem vacinas para a maioria destas doenças.<br><br>III - CORRETA. Na região amazônica, o mosquito <i>Anopheles darlingi</i> é o vetor primário dos protozoários causadores das malárias no Brasil.<br><br>IV - CORRETA. A vigilância e o controle regular do mosquito <i>Culex quinquefasciatus</i> ocorrem em poucos municípios brasileiros, com destaque para alguns da Região Metropolitana do Recife/PE, onde essa espécie está envolvida com a transmissão da filariose linfática.<br><br>V - CORRETA. O mosquito <i>Culex quinquefasciatus</i> se desenvolve, principalmente, em criadouros com elevada quantidade de matéria orgânica, como fossas, esgoto a céu aberto, córregos e  sistemas de esgotamento sanitário sem manutenção.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Todas estão corretas",
                                alternativeHint: "I - CORRETA. O mosquito <i>Aedes aegypti</i> está presente no Brasil desde a década de 1970.<br><br>II - INCORRETA. Tais fatores podem variar bastante de uma área para outra, embora alguns como a sazonalidade possam influenciar a densidade populacional da maioria das espécies de mosquitos. Além disso, não existem vacinas para a maioria destas doenças.<br><br>III - CORRETA. Na região amazônica, o mosquito <i>Anopheles darlingi</i> é o vetor primário dos protozoários causadores das malárias no Brasil.<br><br>IV - CORRETA. A vigilância e o controle regular do mosquito <i>Culex quinquefasciatus</i> ocorrem em poucos municípios brasileiros, com destaque para alguns da Região Metropolitana do Recife/PE, onde essa espécie está envolvida com a transmissão da filariose linfática.<br><br>V - CORRETA. O mosquito <i>Culex quinquefasciatus</i> se desenvolve, principalmente, em criadouros com elevada quantidade de matéria orgânica, como fossas, esgoto a céu aberto, córregos e  sistemas de esgotamento sanitário sem manutenção.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 19,
                        type: "singleAnswer",
                        enunciate: "Sobre os Índices que utilizam a fase de ovo como indicador assinale a sentença CORRETA:",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "O índice de Densidade de Ovos (IDO) é indicado para acompanhar a variação da densidade populacional de <i>Ae. aegypti</i>;",
                                alternativeHint: "CORRETA. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "O IDO pode ser calculado através do número de fêmeas “grávidas” coletadas nas armadilhas CDC;",
                                alternativeHint: "INCORRETA. O IDO só pode ser calculado a partir dos ovos depositados em substratos, como em palhetas, por exemplo.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "A presença de apenas um ovo de <i>Ae. aegypti</i> posto em uma armadilha, indica que uma fêmea visitou aquela armadilha, assim concluímos que 300 fêmeas visitaram uma armadilha que contém 300 ovos dessa espécie.",
                                alternativeHint: "INCORRETA. A relação entre o número de ovos e de fêmeas não é diretamente proporcional, visto que uma mesma fêmea pode depositar mais de um ovo em cada armadilha.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "É possível fazer o monitoramento de <i>Cx. quinquefasciatus</i> através da coleta de ovos por armadilhas como a CDC-luminosa ou a BR-OVT, embora os indicadores mais empregados sejam a DLP e a DA.",
                                alternativeHint: "INCORRETA. O monitoramento de <i>Cx. quinquefasciatus</i> através da coleta de ovos se faz a partir da armadilha BR-OVT, enquanto a armadilha CDC luminosa é utilizada para coletar mosquitos adultos.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "O ovo é o indicador mais utilizado para estimar a densidade populacional dos anofelinos.",
                                alternativeHint: "INCORRETA. Os anofelinos depositam seus ovos isoladamente na superfície da água, portanto seria difícil recuperar estes ovos dos criadouros. Assim, larvas ou adultos seriam  indicadores mais adequados para estimar a densidade populacional desses mosquitos.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 20,
                        type: "singleAnswer",
                        enunciate: "Existem diferentes instrumentos usados para coletar/capturar mosquitos. Assinale a alternativa que correlaciona a fase de vida e o instrumento adotado para as espécies, respectivamente. <i>Cx. quinquefasciatus</i> – <i>An. darlingi</i> – <i>Ae. aegypti</i> – <i>Ae. albopictus</i>",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Ovos/ovitrampa; ovos/BR-OVT; larvas e pupas/conchada; adultos/Armadilha CDC.",
                                alternativeHint: "INCORRETA. Para a coleta de ovos de <i>Cx. quinquefasciatus</i>/BR-OVT; ovos de <i>An. darlingi</i>/conchada; larvas e pupas de <i>Ae. aegypti</i>/pipeta.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Ovos/BR-OVT; larvas e pupas/pipeta; ovos/ovitrampa; adultos/aspirador.",
                                alternativeHint: "INCORRETA. Para a coleta de larvas e pupas de <i>An. darlingi</i>/conchada.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Adultos/armadilha CDC; larvas e pupas/conchada – adultos/armadilha BG-Sentinel; ovos/ovitrampas.",
                                alternativeHint: "CORRETA. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Larvas e pupas/conchada; ovos/aspiração; adultos/aspiração; ovos/ovitrampa.",
                                alternativeHint: "INCORRETA. Para a coleta de ovos de <i>An. darlingi</i>/conchada.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Ovos/BR-OVT; adultos/armadilha CDC; larvas e pupas/pipeta; adultos/armadilha CDC luminosa.",
                                alternativeHint: "INCORRETA. Para a coleta de adultos de <i>Ae. albopictus</i>/armadilha CDC ou aspirador",
                                hintClass: "warning"
                            }
                        ]
                    }
                ]
            },
            {
                unitName: "Unidade 3",
                unitId: 3,
                unitQuestions: [
                    {
                        questionId: 21,
                        type: "singleAnswer",
                        enunciate: "O que a infecção pela bactéria <i>Wolbachia</i> pode causar aos mosquitos?",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Proliferação de outras bactérias que estão no corpo do mosquito.",
                                alternativeHint: "Incorreta. Até onde sabemos a infecção por <i>Wolbachia</i> não perturba as outras bactérias que estão presentes no corpo do mosquito, porque a mesma vive dentro das células dos mosquitos e assim não interage com outras bactérias.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Eliminar todos os microrganismos presentes no corpo do mosquito.",
                                alternativeHint: "Incorreta. A infecção por <i>Wolbachia</i> pode diminuir a presença de alguns microorganismos, como por exemplo os vírus Dengue e Zika.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Causar a morte embrionária dos descendentes ou bloquear a replicação de determinado vírus.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Aumentar a frequência de picadas pelos mosquitos.",
                                alternativeHint: "Incorreto. A infecção por <i>Wolbachia</i> gera consequências na reprodução do mosquito, mas não afeta outros comportamentos como o ato de picar.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Induzir a proliferação de mosquitos no ambiente urbano.",
                                alternativeHint: "Incorreta. A infecção por <i>Wolbachia</i> promove a diminuição do tempo de vida dos mosquitos e, consequentemente, reduz também sua densidade populacional a longo prazo.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 22,
                        type: "singleAnswer",
                        enunciate: "Como o sequenciamento de DNA pode auxiliar no controle de mosquitos?",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Através da detecção de mosquitos resistentes a inseticidas.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Através da detecção do estágio de vida do mosquito presente na natureza.",
                                alternativeHint: "Incorreta. O sequenciamento de DNA não permite diferenciar os estágios de desenvolvimento do mosquito, mas é capaz de identificar diferentes espécies.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Através da indução de mutações nos mosquitos resistentes.",
                                alternativeHint: "Incorreto. O sequenciamento de DNA, apesar de poder ser usado para detectar se a manipulação genética ocorreu como o esperado, o mesmo não é utilizado para modificar geneticamente os mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Através da infecção por <i>Wolbachia</i>.",
                                alternativeHint: "Incorreta. O sequenciamento de DNA não pode ser utilizado para infectar os mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Através da diminuição populacional do vetor.",
                                alternativeHint: "Incorreta. O sequenciamento de DNA não pode ser utilizado diretamente para reduzir o tamanho populacional do vetor.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 23,
                        type: "singleAnswer",
                        enunciate: "Foram conquistados grandes avanços no desenvolvimento de tecnologias que diminuem as populações de mosquitos ou reduzem sua habilidade para a transmissão vetorial de patógenos. Quanto aos métodos que promovem modificações nos mosquitos, classifique as afirmativas abaixo como verdadeiras ou falsas e assinale a alternativa correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A transgenia corresponde à modificação do DNA dos mosquitos através da inserção de genes estranhos na sua linhagem germinativa (geralmente ovos não embrionados);"
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A paratransgenia visa infectar naturalmente mosquitos com a <i>Wolbachia</i> modificada geneticamente com o intuito de utilizar a multiplicidade de interações entre a bactéria simbionte e o inseto para o controle das populações de mosquito;"
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A técnica do inseto estéril (TIE) se baseia na utilização de radiação ionizante para esterilização de pupas-macho;"
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "A bactéria <i>Wolbachia</i> é modificada geneticamente para induzir e dispersar genes que exclusivamente bloqueiam a transmissão de patógenos em populações naturais de insetos vetores;"
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "A técnica do inseto incompatível (TII) se baseia na mortalidade embrionária dos descendentes de cruzamento de insetos infectados por linhagens diferentes de <i>Wolbachia</i> devido à incompatibilidade citoplasmática (IC)."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, F",
                                alternativeHint: "I - Falsa. A transgenia corresponde à modificação do DNA dos mosquitos através da inserção de genes estranhos na sua linhagem germinativa (geralmente ovos embrionados).<br><br>II - Falsa. A paratransgenia visa infectar <u>artificialmente</u> mosquitos com a <i>Wolbachia</i> modificada geneticamente com o intuito de utilizar a multiplicidade de interações entre a bactéria simbionte e o inseto para o controle das populações de mosquito.<br><br>III - Verdadeira. Radiações ionizantes, como raios gama e X, são tradicionalamente utilizadas para a produção de machos estéreis, cuja irradiação pode ocorrer na fase de pupa ou de adulto.<br><br>IV - Falsa. A bactéria <i>Wolbachia</i> é modificada geneticamente para induzir e dispersar genes <u>que bloqueiam</u> a transmissão de patógenos em populações naturais de insetos vetores <u>e também pode ser utilizada para a redução populacional de vetores por Incompatibilidade Citoplasmática (IC)</u>.<br><br>V - Verdadeira. A técnica do inseto incompatível (TII) se baseia na mortalidade embrionária dos descendentes de cruzamento de insetos infectados por linhagens diferentes de <i>Wolbachia</i> devido a incompatibilidade citoplasmática (IC).",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, V",
                                alternativeHint: "I - Falsa. A transgenia corresponde à modificação do DNA dos mosquitos através da inserção de genes estranhos na sua linhagem germinativa (geralmente ovos embrionados).<br><br>II - Falsa. A paratransgenia visa infectar <u>artificialmente</u> mosquitos com a <i>Wolbachia</i> modificada geneticamente com o intuito de utilizar a multiplicidade de interações entre a bactéria simbionte e o inseto para o controle das populações de mosquito.<br><br>III - Verdadeira. Radiações ionizantes, como raios gama e X, são tradicionalamente utilizadas para a produção de machos estéreis, cuja irradiação pode ocorrer na fase de pupa ou de adulto.<br><br>IV - Falsa. A bactéria <i>Wolbachia</i> é modificada geneticamente para induzir e dispersar genes <u>que bloqueiam</u> a transmissão de patógenos em populações naturais de insetos vetores <u>e também pode ser utilizada para a redução populacional de vetores por Incompatibilidade Citoplasmática (IC)</u>.<br><br>V - Verdadeira. A técnica do inseto incompatível (TII) se baseia na mortalidade embrionária dos descendentes de cruzamento de insetos infectados por linhagens diferentes de <i>Wolbachia</i> devido a incompatibilidade citoplasmática (IC).",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, V, F, V, V",
                                alternativeHint: "I - Falsa. A transgenia corresponde à modificação do DNA dos mosquitos através da inserção de genes estranhos na sua linhagem germinativa (geralmente ovos embrionados).<br><br>II - Falsa. A paratransgenia visa infectar <u>artificialmente</u> mosquitos com a <i>Wolbachia</i> modificada geneticamente com o intuito de utilizar a multiplicidade de interações entre a bactéria simbionte e o inseto para o controle das populações de mosquito.<br><br>III - Verdadeira. Radiações ionizantes, como raios gama e X, são tradicionalamente utilizadas para a produção de machos estéreis, cuja irradiação pode ocorrer na fase de pupa ou de adulto.<br><br>IV - Falsa. A bactéria <i>Wolbachia</i> é modificada geneticamente para induzir e dispersar genes <u>que bloqueiam</u> a transmissão de patógenos em populações naturais de insetos vetores <u>e também pode ser utilizada para a redução populacional de vetores por Incompatibilidade Citoplasmática (IC)</u>.<br><br>V - Verdadeira. A técnica do inseto incompatível (TII) se baseia na mortalidade embrionária dos descendentes de cruzamento de insetos infectados por linhagens diferentes de <i>Wolbachia</i> devido a incompatibilidade citoplasmática (IC).",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, F, V",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I - Falsa. A transgenia corresponde à modificação do DNA dos mosquitos através da inserção de genes estranhos na sua linhagem germinativa (geralmente ovos embrionados).<br><br>II - Falsa. A paratransgenia visa infectar <u>artificialmente</u> mosquitos com a <i>Wolbachia</i> modificada geneticamente com o intuito de utilizar a multiplicidade de interações entre a bactéria simbionte e o inseto para o controle das populações de mosquito.<br><br>III - Verdadeira. Radiações ionizantes, como raios gama e X, são tradicionalamente utilizadas para a produção de machos estéreis, cuja irradiação pode ocorrer na fase de pupa ou de adulto.<br><br>IV - Falsa. A bactéria <i>Wolbachia</i> é modificada geneticamente para induzir e dispersar genes <u>que bloqueiam</u> a transmissão de patógenos em populações naturais de insetos vetores <u>e também pode ser utilizada para a redução populacional de vetores por Incompatibilidade Citoplasmática (IC)</u>.<br><br>V - Verdadeira. A técnica do inseto incompatível (TII) se baseia na mortalidade embrionária dos descendentes de cruzamento de insetos infectados por linhagens diferentes de <i>Wolbachia</i> devido a incompatibilidade citoplasmática (IC).",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 24,
                        type: "singleAnswer",
                        enunciate: "A técnica do inseto estéril (TIE) foi desenvolvida em 1950 e está relacionada à liberação de machos estéreis para copular com fêmeas selvagens visando o controle populacional de algumas espécies de vetores. Classifique as afirmativas abaixo como verdadeiras ou falsas e assinale a alternativa correta.",
                        rightAlternativeId: 2,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A exposição a qualquer dose de radiação ionizante leva à esterilização total dos mosquitos, machos ou fêmeas."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A TIE apresenta desvantagens em relação ao uso de inseticidas químicos no controle populacional de mosquitos, pois deixa resíduos tóxicos no ambiente que atingem outras espécies não alvo."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "A TIE é uma técnica espécie-específica justamente por estar relacionada às questões reprodutivas, uma vez que o macho procura as fêmeas de sua espécie para acasalar."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Para a execução dessa técnica é necessária a criação em massa de insetos machos, seguida da esterilização e liberação continuada de um grande número de indivíduos, no menor intervalo de tempo possível entre as solturas, em uma área infestada."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Vários estudos têm comprovado o potencial da TIE para uso efetivo em programas integrados de manejo de espécies de insetos, inclusive mosquitos. Apesar desta técnica ser incompatível com a utilização concomitante de compostos com ação larvicida e/ou ovicida."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, V, V",
                                alternativeHint: "I - Falsa. Doses baixas de radição ionizantes (<10 Gy) não promovem a esterilização de mosquitos, elas podem apenas reduzir o potencial reprodutivo dos mesmos.<br><br>II - Falsa. A TIE é um método de controle genético ambientalmente seguro e espécie-específico, enquanto que os inseticidas químicos podem atingir diversos organismos não alvo, inclusive o homem, e ainda deixar resíduos tóxicos no ambiente.<br><br>III - Verdadeira. A TIE é uma das técnicas mais efetiva para o controle genético de insetos, sobretudo, para aquelas espécies que realizam poucas cópulas durante sua vida.<br><br>IV - Verdadeira. O sucesso da TIE depende da quantidade, capacidade competitiva dos machos estéreis para o acasalamento com fêmeas selvagens, e sua presença constante nas áreas de liberação, para a garantir a supressão populacional geração a geração.<br><br>V - Falsa. A TIE pode ser associada ao uso de ações/agentes de controle com efeito ovicida, larvicida e adulticida.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, F",
                                alternativeHint: "I - Falsa. Doses baixas de radição ionizantes (<10 Gy) não promovem a esterilização de mosquitos, elas podem apenas reduzir o potencial reprodutivo dos mesmos.<br><br>II - Falsa. A TIE é um método de controle genético ambientalmente seguro e espécie-específico, enquanto que os inseticidas químicos podem atingir diversos organismos não alvo, inclusive o homem, e ainda deixar resíduos tóxicos no ambiente.<br><br>III - Verdadeira. A TIE é uma das técnicas mais efetiva para o controle genético de insetos, sobretudo, para aquelas espécies que realizam poucas cópulas durante sua vida.<br><br>IV - Verdadeira. O sucesso da TIE depende da quantidade, capacidade competitiva dos machos estéreis para o acasalamento com fêmeas selvagens, e sua presença constante nas áreas de liberação, para a garantir a supressão populacional geração a geração.<br><br>V - Falsa. A TIE pode ser associada ao uso de ações/agentes de controle com efeito ovicida, larvicida e adulticida.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "F, F, V, V, F",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, F, V",
                                alternativeHint: "I - Falsa. Doses baixas de radição ionizantes (<10 Gy) não promovem a esterilização de mosquitos, elas podem apenas reduzir o potencial reprodutivo dos mesmos.<br><br>II - Falsa. A TIE é um método de controle genético ambientalmente seguro e espécie-específico, enquanto que os inseticidas químicos podem atingir diversos organismos não alvo, inclusive o homem, e ainda deixar resíduos tóxicos no ambiente.<br><br>III - Verdadeira. A TIE é uma das técnicas mais efetiva para o controle genético de insetos, sobretudo, para aquelas espécies que realizam poucas cópulas durante sua vida.<br><br>IV - Verdadeira. O sucesso da TIE depende da quantidade, capacidade competitiva dos machos estéreis para o acasalamento com fêmeas selvagens, e sua presença constante nas áreas de liberação, para a garantir a supressão populacional geração a geração.<br><br>V - Falsa. A TIE pode ser associada ao uso de ações/agentes de controle com efeito ovicida, larvicida e adulticida.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, F, F",
                                alternativeHint: "I - Falsa. Doses baixas de radição ionizantes (<10 Gy) não promovem a esterilização de mosquitos, elas podem apenas reduzir o potencial reprodutivo dos mesmos.<br><br>II - Falsa. A TIE é um método de controle genético ambientalmente seguro e espécie-específico, enquanto que os inseticidas químicos podem atingir diversos organismos não alvo, inclusive o homem, e ainda deixar resíduos tóxicos no ambiente.<br><br>III - Verdadeira. A TIE é uma das técnicas mais efetiva para o controle genético de insetos, sobretudo, para aquelas espécies que realizam poucas cópulas durante sua vida.<br><br>IV - Verdadeira. O sucesso da TIE depende da quantidade, capacidade competitiva dos machos estéreis para o acasalamento com fêmeas selvagens, e sua presença constante nas áreas de liberação, para a garantir a supressão populacional geração a geração.<br><br>V - Falsa. A TIE pode ser associada ao uso de ações/agentes de controle com efeito ovicida, larvicida e adulticida.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 21,
                        type: "singleAnswer",
                        enunciate: "Considerando a estratégia de liberação de mosquitos infectados com <i>Wolbachia</i> atualmente aplicada em diferentes cidades brasileiras, marque a alternativa correta.",
                        rightAlternativeId: 3,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "As larvas são bastante utilizadas para a liberação de indivíduos infectados com <i>Wolbachia</i> no ambiente.",
                                alternativeHint: "Incorreta. A fase mais utilizada é a adulta.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Está ocorrendo a liberação, apenas, de machos adultos infectados com <i>Wolbachia</i>.",
                                alternativeHint: "Incorreta. Mosquitos adultos dos dois sexos estão sendo liberados, no ambiente, através nessa estratégia.",
                                hintClass: "warning"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Está sendo feita liberação de machos adultos esterilizados através da infecção por <i>Wolbachia</i>.",
                                alternativeHint: "Incorreta. A efetividade desta técnica depende do uso de uma linhagem específica da <i>Wolbachia</i>, como a Wmelpop, que depende de machos sexualmente viáveis para cruzar com as fêmeas selvagens e transferir a bactéria.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Está sendo feita liberação de mosquitos machos e fêmeas infectados com <i>Wolbachia</i>.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Como complementação é feita a liberação de larvas do estádio L3 do sexo masculino infectadas por <i>Wolbachia</i>.",
                                alternativeHint: "Incorreta. Nenhuma estratégia atual se baseia na liberação de larvas infectadas com <i>Wolbachia</i>.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 22,
                        type: "singleAnswer",
                        enunciate: "O que é a técnica de xenomonitoramento molecular?",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "Uma abordagem utilizada para eliminar foco de proliferação dos mosquitos.",
                                alternativeHint: "Incorreta. O xenomonitoramento molecular não é uma metodologia para controle populacional de mosquitos.",
                                hintClass: "warning"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "Uma estratégia para monitoramento de patógenos que estão circulando em mosquitos vetores.",
                                alternativeHint: "Correta. Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "Um método de dispersão de inseticidas.",
                                alternativeHint: "Incorreta. O xenomonitoramento não é uma abordagem para o controle de mosquitos baseada na disseminação de inseticidas.",
                                hintClass: "warning"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "Uma armadilha para captura de mosquitos imaturos.",
                                alternativeHint: "Incorreta. O xenomonitoramento não é uma ferramenta para capturar mosquitos, mas uma estratégia mais ampla para vigilância da infecção vetorial.",
                                hintClass: "warning"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "Uma característica comportamental dos mosquitos.",
                                alternativeHint: "Incorreta. O xenomonitoramento não é um comportamento de mosquitos, e sim uma estratégia de vigilância utilizando mosquitos.",
                                hintClass: "warning"
                            }
                        ]
                    },
                    {
                        questionId: 23,
                        type: "singleAnswer",
                        enunciate: "A técnica de mosquitos transgênicos é uma metodologia de modificação de mosquitos promissora, com potencial para complementar as alternativas de controle já existentes, podendo ser usada como uma ferramenta integrada para o controle de doenças. Classifique as afirmativas abaixo como verdadeiras ou falsas e assinale a alternativa correta.",
                        rightAlternativeId: 1,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "“Gene <i>drive mechanism</i>” visa introduzir um traço genético em mosquitos vetores, ou seja, genes que podem conferir, por exemplo, que a população selvagem do mosquito adquira resistência à infecção por patógenos."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "Dois pontos importantes que devem ser levados em consideração para se obter sucesso na metodologia de transgenia são: <br>1 - O traço genético inserido tem que conferir ao mosquito transgênico um fitness vantajoso ou neutro; <br>2 - A substituição da população deve ocorrer dentro de um período relativamente longo de tempo."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Existem dois caminhos para o uso de mosquitos geneticamente modificados no controle de doenças. O primeiro seria a supressão da população selvagem e o segundo, mais inovador, seria a substituição desta população. A supressão populacional seria a alternativa mais viável."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "Independente do tipo de abordagem para a produção dos mosquitos geneticamente modificados, além dos genes efetores da condição desejada (por exemplo - gene letal que impede que a prole chegue à fase adulta) também podem ser adicionados genes repórteres de fluorescência, para facilitar sua diferenciação dos mosquitos selvagens."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "Esta abordagem já vem sendo utilizada em algumas cidades no Brasil com resultados promissores para o controle do mosquito <i>Ae. aegypti</i> e estudos adicionais não são necessários para avaliar o custo-benefício de sua implementação em larga escala."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, F",
                                alternativeHint: "I - Verdadeira. Sim, alguns genes introduzidos nos mosquitos podem conferir, por exemplo, uma redução em sua competência vetorial a patógenos, como o vírus Dengue e plasmódios.<br><br>II - Falsa. O sucesso na metodologia de transgenia será maior quanto menor for o <u>tempo</u> necessário para a substituição da população selvagem do mosquito.<br><br>III - Falsa. A substituição populacional seria a alternativa mais viável, porque ela é autossustentável ao longo do tempo, diferente da supressão que requer a liberação contínua dos mosquitos geneticamente modificados.<br><br>IV - Verdadeira. Algumas linhagens de mosquitos transgênicos, como a <i>Aedes aegypti</i> OX513A, portadora de gene letal, apresenta também fluorescência verde em sua fase larval.<br><br>V - Falsa. A implementação desta abordagem, a nível nacional, ainda requer avaliações de custo, logística de produção e liberação em larga escala.",
                                hintClass: "error"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, F",
                                alternativeHint: "Muito bem!",
                                hintClass: "success"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "V, V, F, V, F",
                                alternativeHint: "I - Verdadeira. Sim, alguns genes introduzidos nos mosquitos podem conferir, por exemplo, uma redução em sua competência vetorial a patógenos, como o vírus Dengue e plasmódios.<br><br>II - Falsa. O sucesso na metodologia de transgenia será maior quanto menor for o <u>tempo</u> necessário para a substituição da população selvagem do mosquito.<br><br>III - Falsa. A substituição populacional seria a alternativa mais viável, porque ela é autossustentável ao longo do tempo, diferente da supressão que requer a liberação contínua dos mosquitos geneticamente modificados.<br><br>IV - Verdadeira. Algumas linhagens de mosquitos transgênicos, como a <i>Aedes aegypti</i> OX513A, portadora de gene letal, apresenta também fluorescência verde em sua fase larval.<br><br>V - Falsa. A implementação desta abordagem, a nível nacional, ainda requer avaliações de custo, logística de produção e liberação em larga escala.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, V, F",
                                alternativeHint: "I - Verdadeira. Sim, alguns genes introduzidos nos mosquitos podem conferir, por exemplo, uma redução em sua competência vetorial a patógenos, como o vírus Dengue e plasmódios.<br><br>II - Falsa. O sucesso na metodologia de transgenia será maior quanto menor for o <u>tempo</u> necessário para a substituição da população selvagem do mosquito.<br><br>III - Falsa. A substituição populacional seria a alternativa mais viável, porque ela é autossustentável ao longo do tempo, diferente da supressão que requer a liberação contínua dos mosquitos geneticamente modificados.<br><br>IV - Verdadeira. Algumas linhagens de mosquitos transgênicos, como a <i>Aedes aegypti</i> OX513A, portadora de gene letal, apresenta também fluorescência verde em sua fase larval.<br><br>V - Falsa. A implementação desta abordagem, a nível nacional, ainda requer avaliações de custo, logística de produção e liberação em larga escala.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "F, F, F, V, V",
                                alternativeHint: "I - Verdadeira. Sim, alguns genes introduzidos nos mosquitos podem conferir, por exemplo, uma redução em sua competência vetorial a patógenos, como o vírus Dengue e plasmódios.<br><br>II - Falsa. O sucesso na metodologia de transgenia será maior quanto menor for o <u>tempo</u> necessário para a substituição da população selvagem do mosquito.<br><br>III - Falsa. A substituição populacional seria a alternativa mais viável, porque ela é autossustentável ao longo do tempo, diferente da supressão que requer a liberação contínua dos mosquitos geneticamente modificados.<br><br>IV - Verdadeira. Algumas linhagens de mosquitos transgênicos, como a <i>Aedes aegypti</i> OX513A, portadora de gene letal, apresenta também fluorescência verde em sua fase larval.<br><br>V - Falsa. A implementação desta abordagem, a nível nacional, ainda requer avaliações de custo, logística de produção e liberação em larga escala.",
                                hintClass: "error"
                            }
                        ]
                    },
                    {
                        questionId: 24,
                        type: "singleAnswer",
                        enunciate: "A utilização de simbiontes como a <i>Wolbachia</i> é uma ferramenta importante no controle populacional de vetores. Classifique as afirmativas abaixo como verdadeiras ou falsas e assinale a alternativa correta.",
                        rightAlternativeId: 0,
                        questionStatus: "",
                        staticAlternatives: [
                            {
                                id: 0,
                                position: 1,
                                alternativeText: "A incompatibilidade citoplasmática (IC) também pode ser uma técnica utilizada para a supressão populacional de mosquitos."
                            },
                            {
                                id: 1,
                                position: 2,
                                alternativeText: "A <i>Wolbachia</i> é um parasita intracelular que compromete a reprodução e a longevidade dos mosquitos."
                            },
                            {
                                id: 2,
                                position: 3,
                                alternativeText: "Apesar de a bactéria <i>Wolbachia</i> ocorrer naturalmente em várias espécies de mosquitos, a mesma pode estar ausente em outras, como por exemplo <i>Ae. aegypti</i>. Entretanto, ela pode ser naturalmente introduzida e usada para o controle populacional desta espécie."
                            },
                            {
                                id: 3,
                                position: 4,
                                alternativeText: "A técnica do inseto incompatível (TII) se baseia na liberação em massa de machos infectados com linhagens de <i>Wolbachia</i> diferentes das encontradas na população natural, para controle da espécie-alvo."
                            },
                            {
                                id: 4,
                                position: 5,
                                alternativeText: "A transferência mecânica de <i>Wolbachia</i>, por contato, tem sido a principal via de dispersão de genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos."
                            }
                        ],
                        alternatives: [
                            {
                                id: 0,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, F",
                                alternativeHint: "I - Verdadeira. Populações de mosquitos de uma mesma espécie portadoras de uma ou mais linhagens divergentes de <i>Wolbachia</i>, embora possam cruzar não produzirão descendentes, como consequência da mortalidade embrionária.<br><br>II - Falsa. A <i>Wolbachia</i> é uma bactéria intracelular simbionte, presente na maioria das espécies de mosquitos, que pode ou não comprometer a reprodução e a longevidade dos mesmos.<br><br>III - Falsa. A bactéria <i>Wolbachia</i> foi introduzida artificialmente na espécie <i>Ae. aegypti</i> para reduzir sua competência vetorial ao vírus Dengue, com a proposta de substituir populações selvagens, visando o controle da transmissão vetorial. Portanto, não se trata de uma abordagem para o controle da densidade populacional do mosquito.<br><br>IV - Verdadeira. Técnicas como a TII e a TIE se baseiam na liberação continua de uma grande quantidade de machos incompatíveis ou estéreis para promover a supressão populacional do inseto-alvo.<br><br>V - Falsa. A capacidade de transferência de <i>Wolbachia</i> através da cópula (via horizontal) e da mãe para os descendentes (via vertical), tem sido proposta como um mecanismo potencial para dispersar genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos.",
                                hintClass: "success"
                            },
                            {
                                id: 1,
                                isDisplayingTip: false,
                                alternativeText: "V, F, F, V, V",
                                alternativeHint: "I - Verdadeira. Populações de mosquitos de uma mesma espécie portadoras de uma ou mais linhagens divergentes de <i>Wolbachia</i>, embora possam cruzar não produzirão descendentes, como consequência da mortalidade embrionária.<br><br>II - Falsa. A <i>Wolbachia</i> é uma bactéria intracelular simbionte, presente na maioria das espécies de mosquitos, que pode ou não comprometer a reprodução e a longevidade dos mesmos.<br><br>III - Falsa. A bactéria <i>Wolbachia</i> foi introduzida artificialmente na espécie <i>Ae. aegypti</i> para reduzir sua competência vetorial ao vírus Dengue, com a proposta de substituir populações selvagens, visando o controle da transmissão vetorial. Portanto, não se trata de uma abordagem para o controle da densidade populacional do mosquito.<br><br>IV - Verdadeira. Técnicas como a TII e a TIE se baseiam na liberação continua de uma grande quantidade de machos incompatíveis ou estéreis para promover a supressão populacional do inseto-alvo.<br><br>V - Falsa. A capacidade de transferência de <i>Wolbachia</i> através da cópula (via horizontal) e da mãe para os descendentes (via vertical), tem sido proposta como um mecanismo potencial para dispersar genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos.",
                                hintClass: "error"
                            },
                            {
                                id: 2,
                                isDisplayingTip: false,
                                alternativeText: "V, F, V, V, V",
                                alternativeHint: "I - Verdadeira. Populações de mosquitos de uma mesma espécie portadoras de uma ou mais linhagens divergentes de <i>Wolbachia</i>, embora possam cruzar não produzirão descendentes, como consequência da mortalidade embrionária.<br><br>II - Falsa. A <i>Wolbachia</i> é uma bactéria intracelular simbionte, presente na maioria das espécies de mosquitos, que pode ou não comprometer a reprodução e a longevidade dos mesmos.<br><br>III - Falsa. A bactéria <i>Wolbachia</i> foi introduzida artificialmente na espécie <i>Ae. aegypti</i> para reduzir sua competência vetorial ao vírus Dengue, com a proposta de substituir populações selvagens, visando o controle da transmissão vetorial. Portanto, não se trata de uma abordagem para o controle da densidade populacional do mosquito.<br><br>IV - Verdadeira. Técnicas como a TII e a TIE se baseiam na liberação continua de uma grande quantidade de machos incompatíveis ou estéreis para promover a supressão populacional do inseto-alvo.<br><br>V - Falsa. A capacidade de transferência de <i>Wolbachia</i> através da cópula (via horizontal) e da mãe para os descendentes (via vertical), tem sido proposta como um mecanismo potencial para dispersar genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos.",
                                hintClass: "error"
                            },
                            {
                                id: 3,
                                isDisplayingTip: false,
                                alternativeText: "F, V, F, V, F",
                                alternativeHint: "I - Verdadeira. Populações de mosquitos de uma mesma espécie portadoras de uma ou mais linhagens divergentes de <i>Wolbachia</i>, embora possam cruzar não produzirão descendentes, como consequência da mortalidade embrionária.<br><br>II - Falsa. A <i>Wolbachia</i> é uma bactéria intracelular simbionte, presente na maioria das espécies de mosquitos, que pode ou não comprometer a reprodução e a longevidade dos mesmos.<br><br>III - Falsa. A bactéria <i>Wolbachia</i> foi introduzida artificialmente na espécie <i>Ae. aegypti</i> para reduzir sua competência vetorial ao vírus Dengue, com a proposta de substituir populações selvagens, visando o controle da transmissão vetorial. Portanto, não se trata de uma abordagem para o controle da densidade populacional do mosquito.<br><br>IV - Verdadeira. Técnicas como a TII e a TIE se baseiam na liberação continua de uma grande quantidade de machos incompatíveis ou estéreis para promover a supressão populacional do inseto-alvo.<br><br>V - Falsa. A capacidade de transferência de <i>Wolbachia</i> através da cópula (via horizontal) e da mãe para os descendentes (via vertical), tem sido proposta como um mecanismo potencial para dispersar genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos.",
                                hintClass: "error"
                            },
                            {
                                id: 4,
                                isDisplayingTip: false,
                                alternativeText: "V, V, V, V, F",
                                alternativeHint: "I - Verdadeira. Populações de mosquitos de uma mesma espécie portadoras de uma ou mais linhagens divergentes de <i>Wolbachia</i>, embora possam cruzar não produzirão descendentes, como consequência da mortalidade embrionária.<br><br>II - Falsa. A <i>Wolbachia</i> é uma bactéria intracelular simbionte, presente na maioria das espécies de mosquitos, que pode ou não comprometer a reprodução e a longevidade dos mesmos.<br><br>III - Falsa. A bactéria <i>Wolbachia</i> foi introduzida artificialmente na espécie <i>Ae. aegypti</i> para reduzir sua competência vetorial ao vírus Dengue, com a proposta de substituir populações selvagens, visando o controle da transmissão vetorial. Portanto, não se trata de uma abordagem para o controle da densidade populacional do mosquito.<br><br>IV - Verdadeira. Técnicas como a TII e a TIE se baseiam na liberação continua de uma grande quantidade de machos incompatíveis ou estéreis para promover a supressão populacional do inseto-alvo.<br><br>V - Falsa. A capacidade de transferência de <i>Wolbachia</i> através da cópula (via horizontal) e da mãe para os descendentes (via vertical), tem sido proposta como um mecanismo potencial para dispersar genes que bloqueiam a transmissão de patógenos em populações naturais de mosquitos.",
                                hintClass: "error"
                            }
                        ]
                    }
                ]
            }
        ]
    },

    /**
     * The STATUS variable is used to store the progress of the current user in the local storage and in the, after the integrated in the UNASUS API.
     */
    STATUS: {
        status: "",
        percentage: 0,
        LTIvalue: 0.0,
        U1_val: 0,
        U2_val: 0,
        U3_val: 0,
        q1_status: "",
        q2_status: "",
        q3_status: "",
        q4_status: "",
        q5_status: "",
        q6_status: "",
        q7_status: "",
        q8_status: "",
        q9_status: "",
        q10_status: "",
        q11_status: "",
        q12_status: "",
        q14_status: "",
        q13_status: "",
        q15_status: "",
        q16_status: "",
        q17_status: "",
        q18_status: "",
        q19_status: "",
        q20_status: "",
        q21_status: "",
        q22_status: "",
        q23_status: "",
        q24_status: ""
    },
    UNIT_QUESTIONS_STATUS_MAP: {
        U1: {
            id: 1,
            questions: [
                {
                    id: 1,
                    status: "q1_status"
                },
                {
                    id: 2,
                    status: "q2_status"
                },
                {
                    id: 3,
                    status: "q3_status"
                },
                {
                    id: 4,
                    status: "q4_status"
                },
                {
                    id: 5,
                    status: "q5_status"
                },
                {
                    id: 6,
                    status: "q6_status"
                },
                {
                    id: 7,
                    status: "q7_status"
                },
                {
                    id: 8,
                    status: "q8_status"
                },
                {
                    id: 9,
                    status: "q9_status"
                },
                {
                    id: 10,
                    status: "q10_status"
                }
            ]
        },
        U2: {
            id: 2,
            questions: [
                {
                    id: 11,
                    status: "q11_status"
                },
                {
                    id: 12,
                    status: "q12_status"
                },
                {
                    id: 13,
                    status: "q13_status"
                },
                {
                    id: 14,
                    status: "q14_status"
                },
                {
                    id: 15,
                    status: "q15_status"
                },
                {
                    id: 16,
                    status: "q16_status"
                },
                {
                    id: 17,
                    status: "q17_status"
                },
                {
                    id: 18,
                    status: "q18_status"
                },
                {
                    id: 19,
                    status: "q19_status"
                },
                {
                    id: 20,
                    status: "q20_status"
                }
            ]
        },
        U3: {
            id: 3,
            questions: [
                {
                    id: 21,
                    status: "q21_status"
                },
                {
                    id: 22,
                    status: "q22_status"
                },
                {
                    id: 23,
                    status: "q23_status"
                },
                {
                    id: 24,
                    status: "q24_status"
                }
            ]
        }
    },

    viewedClinicalCases: {},

    /**
     * Functions related to the PPU as a whole
     */
    getPPUInitDate: function () {
        try {
            var courseInitDate = gApiIntegration.getCourseInitDate();

            if (courseInitDate !== undefined && courseInitDate !== "") {
                return courseInitDate;
            }
        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to return the initialization date of the course " + err);
            }
        }

        return undefined;
    },

    initPPUVisualization: function () {
        try {
            // Save the init date
            var courseInitDate = gApiIntegration.getCourseInitDate();

            if (courseInitDate == undefined || courseInitDate === "") {
                gApiIntegration.setCourseInitDate(gUtils.getUtcTimeNow());

            } else if (Object.keys(courseInitDate).length == 0) {
                gApiIntegration.setCourseInitDate(gUtils.getUtcTimeNow());
            }

            // Save the STATUS variable
            this.STATUS.status = this.COURSE_STATUS.ATTENDED;
            gApiIntegration.setCourseStatus(this.STATUS);

        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to initilize the course init date flag " + err);
            }
        }
    },

    /**
     * Functions related to the course management
     */
    getCourse: function () {
        return this.course;
    },

    getCourseStatus: function () {
        return this.STATUS;
    },

    getUnitsQuestionsMap: function () {
        return this.UNIT_QUESTIONS_STATUS_MAP;
    },

    getUnitKey: function (unitNumber) {
        return "U" + unitNumber;
    },

    getUnitValKey: function (unitNumber) {
        return "U" + unitNumber + "_val";
    },

    getQuestionStatusKey: function (questionNumber) {
        return "q" + questionNumber + "_status";
    },

    getStageId: function (unitNumber) {
        return "U" + unitNumber + "Q";
    },

    saveCourseStatus: function () {
        gApiIntegration.setCourseStatus(this.getCourseStatus());
    },

    resetCourse: function () {
        gApiIntegration.setDownload("", "");
        gApiIntegration.setExternalLink("");
        gApiIntegration.setVideo("");
        gApiIntegration.setCurrentPage("");
        gApiIntegration.setPreviousPage("");
        gApiIntegration.setViewedClinicalCases("");
        gApiIntegration.setHasPresentedWelcomeDialog(false);
        gApiIntegration.setUserCourse("");
        gApiIntegration.setCourseInitDate("");
    },


    /**
     * Update the percentage of question of this educational resource that were answered correctly. 
     * The value must be from 0 .. 100
     */
    calculatePercentage: function (totalNumRightQuestions, totalNumQuestions) {
        try {
            this.getCourseStatus().percentage = Math.round((totalNumRightQuestions / totalNumQuestions) * 100);
        } catch (err) {
            console.log("Failed to calculateStatusPercentage " + err);
        }
    },

    /**
     * Update the LTI value: LTI value vary from 0 .. 1
     */
    calculateLTIvalue: function () {
        try {
            var ltiVal = 0;

            for (var unitNumber = 1; unitNumber <= this.getCourse().units.length; unitNumber++) {
                var unitVal = this.getCourseStatus()[this.getUnitValKey(unitNumber)];

                if (unitVal >= 80 && unitVal < 100) {
                    ltiVal = Number(parseFloat(ltiVal).toFixed(3)) + Number(parseFloat(0.26).toFixed(3));

                } else if (unitVal >= 100) {
                    ltiVal = Number(parseFloat(ltiVal).toFixed(3)) + Number(parseFloat(0.333).toFixed(3));
                }
            }

            if (ltiVal >= 0.999) {
                ltiVal = 1;
            }

            this.getCourseStatus().LTIvalue = ltiVal;

        } catch (err) {
            console.log("Failed to calculateLTIvalue " + err);
        }
    },

    /**
     * Update the course STATUS.status flag
     */
    calculateStatus: function () {
        try {

            if (this.getCourseStatus().LTIvalue >= 0.8 && this.getCourseStatus().LTIvalue <= 0.1) {
                this.getCourseStatus().status = this.COURSE_STATUS.PASSED;
            } else {
                this.getCourseStatus().status = this.COURSE_STATUS.ATTEMPTED;
            }
        } catch (err) {
            console.log("Failed to calculateStatus " + err);
        }
    },

    /**
     * Update the STATUS variable and create a new ANSWER variable if the user is sending it's final answer for a question
     */
    saveQuestionStatus: function (unitId, questionId, questionStatus, submit, selectedAlternative) {
        try {
            // Create a new log for the ANSWER variable if the user is sending it's final answer for a question
            if (submit === true && selectedAlternative !== undefined && selectedAlternative !== '') {
                var answer = {
                    stage: this.getStageId(unitId),
                    ppuQuestionId: questionId,
                    alternative: selectedAlternative,
                    correct: questionStatus === this.QUESTION_STATUS.RIGHT
                };

                gApiIntegration.setAnswer(answer);
            }

            // Calculate the parameters below and update the STATUS variable
            var totalNumQuestions = 0;
            var totalNumRightQuestions = 0;

            // Get the user results per Unit
            for (var unitNumber = 1; unitNumber <= Object.keys(this.getUnitsQuestionsMap()).length; unitNumber++) {
                var numRightQuestionsUnit = 0;
                var unit = this.getUnitsQuestionsMap()[this.getUnitKey(unitNumber)];

                // Update the question status and get the number of right questions of a unit
                for (var questionNumber = 0; questionNumber < unit.questions.length; questionNumber++) {
                    totalNumQuestions++;
                    var question = unit.questions[questionNumber];

                    // Update the question status
                    if (unit.id === unitId && question.id === questionId) {
                        this.getCourseStatus()[question.status] = questionStatus;
                    }

                    // Increment the number of questions answered correctly
                    if (this.getCourseStatus()[question.status] === this.QUESTION_STATUS.RIGHT) {
                        numRightQuestionsUnit += 1;
                    }
                }

                // Calculate the unit grade
                var keyUnitVal = this.getUnitValKey(unitNumber);
                var curUnitVal = this.getCourseStatus()[keyUnitVal];

                // Update Unit value: Unit values vary from 0 .. 100
                if (numRightQuestionsUnit > 0) {
                    curUnitVal = Math.round((numRightQuestionsUnit / unit.questions.length) * 100);
                } else {
                    curUnitVal = 0;
                }

                this.getCourseStatus()[keyUnitVal] = curUnitVal;

                // Update the parameters of the calculation
                totalNumRightQuestions += numRightQuestionsUnit;
            }

            // Update the mandatory attributes of the STATUS variable
            this.calculatePercentage(totalNumRightQuestions, totalNumQuestions);
            this.calculateStatus();
            this.calculateLTIvalue();

            // Persist the updated STATUS variable if needed
            if (submit) {
                gApiIntegration.setCourseStatus(this.getCourseStatus());
            }
        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to save course " + err);
            }
        }
    },

    getUnitConclusionPercentage: function (unitNumber) {
        try {
            return this.getCourseStatus()[this.getUnitValKey(unitNumber)];

        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to getUnitConclusionPercentage " + err);
            }
        }

        return -1;
    },

    hasUserFinishedUnit: function (unitNumber) {
        try {
            return this.getUnitConclusionPercentage(unitNumber) >= 80

        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to execute hasUserFinishedUnit " + err);
            }
        }

        return false;
    },

    hasUserCompletelyFinishedUnit: function (unitNumber) {
        try {
            return this.getUnitConclusionPercentage(unitNumber) === 100;

        } catch (err) {
            console.log("Failed to execute hasUserCompletelyFinishedUnit " + err);
        }
    },

    hasUserFinishedCourse: function () {
        try {
            var hasFinishedAllUnits = true;

            for (var unitNumber = 1; unitNumber <= this.getCourse().units.length; unitNumber++) {
                hasFinishedAllUnits = this.hasUserFinishedUnit(unitNumber);

                if (!hasFinishedAllUnits) {
                    break;
                }
            }

            return hasFinishedAllUnits && this.getCourseStatus().LTIvalue >= 0.7;

        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to return course status " + err);
            }
        }

        return false;
    },

    initPrevCurrentModuleInfoConfirmationDialog: function() {
        try {
            this.prevModuleInfo.pageId = gPages.PAGE_COURSE_PROGRESS;
            this.prevModuleInfo.pageName = "Andamento";
    
            this.nextModuleInfo.pageId = gPages.PAGE_COURSE_UNIT_DETAILS_1;
            this.nextModuleInfo.pageName = "Módulo 1";
    
            if (this.hasUserFinishedUnit(1)) {
                this.prevModuleInfo.pageId = gPages.PAGE_COURSE_UNIT_DETAILS_1;
                this.prevModuleInfo.pageName = "Módulo 1";
    
                this.nextModuleInfo.pageId = gPages.PAGE_COURSE_UNIT_DETAILS_2;
                this.nextModuleInfo.pageName = "Módulo 2";
            }
            
        } catch (err) {
    
            if (unasus.pack.getDebug()) {
                console.log("Failed to initPrevCurrentModuleInfoConfirmationDialog " + err);
            }
        }
    },

    loadUserCourse: function () {
        try {
            var storedUserCourse = gApiIntegration.getCourseStatus();

            if (storedUserCourse == undefined || storedUserCourse === "" || storedUserCourse == null) {
                this.initPPUVisualization();

            } else if (Object.keys(storedUserCourse).length == 0) {
                this.initPPUVisualization();

            } else {
                // Update the STATUS variable with the stored variable
                this.STATUS = storedUserCourse;

                // Get the user results per Unit
                for (var storedUnitNum = 1; storedUnitNum <= Object.keys(this.getUnitsQuestionsMap()).length; storedUnitNum++) {
                    var unit = this.getUnitsQuestionsMap()[this.getUnitKey(storedUnitNum)];

                    // Update the question status and get the number of right questions of a unit
                    for (var storedQuestionNum = 0; storedQuestionNum < unit.questions.length; storedQuestionNum++) {
                        var storedQuestion = unit.questions[storedQuestionNum];
                        var storedQuestionStatus = this.getCourseStatus()[storedQuestion.status]

                        // Variables that indicate the position of the question in the UI model
                        var uiModelUnitPos = -1;
                        var uiModelQuestionPos = -1
                        var uiModelQuestion = undefined;

                        // Get the UI model of the course question by id
                        for (var uiModelUnitNum = 0; uiModelUnitNum < this.getCourse().units.length; uiModelUnitNum++) {
                            var uiModelUnits = this.getCourse().units[uiModelUnitNum];

                            for (var uiModelQuestionNum = 0; uiModelQuestionNum < uiModelUnits.unitQuestions.length; uiModelQuestionNum++) {
                                var listQuestion = uiModelUnits.unitQuestions[uiModelQuestionNum];

                                if (storedQuestion.id === listQuestion.questionId) {
                                    uiModelUnitPos = uiModelUnitNum;
                                    uiModelQuestionPos = uiModelQuestionNum;
                                    uiModelQuestion = listQuestion;
                                }
                            };
                        };

                        // Sincronize the stored Question with it's UI model
                        if (uiModelQuestion != undefined && storedQuestionStatus != undefined) {

                            switch (storedQuestionStatus) {

                                case this.QUESTION_STATUS.WRONG:
                                    uiModelQuestion.questionStatus = this.QUESTION_STATUS.WRONG;
                                    break;

                                case this.QUESTION_STATUS.ANSWERING:
                                    uiModelQuestion.questionStatus = this.QUESTION_STATUS.ANSWERING;
                                    break;

                                case this.QUESTION_STATUS.RIGHT:
                                    uiModelQuestion.questionStatus = this.QUESTION_STATUS.RIGHT;
                                    break;

                                case this.QUESTION_STATUS.CLEAR:
                                    uiModelQuestion.questionStatus = this.QUESTION_STATUS.CLEAR;
                                    break;

                                default:
                                    uiModelQuestion.questionStatus = this.QUESTION_STATUS.CLEAR;
                            }

                            // Update the question UI model    
                            if (uiModelUnitPos != -1 && uiModelQuestionPos != -1) {
                                this.getCourse().units[uiModelUnitPos].unitQuestions[uiModelQuestionPos] = uiModelQuestion;
                            }
                        }
                    }
                }

                this.initPrevCurrentModuleInfoConfirmationDialog();
            }
        } catch (err) {

            if (unasus.pack.getDebug()) {
                console.log("Failed to load the userCourse object from the local storage: " + err);
            }
        }
    },

    /**
     * Functions related to the Clinical Cases
     */
    loadViewedClinicalCases: function () {
        try {
            this.viewedClinicalCases = gApiIntegration.getViewedClinicalCases();

            if (this.viewedClinicalCases == undefined) {
                this.viewedClinicalCases = {};
            }
        } catch (err) {
            return this.PAGE_INDEX;
        }
    },

    hasViewedClinicalCaseN: function (clinicalCaseNumber) {
        this.loadViewedClinicalCases();

        var viewedCaseId = "clinicalCase" + clinicalCaseNumber + this.ID_CONNECTOR + gUnasusApiIntegrationUtils.getPlayerUserId();
        var hasViewedClinicalCase = this.viewedClinicalCases[viewedCaseId];

        if (hasViewedClinicalCase == undefined) {
            return false;

        } else if (hasViewedClinicalCase == false) {
            return false;

        } else {
            return true;
        }
    },

    hasUserViewedAllClinicalCases: function () {
        for (var i = 1; i <= Object.keys(this.CLINICAL_CASES_IDS).length; i++) {

            if (!this.hasViewedClinicalCaseN(i)) {
                return false;
            }
        }

        return true;
    },

    setHasSeenClinicalCase: function (selectedClinicalCaseId) {
        var viewedCaseId = selectedClinicalCaseId + this.ID_CONNECTOR + gUnasusApiIntegrationUtils.getPlayerUserId();

        if (this.viewedClinicalCases == "") {
            this.viewedClinicalCases = {};
        }

        this.viewedClinicalCases[viewedCaseId] = true;

        gApiIntegration.setViewedClinicalCases(this.viewedClinicalCases);
    },

    /**
     * Functions related to the Welcome Dialog
     */
    hasPresentedWelcomeDialog: function () {
        return gApiIntegration.hasPresentedWelcomeDialog();
    },

    setHasPresentedWelcomeDialog: function (hasPresentedDialog) {
        return gApiIntegration.setHasPresentedWelcomeDialog(hasPresentedDialog);
    },

    /**
     * Functions related to the selection of a question-link
     */
    setSelectedQuestionLink: function (unitNumber, questionNumber) {
        try {
            var questionLink = {
                unitNumber: unitNumber,
                questionNumber: questionNumber
            }

            if ((unitNumber != undefined && unitNumber !== "" && unitNumber != null) &&
                (questionNumber != undefined && questionNumber !== "" && questionNumber != null)) {
                window.sessionStorage.setItem(this.IDX_SESSION_VAR_SELECTED_QUESTION_LINK, JSON.stringify(questionLink));
            } else {
                window.sessionStorage.removeItem(this.IDX_SESSION_VAR_SELECTED_QUESTION_LINK);
            }
        } catch (err) {
            console.log("Failed to setSelectedQuestionLink " + err);
        }
    },

    getSelectedQuestionLink: function () {
        try {
            return JSON.parse(window.sessionStorage.getItem(this.IDX_SESSION_VAR_SELECTED_QUESTION_LINK));
        } catch (err) {
            console.log("Failed to getSelectedQuestionLink " + err);
        }

        return undefined;
    },

    hasSelectedQuestionLink: function () {
        var selectedQuestionLink = this.getSelectedQuestionLink();

        if (selectedQuestionLink == undefined || selectedQuestionLink === "" || selectedQuestionLink == null) {
            return false;
        } else {
            return selectedQuestionLink;
        }
    },

    getUnitQuestionListModel: function (unitNumber) {
        try {
            var questionsListModel = {
                groupQuestionsTitle: "QUESTÕES DA<br><br>UNIDADE " + unitNumber,
                questions: []
            };

            // Get Information about a unit of the course
            var unit = this.getUnitsQuestionsMap()[this.getUnitKey(unitNumber)];

            // Update the question status
            for (var questionNumber = 0; questionNumber < unit.questions.length; questionNumber++) {
                var question = unit.questions[questionNumber];
                var questionStatus = this.getCourseStatus()[question.status]

                // Create the data model of the object that is going to represent a question in the list
                if (question != undefined) {

                    if (questionStatus == undefined) {
                        questionStatus = this.QUESTION_STATUS.CLEAR;
                    }

                    var listQuestion = {};
                    listQuestion.unitId = unitNumber;
                    listQuestion.questionId = question.id;
                    listQuestion.questionName = "Questão " + (questionNumber + 1);

                    switch (questionStatus) {

                        case this.QUESTION_STATUS.ANSWERING:

                            if (gImages.browerSupportsSvg()) {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_SVG_ANSWERING;
                            } else {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_PNG_ANSWERING;
                            }

                            break;

                        case this.QUESTION_STATUS.RIGHT:

                            if (gImages.browerSupportsSvg()) {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_SVG_SUCCESS;
                            } else {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_PNG_SUCCESS;
                            }

                            break;

                        case this.QUESTION_STATUS.WRONG:

                            if (gImages.browerSupportsSvg()) {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_SVG_WRONG;
                            } else {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_PNG_WRONG;
                            }

                            break;

                        case this.QUESTION_STATUS.CLEAR:

                            if (gImages.browerSupportsSvg()) {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_SVG_NO_ANSWER;
                            } else {
                                listQuestion.questionStatusImg = gImages.QUESTION_STATUS_IMAGES.ICO_PNG_NO_ANSWER;
                            }

                            break;
                    }
                }

                // Insert the data model of the question into the list
                if (questionNumber > 0) {
                    
                    if (unit.questions[questionNumber - 1].id !== question.id) {
                        questionsListModel.questions.push(listQuestion);
                    }
                } else {
                    questionsListModel.questions.push(listQuestion);
                }
            }

            return questionsListModel;

        } catch (err) {
            console.log("Failed to getUnitQuestionListModel " + err);
        }

        return undefined;
    },

    onWebObjectSelected: function (internetUrl) {

        if (internetUrl != undefined && internetUrl !== '') {
            gApiIntegration.setExternalLink(internetUrl);
            window.open(internetUrl, '_blank');
        }
    },

    getUsername: function() {

        try {
            var username = unasus.pack.getPlayerUserRealName();

            if (username != undefined) {
                username = username.replace(/\s+/g,' ');

                return username;
            }

        } catch (err) {
            console.log("Failed to getUsername " + err);
        }

        return "";
    }

}