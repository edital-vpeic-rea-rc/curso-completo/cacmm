/**
 * mainContent.js is responsible for handling the behavior of the main container of the application.
 * It handles the toggle event (shrink/expand) of the Sidebar and the main container.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @requires gPages
 * @requires gImages
 * @requires Sidebar
 * @license MIT
*/

function MainContainer() {
  this._initialized = false;
}

MainContainer.prototype = {

  DEBUG_MC: true,

  // Selectors
  MAIN_CONTENT_CONTAINER_SELECTOR: ".content-container",
  SIDEBAR_TOGGLE_SELECTOR: ".sidebar-toggle",

  // Classes
  CLASS_TOGGLE_MAIN_CONTENT_CONTAINER: "shrinked-content",

  // Status
  SIDEBAR_STATUS_SHRINKED : "shrinked",
  SIDEBAR_STATUS_EXPANDED : "expanded",

  sidebar: undefined,
  callback: undefined,
  
  /**
   * Setters and Getters
   */ 
  setSidebarToggleEventCallback: function(callbackFunction) {
     this.callback = callbackFunction;
  },

  getSidebarToggleCallback: function(){
    return this.callback;
  },

  /**
   * Sidebar
   */
  build: function (pageSidebar) {
    this.sidebar = pageSidebar;
  },

  isSidebarShrinked: function () {
    try {
      var isShrinked = undefined;

      if (this.sidebar != undefined) {
        $(this.sidebar.SIDEBAR_CONTAINER_SELECTOR).each(function () {
          isShrinked = Number($(this).css("width").replace("px", "")) == 0;
        });
      }

      return isShrinked;

    } catch (err) {
      return undefined;
    }
  },

  shrinkSidebar: function () {
    try {

      if (this.sidebar != undefined) {
        $(this.sidebar.SIDEBAR_CONTAINER_SELECTOR).addClass(this.sidebar.CLASS_TOGGLE_SIDEBAR_MENU);
        $(this.sidebar.SIDEBAR_TOGGLE_CONTAINER_SELECTOR).addClass(this.sidebar.CLASS_TOGGLE_SIDEBAR_MENU);
    
        if (gUi.isEdge()) {
          $(gUi.SIDEBAR_TOGGLE_SELECTOR).removeClass(gUi.CLASS_HIDDEN);
          $(gUi.SIDEBAR_TOGGLE_EDGE_SELECTOR).addClass(gUi.CLASS_HIDDEN);
        }
        
        $(this.MAIN_CONTENT_CONTAINER_SELECTOR).removeClass(this.CLASS_TOGGLE_MAIN_CONTENT_CONTAINER);
      }

    } catch(err) {

      if (this.DEBUG_MC) {
        console.log("MainContainer Warning: failed to shrinkSidebar " + err);
      }
    }
  },

  expandSidebar: function () {
    try {
      $(this.sidebar.SIDEBAR_CONTAINER_SELECTOR).removeClass(this.sidebar.CLASS_TOGGLE_SIDEBAR_MENU);
      $(this.sidebar.SIDEBAR_TOGGLE_CONTAINER_SELECTOR).removeClass(this.sidebar.CLASS_TOGGLE_SIDEBAR_MENU);
  
      if (gUi.isEdge()) {
        $(gUi.SIDEBAR_TOGGLE_SELECTOR).addClass(gUi.CLASS_HIDDEN);
        $(gUi.SIDEBAR_TOGGLE_EDGE_SELECTOR).removeClass(gUi.CLASS_HIDDEN);
      }
  
      $(this.MAIN_CONTENT_CONTAINER_SELECTOR).addClass(this.CLASS_TOGGLE_MAIN_CONTENT_CONTAINER);

    } catch(err) {

      if (this.DEBUG_MC) {
        console.log("MainContainer Warning: failed to shrinkSidebar " + err);
      }
    }
  },

  tryShrinkSidebar: function () {
    var isShrinked = this.isSidebarShrinked();

    if (isShrinked != undefined) {

      if (!isShrinked) {
        this.shrinkSidebar();
      }
    }
  },

  tryExpandSidebar: function () {
    var isShrinked = this.isSidebarShrinked();

    if (isShrinked != undefined) {

      if (isShrinked) {
        this.expandSidebar();
      }
    }
  },

  toggleSidebar: function () {
    try {
      var isShrinked = this.isSidebarShrinked();

      if (isShrinked != undefined) {

        if (!isShrinked) {
          this.shrinkSidebar();

          if (this.getSidebarToggleCallback() != undefined) {
            this.getSidebarToggleCallback()(this.SIDEBAR_STATUS_SHRINKED);
          }
        } else {
          this.expandSidebar();

          if (this.getSidebarToggleCallback() != undefined) {
            this.getSidebarToggleCallback()(this.SIDEBAR_STATUS_EXPANDED);
          }
        }
      }
    } catch (err) {

      if (this.DEBUG_MC) {
        console.log("MainContainer Warning: failed to shrinkSidebar " + err);
      }
    }
  },

  /**
   * Initializers
   */
  initGlogalEvents: function () {
    var ctx = this;

    $(this.sidebar.SIDEBAR_TOGGLE_CONTAINER_SELECTOR).on("click", function () {
      ctx.toggleSidebar();
    });
  },

  initMobileEvents: function () {
    var ctx = this;
  }

}