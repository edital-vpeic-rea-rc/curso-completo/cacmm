/**
 * headerTop2.js handles the actions of the component headerTop2, which is a navigation bar fixed at the top
 * of a container div.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @requires gPages
 * @requires gImages
 * @license MIT
 */

function HeaderTop2() {
  this._initialized = false;
}

HeaderTop2.prototype = {

  HEADER_MENU_ITEM_SELECTOR: " .transparent-filter.selectable",

  MENU_ITEM_STATUS: {
    active: "active",
    inactive: "",
  },

  model: {
    pageId: "",
    title: "",
    brandImg: "images/ico_course_logo.png",
    collapsedMenuIco: "",
    menuItems: []
  },

  target: undefined,

  isHeaderTopMenuLocked: false,

  getTemplate: function () {
    return '<div class="navbar-title">' +
      '<img src="{{brandImg}}"/>' +
      '<p><span>{{{title}}}</p></span>' +
      '</div>' +
      '<a class="navbar-toggler visible-xs visible-sm" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls=".navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">' +
      '<img src="{{collapsedMenuIco}}"/>' +
      '</a>' +
      '<div id="componentHeaderTop2Menu_{{pageId}}" class="collapse navbar-collapse">' +
      '<ul class="navbar-nav">' +
      '{{#menuItems}}' +
      '<li class="nav-item {{activeStatus}} {{flavor}}">' +
      '<a class="nav-link" href="{{link}}">{{name}} <span class="sr-only">(current)</span></a>' +
      '<div class="transparent-filter selectable">' +
      '</li>' +
      '{{/menuItems}}' +
      '</ul>' +
      '</div>';
  },

  build: function (tgt, model) {
    this.target = tgt;
    var template = this.getTemplate();
    var navbarHtml = Mustache.to_html(template, model);

    $(this.target).html(navbarHtml);
  },

  buildHeaderTop2: function (targetSelector, brandImg, pageId, title, selectedItemIdx, selectedUnit) {
    var dataModel = this.model;

    // Initialize the component with the default options
    dataModel.brandImg = brandImg;
    dataModel.pageId = pageId;
    dataModel.title = title;
    dataModel.collapsedMenuIco = gImages.ICO_PNG_HEADER_TOP_2_BTN_COLLAPSE;

    if (gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {

      dataModel.menuItems = [
        {
          svgIco: gImages.ICO_SVG_COURSE_UNITS,
          regularIco: gImages.ICO_PNG_COURSE_UNITS,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_COURSE_UNITS,
          name: gPages.PAGE_COURSE_UNITS_DESCRIPTION,
          flavor: ""
        },
        {
          svgIco: gImages.ICO_SVG_COURSE_PROGRESS,
          regularIco: gImages.ICO_PNG_COURSE_PROGRESS,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_COURSE_PROGRESS,
          name: gPages.PAGE_COURSE_PROGRESS_DESCRIPTION,
          flavor: ""
        },
        {
          svgIco: gImages.ICO_SVG_GLOSSARY,
          regularIco: gImages.ICO_PNG_GLOSSARY,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_GLOSSARY,
          name: gPages.PAGE_GLOSSARY_DESCRIPTION,
          flavor: ""
        },
        {
          svgIco: gImages.ICO_SVG_ABOUT,
          regularIco: gImages.ICO_PNG_ABOUT,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_ABOUT,
          name: gPages.PAGE_ABOUT_DESCRIPTION,
          flavor: ""
        }
      ];

    } else {

      dataModel.menuItems = [
        {
          svgIco: gImages.ICO_SVG_ABOUT,
          regularIco: gImages.ICO_PNG_ABOUT,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_ABOUT,
          name: gPages.PAGE_ABOUT_DESCRIPTION,
          flavor: "text"
        },
        {
          svgIco: gImages.ICO_SVG_GLOSSARY,
          regularIco: gImages.ICO_PNG_GLOSSARY,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_GLOSSARY,
          name: gPages.PAGE_GLOSSARY_DESCRIPTION,
          flavor: "text"
        },
        {
          svgIco: gImages.ICO_SVG_COURSE_PROGRESS,
          regularIco: gImages.ICO_PNG_COURSE_PROGRESS,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_COURSE_PROGRESS,
          name: gPages.PAGE_COURSE_PROGRESS_DESCRIPTION,
          flavor: "text"
        },
        {
          svgIco: gImages.ICO_SVG_COURSE_UNITS,
          regularIco: gImages.ICO_PNG_COURSE_UNITS,
          activeStatus: this.MENU_ITEM_STATUS.inactive,
          link: gPages.PAGE_COURSE_UNITS,
          name: gPages.PAGE_COURSE_UNITS_DESCRIPTION,
          flavor: "text"
        }
      ];
    }

    // Customize the selected menu item
    if (selectedItemIdx > 0) {
      selectedItemIdx = selectedItemIdx - 1;
    }

    if (gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {
      selectedItemIdx = dataModel.menuItems.length - 1 - selectedItemIdx;
    }

    dataModel.menuItems[selectedItemIdx].activeStatus = this.MENU_ITEM_STATUS.active;

    switch (selectedItemIdx) {

      case 0: {

        if (gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {
          dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_UNITS_SELECTED;
          dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_COURSE_UNITS_SELECTED;
        } else {
          dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_ABOUT_SELECTED;
          dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_ABOUT_SELECTED;
        }

        break;
      }

      case 1: {
        dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_PROGRESS_SELECTED;
        dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_COURSE_PROGRESS_SELECTED;

        break;
      }

      case 2: {
        dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_GLOSSARY_SELECTED;
        dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_GLOSSARY_SELECTED;

        break;
      }

      case 3: {

        if (gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {
          dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_ABOUT_SELECTED;
          dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_ABOUT_SELECTED;
        } else {
          dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_UNITS_SELECTED;
          dataModel.menuItems[selectedItemIdx].regularIco = gImages.ICO_PNG_COURSE_UNITS_SELECTED;
        }

        if (selectedUnit != undefined) {

          switch (selectedUnit) {

            case 1: {
              dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_UNITS_SELECTED_1;
              break;
            }

            case 2: {
              dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_UNITS_SELECTED_2;
              break;
            }

            case 3: {
              dataModel.menuItems[selectedItemIdx].svgIco = gImages.ICO_SVG_COURSE_UNITS_SELECTED_3;
              break;
            }
          }
        }

        break;
      }
    }

    this.build(targetSelector, dataModel);
  },

  lockHeader: function (lock) {
    this.isHeaderTopMenuLocked = lock;
  },

  isHeaderLocked: function () {
    return this.isHeaderTopMenuLocked;
  },

  initEvents: function () {
    var ctx = this;

    $(this.target + this.HEADER_MENU_ITEM_SELECTOR).on("click", function () {
      if (!ctx.isHeaderLocked()) {
        var navItemLink = $(this).parent().find("a").attr("href");
        gPages.updateCurrentPage(navItemLink);
      }
    });

    $(this.target + this.HEADER_MENU_ITEM_SELECTOR).on("mouseenter", function () {
      if (!ctx.isHeaderLocked()) {
        $(this).parent().find("a").addClass("hovered");
      }
    });

    $(this.target + this.HEADER_MENU_ITEM_SELECTOR).on("mouseout", function () {
      if (!ctx.isHeaderLocked()) {
        $(this).parent().find("a").removeClass("hovered");
      }
    });
  },

  reset: function () {
    this.model.pageId = "",
    this.model.title = "";
    this.model.collapsedMenuIco = "";
    this.model.menuItems = [];
  }

}