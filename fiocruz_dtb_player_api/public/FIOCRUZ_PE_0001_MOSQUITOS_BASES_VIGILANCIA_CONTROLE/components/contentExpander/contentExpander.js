/**
 * contentExpander.js is a component used to reveal the content of a section, by toggle its height.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ContentExpander() {
	this._initialized = false;
}

ContentExpander.prototype = {

    DEBUG_CE : true,

    CONTENT_EXPANDER_SELECTOR : " .content-expander",
    TOGGLE_BTN_SELECTOR : " .btn-selector",
    HEADER_SELECTOR : " .cex-header",
    BODY_SELECTOR : " .cex-body",
    BODY_CONTAINER_SELECTOR : " .cex-body .body-container",

    CLASS_EXPANDED : "expanded",

    model : {
        title: ""
    },

    icon_svg: gImages.ICO_SVG_ARROW_DOWN,
    icon_png: gImages.ICO_PNG_ARROW_DOWN,

    target : undefined,
    callback: undefined,

    isToggleCommandLockedForShrinking: false,

    /**
     * Events
     */
    closeAllOtherExpanders: function() {
        try {
            var ctx = this;

            $(this.CONTENT_EXPANDER_SELECTOR).each(function() {
                $(this).find(ctx.HEADER_SELECTOR).removeClass(ctx.CLASS_EXPANDED);
                $(this).find(ctx.BODY_SELECTOR).removeClass(ctx.CLASS_EXPANDED);
                $(this).find(ctx.BODY_CONTAINER_SELECTOR).removeClass(ctx.CLASS_EXPANDED);
            })

        } catch (err) {

            if (this.DEBUG_CE) {
                console.log("ContentExpander Warning: Failed to closeAllOtherExpanders" + err);
            }
        }
    },

    toggleContainerHeight: function(contentExpander) {
        try {

            // Expand container and display content
            if (!$(contentExpander).find(this.HEADER_SELECTOR).hasClass(this.CLASS_EXPANDED)) {

                if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
                    this.closeAllOtherExpanders();
                }
                
                $(contentExpander).find(this.HEADER_SELECTOR).addClass(this.CLASS_EXPANDED);
                $(contentExpander).find(this.BODY_SELECTOR).addClass(this.CLASS_EXPANDED);
                $(contentExpander).find(this.BODY_CONTAINER_SELECTOR).addClass(this.CLASS_EXPANDED);
    
                if (this.callback != undefined) {
                    this.callback();
                }
    
            // Shrink content
            } else {
                $(contentExpander).find(this.HEADER_SELECTOR).removeClass(this.CLASS_EXPANDED);
                $(contentExpander).find(this.BODY_SELECTOR).removeClass(this.CLASS_EXPANDED);
                $(contentExpander).find(this.BODY_CONTAINER_SELECTOR).removeClass(this.CLASS_EXPANDED);
            }
        } catch(err) {

            if (this.DEBUG_CE) {
                console.log("ContentExpander Warning: Failed to toggleContainerHeight" + err);
            }
        }
    },

    expandContent: function(expand) {
        try {

            if (expand && !$(this.target).find(this.HEADER_SELECTOR).hasClass(this.CLASS_EXPANDED)) {
                $(this.target).find(this.HEADER_SELECTOR).addClass(this.CLASS_EXPANDED);
                $(this.target).find(this.BODY_SELECTOR).addClass(this.CLASS_EXPANDED);
                $(this.target).find(this.BODY_CONTAINER_SELECTOR).addClass(this.CLASS_EXPANDED);

            } else if ($(this.target).find(this.HEADER_SELECTOR).hasClass(this.CLASS_EXPANDED)) {
                $(this.target).find(this.HEADER_SELECTOR).removeClass(this.CLASS_EXPANDED);
                $(this.target).find(this.BODY_SELECTOR).removeClass(this.CLASS_EXPANDED);
                $(this.target).find(this.BODY_CONTAINER_SELECTOR).removeClass(this.CLASS_EXPANDED);
            }

        } catch(err) {

            if (this.DEBUG_CE) {
                console.log("ContentExpander Warning: Failed to expandContent" + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        $(ctx.target + ctx.CONTENT_EXPANDER_SELECTOR).on("click", function() {
            ctx.toggleContainerHeight($(this).parent());
        });
        $(ctx.target + ctx.HEADER_SELECTOR).on("click", function() {
            ctx.toggleContainerHeight($(this).parent());
        });
    },

    /**
     * Initialization
     */
    getHeaderTemplate: function () {
        return '<div class="cex-header">' +
            '<div class="cex-title">' +
            '<h3>{{title}}</h3>' +
            '</div>' +
            '<div class="cex-toggle-btn">' +
            '<object class="" data="{{icon_svg}}" type="image/svg+xml">' +
            '<img src="{{icon_png}}">' +
            '</object>' +
            '<div class="btn-selector"></div>' +
            '</div>' +
            '</div>';
    },

    build : function(tgt, model, callback) {
        try {

            if (tgt != undefined && model != undefined) {
                this.target = tgt;
                this.model = model;
                this.callback = callback;

                if (this.model.icon_svg === undefined) {
                    this.model.icon_svg = this.icon_svg;
                }
                
                if (this.model.icon_png === undefined) {
                    this.model.icon_png = this.icon_png
                }
        
                var headerTemplate = this.getHeaderTemplate();
                var htmlHeader = Mustache.to_html(headerTemplate, this.model);
                
                $(this.target).prepend(htmlHeader);
                this.initEvents();
            }

        } catch(err) {

            if (this.DEBUG_CE) {
                console.log("ContentExpander Warning: Failed to build" + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target + this.HEADER_SELECTOR).remove();

            // Remove events injected by the object
            $(this.target + this.CONTENT_EXPANDER_SELECTOR).off();
            $(this.target + this.HEADER_SELECTOR).off();

        } catch(err) {

            if (this.DEBUG_CE) {
                console.log("ContentExpander Warning: Failed to destroy " + err);
            }
        }
    }

}