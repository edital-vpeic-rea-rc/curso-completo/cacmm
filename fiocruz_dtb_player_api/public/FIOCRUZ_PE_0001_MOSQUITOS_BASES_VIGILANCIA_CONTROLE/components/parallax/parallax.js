/**
 * parallax.js adds a parallax effect to a container, using a background image that is going to be moved
 * according to the scroll movement of the screen or by hovering the mouse over the parallax container.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function Parallax() {
	this._initialized = false;
}

Parallax.prototype = {

    DEBUG_PARALLAX : true,

    MAX_MOVEMENT_DELTA : 0.5, // Set a maximum range for the movement of the bg animation
    
    model : {
        parallaxLayers : []
    },

    target : undefined,
    layersSelectors : [],

    mY : 0,
    mX : 0,
    isAnimating : false,

    /**
     * Events
     */
    cleanLayerSelectors: function (target) {
        try {
            var cleanedLayers = [];

            for (var i = 0; i < this.layersSelectors.length; i++) {

                if (gUi.isOnMobileViewMode()) {
                    this.MAX_MOVEMENT_DELTA = this.MAX_MOVEMENT_DELTA * 1.2;
                }

                var layerSelector = this.layersSelectors[i];
                var selectorNameParts = layerSelector.split(target.replace("#", ""));

                if (selectorNameParts.length > 1) {
                    cleanedLayers.push(layerSelector);
                }
            }

            return cleanedLayers;
        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to apply parallax to layer: " + err);
            }
        }
    },

    animateLayer: function (layer, speedMultiplier, addZoom, deltaX, deltaY) {
        try {

            if (layer != undefined) {

                if ($(layer).css("background-position-x") != undefined) {
                    var posX = Number($(layer).css("background-position-x").replace("px", ""));
                    var posY = Number($(layer).css("background-position-y").replace("px", ""));
                    var horizontalPosition = Math.round(posX + (deltaX * speedMultiplier)) + "px";
                    var verticalPosition = Math.round(posY + (deltaY * speedMultiplier)) + "px";

                    $(layer).css({
                        'background-position': "top " + verticalPosition + " right " + horizontalPosition,
                        'background-position-y': verticalPosition,
                        'background-position-X': horizontalPosition
                    });
                }
            }
        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to apply parallax to layer: " + err);
            }
        }
    },

    parallaxIt: function (pageY, pageX) {
        try {
            // Regulate the movement
            var deltaY = pageY - this.mY;
            var deltaX = pageX - this.mX;

            if (deltaY > this.MAX_MOVEMENT_DELTA) {
                deltaY = this.MAX_MOVEMENT_DELTA;

            } else if (deltaY < -this.MAX_MOVEMENT_DELTA) {
                deltaY = -this.MAX_MOVEMENT_DELTA;
            }

            if (deltaX > this.MAX_MOVEMENT_DELTA) {
                deltaX = this.MAX_MOVEMENT_DELTA;

            } else if (deltaX < -this.MAX_MOVEMENT_DELTA) {
                deltaX = -this.MAX_MOVEMENT_DELTA;
            }

            deltaY = -deltaY;
            deltaX = -deltaX;

            // Update the last movement position 
            this.mY = pageY;
            this.mX = pageX;

            // Animate the background
            this.animateLayer(this.target, 1, false, deltaX, deltaY);
            var layerSelectors = this.cleanLayerSelectors(this.target);

            for (var i = 0; i < layerSelectors.length; i++) {
                var layerSelector = this.target + " " + layerSelectors[i];

                if ((i % 2) == 0) {
                    this.animateLayer(layerSelector, i + 2, true, -deltaX, -deltaY);
                } else {
                    this.animateLayer(layerSelector, i + 4, false, deltaX, deltaY);
                }
            }
        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to parallaxIt: " + err);
            }
        }
    },

    updateContainerSize: function() {
        try {
            var ctx = this;
            var containerHeight = $(ctx.target).css("width");
            
            // Ensure that the container is going to be displayed
            $(ctx.target).stop();
            $(ctx.target).animate({
                height: containerHeight
            }, 250);
    
            // Add the necessary delay to handle the final state of the element
            setTimeout(function() {
                containerHeight = $(ctx.target).css("width");
    
                $(ctx.target).stop();
                $(ctx.target).animate({
                    height: containerHeight
                });
            }, 250);

        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to updateContainerSize: " + err);
            }
        }
    },

    /**
     * Initialization
     */ 
    initEvents: function() {
        var ctx = this;

        $(window).resize(function() {
            ctx.updateContainerSize();
        });

        $(ctx.target).bind('resize', function(e) {
            ctx.updateContainerSize();
        });

        $(ctx.target).mousemove(function (e) {
            ctx.parallaxIt(e.pageY, e.pageX);
        });
     
        $(window).scroll(function () {
            ctx.parallaxIt(window.scrollY, window.scrollX);
        });
    },

    initOutterContainer: function() {
        try {
            $(this.target).css("background", "url(" + this.model.bgImage + ")");
            $(this.target).css("background-repeat", "repeat");
            $(this.target).css("background-position-x", "-100px");
            $(this.target).css("background-position-y", "-100px");
            $(this.target).css("background-size", "calc(100% + 200px) calc(100% + 200px)");
        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to initOutterContainer: " + err);
            }
        }
    },

    getLayerTemplate: function() {
        return '<div class="parallax-layer {{layerId}}"'+
               'style="background: url(\'{{{imgSrc}}}\');'+
               'background-repeat: repeat;'+
               'background-position-x: 0px;'+
               'background-position-y: 0px;'+
               'background-size: 100% 100%;">'+
               '</div>';
    },

    getLayerId: function(layerNumber) {
        return this.target.replace("#", "") + "_layer_" + layerNumber;
    },

    initLayersSetContainerHeight: function(layersImages) {
        var ctx = this;

        setTimeout(function() {
            
            try {

                for (var i = 0; i < layersImages.length; i++) {
                    var layerImg = layersImages[i];
                    var layerId = ctx.getLayerId(i);

                    var layerModel = {};
                    layerModel.layerId = ctx.getLayerId(i);
                    layerModel.imgSrc = layersImages[i];

                    ctx.layersSelectors.push("." + layerModel.layerId);

                    var layerTemplate = ctx.getLayerTemplate();
                    var layerHtml = Mustache.to_html(layerTemplate, layerModel);

                    $(ctx.target).append($(layerHtml));
                }

                ctx.updateContainerSize();

            } catch (err) {

                if (this.DEBUG_PARALLAX) {
                    console.log("Parallax Warning: Failed to initLayersSetContainerHeight: " + err);
                }
            }

        }, 500);
    },

    build: function(tgt, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this.target = tgt;
                this.model = model;

                this.initOutterContainer();
                this.initLayersSetContainerHeight(this.model.parallaxLayers);
                this.initEvents();
            }
        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to build: " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.target).unbind();
            $(this.target).off('mousemove');

        } catch (err) {

            if (this.DEBUG_PARALLAX) {
                console.log("Parallax Warning: Failed to destroy: " + err);
            }
        }
    }

}