/**
 * timeline.js handles the actions of the object Timeline. 
 * 
 * Timeline is a UI presenter that displays content according to a selected time period, 
 * time periods are represented as imagens available at the bottom of the component. 
 * 
 * As the user drags an image from the selector, the container with the list of images, to the center,
 * the content being presented at container at the top of the component changes.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function Timeline() {
    this._initialized = false;
}

Timeline.prototype = {

    TIMELINE_WS: true,

    SELECTOR_PERIOD_SELECTOR: ".timeline .tl-period-selector .selector-objs-container .period-selector-obj .selector",
    SELECTOR_PERIOD_SELECTOR_CONTAINER: " .tl-period-selector-outter-container .selector-objs-container",
    SELECTOR_PERIOD_SELECTOR_CONTAINER_POS: " .tl-period-selector-outter-container .selector-objs-container .period-selector-obj",
    SELECTOR_POS_PERIOD_OBJECT: " .tl-period-selector-description span",
    SELECTOR_TIMELINE_PERIOD_CONTENT: " .tl-pcc",
    SELECTOR_PERIOD_TITLE: " .tl-selected-period-description .title",

    CLASS_SELECTED: "selected",
    CLASS_HIDDEN: "hidden",

    OFFSET_COMPONENT_MARGIN_LEFT_DESKTOP: 1.2,

    model: {
        title: "",
        psoTitle: "",
        periodSelectors: [
            {
                psoPos: 0,
                psoImgSvg: "",
                psoImgPng: ""
            },
        ]
    },
    target: undefined,

    cursorWidth: 0,
    selectedPeriod: 0,

    getPeriodSelectorCursorId: function () {
        return this.target + '_psc';
    },

    /**
     * Events
     */
    applyAnimationToSelectedPeriod: function (selectedPsoPos) {
        try {
            var ctx = this;

            $(this.target + this.SELECTOR_PERIOD_SELECTOR_CONTAINER_POS).each(function () {
                var psoPos = Number($(this).find(ctx.SELECTOR_POS_PERIOD_OBJECT).text());

                if (psoPos === selectedPsoPos) {
                    $(this).addClass(ctx.CLASS_SELECTED);
                } else {
                    $(this).removeClass(ctx.CLASS_SELECTED);
                }
            });
        } catch (err) {

            if (this.TIMELINE_WS) {
                console.log("Timeline WS Warning: object failed to applyAnimationToSelectedPeriod " + err);
            }
        }
    },

    displayContentForSelectedPeriod: function (selectedPeriod) {
        try {
            var psoContentObjs = $(this.target + this.SELECTOR_TIMELINE_PERIOD_CONTENT);

            for (var i = 0; i < $(this.target + this.SELECTOR_TIMELINE_PERIOD_CONTENT).length; i++) {
                var psoContent = $(this.target + this.SELECTOR_TIMELINE_PERIOD_CONTENT)[i];
                var psoModel = this.model.data[i];

                if (i === selectedPeriod) {
                    $(this.target + this.SELECTOR_PERIOD_TITLE).text(psoModel.title);

                    $(psoContent).removeClass(this.CLASS_HIDDEN);
                } else {
                    $(psoContent).addClass(this.CLASS_HIDDEN);
                }

                $(this.target + this.SELECTOR_TIMELINE_PERIOD_CONTENT)[i] = psoContent;
            }
        } catch(err) {

            if (this.TIMELINE_WS) {
                console.log("Timeline WS Warning: Failed to displayContentForSelectedPeriod, " + err);
            }
        }
    },
    
    selectSlide: function (selectedObj) {
        try {
            var selectedPeriod = $(selectedObj).find(this.SELECTOR_POS_PERIOD_OBJECT).text();

            if (selectedPeriod != undefined) {
                var idxSelectedPeriod = Number(selectedPeriod);

                this.displayContentForSelectedPeriod(idxSelectedPeriod);
                this.applyAnimationToSelectedPeriod(idxSelectedPeriod);
            }
        } catch (err) {

            if (this.TIMELINE_WS) {
                console.log("Timeline WS Warning: Failed to selectSlide, " + err);
            }
        }
    },

    initEvents: function () {
        var ctx = this;

        $(this.target + this.SELECTOR_PERIOD_SELECTOR).mousemove(function () {
            ctx.selectSlide($(this).parent());
        });

        $(this.target + this.SELECTOR_PERIOD_SELECTOR).on('click', function () {
            ctx.selectSlide($(this).parent());
        });
    },

    /**
     * Initialization
     */
    initUI: function () {
        this.applyAnimationToSelectedPeriod(0);
    },

    getTemplate: function () {
        return '<div class="tl-header hidden">' +
            '<p class="title">{{{title}}}</p>' +
            '</div>' +
            '<div class="tl-period-selector"> ' +
            '<div> ' +
            '<div class="tl-period-selector-outter-container"> ' +
            '<div class="selector-objs-container"> ' +
            '{{#periodSelectors}}' +
            '<div class="period-selector-obj selected"> ' +
            '<object data = "{{psoImgSvg}}" type = "image/svg+xml" class="tl-period-selector-img"> ' +
            '<img src = "{{psoImgPng}}" class="tl-period-selector-img"> ' +
            '</object> ' +
            '<div class="tl-period-selector-description"> ' +
            '<div class="ruller"></div> ' +
            '<span>{{psoPos}}</span> ' +
            '</div> ' +
            '<div class="selector"></div>' +
            '</div> ' +
            '{{/periodSelectors}}' +
            '</div> ' +
            '</div> ' +
            '</div> ' +
            '</div> ' +
            '<div class="tl-selected-period-description"> ' +
            '<h3 class="title">{{{psoTitle}}}</h3> ' +
            '</div> ';
    },

    build: function (tgt, model) {

        if (tgt != undefined && model != undefined) {
            this._initialized = true;
            this.target = tgt;
            this.model = model;

            var template = this.getTemplate();
            var componentHtml = Mustache.to_html(template, this.model);

            $(this.target).prepend(componentHtml);
            this.initUI();
            this.initEvents();
        }
    }

}