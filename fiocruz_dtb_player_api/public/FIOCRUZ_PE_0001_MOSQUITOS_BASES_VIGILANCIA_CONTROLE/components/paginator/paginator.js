/**
 * Paginator makes the navigation between .page objects easier.
 * It provides a navigation bar at the bottom of a container, allowing the selection of any page,
 * by clicking at a specific page index or by selecting the previous or next buttons.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function Paginator() {
    this._initialized = false;
}

Paginator.prototype = {

    DEBUG_PAGINATOR: true,

    SELECTOR_PAGE_CONTAINER: ".page-container",
    SELECTOR_PAGES: ".page-container .page",
    SELECTOR_PAGE_SELECTED: ".page.selected",
    SELECTOR_BTN_PREVIOUS_PAGE: ".page-selector .previous",
    SELECTOR_BTN_NEXT_PAGE: ".page-selector .next",
    SELECTOR_BTN_IDXS: ".page-selector .idxs",
    SELECTOR_BTN_PAGINATOR_BAR_IDX: ".page-idx",
    SELECTOR_BTN_PAGE_IDX: ".page-selector .idxs .page-idx",

    CLASS_PAGE: "page",
    CLASS_SELECTED: "selected",

    target: "",
    callback: undefined,
    currentPage: -1,
    numPages: 0,

    /**
     * Events
     */
    navigateToPageN: function (pageIdx) {
        try {
            var ctx = this;
            var numPaginatorBars = 0;

            // Update the page selector buttons of the paginators' upper and bottom bars, which are attached to the target element
            $(ctx.target + " " + ctx.SELECTOR_BTN_IDXS).each(function () {
                var btnPaginatorBarIdx = 0;

                $(this).find(ctx.SELECTOR_BTN_PAGINATOR_BAR_IDX).each(function () {

                    if (btnPaginatorBarIdx === (pageIdx - 1) && !$(this).hasClass(ctx.CLASS_SELECTED)) {
                        $(this).addClass(ctx.CLASS_SELECTED);

                    } else if (btnPaginatorBarIdx !== (pageIdx - 1)) {
                        $(this).removeClass(ctx.CLASS_SELECTED);
                    }

                    btnPaginatorBarIdx++;
                });

                btnPaginatorBarIdx = 0;
                numPaginatorBars++;
            });

            // Update the page
            var pageNumber = 1;

            $(ctx.target + " " + ctx.SELECTOR_PAGES).each(function () {

                if (pageNumber === pageIdx && !$(this).hasClass(ctx.CLASS_SELECTED)) {
                    $(this).addClass(ctx.CLASS_SELECTED);

                } else if (pageNumber !== pageIdx) {
                    $(this).removeClass(ctx.CLASS_SELECTED);
                }

                pageNumber++;
            });

            // Invoke the callback function to let it know about the conclusion of the select page event
            if (typeof(this.callback) !== "undefined") {
                this.callback(pageIdx);
            }
        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to change the selected paginator btn " + err);
            }
        }
    },

    previousPage: function () {
        try {

            if (this.currentPage > 1) {
                this.currentPage--;
            }
    
            this.navigateToPageN(this.currentPage);
        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to previousPage " + err);
            }
        }
    },

    nextPage: function () {
        try {

            if (this.currentPage < this.numPages) {
                this.currentPage++;
            }

            this.navigateToPageN(this.currentPage);

        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to nextPage " + err);
            }
        }
    },

    navigateToSelectedPage: function (selectedIdxBtn) {
        try {
            var selectedIdx = Number($(selectedIdxBtn).html());

            if (selectedIdx >= 0 && selectedIdx <= this.numPages) {
                this.currentPage = selectedIdx;
            }

            this.navigateToPageN(this.currentPage);

        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to navigateToSelectedPage " + err);
            }
        }
    },

    initEvents: function () {
        var ctx = this;

        $(ctx.SELECTOR_BTN_PREVIOUS_PAGE).on("click", function () {
            ctx.previousPage();
        });

        $(ctx.SELECTOR_BTN_NEXT_PAGE).on("click", function () {
            ctx.nextPage();
        });

        $(ctx.SELECTOR_BTN_PAGE_IDX).on("click", function () {
            ctx.navigateToSelectedPage($(this));
        });
    },

    /**
     * Initialization
     */
    initPages: function () {
        try {
            this.numPages = Number($(this.target + " " + this.SELECTOR_PAGES).length);
            this.currentPage = 1;

        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to initPages " + err);
            }
        }
    },

    getPageSelectorTemplate: function () {
        return '<div class="page-selector">' +
            '<div class="previous">' +
            '<span>&lsaquo;</span>' +
            '</div>' +
            '<div class="idxs">' +
            '{{#pageIdxs}}'+
            '<span class="page-idx {{isSelected}}">{{pageIdx}}</span>' +
            '{{/pageIdxs}}'+
            '</div>' +
            '<div class="next">' +
            '<span>&rsaquo;</span>' +
            '</div>' +
            '</div>';
    },

    getPageSelectorModel: function() {
        try {
            var model = { 
                "pageIdxs": [] 
            };

            for (var i = 1; i <= this.numPages; i++) {
                var pageBtnSelector = {};
                pageBtnSelector.pageIdx = i;

                if (i === 1) {
                    pageBtnSelector.isSelected = this.CLASS_SELECTED;
                } else {
                    pageBtnSelector.isSelected = "";
                }

                model.pageIdxs.push(pageBtnSelector);
            }

            return model;

        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to getPageSelectorModel " + err);
            }

            return undefined;
        }
    },

    addPageSelectors: function () {
        var model = this.getPageSelectorModel();
        var template = this.getPageSelectorTemplate();
        var pageSelector = Mustache.to_html(template, model);

        $(this.target).prepend(pageSelector);
        $(this.target).append(pageSelector);
    },

    build: function (tgt, callback) {
        try {

            if (typeof(tgt) !== "undefined") {
                this.target = tgt;
                this.callback = callback;
    
                this.initPages();
                this.addPageSelectors();
                this.initEvents();
            }

        } catch (err) {

            if (this.DEBUG_PAGINATOR) {
                console.log("Paginator Warning: Failed to build object " + err);
            }
        }
    }

}