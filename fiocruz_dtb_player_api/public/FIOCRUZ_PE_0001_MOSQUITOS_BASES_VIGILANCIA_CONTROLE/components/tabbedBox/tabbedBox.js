/**
 * tabbedBox.js is a component that encapsulates the behavior of a tab layout. 
 * If the user selects a tab, the actions related to the UI of the TabbedBox are triggered,
 * in order to show that the selection worked and a callback function is then executed, 
 * letting the caller function handle any other action that might be related to the selection of the tab.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function TabbedBox() {
	this._initialized = false;
}

TabbedBox.prototype = {

    DEBUG_TABBED_BOX: true,
    
    MENU_ITEM_SELECTOR: " .tabbed-box-menu li",
    CLASS_SELECTED: "selected",

    model : {
        tabbedBoxOptions : [
            {
                label: "",
                selected: ""
            }
        ]
    },

    target : undefined,

    callback: undefined,

    /**
     * Initialization
     */
    getTabbedBoxMenu: function () {
        return '<ul class="tabbed-box-menu">' +
            '{{#tabbedBoxOptions}}'+
            '<li id="{{id}}" class="{{selected}}"><a href="#">{{label}}</a></li>' +
            '{{/tabbedBoxOptions}}'+
            '</ul>';
    }, 

    /**
     * Events
     */
    selectTab: function(selectedItemId) {
        try {
            var ctx = this;

            if (selectedItemId != undefined) {
                this.callback(selectedItemId);
            }

            // Update the selected option of the tabbed menu
            $(ctx.target + ctx.MENU_ITEM_SELECTOR).each(function() {
                var listItemId = $(this).attr("id");

                if (listItemId === selectedItemId) {
                    $(this).addClass(ctx.CLASS_SELECTED);
                } else {
                    $(this).removeClass(ctx.CLASS_SELECTED);
                }
            });

            // Update the selected option of the tabbed menu
            $(ctx.target + ctx.MENU_ITEM_SELECTOR).each(function() {
                var listItemId = $(this).attr("id");

                if (listItemId === selectedItemId) {
                    $(this).addClass(ctx.CLASS_SELECTED);
                } else {
                    $(this).removeClass(ctx.CLASS_SELECTED);
                }
            });
        } catch(err) {

            if (this.DEBUG_TABBED_BOX) {
                console.log("TabbedBox Warning: Failed to selectTab " + err);
            }
        }
    },

    onTabbedBoxItemSelected: function(selectedTab) {
        try {
            var selectedItemId = $(selectedTab).attr("id");
            this.selectTab(selectedItemId);
        } catch(err) {

            if (this.DEBUG_TABBED_BOX) {
                console.log("TabbedBox Warning: Failed to onTabbedBoxItemSelected " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        $(ctx.target + ctx.MENU_ITEM_SELECTOR).on("click", function(){
            ctx.onTabbedBoxItemSelected($(this));
        });
    },

    build : function(tgt, model, callback) {
        try {
            this.target = tgt;
            this.callback = callback;

            if (model != undefined) {
                this.model = model;

                var template = this.getTabbedBoxMenu();
                var html = Mustache.to_html(template, this.model);
        
                $(this.target).prepend(html);
                this.initEvents();
            }
        } catch(err) {

            if (this.DEBUG_TABBED_BOX) {
                console.log("TabbedBox Warning: Failed to onTabbedBoxItemSelected " + err);
            }
        }
    }

}