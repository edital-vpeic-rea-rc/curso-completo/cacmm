/**
 * contentSelector.js handles the actions of the component contentSelector, which is a container 
 * that allows the selection of a content object which is stored within a given list.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ContentSelector() {
	this._initialized = false;
}

ContentSelector.prototype = {

    DEBUG_CS: true,

    SELECTOR_CONTENT_OBJ: " .contents.shelf .content-object .description, .contents.shelf .content-object .meta .selector, .contents.shelf .content-object .toolbar .selector",

    model: {
        hasHeader: true,
        hasFooter: true,
        title: "",
        footerNote: "",
        contentList: [
            {
                title: "",
                author: "",
                initialPage: 0,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: "",
                icoInternetPng: "",
                fileUrl: "",
                internetUrl: ""
            }
        ]
    },

    target : undefined,
    callback : undefined,

    /**
     * Events
     */
    getObjectId: function(fakeId) {
        return this.target + '_contents_obj_' + fakeId;
    },

    getFakeId: function(objId) {
        try {
            var prefix = this.target + '_contents_obj_';
            var fakeId = objId.replace(prefix, '');

            return fakeId;

        } catch(err) {

            if (this.DEBUG_CS) {
                console.log("ContentSelector Warning: Failed to getFakeId " + err);
            }

            return undefined;
        }
    },

    getObjectById: function(objId) {
        try {
            var ctx = this;
            var fakeId = ctx.getFakeId(objId);

            for (var i = 0; i < this.model.contentList.length; i++) {
                var obj = this.model.contentList[i];

                if (obj.id === Number(fakeId)) {
                    return obj;
                }
            };
        } catch(err) {

            if (this.DEBUG_CS) {
                console.log("ContentSelector Warning: Failed to getObjectById " + err);
            }
        }
    },

    getSelectedContentObject: function(selectedObj) {
        try {
            var objId = $(selectedObj).attr("item-id");
            var contentObj = this.getObjectById(objId);
            
            if (contentObj != undefined) {
                this.callback(contentObj.fileUrl, contentObj.internetUrl, contentObj.title, contentObj.initialPage, contentObj.fileType);
            }
            
        } catch(err) {

            if (this.DEBUG_CS) {
                console.log("ContentSelector Warning: Failed to getSelectedContentObject " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        // Handle the selection of a content object
        $(this.target + this.SELECTOR_CONTENT_OBJ).on('click', function() {
            ctx.getSelectedContentObject($(this));
        });

    },

    /**
     * Initialization
     */
    getTemplate: function () {
        return '{{#hasHeader}}'+
            '<div class="contents header">' +
            '<h3>{{{title}}}</h3>' +
            '</div>' +
            '{{/hasHeader}}' +
            '{{^hasHeader}}' +
            '{{/hasHeader}}' +
            '<div class="contents shelf">' +
            '{{#contentList}}'+
            '{{#publisherIconSvg}}' +
            '<div class="content-object info">' +
            '{{/publisherIconSvg}}' +
            '{{^publisherIconSvg}}' +
            '<div class="content-object no-meta-info">' +
            '{{/publisherIconSvg}}' +
            '<div item-id="'+ this.target + '_contents_obj_{{id}}" class="description">' +
            '<h4 class="title">{{{title}}}</h4>' +
            '<small class="author">{{{author}}}</small>' +
            '</div>' +
            '{{#publisherIconSvg}}' +
            '<div item-id="'+ this.target + '_contents_obj_{{id}}" class="meta">' +
            '<object data="{{publisherIconSvg}}" type="image/svg+xml" class="publisher-img">' +
            '<img src="{{publisherIconPng}}" class="publisher-img">' +
            '</object>' +
            '<div item-id="'+ this.target + '_contents_obj_{{id}}" class="selector"></div>' +
            '</div>' +
            '{{/publisherIconSvg}}' +
            '{{^publisherIconSvg}}' +
            '{{/publisherIconSvg}}' +
            '<div class="toolbar">' +
            '<div item-id="'+ this.target + '_contents_obj_{{id}}" class="outter-container">' +
            '{{#fileUrl}}' +
            '<object data="{{icoReadSvg}}" type="image/svg+xml" class="toolbar-item">' +
            '<img src="{{icoReadPng}}">' +
            '{{/fileUrl}}' +
            '{{^fileUrl}}' +
            '<object data="{{icoInternetSvg}}" type="image/svg+xml" class="toolbar-item">' +
            '<img src="{{icoInternetPng}}">' +
            '{{/fileUrl}}' +
            '</object>' +
            '</div>' +
            '<div item-id="'+ this.target + '_contents_obj_{{id}}" class="selector"></div>' +
            '</div>' +
            '</div>' +
            '{{/contentList}}'+
            '</div>' +
            '{{#hasFooter}}'+
            '<div class="contents footer">' +
            '<small>{{{footerNote}}}</small>' +
            '</div>' +
            '{{/hasFooter}}'+
            '{{^hasFooter}}'+
            '{{/hasFooter}}';
    }, 

    build : function(tgt, callback, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this._initialized = true;
                this.target = tgt;
                this.model = model;
                this.callback = callback;

                var template = this.getTemplate();
                var componentHtml = Mustache.to_html(template, this.model);
                $(this.target).append(componentHtml);

                this.initEvents();
            }
        } catch(err) {

            if (this.DEBUG_CS) {
                console.log("ContentSelector Warning: Failed to build object " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.target + this.SELECTOR_CONTENT_OBJ).off();

        } catch(err) {

            if (this.DEBUG_CS) {
                console.log("ContentSelector Warning: Failed to destroy object " + err);
            }
        }
    }

}