/**
 * simpleTooltip.js Creates a tooltip on the left || top || right || bottom of an element, letting
 * the user insert custom HTML inside the tooltip.
 * 
 * Minor adjustments to the positioning of the tooltips must be made by overriding a few style classes
 * on the css of the page, where the tooltip is applied.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

/**
 * Global parameters used to build SimpleTooltip objects
 */
SimpleTooltipParams = {
    DISPLAY_AT_REGULAR_DEVICES : {
        TOP: "st-display-regular-top",
        RIGHT: "st-display-regular-right",
        BOTTOM: "st-display-regular-bottom",
        LEFT: "st-display-regular-left"
    },
    DISPLAY_AT_MOBILE_DEVICES : {
        TOP: "st-display-mobile-top",
        BOTTOM: "st-display-mobile-bottom"
    },
    ST_STATES : {
        OPENED : "OPENED",
        CLOSED : "CLOSED"
    }
}

/**
 * SimpleTooltip object
 */
function SimpleTooltip() {
	this._initialized = false;
}

SimpleTooltip.prototype = {

    DEBUG_ST : true,

    SIMPLE_TOOLTIP_OUTTER_CONTAINER: " .spu-outter",
    SIMPLE_TOOLTIP_CLOSE_BTN_SELECTOR : " .spu-outter .spu-header .spu-toolbox > .spu-btn.close",

    model : {
        displayAtRegular: 'st-display-regular-right',
        displayAtMobile: 'st-display-mobile-bottom',
        titleCode: '<div class="class-a-or-b">title</div>',
        bodyCode: '<div class="class-c-or-d">content</div>',
    },
    target : undefined,
    callback: undefined,

    getIdFromSelector: function(selector) {
        try {
            return selector.replace("#", "").replace(".", "");

        } catch (err) {
            
            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: could not getIdFromSelector, " + err);
            }
        }

        return "";
    },

    getSelector: function(className) {
        try {
            return this.target + className;

        } catch (err) {
            
            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: could not getSelector, " + err);
            }

            return undefined;
        }
    },

    /**
     * Events
     */
    openTooltip: function() {
        try {
            $(this.target).removeClass(gUi.CLASS_HIDDEN);

            if (typeof(this.callback) !== "undefined") {
                this.callback(SimpleTooltipParams.ST_STATES.OPENED, this.target);
            }

        } catch (err) {
            
            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: could not openTooltip, " + err);
            }
        }
    },

    closeTooltip: function() {
        try {
            $(this.target).addClass(gUi.CLASS_HIDDEN);

            if (typeof(this.callback) !== "undefined") {
                this.callback(SimpleTooltipParams.ST_STATES.CLOSED, this.target);
            }

        } catch (err) {
            
            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: could not closeTooltip, " + err);
            }
        }
    },

    toggleTooltip: function() {
        try {
            
            if ($(this.target).hasClass(gUi.CLASS_HIDDEN)) {
                this.openTooltip();
            } else {
                this.closeTooltip();
            }

        } catch (err) {
            
            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: could not toggleTooltip, " + err);
            }
        }  
    },

    /**
     * Initialization
     */
    initEvents: function() {
        var ctx = this;

        $(ctx.getSelector(ctx.SIMPLE_TOOLTIP_CLOSE_BTN_SELECTOR)).on("click", function() {
            ctx.closeTooltip();
        });

    },

    initUI: function () {
        try {
            // Add the display at classes to the tooptip outter container
            $(this.target).addClass(this.model.displayAtRegular);
            $(this.target).addClass(this.model.displayAtMobile);

            this.closeTooltip();

        } catch (err) {

            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: Could not initUI of this SimpleTooltip object.");
            }
        }
    },

    getTemplate: function () {
        return  '<div class="spu-outter">' +
            '<div class="spu-header">' +
            '<div class="spu-title">' +
            '{{{titleCode}}}' +
            '</div>' +
            '<div class="spu-toolbox">' +
            '<button type="button" class="spu-btn close" data-dismiss="modal">×</button>' +
            '</div>' +
            '</div>' +
            '<div class="spu-body">' +
            '{{{bodyCode}}}' +
            '</div>' + 
            '</div>';
    },

    buildSkeleton: function(tgt, model, callback) {
        try {
            this.target = tgt;
            this.model = model;
            this.callback = callback;
    
            var template = this.getTemplate();
            var html = Mustache.to_html(template, this.model);
    
            $(this.target).html(html);

        } catch (err) {

            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Warning: Could not buildSkeleton.");
            }
        }
    },

    build : function(tgt, model, callback) {
        try {

            if (tgt != undefined && model != undefined) {
                this._initialized = true;

                this.buildSkeleton(tgt, model, callback);
                this.initUI();
                this.initEvents();
            }
        } catch(err) {

            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Failed to build SimpleTooltip object " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html();

            // Remove events injected by the object
            $(this.getSelector(this.SIMPLE_TOOLTIP_CLOSE_BTN_SELECTOR)).off();
        } catch(err) {

            if (this.DEBUG_ST) {
                console.log("SimpleTooltip Failed to destroy object " + err);
            }
        }
    }

}