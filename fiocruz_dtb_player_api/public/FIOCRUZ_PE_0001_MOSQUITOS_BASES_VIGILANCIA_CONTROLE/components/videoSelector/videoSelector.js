/**
 * videoSelector.js handles the actions of the component VideoSelector, which is a container 
 * that allows the selection of a video object which is stored within an horizontal list.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function VideoSelector() {
	this._initialized = false;
}

VideoSelector.prototype = {

    DEBUG_VS: true,

    SELECTOR_VIDEO_OBJ: " .vs.shelf .video-object .vo.filter",
    SELECTOR_VIDEO_OBJ_1: " .vs.shelf .video-object .vo.filter div div .vo-filter-img-container .selector",
    
    model: {
        hasHeader: true,
        hasFooter: true,
        title: "",
        footerNote: "",
        videosList: [
            {
                coverImg: "",   
                title: "",
                subTitle: "",
                imgSvgVideoSrc: "",
                imgPngVideoSrc: "",
                videoSrcText: "",
                imgSvgFilterIcon: "",
                imgPngFilterIcon: "",
                filterText: "",
                videoUrl: ""
            }
        ]
    },

    target : undefined,
    callback : undefined,

    /**
     * Events
     */
    getObjectId: function(fakeId) {
        return this.target + '_video_obj_' + fakeId;
    },

    getFakeId: function(objId) {
        try {

            if (objId != undefined) {
                var prefix = this.target + '_video_obj_';
                var fakeId = objId.replace(prefix, '');
                
                return fakeId;
            }
        } catch (err) {

            if (this.DEBUG_VS) {
                console.log("VideoSelector Warning: Failed to getFakeId " + err);
            }
        }

        return undefined;
    },

    getObjectById: function(objId) {
        try {
            var ctx = this;
            var fakeId = ctx.getFakeId(objId);

            if (fakeId != undefined) {

                for (var i = 0; i < this.model.videosList.length; i++) {
                    var obj = this.model.videosList[i];
    
                    if (obj.id === Number(fakeId)) {
                        return obj;
                    }
                };
            }
        } catch(err) {

            if (this.DEBUG_VS) {
                console.log("VideoSelector Warning: Failed to getObjectById " + err);
            }
        }

        return undefined;
    },

    getSelectedVideoObject: function(selectedObj) {
        try {
            var objId = $(selectedObj).attr("id");
            var videoObj = this.getObjectById(objId);

            if (videoObj != undefined) {
                this.callback(videoObj.fileUrl, videoObj.internetUrl, videoObj.title, videoObj.subTitle, videoObj.videoResolution);
            }
        } catch(err) {

            if (this.DEBUG_VS) {
                console.log("VideoSelector Warning: Failed to get the selected video object " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        // Handle the selection of a video object
        $(this.target + this.SELECTOR_VIDEO_OBJ).on('click', function() {
            ctx.getSelectedVideoObject($(this));
        });

        $(this.target + this.SELECTOR_VIDEO_OBJ_1).on('click', function() {
            ctx.getSelectedVideoObject($(this));
        });

    },

    /**
     * Initialization
     */
    getTemplate: function () {
        return '{{#hasHeader}}' +
            '<div class="vs header">' +
            '<h3>{{{title}}}</h3>' +
            '</div>' +
            '{{/hasHeader}}' +
            '{{^hasHeader}}' +
            '{{/hasHeader}}' +
            '<div class="vs shelf">' +
            '<ul>' +
            '{{#videosList}}' +
            '<li>' +
            '<div class="video-object">' +
            '<div class="vo header">' +
            '<div>' +
            '<img src="{{coverImg}}" class="vo-header-img">' +
            '</div>' +
            '</div>' +
            '<div class="vo description">' +
            '<div>' +
            '<h4 class="title">{{{title}}}</h4>' +
            '<small>{{{subTitle}}}</small>' +
            '</div>' +
            '</div>' +
            '<div class="vo video-source">' +
            '<div>' +
            '<object data="{{imgSvgVideoSrc}}" type="image/svg+xml" class="vo-video-source-img">' +
            '<img src="{{imgPngVideoSrc}}">' +
            '</object>' +
            '<small class="video-src">{{videoSrcText}}</small>' +
            '</div>' +
            '</div>' +
            '<div id="'+ this.target + '_video_obj_{{id}}" class="vo filter">' +
            '<div>' +
            '<div>' +
            '<div class="vo-filter-img-container">' +
            '<object data="{{imgSvgFilterIcon}}" type="image/svg+xml" class="vo-filter-img">' +
            '+<img src="{{imgPngFilterIcon}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</div>' +
            '<small class="extra-info">{{{filterText}}}</small>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '{{/videosList}}' +
            '</ul>' +
            '</div>' +
            '{{#hasFooter}}' +
            '<div class="vs footer">' +
            '<small>{{{footerNote}}}</small>' +
            '</div>' +
            '{{/hasFooter}}' +
            '{{^hasFooter}}' +
            '{{/hasFooter}}';
    }, 

    build : function(tgt, callback, model) {

        if (tgt != undefined && model != undefined) {
            this._initialized = true;
            this.target = tgt;
            this.callback = callback;
            this.model = model;

            var template = this.getTemplate();
            var videoComponentHtml = Mustache.to_html(template, this.model);
            $(this.target).append(videoComponentHtml);

            this.initEvents();
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.target + this.SELECTOR_VIDEO_OBJ).off();
            $(this.target + this.SELECTOR_VIDEO_OBJ_1).off();

        } catch(err) {

            if (this.DEBUG_VS) {
                console.log("VideoSelector Warning: Failed to destroy video object " + err);
            }
        }
    }

}