/**
 * The FullScreeMsg dialog is used to display a very important message that must ocupy the entire
 * screen.
 * You can pass in a Title, a message, an extra message and have a confirmation button 
 * to dismiss the dialog as well as a close button
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function FullScreenDialog() {
    this._initialized = false;
}

FullScreenDialog.prototype = {

    SELECTOR_BTN_START: " .btn-start",
    SELECTOR_BTN_START_ICO: " .ico-btn-start",

    CLASS_DISMISSED: "dismissed",
    CLASS_HIDDEN: "hidden",

    model: {
        title: "",
        msg: "",
        extraMsg: "",
        hasCloseBtn: false,
        hasConfirmBtn: true,
        icoStartImg: "",
    },

    target: undefined,

    callback: undefined,

    getTemplate: function () {
        return '<div class="fsm-body no-header no-footer center">' +
            '<div class="msg-container">' +
            '{{#title}}' +
            '<h1 class="fsm-title">{{title}}</h1>' +
            '{{/title}}' +
            '{{#msg}}' +
            '<div class="fsm-msg">' +
            '<p>{{msg}}</p>' +
            '</div>' +
            '{{/msg}}' +
            '{{#imgMsg}}' +
            '<div class="fsm-img-msg-container">'+
            '<img src="{{imgMsg}}" class="fsm-img-msg">'+
            '</div>'+
            '{{/imgMsg}}' +
            '{{#extraMsg}}' +
            '<div class="fsm-extra-info">' +
            '<p>{{extraMsg}}</p>' +
            '</div>' +
            '{{/extraMsg}}' +
            '{{#icoStartImg}}' +
            '<a class="btn btn-start">' +
            '<span>' +
            '<img src="{{icoStartImg}}" class="ico-btn-start"> <small>Começar</small>' +
            '</span>' +
            '</a>' +
            '{{/icoStartImg}}' +
            '</div>' +
            '</div>';
    },

    /**
     * Events
     */
    initEvents: function() {
        var ctx = this;

        $(this.target + this.SELECTOR_BTN_START).on("click", function() {
            ctx.callback();
        });

        $(this.target + this.SELECTOR_BTN_START_ICO).on("click", function() {
            ctx.callback();
        });
    },

    dismiss: function() {
        $(this.target).addClass(this.CLASS_DISMISSED);
    },

    show: function() {
        $(this.target).removeClass(this.CLASS_HIDDEN);
    },

    build: function (tgt, model, callback) {
        this.target = tgt;
        this.callback = callback;

        if (model != undefined) {
            this.model = model;
            var template = this.getTemplate();
            var dialogHtml = Mustache.to_html(template, this.model);
            $(this.target).html(dialogHtml);

            this.initEvents();
        }
    }

}
