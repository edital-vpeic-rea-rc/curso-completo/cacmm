/**
 * This scripts handles the object used to illustrate the effects of the congenital Zika Virus in a new born
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function CongenitalZikaVirus() {
    this._initialized = false;
}

CongenitalZikaVirus.prototype = {

    CONGENITAL_ZIKV_DEBUG: true,

    TIMEOUTS: {
        SCROLL_BOTTOM: 750,
        ANIMATION_DRAW_LINE: 250
    },

    CLASSES: {
        CONNECTED: "connected",
        SELECTED: "selected"
    },

    SELECTORS: {
        PAGE_BODY: "html, body",
        ALL_BUTTONS: ".main-baby-selectors .selector .icon",
        BTN_ICON: ".icon",
        BTN_LINE: ".line",
        V_LINE: ".v-line",
        HEAD_TRIGGER: ".mb-head-selector",
        FACE_TRIGGER: ".mb-face-selector",
        THROAT_TRIGGER: ".mb-throat-selector",
        BODY_TRIGGER: ".mb-body-selector",
        TOPIC_INFO: ".congenital-syndrom .topics .topic",
    },

    TRIGGER_CONTENT_MAPPING: {
        ".mb-head-selector": [
            ".mb-head-topic-1",
            ".mb-head-topic-2"
        ],
        ".mb-face-selector": [
            ".mb-face-topic-1"
        ],
        ".mb-throat-selector": [
            ".mb-throat-topic-1"
        ],
        ".mb-body-selector": [
            ".mb-body-topic-1"
        ]
    },

    model: {
        selectedItem: undefined,
        img_baby: "images/img_congenital_syndron_main_baby.png",
        info_topic_head_1: {
            img: "images/img_congenital_syndron_topic_1.gif",
            content: [
                {
                    text: "Malformações no SNC (calcificações cerebrais, distúrbio do desenvolvimento cortical cerebral, ventrículomegalia, hipoplasia do tronco cerebral, do cerebelo e corpo caloso)"
                },
                {
                    text: "Dismorfismo característico da caixa craniana, incluindo microcefalia (-2dp)"
                }
            ]
        },
        info_topic_head_2: {
            img: "images/img_congenital_syndron_topic_2.gif",
            content: [
                {
                    text: "Crises epilépticas"
                }
            ]
        },
        info_topic_face_1: {
            img: "images/img_congenital_syndron_topic_3.gif",
            content: [
                {
                    text: "Alterações neurossensoriais (auditiva e visual)"
                }
            ]
        },
        info_topic_throat_1: {
            img: "images/img_congenital_syndron_topic_4.gif",
            content: [
                {
                    text: "Dificuldades de sucção, deglutição, até disfagia"
                }
            ]
        },
        info_topic_body_1: {
            img: "images/img_congenital_syndron_topic_5.gif",
            content: [
                {
                    text: "Comprometimento do sistema osteomioarticular (artrogripose)"
                },
                {
                    text: "Atraso no desenvolvimento neuropsicomotor com alterações do tônus, hiperexcitabilidade, exagero ou persistência dos reflexos primitivos"
                }
            ]
        },
        selector_classes: [
            {
                class: "mb-head-selector"
            },
            {
                class: "mb-face-selector"
            },
            {
                class: "mb-throat-selector"
            },
            {
                class: "mb-body-selector"
            }
        ]
    },

    target: undefined,
    isSelectingTopic: false,

    // Getters and Setters
    getTargetId: function () {
        try {
            return $(this.target).attr("id");

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to getTargetId " + err);
            }

            return "";
        }
    },

    // Other methods
    getTemplate: function () {
        return '';
    },

    drawLineFromObjSelectorToImage: function (horizontalLine, objA, objB) {
        try {
            var ctx = this;

            // Start animation after a timeout
            setTimeout(function () {
                coordAX = $(objA).position().left;
                coordAY = $(objA).position().top;
                var distance = 0;
                var coordBX = 0;
                var coordBY = 0;

                if (horizontalLine) {
                    distance = $(objB).offset().left - $(objA).offset().left + 50;
                    coordBX = coordAX + distance;
                    coordBY = $(objA).position().top;

                    // Draw line from object A to Object B on canvas
                    $(objA).find(ctx.SELECTORS.BTN_LINE).animate({
                        "width": distance
                    }, ctx.TIMEOUTS.ANIMATION_DRAW_LINE);
                } else {
                    distance = $(objB).offset().top - $(objA).offset().top + 50;
                    coordBX = $(objA).position().left;
                    coordBY = coordAY + distance;

                    // Draw line from object A to Object B on canvas
                    $(objA).find(ctx.SELECTORS.BTN_LINE).animate({
                        "height": distance
                    }, ctx.TIMEOUTS.ANIMATION_DRAW_LINE);
                }

                $(ctx.SELECTORS.V_LINE).addClass(ctx.CLASSES.CONNECTED);

                // Finish the selection event after the animation is complete
                ctx.isSelectingTopic = false;

            }, ctx.TIMEOUTS.ANIMATION_DRAW_LINE);
        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to drawLineFromCoordAB " + err);
            }

            // Finish the selection event after the animation is complete
            ctx.isSelectingTopic = false;
        }
    },

    hideLines: function () {
        try {
            $(this.SELECTORS.BTN_LINE).each(function () {
                $(this).css("heigth", "0px");
                $(this).css("width", "0px");
            });

            $(this.SELECTORS.V_LINE).removeClass(this.CLASSES.CONNECTED);
        } catch (err) {
            console.log("Warning: Failed to hideLines " + err);
        }
    },

    drawLinesSelectedItem: function (firedTrigger) {
        try {
            this.hideLines();

            // Show the selected content container
            var contentContainers = this.TRIGGER_CONTENT_MAPPING[firedTrigger];

            // If there is more than one topic for the selected item
            if (contentContainers.length > 1) {

                // Draw lines between the topics of the selected item
                for (var i = 0; i < contentContainers.length; i++) {

                    if (i > 0) {
                        var previousItem = contentContainers[i - 1];
                        var nextItem = contentContainers[i];
                        this.drawLineFromObjSelectorToImage(false, previousItem, nextItem);
                    }
                }
            }

            // Draw line from item selector to first content item
            var contentContainer = contentContainers[0];
            this.drawLineFromObjSelectorToImage(true, firedTrigger, contentContainer);

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to drawLinesSelectedItem: " + err);
            }
        }
    },

    showContent: function (firedTrigger) {
        try {
            this.model.selectedItem = firedTrigger;

            // Add class to identify the selected option
            $(this.SELECTORS.ALL_BUTTONS).removeClass(this.CLASSES.SELECTED);
            $(firedTrigger).find(this.SELECTORS.BTN_ICON).addClass(this.CLASSES.SELECTED);

            // Hide all content containers
            $(this.SELECTORS.TOPIC_INFO).addClass(gUi.CLASS_HIDDEN);

            // Show the selected content container
            var contentContainers = this.TRIGGER_CONTENT_MAPPING[firedTrigger];

            for (var i = 0; i < contentContainers.length; i++) {
                var contentContainerSelector = contentContainers[i];
                $(contentContainerSelector).removeClass(gUi.CLASS_HIDDEN);
            }

            // Draw line from button to content
            this.drawLinesSelectedItem(firedTrigger);

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to showContent " + err);
            }
        }
    },

    redrawSelectedItem: function () {
        try {

            if (!gUi.isOnMobileViewMode()) {
                this.showContent(this.model.selectedItem);
            }

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to redrawSelectedItem " + err);
            }
        }
    },

    scrollToContentForMobile: function (selectedTrigger) {
        try {
            var offsetTop = $(selectedTrigger).offset().top;

            switch (selectedTrigger) {

                case this.SELECTORS.HEAD_TRIGGER: {
                    $(this.SELECTORS.PAGE_BODY).animate({
                        scrollTop: offsetTop + 100
                    }, this.TIMEOUTS.SCROLL_BOTTOM);

                    break;
                }

                case this.SELECTORS.FACE_TRIGGER: {
                    $(this.SELECTORS.PAGE_BODY).animate({
                        scrollTop: offsetTop + 95
                    }, this.TIMEOUTS.SCROLL_BOTTOM);

                    break;
                }

                case this.SELECTORS.THROAT_TRIGGER: {
                    $(this.SELECTORS.PAGE_BODY).animate({
                        scrollTop: offsetTop + 90
                    }, this.TIMEOUTS.SCROLL_BOTTOM);

                    break;
                }

                case this.SELECTORS.BODY_TRIGGER: {
                    $(this.SELECTORS.PAGE_BODY).animate({
                        scrollTop: offsetTop + 80
                    }, this.TIMEOUTS.SCROLL_BOTTOM);

                    break;
                }
            }

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to redrawSelectedItem " + err);
            }
        }
    },

    // Events
    initEvents: function () {
        try {
            var ctx = this;

            $(ctx.SELECTORS.HEAD_TRIGGER).on("click", function () {
                if (!ctx.isSelectingTopic) {
                    ctx.isSelectingTopic = true;
                    ctx.showContent(ctx.SELECTORS.HEAD_TRIGGER);

                    if (gUi.isOnMobileViewMode()) {
                        ctx.scrollToContentForMobile(ctx.SELECTORS.HEAD_TRIGGER);
                    }

                } else if (this.CONGENITAL_ZIKV_DEBUG) {
                    console.log("Warning: Can't select option, it is currently finishing the selection of an item.");
                }
            });

            $(ctx.SELECTORS.FACE_TRIGGER).on("click", function () {
                if (!ctx.isSelectingTopic) {
                    ctx.isSelectingTopic = true;
                    ctx.showContent(ctx.SELECTORS.FACE_TRIGGER);

                    if (gUi.isOnMobileViewMode()) {
                        ctx.scrollToContentForMobile(ctx.SELECTORS.FACE_TRIGGER);
                    }   

                } else if (this.CONGENITAL_ZIKV_DEBUG) {
                    console.log("Warning: Can't select option, it is currently finishing the selection of an item.");
                }
            });

            $(ctx.SELECTORS.THROAT_TRIGGER).on("click", function () {
                if (!ctx.isSelectingTopic) {
                    ctx.isSelectingTopic = true;
                    ctx.showContent(ctx.SELECTORS.THROAT_TRIGGER);

                    if (gUi.isOnMobileViewMode()) {
                        ctx.scrollToContentForMobile(ctx.SELECTORS.THROAT_TRIGGER);
                    }   

                } else if (this.CONGENITAL_ZIKV_DEBUG) {
                    console.log("Warning: Can't select option, it is currently finishing the selection of an item.");
                }
            });

            $(ctx.SELECTORS.BODY_TRIGGER).on("click", function () {
                if (!ctx.isSelectingTopic) {
                    ctx.isSelectingTopic = true;
                    ctx.showContent(ctx.SELECTORS.BODY_TRIGGER);

                    if (gUi.isOnMobileViewMode()) {
                        ctx.scrollToContentForMobile(ctx.SELECTORS.BODY_TRIGGER);
                    }   

                } else if (this.CONGENITAL_ZIKV_DEBUG) {
                    console.log("Warning: Can't select option, it is currently finishing the selection of an item.");
                }
            });

            $(window).resize(function () {
                ctx.redrawSelectedItem();
            });

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to initEvents " + err);
            }
        }
    },

    // Initialization
    getTemplate: function () {
        return '<div class="row">' +
            '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">' +
            '<div class="main-baby">' +
            '<img src="{{img_baby}}">' +
            '</div>' +
            '<div class="main-baby-selectors">' +
            '{{#selector_classes}}' +
            '<div class="selector {{class}}">' +
            '<span class="line"></span>' +
            '<span class="icon"></span>' +
            '</div>' +
            '{{/selector_classes}}' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">' +
            '<div class="topics">' +
            '<div class="topic mb-head-topic-1">' +
            '{{#info_topic_head_1}}' +
            '<span class="v-line"></span>' +
            '<img src="{{img}}">' +
            '<div class="mb-topic-info">' +
            '<div>' +
            '<ul>' +
            '{{#content}}' +
            '<li>{{{text}}}</li>' +
            '{{/content}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '{{/info_topic_head_1}}' +
            '</div>' +
            '<div class="topic mb-head-topic-2">' +
            '{{#info_topic_head_2}}' +
            '<img src="{{img}}">' +
            '<div class="mb-topic-info">' +
            '<div>' +
            '<ul>' +
            '{{#content}}' +
            '<li>{{{text}}}</li>' +
            '{{/content}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '{{/info_topic_head_2}}' +
            '</div>' +
            '<div class="topic mb-face-topic-1 hidden">' +
            '{{#info_topic_face_1}}' +
            '<img src="{{img}}">' +
            '<div class="mb-topic-info">' +
            '<div>' +
            '<ul>' +
            '{{#content}}' +
            '<li>{{{text}}}</li>' +
            '{{/content}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '{{/info_topic_face_1}}' +
            '</div>' +
            '<div class="topic mb-throat-topic-1 hidden">' +
            '{{#info_topic_throat_1}}' +
            '<img src="{{img}}">' +
            '<div class="mb-topic-info">' +
            '<div>' +
            '<ul>' +
            '{{#content}}' +
            '<li>{{{text}}}</li>' +
            '{{/content}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '{{/info_topic_throat_1}}' +
            '</div>' +
            '<div class="topic mb-body-topic-1 hidden">' +
            '{{#info_topic_body_1}}' +
            '<img src="{{img}}">' +
            '<div class="mb-topic-info">' +
            '<div>' +
            '<ul>' +
            '{{#content}}' +
            '<li>{{{text}}}</li>' +
            '{{/content}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '{{/info_topic_body_1}}' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    },

    build: function (tgt) {
        try {
            this.target = tgt;

            if (tgt !== undefined) {
                this._initialized = true;

                var template = this.getTemplate();
                var html = Mustache.to_html(template, this.model);

                $(this.target).append(html);

                this.initEvents();
                this.showContent(this.SELECTORS.HEAD_TRIGGER);
            }
        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to build " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.SELECTORS.HEAD_TRIGGER).off();
            $(this.SELECTORS.FACE_TRIGGER).off();
            $(this.SELECTORS.THROAT_TRIGGER).off();
            $(this.SELECTORS.BODY_TRIGGER).off();

        } catch (err) {

            if (this.CONGENITAL_ZIKV_DEBUG) {
                console.log("Warning: Failed to destroy " + err);
            }
        }
    }

}