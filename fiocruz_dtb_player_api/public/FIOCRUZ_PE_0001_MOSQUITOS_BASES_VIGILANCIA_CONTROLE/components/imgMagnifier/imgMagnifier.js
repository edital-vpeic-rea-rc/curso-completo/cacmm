/**
 * imgMagnifier.js handles the actions of the component ImgMagnifier, which is just a view that increases the
 * size of a certain area of an image .
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ImgMagnifier() {
	this._initialized = false;
}

ImgMagnifier.prototype = {

    DEBUG: true,

    img: undefined,
    glass: undefined,
    zoom: undefined,
    w: 0, 
    h: 0, 
    bw: 0, 

    getCursorPos: function(e) {
        try {
            var a; 
            var x = 0; 
            var y = 0;
            
            // Get the x and y positions of the image
            e = e || window.event;
            a = document.getElementById($(ctx.img).attr("id")).getBoundingClientRect();
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;

            return {x : x, y : y};

        } catch (err) {

            if (ctx.DEBUG) {
                console.log("ImgMagnifier warning: Failed to getCursorPos " + err);
            }
        }

        return {x: 0, y: 0};
    },

    moveMagnifier: function(e) {
        try {
            e.preventDefault();
            var pos, x, y;

            // Get the cursor's x and y positions:
            pos = ctx.getCursorPos(e);
            x = pos.x;
            y = pos.y;

            // Prevent the magnifier glass from being positioned outside the image
            if (x > $(ctx.img).width() - (ctx.w / ctx.zoom)) {
                x = $(ctx.img).width() - (ctx.w / ctx.zoom);
            }

            if (x < ctx.w / ctx.zoom) {
                x = ctx.w / ctx.zoom;
            }

            if (y > $(ctx.img).height - (ctx.h / ctx.zoom)) {
                y = $(ctx.img.height) - (ctx.h / ctx.zoom);
            }

            if (y < ctx.h / ctx.zoom) {
                y = ctx.h / ctx.zoom;
            }

            // Apply the zooming effect of the magnifier and move it to the cursor's position
            var bgPosX = "-" + ((x * ctx.zoom) - ctx.w + ctx.bw + $(ctx.glass).width()/ 2) + "px ";
            var bgPosY = "-" + ((y * ctx.zoom) - ctx.h + ctx.bw + $(ctx.glass).height()/ 2) + "px ";

            $(ctx.glass).css({
                "top": (y) + "px",
                "left": (x) + "px",
                "background-position": bgPosX + bgPosY
            });

        } catch (err) {

            if (ctx.DEBUG) {
                console.log("ImgMagnifier warning: Failed to moveMagnifier " + err);
            }
        }
    },

    magnify: function (imgID, zoom) {
        try {
            ctx = this;
            this.zoom = zoom;
            this.img = $(imgID);

            $(this.img).parent().prepend('<div class="img-magnifier-glass"></div>');
            this.glass = $(this.img).parent().find(".img-magnifier-glass");

            $(this.glass).css({
                "background-image" : "url('" + $(this.img).attr("src") + "')",
                "background-repeat" : "no-repeat",
                "background-size" : ($(this.img).width() * this.zoom) + "px " + ($(this.img).height() * this.zoom) + "px"
            });

            this.bw = 3;
            this.w = $(this.glass).offset().left / 2;
            this.h = $(this.glass).offset().top / 2;

            $(this.glass).on("mousemove", this.moveMagnifier);
            $(this.img).on("mousemove", this.moveMagnifier);
            $(this.glass).on("touchmove", this.moveMagnifier);
            $(this.img).on("touchmove", this.moveMagnifier);

        } catch (err) {

            if (this.DEBUG) {
                console.log("ImgMagnifier warning: Failed to magnify " + err);
            }
        }
    }

}