/**
 * simpleGraph.js Creates a graph displayed above a background and a canvas, 
 * which makes the connections between adjacent nodes of a graph.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function SimpleGraph() {
	this._initialized = false;
}

SimpleGraph.prototype = {

    DEBUG_SG : true,

    // Canvas related
    POSTFIX_CANVAS_ID : "_SgCanvas",
    INITIAL_CANVAS_CONTEXT: "2d",
    PREFIX_LINE_STROKE_STYLE : "rgba(0, 149, 255, ",
    POSTFIX_LINE_STROKE_STYLE : ")",
    LINE_CORNER_STYLE : "round",
    STROKE_OPACITY : 0.8,
    COLOR_END_POINT : "#ffffff",
    COLOR_START_POINT : "#3367D6",

    // Timeouts
    TIMEOUT_REFRESH_SELECTED_NODE : 500,
    TIMEOUT_INIT_CANVAS: 5000,
    TIMEOUT_SCROLL_CANVAS: 16000,
    TIMEOUT_SCROLL_CANVAS_MOBILE: 8000,
    TIMEOUT_HIDE_ICON_BG: 75,
    TIMEOUT_ALLOW_SELECTION_AFTER_EMPHASIS_CALLBACK: 3000,

    // Thresholds
    TIME_MULTIPLIER_STEP_2_SHOW_CONNECTIONS_ANIM: 0.3,
    TIME_MULTIPLIER_STEP_3_SHOW_CONNECTIONS_ANIM: 0.8,

    // Selectors
    SELECTORS : {
        CANVAS : " .simple-graph .sb-canvas",
        NODE : ".simple-graph .sb-nodes .sb-node",
        NODE_IMG : ".simple-graph .sb-nodes .sb-node .sb-node-icon img",
        NODE_IMG_1 : " .sb-node-icon img",
        NODE_IMG_2 : ", .sg-icon",
        NODE_IMG_OVERLAY: " .sb-nodes .sb-node .sb-node-icon img.sg-icon-overlay",
        NODE_IMG_OVERLAY_1: ".sg-icon-overlay",
        NODE_ICON : ".sb-node-icon",
        NODE_ICON_IMG: ".sg-icon",
        NODE_CONTENT : ".sb-node-content",
        MODAL_WINDOW : " .sg-modal-window",
        MODAL_WINDOW_CONTENT : " .sg-modal-window-content",
        MODAL_WINDOW_BTN_CLOSE: " .sg-modal-window-btn.close"
    },

    CLASSES : {
        EMPHASIZE_CONNECTIONS : "emphasize-connections",
        ANIMATE: "animate",
        ICON_OVERLAY: "sg-icon-overlay"
    },

    // Node related
    NODE_INFIX : "_sbNode_",
    CSS_SELECTED_NODE : {
        "background-color" : "rgba(51, 51, 51, 0.9)"
    },
    CSS_SELECTED_NODE_WHEN_REFRESHED : {
        "opacity" : 1
    },
    CSS_NODES_WHEN_ONE_IS_SELECTED : {
        "opacity" : 0
    },
    CSS_NODES_WHEN_REFRESHED : {
        "background-color" : "rgba(51, 51, 51, 0)"
    },
    CSS_NODE_ICON_ON_HOVER: {
        "opacity": 0,
        "visibility": "hidden"
    },
    CSS_NODE_ICON: {
        "opacity": 1,
        "visibility": "visible"
    },
    BUILDING_GIF_SRC_CONNECTOR : "_b",
    BUILDING_GIF_UP_POSTFIX : "_up.gif",
    BUILDING_GIF_UP_INFIX : "_up",

    // Main variables
    target : undefined,
    model : {
        bgImage : "",
        sgNodes : [
            {
                nodeIdx: "",
                nodeIcon: "",
                nodeTitle: "",
                nodeContent: "",
                nodeEdges: [
                    [{x: 0, y: 0}]
                ]
            }
        ]
    },
    callbackNodeClicked: undefined,
    callbackCloseModalWindow: undefined,
    callbackParamsCloseModalWindow: undefined,
    idxNodeInitialConnections: 0,

    callbackFinishedEmphasisOnConnection: undefined,
    callbackEmphasisOnConnectionStep1: undefined,
    callbackEmphasisOnConnectionStep2: undefined,
    callbackEmphasisOnConnectionStep3: undefined,    

    // State variables
    hasEmphasizedNodesConnections: false,
    isGNodeSelectionLocked: false,
    isAnimatingNode: false,

    setNodeSelectionAllowed: function(isSelectionLocked) {
        this.isGNodeSelectionLocked = isSelectionLocked;
    },

    isNodeSelectionAllowed: function() {
        return this.isGNodeSelectionLocked;
    },

    // Getters and Setters
    getIdFromSelector: function(selector) {
        try {
            return selector.replace("#", "").replace(".", "");

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not getIdFromSelector, " + err);
            }
        }

        return "";
    },

    getSelectorFromId: function(objId) {
        try {
            return "#" + objId;

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getSelectorFromId, " + err);
            }
        }
    },

    getNodeIdPrefix: function() {
        try {
            return this.getIdFromSelector(this.target) + this.NODE_INFIX;

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not getNodeIdPrefix, " + err);
            }
        }

        return "";
    },

    getNodeSelectorFromIdx: function(nodeIdx) {
        try {
            return this.target + this.NODE_INFIX + nodeIdx;

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getNodeSelectorFromIdx, " + err);
            }
        }
    },

    getCanvasId: function() {
        try {
            return this.getIdFromSelector(this.target) + this.POSTFIX_CANVAS_ID;

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not getIdForCanvas, " + err);
            }
        }

        return "";
    },

    getNodeByIdx: function(nodeIdx) {
        try {
            
            for (var i = 0; i < this.model.sgNodes.length; i++) {
                var node = this.model.sgNodes[i];
                
                if (node.nodeIdx === nodeIdx) {
                    return node;
                }
            }

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not getNodeByIdx, " + err);
            }
        }

        return undefined;
    },

    getNodeIdxFromObject: function(nodeObj) {
        try {
            return $(nodeObj).attr("id").replace(this.getNodeIdPrefix(), "");
        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getNodeIdxFromObject, " + err);
            }
        }
    },

    getYCoordinateFromPercentage: function(yPercentage) {
        try {
            var canvas = document.getElementById(this.getCanvasId());
            return (canvas.height * 0.01) * yPercentage;

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getYCoordinateFromPercentage, " + err);
            }
        }

        return 0;
    },

    getXCoordinateFromPercentage: function(xPercentage) {
        try {
            var canvas = document.getElementById(this.getCanvasId());
            return (canvas.width * 0.01) * xPercentage;

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getXCoordinateFromPercentage, " + err);
            }
        }

        return 0;
    },

    getCircleLineWidth: function() {
        try {

            if (gUi.isOnMobileViewMode()) {
                return 7;
            } else {
                return 8;
            }

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getCircleLineWidth, " + err);
            }
        }

        return 7;
    },

    getLineStrokeWidth: function() {
        try {

            if (gUi.isOnMobileViewMode()) {
                return 9;
            } else {
                return 17;
            }

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getLineStrokeWidth, " + err);
            }
        }

        return 12;
    },

    setStartPointStyle: function(ctxCanvas) {
        try {
            ctxCanvas.strokeStyle = this.COLOR_START_POINT;
            ctxCanvas.lineWidth = this.getCircleLineWidth();

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not setStartPointStyle, " + err);
            }
        }

        return undefined;
    },

    setEndPointStyle: function(ctxCanvas) {
        try {
            ctxCanvas.strokeStyle = this.COLOR_END_POINT;
            ctxCanvas.lineWidth = this.getCircleLineWidth();

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not setEndPointStyle, " + err);
            }
        }

        return undefined;
    },

    setLineStrokeStyle: function(ctxCanvas, opacity) {
        try {
            ctxCanvas.strokeStyle = this.PREFIX_LINE_STROKE_STYLE + opacity + this.POSTFIX_LINE_STROKE_STYLE;
            ctxCanvas.lineWidth = this.getLineStrokeWidth();
            ctxCanvas.lineJoin = this.LINE_CORNER_STYLE;

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not setLineStrokeStyle, " + err);
            }
        }

        return undefined;
    },

    /**
     * Events
     */
    erasePaths: function(nodeTooltipSelector) {
        try {

            if (nodeTooltipSelector != undefined) {
                var canvas = document.getElementById(this.getCanvasId());
                var ctxCanvas = canvas.getContext(this.INITIAL_CANVAS_CONTEXT);
                ctxCanvas.clearRect(0, 0, canvas.width, canvas.height);
                ctxCanvas.beginPath();
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not erasePaths, " + err);
            }
        }
    },

    drawCircle: function(ctxCanvas, circleCenterX, circleCenterY) {
        try {

            if (gUi.isOnMobileViewMode()) {
                ctxCanvas.arc(circleCenterX, circleCenterY, 1.2, 0, 2 * Math.PI);
            } else {
                ctxCanvas.arc(circleCenterX, circleCenterY, 5, 0, 2 * Math.PI);
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not drawCircle, " + err);
            }
        }
    },

    drawPathLines: function(ctxCanvas, listVertices) {
        try {
            // set the initial position of a line
            ctxCanvas.moveTo(
                this.getXCoordinateFromPercentage(listVertices[0].x),
                this.getYCoordinateFromPercentage(listVertices[0].y)
            );

            // Draw all lines of a path
            for (var i = 1; i < listVertices.length; i++) {
                ctxCanvas.lineTo(
                    this.getXCoordinateFromPercentage(listVertices[i].x), 
                    this.getYCoordinateFromPercentage(listVertices[i].y)
                );
            }

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not drawPathLines, " + err);
            }
        }
    },

    drawPath: function(listVertices, opacity) {
        try {

            if (listVertices != undefined) {
                var canvas = document.getElementById(this.getCanvasId());
                var ctxCanvas = canvas.getContext(this.INITIAL_CANVAS_CONTEXT);

                // Draw the lines of a path
                ctxCanvas.beginPath();
                this.setLineStrokeStyle(ctxCanvas, opacity);
                this.drawPathLines(ctxCanvas, listVertices);
                ctxCanvas.stroke();

                // Draw a circle at the end of a path
                ctxCanvas.beginPath();
                this.setEndPointStyle(ctxCanvas);
                this.drawCircle(
                    ctxCanvas,
                    this.getXCoordinateFromPercentage(listVertices[listVertices.length - 1].x),
                    this.getYCoordinateFromPercentage(listVertices[listVertices.length - 1].y)
                );
                ctxCanvas.stroke();

                // Draw a circle at the begining of a path
                ctxCanvas.beginPath();
                this.setStartPointStyle(ctxCanvas);
                this.drawCircle(
                    ctxCanvas,
                    this.getXCoordinateFromPercentage(listVertices[0].x),
                    this.getYCoordinateFromPercentage(listVertices[0].y)
                );
                ctxCanvas.stroke();
            }

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not drawPath, " + err);
            }
        }
    },

    drawPathsToAdjacentNodes: function(nodeIdx) {
        try {
            var ctx = this;
            var node = ctx.getNodeByIdx(nodeIdx);

            if (node != undefined) {
                ctx.erasePaths();

                for (var i = 0; i < node.nodeEdges.length; i++) {
                    ctx.drawPath(node.nodeEdges[i], this.STROKE_OPACITY);
                }
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not drawPathsToAdjacentNodes, " + err);
            }
        }
    },

    initCanvas: function() {
        try {
            var width = $(this.target).width();
            var height = $(this.target).height();

            var canvas = document.getElementById(this.getCanvasId());
            canvas.width = width;
            canvas.height = height;
        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not initCanvas, " + err);
            }
        }
    },

    resetGraphBg: function () {
        try {
            var ctx = this;

            // Ensure that the canvas is going to have the proper dimensions
            ctx.initCanvas();

            // Restore nodes and background
            $(ctx.target + ctx.SELECTORS.NODE).each(function () {
                // Refresh style of all nodes
                $(this).css(ctx.CSS_NODES_WHEN_REFRESHED);

                // Refresh the style of the selected node
                $(this).find(ctx.SELECTORS.NODE_IMG_1).animate(
                    ctx.CSS_SELECTED_NODE_WHEN_REFRESHED, 
                    ctx.TIMEOUT_REFRESH_SELECTED_NODE
                );
            });

            // Erase canvas content
            ctx.erasePaths();

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not resetGraphBg, " + err);
            }
        }
    },

    makeGraphBgDarker: function (selectedNodeIdx) {
        try {
            var ctx = this;

            $(ctx.target + ctx.SELECTORS.NODE).each(function () {
                var nodeObjIdx = ctx.getNodeIdxFromObject($(this));

                if (nodeObjIdx === selectedNodeIdx) {
                    $(this).css(ctx.CSS_SELECTED_NODE);

                } else {
                    $(this).find(ctx.SELECTORS.NODE_IMG_1).animate(
                        ctx.CSS_NODES_WHEN_ONE_IS_SELECTED,
                        ctx.TIMEOUT_REFRESH_SELECTED_NODE
                    );
                }
            });

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not makeGraphBgDarker, " + err);
            }
        }
    },

    triggerOnNodeClickedCallback: function(selectedNodeIdx) {
        try {

            if (this.callbackNodeClicked !== undefined) {
                this.callbackNodeClicked(selectedNodeIdx);
            }

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not triggerOnNodeClickedCallback, " + err);
            }
        }
    },

    triggerOnConnectionsEmphasisDone: function() {
        try {

            if (this.callbackFinishedEmphasisOnConnection !== undefined) {
                this.callbackFinishedEmphasisOnConnection();
            }

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not triggerOnConnectionsEmphasisDone, " + err);
            }
        }
    },

    triggerOnConnectionsEmphasisStepDone: function(stepIdx) {
        try {

            switch (stepIdx) {

                case 1: 
                    if (this.callbackEmphasisOnConnectionStep1 !== undefined) {
                        this.callbackEmphasisOnConnectionStep1();
                    }
                    break;

                case 2: 
                    if (this.callbackEmphasisOnConnectionStep2 !== undefined) {
                        this.callbackEmphasisOnConnectionStep2();
                    }
                    break;

                case 3: 
                    if (this.callbackEmphasisOnConnectionStep3 !== undefined) {
                        this.callbackEmphasisOnConnectionStep3();
                    }
                    break;
            }
            
        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not triggerOnConnectionsEmphasisStepDone, " + err);
            }
        }
    },

    openContentSelectedNode: function(selectedNode) {
        try {
            // Identify the node
            var nodeIdx = this.getNodeIdxFromObject($(selectedNode).parent().parent());
            
            // Let a client know that a click event just happened
            this.triggerOnNodeClickedCallback(nodeIdx);

            // Update the state of the graph container to increase the attention to the selected node
            this.erasePaths();
            this.makeGraphBgDarker(nodeIdx);

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: could not openContentSelectedNode, " + err);
            }
        }
    },

    selectNodeWithIdx: function(nodeIdx) {
        try {
            this.openContentSelectedNode($(this.target + this.SELECTORS.NODE_IMG)[nodeIdx]);

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not selectNodeWithIdx, " + err);
            }
        }
    },

    scrollToDisplayConnections: function() {
        try {
            var ctx = this;
            var canvasId = ctx.getSelectorFromId(ctx.getCanvasId());

            // Scroll the page down to show the connections
            var verticalScrollDistance = ($(canvasId).offset().top + $(canvasId).height()) * 0.75;
            var scrollTimeout = ctx.TIMEOUT_SCROLL_CANVAS;

            if (gUi.isOnMobileViewMode()) {
                scrollTimeout = ctx.TIMEOUT_SCROLL_CANVAS_MOBILE;
            }

            $(gUi.HTML_BODY_SELECTOR).animate({
                scrollTop: verticalScrollDistance
            }, scrollTimeout, function() {
                $(gUi.HTML_BODY_SELECTOR).scrollTop($(canvasId).offset().top - 100);

                // Invoke a callback that initializes one of the nodes
                var idxSelectedNode = Number(ctx.idxNodeInitialConnections) + 1;

                ctx.adjustZLayersEmphasizeConnections(false);
                ctx.triggerOnConnectionsEmphasisDone();
                ctx.makeGraphBgDarker(idxSelectedNode.toString());

                setTimeout(function() {
                    ctx.setNodeSelectionAllowed(true);
                }, ctx.TIMEOUT_ALLOW_SELECTION_AFTER_EMPHASIS_CALLBACK);
            });

            // Trigger the callback methods during the scrolling event
            ctx.triggerOnConnectionsEmphasisStepDone(1);

            setTimeout(function() {
                ctx.triggerOnConnectionsEmphasisStepDone(2);
            }, scrollTimeout * ctx.TIME_MULTIPLIER_STEP_2_SHOW_CONNECTIONS_ANIM);

            setTimeout(function() {
                ctx.triggerOnConnectionsEmphasisStepDone(3);
            }, scrollTimeout * ctx.TIME_MULTIPLIER_STEP_3_SHOW_CONNECTIONS_ANIM);

        } catch (err) {
            this.setNodeSelectionAllowed(true);

            if (this.DEBUG_SG) {
                console.log("Warning: scrollToDisplayConnections, " + err);
            }
        }
    },

    makeBgDarkerToEmphasizeConnections: function () {
        try {
            var firstNode = $(this.target + this.SELECTORS.NODE)[0];
            $(firstNode).css(this.CSS_SELECTED_NODE);
    
        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not makeBgDarkerToEmphasizeConnections, " + err);
            }
        }
    },

    adjustZLayersEmphasizeConnections: function(adjust) {
        try {
            var ctx = this;
            var canvasId = ctx.getSelectorFromId(ctx.getCanvasId());

            if (adjust) {
                $(canvasId).addClass(ctx.CLASSES.EMPHASIZE_CONNECTIONS);

                ctx.model.sgNodes.forEach(function(node) {
                    $(ctx.getNodeSelectorFromIdx(node.nodeIdx)).find(ctx.SELECTORS.NODE_ICON).css("z-index", "8");
                });
            } else {
                $(canvasId).removeClass(ctx.CLASSES.EMPHASIZE_CONNECTIONS);

                ctx.model.sgNodes.forEach(function(node) {
                    $(ctx.getNodeSelectorFromIdx(node.nodeIdx)).find(ctx.SELECTORS.NODE_ICON).css("z-index", "1");
                });
            }

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: adjustZLayersEmphasizeConnections, " + err);
            }
        }
    },

    emphasizeNodesAndConnections: function() {
        try {
            
            if (!this.hasEmphasizedNodesConnections) {
                this.hasEmphasizedNodesConnections = true;
                this.setNodeSelectionAllowed(false);
                
                this.initCanvas();
                this.adjustZLayersEmphasizeConnections(true);
                this.makeBgDarkerToEmphasizeConnections();
                this.drawPathsToAdjacentNodes(this.idxNodeInitialConnections);
                this.scrollToDisplayConnections();
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not emphasizeNodesAndConnections, " + err);
            }
        }
    },

    showModalContent: function(content, callback, callbackParams) {
        try {
            this.callbackCloseModalWindow = callback;
            this.callbackParamsCloseModalWindow = callbackParams;

            $(this.target + this.SELECTORS.MODAL_WINDOW_CONTENT).html(content);
            $(this.target + this.SELECTORS.MODAL_WINDOW).addClass(gUi.CLASS_SHOW);

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not showModalContent, " + err);
            }
        }
    },

    hideModalContent: function() {
        try {
            $(this.target + this.SELECTORS.MODAL_WINDOW).removeClass(gUi.CLASS_SHOW);
            $(this.target + this.SELECTORS.MODAL_WINDOW_CONTENT).html("");

            if (this.callbackCloseModalWindow != undefined && this.callbackParamsCloseModalWindow != undefined) {
                this.callbackCloseModalWindow.apply(null, this.callbackParamsCloseModalWindow);
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not hideModalContent, " + err);
            }
        }
    },

    animateNode: function (nodeIcon, up) {
        var ctx = this;

        if (!ctx.isAnimatingNode) {
            ctx.isAnimatingNode = true;

            try {
                
                if (up) {
                    var nodeIdx = ctx.getNodeIdxFromObject($(nodeIcon).parent().parent());
                    var node = ctx.getNodeByIdx(nodeIdx);

                    if (node.nodeHasAnimationLayer) {
                        var imageSrcParts = $(nodeIcon).attr("src").split(ctx.BUILDING_GIF_SRC_CONNECTOR);
                        var imageSrcPrefix = imageSrcParts[0];

                        if (imageSrcParts[1] != undefined) {
                            var imageIdx = imageSrcParts[1]
                                .replace(gUi.POSTFIX_GIF, "")
                                .replace(gUi.POSTFIX_SVG_IMG, "")
                                .replace(gUi.POSTFIX_PNG_IMG, "")
                                .replace(ctx.BUILDING_GIF_UP_INFIX, "");

                            var newImageSrc = imageSrcPrefix + ctx.BUILDING_GIF_SRC_CONNECTOR + imageIdx + ctx.BUILDING_GIF_UP_POSTFIX;

                            if (newImageSrc !== "") {
                                var img = $('<img class="' + ctx.CLASSES.ICON_OVERLAY + '" src="' + newImageSrc + '"/>');
                                $(nodeIcon).parent().append(img);

                                $(nodeIcon).animate(
                                    ctx.CSS_NODE_ICON_ON_HOVER, 
                                    ctx.TIMEOUT_HIDE_ICON_BG, 
                                    function() {
                                        ctx.isAnimatingNode = false;
                                    }
                                );
                            }
                        }
                    } else {
                        $(nodeIcon).addClass(ctx.CLASSES.ANIMATE);

                        ctx.isAnimatingNode = false;
                    }
                } else {
                    $(nodeIcon).parent().find(ctx.SELECTORS.NODE_ICON_IMG).css(ctx.CSS_NODE_ICON);
                    
                    $(nodeIcon).remove();

                    ctx.isAnimatingNode = false;
                }
            } catch (err) {
                
                if (ctx.DEBUG_SG) {
                    console.log("Warning: could not animateNode, " + err);
                }

                ctx.isAnimatingNode = false;
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        $(ctx.target + ctx.SELECTORS.NODE_IMG).on("mouseenter", function() {

            if (!gUi.isOnMobileViewMode() && ctx.isNodeSelectionAllowed()) {
                ctx.animateNode(this, true);
            }
        });

        $(ctx.target).on("click", ctx.SELECTORS.NODE_IMG_OVERLAY_1 + ctx.SELECTORS.NODE_IMG_2, function() {

            if (ctx.isNodeSelectionAllowed()) {
                ctx.openContentSelectedNode(this);
            }
        });

        $(ctx.target).on("mouseleave", ctx.SELECTORS.NODE_IMG_OVERLAY_1, function() {
            
            if (!gUi.isOnMobileViewMode()) {
                ctx.animateNode(this, false);
            }
        });

        $(ctx.target).on("mouseover", function() {

            if (!gUi.isOnMobileViewMode()) {
                ctx.emphasizeNodesAndConnections();
            }
        });

        $(ctx.target).on("touchstart", function() {

            if (!gUi.isOnMobileViewMode()) {
                ctx.emphasizeNodesAndConnections();
            }
        });

        $(ctx.target + ctx.SELECTORS.MODAL_WINDOW_BTN_CLOSE).on("click", function(){
            ctx.hideModalContent();
        });

    },

    /**
     * Initialization
     */ 
    initNodes: function() {
        try {
            var ctx = this;
            
            // Defines the position of each node's icon and content
            ctx.model.sgNodes.forEach(function(node) {
                $(ctx.getNodeSelectorFromIdx(node.nodeIdx)).find(ctx.SELECTORS.NODE_ICON).css({
                    "left"              : node.nodePosition.left + "%",
                    "top"               : node.nodePosition.top + "%",
                    "width"             : node.nodePosition.width,
                    "height"            : node.nodePosition.height,
                    "-moz-transform"    : "rotate(" + node.nodePosition.rotation + "deg)",
                    "-webkit-transform" : "rotate(" + node.nodePosition.rotation + "deg)",
                    "transform"         : "rotate(" + node.nodePosition.rotation + "deg)"
                });

                $(ctx.getNodeSelectorFromIdx(node.nodeIdx)).find(ctx.SELECTORS.NODE_CONTENT).css({
                    "left"   : node.nodePosition.left + "%",
                    "top"    : node.nodePosition.top + "%",
                    "width"  : node.nodePosition.width,
                    "height" : node.nodePosition.height
                });
            });

        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not initNodes, " + err);
            }
        }
    },

    initUI: function () {
        try {
            this.initNodes();

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Could not init the SimpleGraph object.");
            }
        }
    },
    
    getTemplate: function () {
        return '<div class="sg-bg">' +
            '<img src="{{bgImageBackLayer}}" class="sg-bg-img">' +
            '</div>' +
            '<div class="sb-canvas">' +
            '<canvas id="' + this.getIdFromSelector(this.target) + '_SgCanvas" class="sb-canvas" width="200" height="200"></canvas>' +
            '</div>' +
            '<div class="sg-bg-front">' +
            '<img src="{{bgImageFrontLayer}}" class="sg-bg-img">' +
            '</div>' +
            '<div class="sb-nodes">' +
            '{{#sgNodes}}' +
            '<div id="' + this.getIdFromSelector(this.target) + '_sbNode_{{nodeIdx}}" class="sb-node sb-node-{{nodeIdx}}">' +
            '<div class="sb-node-icon">' +
            '<img src="{{nodeIcon}}" class="sg-icon">' +
            '</div>' +
            '<div class="sb-node-content">' +
            '{{{nodeContent}}}' +
            '</div>' +
            '</div>' +
            '{{/sgNodes}}' +
            '<div class="sg-modal-window">' +
            '<div class="sg-modal-window-container">'+
            '<div class="sg-modal-window-toolbox">' +
            '<button type="button" class="sg-modal-window-btn close" data-dismiss="modal">×</button>' +
            '</div>' +
            '<div class="sg-modal-window-content"></div>' +
            '</div>' +
            '</div>' +
            '<div id="sgPointer"></div>' +
            '</div>';
    },

    buildSkeleton: function(tgt, model, callbackNodeClicked, idxNodeInitialConnections,
                            callbackFinishedEmphasisOnConnection, callbackEmphasisOnConnectionStep1, 
                            callbackEmphasisOnConnectionStep2, callbackEmphasisOnConnectionStep3) {
        try {
            // Basic parameters
            this.target = tgt;
            this.model = model;
            this.callbackNodeClicked = callbackNodeClicked;
            this.idxNodeInitialConnections = idxNodeInitialConnections;

            // Optional parameters
            this.callbackFinishedEmphasisOnConnection = callbackFinishedEmphasisOnConnection;
            this.callbackEmphasisOnConnectionStep1 = callbackEmphasisOnConnectionStep1; 
            this.callbackEmphasisOnConnectionStep2 = callbackEmphasisOnConnectionStep2;
            this.callbackEmphasisOnConnectionStep3 = callbackEmphasisOnConnectionStep3;
    
            var template = this.getTemplate();
            var html = Mustache.to_html(template, this.model);
    
            $(this.target).html(html);

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Could not buildSkeleton.");
            }
        }
    },

    /**
     * Mandatory parameters:
     * @param tgt: The DOM element that is going to receive the HTML code of the object
     * @param model: The variable that carries the data model of the object
     * @param callbackNodeClicked: The client method that is called when the user clicks on a node
     * @param idxNodeInitialConnections: The index of the node that is initially going to be emphasized, will have it's connections shown
     * 
     * Optional parameters:
     * @param callbackFinishedEmphasisOnConnection: The client method that is called when the method that shows the connections is finishes
     * @param callbackEmphasisOnConnectionStep1: The client method that is called at the 1# step of the method that presents the connections
     * @param callbackEmphasisOnConnectionStep2: The client method that is called at the 2# step of the method that presents the connections
     * @param callbackEmphasisOnConnectionStep3: The client method that is called at the 3# step of the method that presents the connections
     */
    build : function(tgt, model, callbackNodeClicked, idxNodeInitialConnections, 
                    callbackFinishedEmphasisOnConnection, callbackEmphasisOnConnectionStep1, 
                    callbackEmphasisOnConnectionStep2, callbackEmphasisOnConnectionStep3) {
        try {

            if (tgt != undefined && model != undefined && callbackNodeClicked != undefined && 
                idxNodeInitialConnections != undefined) {
                this._initialized = true;

                this.buildSkeleton(tgt, model, callbackNodeClicked, idxNodeInitialConnections,
                                   callbackFinishedEmphasisOnConnection, callbackEmphasisOnConnectionStep1, 
                                   callbackEmphasisOnConnectionStep2, callbackEmphasisOnConnectionStep3);
                     
                this.initUI();
                this.initEvents();
            }
        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Failed to build SG object " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.target).off();
            $(this.target + this.SELECTORS.NODE_IMG).off();
            $(this.target + this.SELECTORS.MODAL_WINDOW_BTN_CLOSE).off();
            
        } catch(err) {

            if (this.DEBUG_SG) {
                console.log("Failed to destroy SG object " + err);
            }
        }
    }

}