/**
 * 
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function HorizontalTabbedBox() {
	this._initialized = false;
}

HorizontalTabbedBox.prototype = {

    HORIZONTAL_TABBED_BOX_DEBUG: true,

    INFIX_HTB_BTN : "_htb_btn_",
    TIMEOUT_HTB_PRESENT_BTN_ANIMATION: 500,

    model : {
        title: '',
        htbButtons: [
            {
                htbBtnText: '',
            }
        ],
        htbSelectedImgSource: ''
    },

    target : undefined,

    // Getters and Setters
    getTargetId: function() {
        try {
            return $(this.target).attr("id");

        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to getTargetId " + err);
            }
        }
    },

    getHtbBtnIdPrefix: function() {
        try {
            return this.getTargetId() + this.INFIX_HTB_BTN;

        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to getHtbBtnId " + err);
            }
        }

        return "";
    },

    getSelectorHtbBtn: function(btnIdx) {
        try {
            return "#" + this.getHtbBtnIdPrefix() + btnIdx;

        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to getSelectorHtbBtn " + err);
            }
        }

        return "";
    },

    // Other methods
    getDesktopScreenTemplate: function () {
        return '<div class="htb-header">' +
            '<h3>{{{title}}}</h3>' +
            '</div>' +
            '<div class="htb-body">' +
            '<div class="row">' +
            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 htb-btns-container">' +
            '<div>' +
            '{{#htbButtons}}' +
            '<div id="'+ this.getHtbBtnIdPrefix() +'{{idx}}" class="htb-btn">' +
            '<div class="htb-btn-name-content">{{{htbBtnText}}}</div>' +
            '</div>' +
            '{{/htbButtons}}' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
            '<div class="htb-img-container">' +
            '<img src="{{htbSelectedImgSource}}" class="htb-img">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    }, 

    getMobileScreenTemplate: function() {
        return '<div class="htb-header">' +
            '<h3>{{{title}}}</h3>' +
            '</div>' +
            '<div class="htb-body">' +
            '<div class="row">' +
            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
            '<div class="htb-img-container">' +
            '<img src="{{htbSelectedImgSource}}" class="htb-img">' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 htb-btns-container">' +
            '<div>' +
            '{{#htbButtons}}' +
            '<div id="'+ this.getHtbBtnIdPrefix() +'{{idx}}" class="htb-btn">' +
            '<div class="htb-btn-name-content">{{{htbBtnText}}}</div>' +
            '</div>' +
            '{{/htbButtons}}' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    },

    // Events
    showHtbButton: function(btnIdx, btnSelector) {
        try {
            var idx = Number(btnIdx);

            // Hide the buttons
            $(btnSelector).addClass(gUi.CLASS_HIDDEN);
            $(btnSelector).removeClass(gUi.CLASS_SHOW);

            // Animate the buttons sequentially
            if (idx < 2) {
                $(btnSelector).removeClass(gUi.CLASS_HIDDEN);
                $(btnSelector).addClass(gUi.CLASS_SHOW);
            } else {
                
                setTimeout(function() {
                    $(btnSelector).removeClass(gUi.CLASS_HIDDEN);
                    $(btnSelector).addClass(gUi.CLASS_SHOW);
                }, idx * this.TIMEOUT_HTB_PRESENT_BTN_ANIMATION);
            }
        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to showHtbButton " + err);
            }
        }
    },

    presentTabsAnimation: function() {
        try {

            for(var i = 1; i <= this.model.htbButtons.length; i++) {
                var htbButton = this.model.htbButtons[i - 1];
                var htnButtonSelector = this.getSelectorHtbBtn(htbButton.idx);
                this.showHtbButton(htbButton.idx, htnButtonSelector);
            }
        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to presentTabsAnimation " + err);
            }
        }
    },

    build : function(tgt, model) {
        try {
            this.target = tgt;

            if (tgt !== undefined && model != undefined) {
                this._initialized = true;
                this.model = model;

                var template = "";

                if (gUi.isOnMobileViewMode()) {
                    template = this.getMobileScreenTemplate();
                } else {
                    template = this.getDesktopScreenTemplate();    
                }
                
                var html = Mustache.to_html(template, this.model);

                $(this.target).html(html);
            }
        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to build " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

        } catch(err) {

            if (this.HORIZONTAL_TABBED_BOX_DEBUG) {
                console.log("Horizontal TB Warning: Failed to destroy " + err);
            }
        }
    }

}