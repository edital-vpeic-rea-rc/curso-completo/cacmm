/**
 * footer.js handles the actions of the component Footer, which is just a static view placed at the very 
 * bottom of the page.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function Footer() {
	this._initialized = false;
}

Footer.prototype = {

    model : {
        footerImages : []
    },

    target : undefined,

    useDefaultFooter : false,

    useDefaultFooter : function(useDefault) {
        this.useDefaultFooter = useDefault;
    },

    getTemplate: function () {
       return '<div class="tablet-desktop">' +
       '{{#footerImages}}' +
       '<object data="{{imgUrlSvg}}" type="image/svg+xml" class="img-response footer-img">' +
       '<img src="{{imgUrlPng}}" class="img-response footer-img">' +
       '</object>' +
       '{{/footerImages}}' +
       '</div>' +
       '<div class="container">' +
       '<div class="row">' +
       '<div class="col-xs-12">' +
       '<div class="mobile-version center-block">' +
       '{{#footerImages}}' +
       '<object data="{{imgUrlSvg}}" type="image/svg+xml" class="img-response footer-img center-block">' +
       '<img src="{{imgUrlPng}}" class="img-response footer-img center-block">' +
       '</object>' +
       '{{/footerImages}}' +
       '</div>' +
       '</div>' +
       '</div>' +
       '</div>';
    },

    initDefaultFooter: function () {
        try {
            this.model.footerImages = [
                {
                    imgUrlSvg: "images/ico_svg_ead_white.svg",
                    imgUrlPng: "images/ico_png_ead_white_2.png"
                },
                {
                    imgUrlSvg: "images/ico_svg_campus_white.svg",
                    imgUrlPng: "images/ico_png_campus_white.png"
                },
                {
                    imgUrlSvg: "images/ico_svg_iam_white.svg",
                    imgUrlPng: "images/ico_png_iam_white_2.png"
                },
                {
                    imgUrlSvg: "images/ico_svg_sus_white.svg",
                    imgUrlPng: "images/ico_png_sus_white.png"
                },
                {
                    imgUrlSvg: "images/ico_svg_ms_white.svg",
                    imgUrlPng: "images/ico_png_ms_white.png"
                },
                {
                    imgUrlSvg: "images/ico_svg_federal_government_white_gray.svg",
                    imgUrlPng: "images/ico_png_federal_government_white_gray.png"
                }
            ];

            var template = this.getTemplate();
            var footerHtml = Mustache.to_html(template, this.model);

            $(this.target).html(footerHtml);

        } catch(err) {
            console.log(err);
        }
    },

    build : function(tgt, model) {
        this.target = tgt;

        if (this.useDefaultFooter) {
            this.initDefaultFooter();

        } else if (this.model != undefined) {
            this.model = model;
            var template = this.getTemplate();
            var footerHtml = Mustache.to_html(template, this.model);
            $(this.target).html(footerHtml);
        }
    },
    
    reset : function() {
        this.model = {};
        this.model.footerImages = [];
    }

}