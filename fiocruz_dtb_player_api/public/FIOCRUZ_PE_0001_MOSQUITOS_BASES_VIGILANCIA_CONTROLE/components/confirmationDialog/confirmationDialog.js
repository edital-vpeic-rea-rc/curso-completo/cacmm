/**
 * confirmationDialog.js handles the actions of the component ConfirmationDialog, which is just an 
 * information window that allows the user to click on the confirmation button, 
 * at the bottom right of the window, or on the cancel button, at the bottom left of the window. 
 * 
 * Once the user clicks on a button, the dialog is hidden with display:none and than the callback 
 * function wired for the clicked button gets triggered.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ConfirmationDialog() {
	this._initialized = false;
}

ConfirmationDialog.prototype = {

    CD_DEBUG: true,

    model : {
        headerMsg: "",
        icoBtnClose: "",
        mainMsg: "",
        actionMsg: "",
        cancelBtnText: "",
        confirmBtnText: "",
        callbackCancelAction: undefined,
        callbackConfirmAction: undefined
    },
    selectors : {
        CANCEL_BTN: "",
        CONFIRM_BTN: "",
        CLOSE_BTN: " .cd-btn-close"
    },
    classes: {
        TOGGLE_DIALOG_VISIBILITY: "show-dialog",
        CANCEL_BTN: "cancel-btn",
        CONFIRM_BTN: "confirm-btn",
        CLOSE_BTN: "cd-btn-close"
    },
    target : undefined,

    /**
     * Other events
     */
    show: function(showDialog) {
        try {

            if (showDialog) {
                $(this.target).addClass(this.classes.TOGGLE_DIALOG_VISIBILITY);
            } else {
                $(this.target).removeClass(this.classes.TOGGLE_DIALOG_VISIBILITY);
            }

        } catch (err) {

            if (this.CD_DEBUG) {
                console.log("ConfirmationDialog warning: Failed to show " + err);
            }
        }
    },

    /**
     * Initializaiton
     */
    initEvents: function() {
        try {
            var ctx = this;

            $(ctx.selectors.CANCEL_BTN).on("click", function() {
                
                if (ctx.model.callbackCancelAction != undefined) {
                    ctx.model.callbackCancelAction();
                    ctx.show(false);
                }
            });

            $(ctx.selectors.CONFIRM_BTN).on("click", function() {
                
                if (ctx.model.callbackConfirmAction != undefined) {
                    ctx.model.callbackConfirmAction();
                    ctx.show(false);
                }
            });

            $(ctx.target + ctx.selectors.CLOSE_BTN).on("click", function() {
                ctx.show(false);
            })

        } catch (err) {

            if (this.CD_DEBUG) {
                console.log("ConfirmationDialog warning: Failed to initEvents " + err);
            }
        }
    },

    getTemplate: function () {
        return '<div class="cd-container">' +
        '<div class="cd-header">' +
        '{{{headerMsg}}}'+
        '<img class="cd-btn-close" src="{{icoBtnClose}}">'+
        '</div>' +
        '<div class="cd-body">' +
        '{{{mainMsg}}}'+
        '</div>' +
        '<div class="cd-footer">' +
        '<div class="cd-action-msg">'+
        '<small>{{actionMsg}}</small>' +
        '</div>'+
        '<button class="btn btn-default {{classCancelBtn}}">{{{cancelBtnText}}}</button>' +
        '<button class="btn btn-default {{classConfirmBtn}}">{{{confirmBtnText}}}</button>' +
        '</div>' +
        '</div>';
     },

    build : function(tgt, model) {
        try {
            this.target = tgt;

            if (this.model != undefined) {
                this.model = model;
                this.model.classCancelBtn = this.classes.CANCEL_BTN;
                this.model.classConfirmBtn = this.classes.CONFIRM_BTN;
    
                this.selectors.CANCEL_BTN = this.target + " ." + this.classes.CANCEL_BTN;
                this.selectors.CONFIRM_BTN = this.target + " ." + this.classes.CONFIRM_BTN;
    
                var template = this.getTemplate();
                var html = Mustache.to_html(template, this.model);
    
                $(this.target).html(html);
    
                this.initEvents();
            }

        } catch (err) {

            if (this.CD_DEBUG) {
                console.log("ConfirmationDialog warning: Failed to build " + err);
            }
        }
    },
    
    reset : function() {
        this.model = {};
    },

    rebuild : function(tgt, model) {
        try {

            if (this.target != undefined) {
                $(this.target).html();
            }

            this.reset();
            this.build(tgt, model);

        } catch (err) {

            if (this.CD_DEBUG) {
                console.log("ConfirmationDialog warning: Failed to rebuild " + err);
            }
        }
    }

}