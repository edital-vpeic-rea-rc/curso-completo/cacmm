/**
 * pageLoader.js handles the actions of the component PageLoader
 * 
 * A PageLoader is a dialog that is displayed while content is getting loaded.
 * It's supposed to be called at the beginning of a loading event and dismissed at the end.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function PageLoader() {
	this._initialized = false;
}

PageLoader.prototype = {

    SELECTOR_INFO_CONTAINER: " .info-container",
    SELECTOR_MAIN_MSG : " .info-container .main-msg",
    SELECTOR_SUPPORT_MSG : " .info-container .support-msg",
    SELECTOR_PL_TARGET: ".pl-target",

    CLASS_DISABLED : "flipOutX",
    CLASS_SHOWING : "showing",
    CLASS_HIDDEN : "hidden",

    MINIMUM_DISPLAY_TIME: 1300,
    MAXIMUM_DISPLAY_TIME: 10000,

    model : {
        img: "",
        mainMsg: "",
        supportMsg: ""
    },
    target : undefined,
    callback : undefined,
    startTime: undefined,

    /**
     * Getters and Setters
     */
    updateMainMsg: function(msg) {
        this.mainMsg = msg;
        $(this.target + this.SELECTOR_MAIN_MSG).html(msg);
    },

    updateSupportMsg: function(msg) {
        this.supportMsg = msg;
        $(this.target + this.SELECTOR_SUPPORT_MSG).html(msg);
    },

    /**
     * Events
     */
    show: function() {
        var ctx = this;
        ctx.startTime = new Date();

        if (!$(this.target).hasClass(this.CLASS_SHOWING)) {
            $(this.target + this.SELECTOR_INFO_CONTAINER).removeClass(this.CLASS_DISABLED);
            $(this.target).addClass(this.CLASS_SHOWING);
            $(this.SELECTOR_PL_TARGET).addClass(this.CLASS_HIDDEN);
        }

        setTimeout(function() {
            ctx.dismissDialog();
        }, ctx.MAXIMUM_DISPLAY_TIME);
    },

    getElapsedTimeSinceStart: function() {
        var ellapsedTimeSinceStart = new Date() - this.startTime;
        return ellapsedTimeSinceStart /= 1000;
    },

    dismissDialog: function() {
        var ctx = this;

        $(ctx.target).removeClass(ctx.CLASS_SHOWING);
        $(ctx.target).addClass(ctx.CLASS_DISABLED);
        $(ctx.SELECTOR_PL_TARGET).removeClass(ctx.CLASS_HIDDEN);

        setTimeout(function(){
            $(ctx.target + ctx.SELECTOR_INFO_CONTAINER).addClass(ctx.CLASS_HIDDEN);
        }, 1000);
    },

    dismiss: function() {
        var ctx = this;
        var dismissDialogTimeout = ctx.getElapsedTimeSinceStart();

        if (dismissDialogTimeout < ctx.MINIMUM_DISPLAY_TIME) {
            dismissDialogTimeout = ctx.MINIMUM_DISPLAY_TIME;

            setTimeout(function () {
                ctx.dismissDialog();
            }, dismissDialogTimeout);
        } else {
            ctx.dismissDialog();
        }
    },

    /**
     * Initialization
     */
    getTemplate: function () {
        return '<div class="info-container">' +
            '{{#img}}'+
            '<img src="{{img}}" class="loading-img">' +
            '{{/img}}'+
            '{{#mainMsg}}'+
            '<h3 class="main-msg">{{mainMsg}}</h3>'+
            '{{/mainMsg}}'+
            '{{#supportMsg}}'+
            '<p class="support-msg">{{supportMsg}}</p>'+
            '{{/supportMsg}}'+
            '</div>';
    },

    build : function(tgt, callback, model) {
        if (tgt != undefined && model != undefined) {
            this.target = tgt;
            this.model = model;

            var template = this.getTemplate();
            var html = Mustache.to_html(template, this.model);
    
            $(this.target).html(html);
        }
    }

}