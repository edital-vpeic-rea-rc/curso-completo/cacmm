/**
 * modalSimpleMessage.js simply displays the modal dialog used to inform the user about the conclusion of a 
 * unit
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @requires gImages
 * @license MIT
 */

function ModalSimpleMessage() {
	this._initialized = false;
}

ModalSimpleMessage.prototype = { 

    SELECTOR_BTN_VIDEO_DOWNLOAD: " .download-link",
    PREFIX_UNIT_QUESTIONS_STATUS_CONTAINER: "unitQuestionsStatus_",
    CLASS_UNIT_QUESTION_STATUS: "unit-question-status",

    model : {
        img: "",
        title: "",
        msg : "",
        msgSupport: "",
        localVideo: false,
        unitNumber: -1,
        videoContainerSize: 320,
        videoUrl: "",
        videoSupportMsg:"",
        videoResolution: -1,
        icoBtnDownload: "./images/ico_svg_download_white.svg"
    },

    target : undefined,
    hasDisplayedFinishedUnitMsg: false,
    hasDisplayedFinishedCourseMsg: false,

    /**
     * Events
     */
    show: function() {
        try {
            $(this.target).modal("show");

            // Create a new Video object in the persistency layer
            if (this.model.hasVideo) {
                gApiIntegration.setVideo(this.model.videoUrl, this.model.videoResolution);
            }

            // Create a new Download object in the persistency layer
            if (this.model.localVideo) {
                gApiIntegration.setDownload(gFiles.FILE_TYPE.VIDEO, this.model.title);
            }
        } catch(err) {
            console.log("Failed to show modalSimpleMessage object: " + err);
        }
    },

    hide: function() {
        $(this.target).modal("hide");
    },

    getActionBtnsContainer: function(unitNumber, msg) {
        try {

            if (unitNumber <= 2) {
                return '<div class="action-btns"><p>' + msg + '</p><button class="btn btn-default action-btn left" next-page-url="courseProgress.html">andamento</button><button class="btn btn-default action-btn right" next-page-url="courseUnitDetails' + (unitNumber + 1) + '.html">módulo ' + (unitNumber + 1) + '</button></div>';
            } else {
                return '<div class="action-btns"><p>' + msg + '</p><button class="btn btn-default action-btn center" next-page-url="courseProgress.html">andamento</button></div>';
            }

        } catch (err) {
            console.log("Failed to getActionBtnsContainer: " + err);
        }
    },

    getSelectorBtnActionBtnsContainer: function() {
        try {
            return this.target + " .action-btns .action-btn";

        } catch (err) {
            console.log("Failed to getSelectorBtnActionBtnsContainer: " + err);
        }
    },

    getUnitQuestionStatusContainer: function(unitNumber) {
        try {
            return '<div id="'+ this.PREFIX_UNIT_QUESTIONS_STATUS_CONTAINER + unitNumber + '" class="'+ this.CLASS_UNIT_QUESTION_STATUS +'"></div>';

        } catch(err) {
            console.log("Failed to getUnitQuestionStatusContainer " + err);
        }

        return "";
    },

    getSelectorUnitQuestionStatusContainer: function(unitNumber) {
        try {
            return "#" + this.PREFIX_UNIT_QUESTIONS_STATUS_CONTAINER + unitNumber;

        } catch(err) {
            console.log("Failed to getSelectorUnitQuestionStatusContainer " + err);
        }

        return "";
    },

    updateModalToFinishedCourse: function(unitNumber) {
        var msg = gMsgs.MSG_YOU_FINISHED_EDUCATIONAL_RESOURCE + 
                  this.getUnitQuestionStatusContainer(unitNumber) +
                  this.getActionBtnsContainer(unitNumber, gMsgs.MSG_BEFORE_ACTION_BTNS);

        var model = {
            img: gImages.ICO_PNG_COURSE_SUCCESS,
            title: gMsgs.MSG_CONGRATS,
            msg : msg,
            msgSupport: '',
            unitNumber: unitNumber
        }

        this.build(this.target, model);
    },

    updateModalToFinishedUnit: function(unitNumber) {
        try {
            var msg = "Você finalizou " + gData.getUnitConclusionPercentage(unitNumber) + "% das questões desta unidade" +
                      this.getUnitQuestionStatusContainer(unitNumber) +
                      this.getActionBtnsContainer(unitNumber, gMsgs.MSG_BEFORE_ACTION_BTNS);

            this.setMessage(msg);
        } catch(err) {
            console.log("Failed to updateModalToFinishedUnit " + err);
        }
    },

    /**
     * Create the ListQuestionsStatus HTML object to represent the list of questions
     */
    displayUnitQuestionStatus: function(unitNumber) {
        var modelStatusUnitQuestions = gData.getUnitQuestionListModel(unitNumber);
                            
        if (typeof(modelStatusUnitQuestions) !== "undefined") {
            // Build the list with the status of the questions for a unit
            var questionsList = new ListQuestionsStatus();
            var componentTarget = this.getSelectorUnitQuestionStatusContainer(unitNumber);
            questionsList.build(componentTarget, modelStatusUnitQuestions, undefined);

            // Update the label that indicates the status
            $(componentTarget).find("label").html("Este é o seu desempenho na Unidade " + unitNumber);
        } else {
            $(this.getSelectorUnitQuestionStatusContainer(unitNumber)).html("Houve uma falha ao carregar o status das questões da Unidade " + unitNumber);
        }
    },

    verifyConditionsAndDisplayModal: function (unitNumber) {
        try {
            
            if (gData.hasUserFinishedCourse() && !this.hasDisplayedFinishedCourseMsg) {
                this.updateModalToFinishedCourse(unitNumber);
                this.show();
                this.displayUnitQuestionStatus(unitNumber);

                this.hasDisplayedFinishedCourseMsg = true;

            } else if (gData.hasUserFinishedUnit(unitNumber) && !this.hasDisplayedFinishedUnitMsg) {
                this.updateModalToFinishedUnit(unitNumber);
                this.show();
                this.displayUnitQuestionStatus(unitNumber);
            }

            if (gData.hasUserCompletelyFinishedUnit(unitNumber)) {
                this.hasDisplayedFinishedUnitMsg = true;
            }
        } catch (err) {
            console.log("Failed to get user status in course " + err);
        }
    },

    navigateToNextUrl: function(actionBtnObj) {
        try {

            if (actionBtnObj != undefined) {
                var nextUrl = $(actionBtnObj).attr("next-page-url");

                if (nextUrl != undefined) {
                    gData.setSelectedQuestionLink(undefined, undefined);
                    gPages.updateCurrentPage(nextUrl);
                }
            }

        } catch (err) {
            console.log("Failed to navigateToNextUrl " + err);
        }
    },

    initEvents: function() {
        var ctx = this;

        $(ctx.target).on("hidden.bs.modal", function () {
            $(ctx.target).find("iframe").attr("src", '');
            $(ctx.target).find("video").attr("src", '');
            ctx.hide();
        });

        $(ctx.getSelectorBtnActionBtnsContainer()).on("click", function() {
            ctx.navigateToNextUrl(this);
        });

        $(ctx.target + ctx.SELECTOR_BTN_VIDEO_DOWNLOAD).attr({target: '_blank', href  : ctx.model.videoUrl});  
    },

    /**
     * Initialization
     */
    getTemplate : function() {
        return '<div class="modal-dialog {{contentType}}">'+
            '<div class="modal-content">'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
            '<div class="modal-body">'+
            '<img src="{{img}}">'+
            '<h3 class="heading-2">{{{title}}}</h3>'+
            '<p>{{{msg}}}</p>'+
            '{{#videoUrl}}'+
            '{{#localVideo}}'+
            '<video width="{{videoContainerSize}}" controls>' +
            '<source src="{{videoUrl}}" type="video/mp4">' +
            '{{videoSupportMsg}}' +
            '</video>' +
            '{{/localVideo}}'+
            '{{^localVideo}}' +
            '<iframe width="{{videoContainerSize}}" height="auto" type="video/mp4" controls src="{{videoUrl}}" ' +
            'allowfullscreen="allowfullscreen" '+
            'mozallowfullscreen="mozallowfullscreen" '+
            'msallowfullscreen="msallowfullscreen" '+
            'oallowfullscreen="oallowfullscreen" '+
            'webkitallowfullscreen="webkitallowfullscreen">'+
            '{{videoSupportMsg}}' +
            '</iframe>' +
            '{{/localVideo}}' +
            '{{/videoUrl}}'+
            '{{#msgSupport}}' + 
            '<a class="support-msg download-link unit-{{unitNumber}}">{{{msgSupport}}}</a>'+
            '{{/msgSupport}}' + 
            '</div>'+
            '</div>'+
            '</div>';
    },
  
    initModal: function() {
        $ (this.target).modal({ 
            show: false,
            backdrop: true
        });
    },

    build : function(tgt, model) {

        if (tgt != undefined && model != undefined) {

            if (model.videoUrl != undefined) {
                model.contentType = "video";
                model.hasVideo = true;
                model.icoBtnDownload = this.model.icoBtnDownload;
            }

            this.target = tgt;
            this.model = model;
            var template = this.getTemplate();
            var navbarHtml = Mustache.to_html(template, this.model);
            
            $(this.target).html(navbarHtml);
            this.initModal();
            this.initEvents();
        }
    },

    /**
     * Getters and setters
     */
    setTitle: function(title) {
        this.model.title = title;
        this.build(this.target, this.model);
    },

    setMessage: function(msg) {
        this.model.msg = msg;
        this.build(this.target, this.model);
    },

    setVideo: function(videoContainerSize, videoUrl, videoSupportMsg) {
        this.modal.videoContainerSize = videoContainerSize;
        this.modal.videoUrl = videoUrl;
        this.modal.videoSupportMsg = videoSupportMsg;
        this.build(this.target, this.model);
    }

}
