/**
 * question.js handles the actions of the component Question, which is an object 
 * that can be used within an evaluation form, to display a question and gather the
 * user answer.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Diego M. Silva
 * @version 0.0.1
 * @requires jQuery
 * @requires Mustache
 * @license MIT
 */

function Question() {
  this._initialized = false;
};

Question.prototype = {

  PREFIX_ID_QUESTION: "#question_",
  PREFIX_ID_ALTERNATIVE: "#alternative_",
  ID_JOINER: "_",

  SELECTOR_ALTERNATIVES: " .body .alternative",
  SELECTOR_ALTERNATIVE_HINT: " .alternative-hint",
  SELECTOR_ALTERNATIVE_RADIO_BTN: " .body .alternative .selector.radio input",
  SELECTOR_ALTERNATIVE_RADIO_BTN_LEFT_TF: " .body .alternative .selector .left input",
  SELECTOR_ALTERNATIVE_RADIO_BTN_RIGHT_TF: " .body .alternative .selector .right input",
  SELECTOR_TRY_AGAIN_BTN: " .question-toolbox .try-again",
  SELECTOR_SUBMIT_BTN: " .question-toolbox .submit",
  SELECTOR_SUCCESFUL_ANSWER: " h4 span",
  SELECTOR_IMGS_QUESTION_STATUS: " h4 span .safe-img",
  SELECTOR_IMG_SVG_OBJ_QUESTION_STATUS: " h4 span .safe-img object",
  SELECTOR_IMG_PNG_QUESTION_STATUS: " h4 span .safe-img object img",
  SELECTOR_ALTERNATIVE_SELECTOR: " .alternative .selector",
  SELECTOR_ALTERNATIVE_TRUE_INPUT: " .left input",
  SELECTOR_ALTERNATIVE_FALSE_INPUT: " .right input",
  SELECTOR_CHECKBOX_INPUT: " .checkbox input",
  SELECTOR_QUESTIONS_SINGLE_ANSWER: " .single-answer",
  SELECTOR_QUESTIONS_SINGLE_ANSWER_GROUPING: " .single-answer-grouping",
  SELECTOR_QUESTIONS_TF: " .tf",
  SELECTOR_QUESTION_MESSAGE: " .question-message",

  CLASS_HIDDEN: "hidden",
  CLASS_ALTERNATIVE: "alternative",
  CLASS_ERROR: "error",
  CLASS_WARNING: "warning",
  CLASS_SUCCESS: "success",

  T_OR_F: {
    T: "t",
    F: "f",
  },

  QUESTION_STATUS: {
    WRONG: "wrong",
    ANSWERING: "answering",
    RIGHT: "right",
    CLEAR: ""
  },

  QUESTION_IMAGES: {
    ICO_PNG_WRONG: "images/ico_png_wrong.png",
    ICO_SVG_WRONG: "images/ico_svg_wrong.svg",
    ICO_PNG_ANSWERING: "images/ico_png_attention.png",
    ICO_SVG_ANSWERING: "images/ico_svg_attention.svg",
    ICO_PNG_SUCCESS: "images/ico_png_rigth.png",
    ICO_SVG_SUCCESS: "images/ico_svg_right.svg",
    ICO_PNG_NO_ANSWER: "images/ico_png_no_answer.png",
    ICO_SVG_NO_ANSWER: "images/ico_svg_no_answer.svg",
  },

  ALTERNATIVE_LETTERS_TRANSFORMATION: {
    ROMANIZE: 1,
    ALGARISM: 2,
    ALPHABET: 3
  },

  ALPHABET: "abcdefghijklmnopqrstuvwxyz".split(''),

  getQuestionIdSelector: function (unitNumber, questionNumber) {
    return this.PREFIX_ID_QUESTION + unitNumber + this.ID_JOINER + questionNumber;
  },

  getAlternativeInput: function (target, unitNumber, questionNumber, alternative) {
    return $(target + " " + this.PREFIX_ID_ALTERNATIVE + unitNumber + this.ID_JOINER + questionNumber + this.ID_JOINER + alternative + " input");
  },

  getAlternative: function (target, unitNumber, questionNumber, alternative) {
    try {
      return $(target + " " + this.PREFIX_ID_ALTERNATIVE + unitNumber + this.ID_JOINER + questionNumber + this.ID_JOINER + alternative)[0];

    } catch (err) {
      console.log("Failed to getAlternative " + err);
      return undefined;
    }
  },

  setQuestionStatusImg: function (target, questionStatus){
    try {

      switch (questionStatus) {

        case this.QUESTION_STATUS.WRONG: {
          $(target + this.SELECTOR_IMGS_QUESTION_STATUS).css('display', 'inline-block');
          $(target + this.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', this.QUESTION_IMAGES.ICO_PNG_WRONG);
          $(target + this.SELECTOR_IMG_SVG_OBJ_QUESTION_STATUS).attr('data', this.QUESTION_IMAGES.ICO_SVG_WRONG);

          break;
        }

        case this.QUESTION_STATUS.ANSWERING: {
          $(target + this.SELECTOR_IMGS_QUESTION_STATUS).css('display', 'inline-block');
          $(target + this.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', this.QUESTION_IMAGES.ICO_PNG_ANSWERING);
          $(target + this.SELECTOR_IMG_SVG_OBJ_QUESTION_STATUS).attr('data', this.QUESTION_IMAGES.ICO_SVG_ANSWERING);

          break;
        }

        case this.QUESTION_STATUS.RIGHT: {
          $(target + this.SELECTOR_IMGS_QUESTION_STATUS).css('display', 'inline-block');
          $(target + this.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', this.QUESTION_IMAGES.ICO_PNG_SUCCESS);
          $(target + this.SELECTOR_IMG_SVG_OBJ_QUESTION_STATUS).attr('data', this.QUESTION_IMAGES.ICO_SVG_SUCCESS);

          break;
        }

        case this.QUESTION_STATUS.CLEAR: {
          $(target + this.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', this.QUESTION_IMAGES.ICO_PNG_NO_ANSWER);
          $(target + this.SELECTOR_IMG_SVG_OBJ_QUESTION_STATUS).attr('data', this.QUESTION_IMAGES.ICO_SVG_NO_ANSWER);
          $(target + this.SELECTOR_IMGS_QUESTION_STATUS).css('display', 'none');

          break;
        }
      }
    } catch (err) {
      console.log("Failed to setQuestionStatusImg " + err);
    }
  },

  romanize: function (num) {
    if (!+num)
      return false;

    var digits = String(+num).split(""),
      key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
        "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
      roman = "",
      i = 3;

    while (i--)
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;

    return roman;
  },

  addRomanAlgarismsForAlternatives: function (listAlternatives) {
    try {

      for (var i = 0; i < listAlternatives.length; i++) {
        listAlternatives[i].romanRepresentation = this.romanize(listAlternatives[i].position);
      }

      return listAlternatives;

    } catch (err) {
      console.log("Failed to convert number into Roman algarism " + err);
    }

    return listAlternatives;
  },

  getAlphabetLetterForAlternatives: function (listAlternatives, upperCase) {
    try {

      for (var i = 0; i < listAlternatives.length; i++) {

        if (upperCase) {
          listAlternatives[i].alternativeLetter = this.ALPHABET[i].toUpperCase();
        } else {
          listAlternatives[i].alternativeLetter = this.ALPHABET[i];
        }
      }

      return listAlternatives;

    } catch (err) {
      console.log("Failed to convert number into Roman algarism " + err);
    }

    return listAlternatives;
  }

}


/**
 * SingleAnswerQuestion
 * Uses the attributes and functions of the Question class, via 'SingleAnswerQuestion.base' attribute.
 * The SingleAnswerQuestion allows the selection of only one right alternative for the question.
 * 
 * A sample of the expected data model for a SingleAnswerQuestion object is:
   model: {
      btnTryAgainText: "Tentar de novo",
      btnSubmitText: "Enviar Resposta",
      lang: "",
      unitId: -1,
      questionId: -1,
      unitNumber: -1,
      questionNumber: -1,
      enunciate: "",
      rightAlternative: -1,
      questionStatusImg: "",
      questionErrorMsg: "",
      alternatives: [
         {
            alternativeText: "",
            alternativeHint: "",
            hintClass: ""
         }
      ],
      staticAlternatives: [
         {
            id: 0,
            position: 1,
            numericalRepresentation: "",
            alternativeText: "10 - 5"
         }
      ]
   },
 */
function SingleAnswerQuestion() {
  this._initialized = false;
};

SingleAnswerQuestion.prototype = {

  base: jQuery.extend({}, new Question()),
  model: {},
  target: undefined,
  gotItRight: false,
  allowedToSelect: true,
  selectedAlternativeId: -1,
  callback: undefined,
  callbackClickBtnTryAgain: undefined,

  /**
   * Getters and Setters
   */
  setModel: function (model) {
    this.model = model;
  },

  getModel: function () {
    return this.model;
  },

  setTarget: function (target) {
    this.target = target;
  },

  getTarget: function () {
    return this.target;
  },

  setUserGotItRight: function (gotItRight) {
    this.gotItRight = gotItRight;
  },

  userGotItRight: function () {
    return this.gotItRight;
  },

  setIsAllowedToSelect: function(isAllowed) {
    this.allowedToSelect = isAllowed;
  },

  isAllowedToSelect: function() {
    return this.allowedToSelect;
  },

  setSelectedAlternativeId: function (alternativeId) {
    this.selectedAlternativeId = alternativeId;
  },

  getSelectedAlternativeId: function () {
    return this.selectedAlternativeId;
  },

  setCallback: function (callbackFunction) {
    this.callback = callbackFunction;
  },

  getCallback: function () {
    return this.callback;
  },

  setCallbackBtnTryAgain: function (callbackFunction) {
    this.callbackClickBtnTryAgain = callbackFunction;
  },

  getCallbackBtnTryAgain: function() {
    return this.callbackClickBtnTryAgain;
  },

  getTemplate: function () {
    return '<div id="question_{{unitNumber}}_{{questionNumber}}" class="question">' +
      '<h4 class="txt question-status-container">' +
      '<span>' +
      '<div class="safe-img">' +
      '<object data="{{questionStatusSvg}}" type="image/svg+xml" class="nav-item-ico">' +
      '<img src="{{questionStatusPng}}" class="nav-item-ico">' +
      '</object>' +
      '</div>' +
      '</span>' +
      '<b>Questão {{questionNumber}}</b>' +
      '</h4>' +
      '<div class="header">' +
      '<div class="enunciate">' +
      '<p class="txt">{{{enunciate}}}</p>' +
      '</div>' +
      '</div>' +
      '<div class="body">' +
      '{{#hasStaticAlternatives}}' +
      '<div class="static-alternatives">' +
      '{{#staticAlternatives}}' +
      '<div class="label-container">' +
      '<b><span>{{romanRepresentation}}</span></b> {{{alternativeText}}}' +
      '</div>' +
      '{{/staticAlternatives}}' +
      '</div>' +
      '{{/hasStaticAlternatives}}' +
      '{{^hasStaticAlternatives}}' +
      '{{/hasStaticAlternatives}}' +
      '{{#alternatives}}' +
      '<div id="alternative_{{unitNumber}}_{{questionNumber}}_{{id}}" class="alternative">' +
      '<div class="selector radio">' +
      '<input type="radio" name="optradio_{{questionNumber}}">' +
      '</div>' +
      '<div class="text">' +
      '<div class="alternative-text">' +
      '{{#hasStaticAlternatives}}' +
      '{{{alternativeText}}}' +
      '{{/hasStaticAlternatives}}' +
      '{{^hasStaticAlternatives}}' +
      '<b><span>{{{alternativeLetter}}}</span></b> {{{alternativeText}}}' +
      '{{/hasStaticAlternatives}}' +
      '</div>' +
      '<div class="alternative-hint {{hintClass}} hidden">' +
      '{{{alternativeHint}}}' +
      '</div>' +
      '</div>' +
      '</div>' +
      '{{/alternatives}}' +
      '</div>' +
      '<div class="question-toolbox">' +
      '<a class="btn btn-default try-again">{{btnTryAgainText}}</a>' +
      '<a class="btn btn-primary submit">{{btnSubmitText}}</a>' +
      '</div>' +
      '</div>';
  },

  submitQuestion: function () {
    try {
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      // Process the result
      if (this.getSelectedAlternativeId() == this.getModel().rightAlternative) {
        this.setUserGotItRight(true);
      } else {
        var selectedAlternative = this.base.getAlternative(this.getTarget(), this.getModel().unitNumber, this.getModel().questionNumber, this.getSelectedAlternativeId());

        if (selectedAlternative !== undefined) {
          $(selectedAlternative).find(this.base.SELECTOR_ALTERNATIVE_HINT).removeClass(this.base.CLASS_WARNING);
          $(selectedAlternative).find(this.base.SELECTOR_ALTERNATIVE_HINT).addClass(this.base.CLASS_ERROR);
        }
      }

      // Reveal the hint message of the question
      if (!this.userGotItRight()) {
        this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.WRONG);

        // Update the question status
        this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.WRONG, true, this.getSelectedAlternativeId());
      } else {
        $(questionIdSelector + this.base.SELECTOR_TRY_AGAIN_BTN).css("opacity", "0.5");
        $(questionIdSelector + this.base.SELECTOR_SUBMIT_BTN).css("opacity", "0.5");

        this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.RIGHT);

        // Update the question status
        this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.RIGHT, true, this.getSelectedAlternativeId());
      }

      // Reveal the hint message of the alternative
      if (this.getSelectedAlternativeId() !== -1) {
        var selectedAlternative = this.base.getAlternative(this.getTarget(), this.getModel().unitNumber, this.getModel().questionNumber, this.getSelectedAlternativeId());

        if (selectedAlternative !== undefined) {
          $(selectedAlternative).find(this.base.SELECTOR_ALTERNATIVE_HINT).removeClass(this.base.CLASS_HIDDEN);
        }
      }

      // Display the current status of the question on the Question's header
      $(questionIdSelector + this.base.SELECTOR_IMG_PNG_QUESTION_STATUS).removeClass(this.base.CLASS_HIDDEN)

      // Force the user to click on the 'try again' button
      this.setIsAllowedToSelect(false);

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to submitQuestion " + err);
    }
  },

  validateSelectedAlternative: function (selectedAlternative) {
    try {
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      // Update the image that represents the status of the question
      this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.ANSWERING);

      $(questionIdSelector + this.base.SELECTOR_SUCCESFUL_ANSWER).removeClass(this.base.CLASS_HIDDEN);

      // Update the question status
      this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.ANSWERING, true);

      var firstParent = $(selectedAlternative).parent();
      var secondParent = $(selectedAlternative).parent().parent();
      var idParts = [];

      if ($(firstParent).hasClass(this.base.CLASS_ALTERNATIVE)) {
        idParts = $(firstParent).attr("id").split("_");
      } else {
        idParts = $(secondParent).attr("id").split("_");
      }

      this.setSelectedAlternativeId(Number(idParts[3]));

    } catch (err) {
      console.log("SingleAnswerQuestion: Warning: Failed to validateSelectedAlternative " + err);
    }
  },

  resetQuestionStatus: function (questionIdSelector) {
    try {
      $(questionIdSelector + this.base.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', "");

      // Update the question status
      this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.CLEAR, true);

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to resetQuestionStatus " + err);
    }
  },

  hideHints: function () {
    try {
      var ctx = this;
      var questionIdSelector = ctx.base.getQuestionIdSelector(ctx.getModel().unitNumber, ctx.getModel().questionNumber);
      var alternativesHints = $(questionIdSelector + ctx.base.SELECTOR_ALTERNATIVE_HINT);

      $(alternativesHints).each(function () {
        if (!$(this).hasClass(ctx.base.CLASS_HIDDEN)) {
          $(this).addClass(ctx.base.CLASS_HIDDEN);
        }
      });

    } catch (err) {
      console.log("SingleAnswerQuestion: Warning: Failed to hideHints " + err);
    }
  },

  ensureSelectionRightAlternative: function () {
    try {
      this.base.getAlternativeInput(
        this.getTarget(), 
        this.getModel().unitNumber, 
        this.getModel().questionNumber, 
        this.getModel().rightAlternative
      ).prop('checked', true);

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to ensureSelectionRightAlternative " + err);
    }
  },

  onRadioBtnClick: function (obj) {
    try {

      if (!this.userGotItRight()) {
        this.hideHints();
        this.validateSelectedAlternative($(obj));
        
      } else {
        this.ensureSelectionRightAlternative();
      }

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed on onRadioBtnClick " + err);
    }
  },

  revertClick: function() {
    try {
      this.base.getAlternativeInput(this.getTarget(), this.getModel().unitNumber, this.getModel().questionNumber, this.getSelectedAlternativeId()).prop('checked', true);

    } catch (err) {
      console.log("SingleAnswerQuestion: Warning: Failed to revertClick " + err);
    }
  },

  getPositionRightAlternative: function() {
    try {
      
      for (var i = 0; i < this.getModel().alternatives.length; i++) {
        alternative = this.getModel().alternatives[i];

        if (alternative.id === this.getModel().rightAlternative) {
          return i;
        }
      }

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to getPositionRightAlternative " + err);
    }

    return -1;
  },

  verifyStateForAnsweredQuestion: function () {
    try {
      var ctx = this;
      var questionIdSelector = ctx.base.getQuestionIdSelector(ctx.getModel().unitNumber, ctx.getModel().questionNumber);
      var alternativesHints = $(questionIdSelector + ctx.base.SELECTOR_ALTERNATIVE_HINT);

      if (ctx.getModel().questionStatus === ctx.base.QUESTION_STATUS.RIGHT) {
        ctx.setUserGotItRight(true);
        ctx.setIsAllowedToSelect(false);

        var rightAlternative = $(alternativesHints)[ctx.getPositionRightAlternative()]
        $(rightAlternative).removeClass(ctx.base.CLASS_HIDDEN);

        $(questionIdSelector + ctx.base.SELECTOR_TRY_AGAIN_BTN).css("opacity", "0.5");
        $(questionIdSelector + ctx.base.SELECTOR_SUBMIT_BTN).css("opacity", "0.5");

        ctx.base.setQuestionStatusImg(questionIdSelector, ctx.base.QUESTION_STATUS.RIGHT);
        ctx.ensureSelectionRightAlternative();
      } else {
        ctx.setIsAllowedToSelect(true);
      }

    } catch (err) {
      console.log("SingleAnswerQuestion: Warning: Failed to verifyStateForAnsweredQuestion " + err);
    }
  },

  rebuild: function() {
    try {
      $(this.getTarget()).html("");

      var template = this.getTemplate();
      var questionHtml = Mustache.to_html(template, this.getModel());
      $(this.getTarget()).append(questionHtml);

      this.base.setQuestionStatusImg(this.getTarget(), this.getModel().questionStatus);
      this.verifyStateForAnsweredQuestion();

      this.initEvents();

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to rebuild " + err);
    }
  },

  /**
   * Resort the alternatives of a question in a random way in order to make it more dificult to 
   * memorize the position of the right alternatives.
   * 
   * This function uses the 'Fisher–Yates Shuffle' algorithm, O(n), to make the shuffling process.
   * https://bost.ocks.org/mike/shuffle/
   */
  shuffleAlternatives: function() {
    try {

      if (this.getModel().shuffleAlternatives) {
        var currentRightAlternativeId = this.getModel().alternatives[this.getModel().rightAlternative];
        var items = this.getModel().alternatives;
        var m = items.length;
        var i = undefined;
        var t = undefined;

        // While there remain elements to shuffle…
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = items[m];
            items[m] = items[i];
            items[i] = t;
        }

        // Update the alternative letters
        if (this.getModel().alternativeLetterTransformation === this.base.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET) {
          items = this.base.getAlphabetLetterForAlternatives(items, true);

        } else if (this.getModel().alternativeLetterTransformation === this.base.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE) {
          items = this.base.addRomanAlgarismsForAlternatives(items);
        }
      
        this.getModel().alternatives = items;
      }

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to shuffleAlternatives " + err);
    }
  },

  initEvents: function () {
    try {
      var ctx = this;
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      $(questionIdSelector + this.base.SELECTOR_ALTERNATIVE_RADIO_BTN).on("click", function () {
        if (ctx.isAllowedToSelect()) {
          ctx.onRadioBtnClick($(this));
        } else {
          ctx.revertClick();
        }
      });

      $(questionIdSelector + this.base.SELECTOR_SUBMIT_BTN).on("click", function () {
        if (ctx.isAllowedToSelect()) {
          ctx.submitQuestion();
        }
      });

      $(questionIdSelector + this.base.SELECTOR_TRY_AGAIN_BTN).on("click", function () {
        if (!ctx.userGotItRight()) {
          ctx.resetQuestionStatus(questionIdSelector);
          ctx.hideHints();
          ctx.setIsAllowedToSelect(true);
          ctx.shuffleAlternatives();
          ctx.rebuild();
          
          if (ctx.getCallbackBtnTryAgain() != undefined) {
            ctx.getCallbackBtnTryAgain()();
          }
        }
      });

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to initEvents " + err);
    }
  },

  build: function (tgt, model, callback, callbackBtnTryAgain) {
    try {

      if (tgt != undefined && model != undefined && callback != undefined) {
        this.setTarget(tgt);
        this.setModel(model);
        this.setCallback(callback);
        this.setCallbackBtnTryAgain(callbackBtnTryAgain);
        this.rebuild();
      }

    } catch(err) {
      console.log("SingleAnswerQuestion: Warning: Failed to build " + err);
    }
  }

};


/**
 * TrueOrFalseQuestion
 * The class TrueOrFalseQuestion allows a combination of correct answers for each alternative, 
 * in order to generate the correct answer of the question.
 * 
 * A sample of the expected data model for a TrueOrFalseQuestion object is:
   model: {
      btnTryAgainText: "Tentar de novo",
      btnSubmitText: "Enviar Resposta",
      lang: "",
      unitId: -1,
      questionId: -1,
      unitNumber: -1,
      questionNumber: -1,
      enunciate: "",
      rightAlternative: -1,
      questionErrorMsg: "",
      alternatives: [
         {
            isTrue: false,
            id: 0,
            alternativeText: "",
            alternativeHint: "",
            hintClass: ""
         }
      ]
   }
*/
function TrueOrFalseQuestion() {
  this._initialized = false;
};

TrueOrFalseQuestion.prototype = {

  SELECTOR_ALTERNATIVE_HINT: " .body .alternative .text .alternative-hint",

  base: jQuery.extend({}, new Question()),
  model: {},
  target: undefined,
  gotItRight: false,
  selectedAlternativeId: -1,
  allowedToSelect: true,
  selectedAlternatives: {},
  callback: undefined,
  callbackClickBtnTryAgain: undefined,

  /**
   * Getters and Setters
   */
  setModel: function (model) {
    this.model = model;
  },

  getModel: function () {
    return this.model;
  },

  setTarget: function (target) {
    this.target = target;
  },

  getTarget: function () {
    return this.target;
  },

  setUserGotItRight: function (gotItRight) {
    this.gotItRight = gotItRight;
  },

  userGotItRight: function () {
    return this.gotItRight;
  },

  setIsAllowedToSelect: function(isAllowed) {
    this.allowedToSelect = isAllowed;
  },

  isAllowedToSelect: function() {
    return this.allowedToSelect;
  },

  setSelectedAlternativeId: function (alternativeId) {
    this.selectedAlternativeId = alternativeId;
  },

  getSelectedAlternativeId: function () {
    return this.selectedAlternativeId;
  },

  setSelectedAlternatives: function (alternatives) {
    this.selectedAlternatives = alternatives;
  },

  getSelectedAlternatives: function () {
    return this.selectedAlternatives;
  },

  setCallback: function (callbackFunction) {
    this.callback = callbackFunction;
  },

  getCallback: function () {
    return this.callback;
  },

  setCallbackBtnTryAgain: function (callbackFunction) {
    this.callbackClickBtnTryAgain = callbackFunction;
  },

  getCallbackBtnTryAgain: function() {
    return this.callbackClickBtnTryAgain;
  },

  // Other methods
  getTemplate: function () {
    return '<div id="question_{{unitNumber}}_{{questionNumber}}" class="question tf">' +
      '<h4 class="txt question-status-container">' +
      '<span>' +
      '<div class="safe-img">' +
      '<object data="{{questionStatusSvg}}" type="image/svg+xml" class="nav-item-ico">' +
      '<img src="{{questionStatusPng}}" class="nav-item-ico">' +
      '</object>' +
      '</div>' +
      '</span>' +
      '<b>Questão {{questionNumber}}</b>' +
      '</h4>' +
      '<div class="header">' +
      '<div class="selector-label">' +
      '<span class="left">{{trueAlternativeSelectorLabel}}</span> <b>|</b> <span class="right">{{{falseAlternativeSelectorLabel}}}</span>' +
      '</div>' +
      '<div class="enunciate">' +
      '<p class="txt">{{{enunciate}}}</p>' +
      '</div>' +
      '</div>' +
      '<div class="body">' +
      '{{#alternatives}}' +
      '<div id="alternative_{{unitNumber}}_{{questionNumber}}_{{id}}" class="alternative">' +
      '<div class="selector">' +
      '<div class="radio left t">' +
      '<input type="radio" name="optradio_{{id}}_{{questionNumber}}"> <small>{{{trueAlternativeSelectorLabel}}}</small>' +
      '</div>' +
      '<div class="radio right f">' +
      '<input type="radio" name="optradio_{{id}}_{{questionNumber}}"> <small>{{{falseAlternativeSelectorLabel}}}</small>' +
      '</div>' +
      '</div>' +
      '<div class="text">' +
      '<div class="alternative-text">' +
      '{{{alternativeText}}}' +
      '</div>' +
      '<div class="alternative-hint {{hintClass}} hidden">' +
      '{{{alternativeHintSuccess}}}' +
      '</div>' +
      '</div>' +
      '</div>' +
      '{{/alternatives}}' +
      '</div>' +
      '<div class="question-toolbox">' +
      '<a class="btn btn-default try-again">{{btnTryAgainText}}</a>' +
      '<a class="btn btn-primary submit">{{btnSubmitText}}</a>' +
      '</div>' +
      '</div>';
  },

  getPositionAlternativeById: function(alternativeId) {
    try {
      
      for (var i = 0; i < this.getModel().alternatives.length; i++) {
        alternative = this.getModel().alternatives[i];

        if (alternative.id === alternativeId) {
          return i;
        }
      }

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to getPositionAlternativeById " + err);
    }

    return -1;
  },

  validateFinalAnswer: function () {
    try {
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      // Validate all alternatives that were answered so far
      this.setUserGotItRight(true);
      
      for (var i = 0; i < this.getModel().alternatives.length; i++) {
        var alternative = this.getModel().alternatives[i];
        var posAlternative = this.getPositionAlternativeById(alternative.id);
        var alternativeView = $(questionIdSelector + this.base.SELECTOR_ALTERNATIVE_HINT)[posAlternative];
        var selectedAlternativeOption = this.getSelectedAlternatives()[alternative.id];
  
        if (alternative != undefined && selectedAlternativeOption != undefined) {
  
          if (alternative.isTrue && selectedAlternativeOption === this.base.T_OR_F.T) {
            $(alternativeView).removeClass(this.base.CLASS_ERROR);
            $(alternativeView).removeClass(this.base.CLASS_WARNING);
            $(alternativeView).addClass(this.base.CLASS_SUCCESS);
  
            // Add success feedback
            $(alternativeView).html(alternative.alternativeHintSuccess);
  
          } else if (!alternative.isTrue && selectedAlternativeOption === this.base.T_OR_F.F) {
            $(alternativeView).removeClass(this.base.CLASS_ERROR);
            $(alternativeView).removeClass(this.base.CLASS_WARNING);
            $(alternativeView).addClass(this.base.CLASS_SUCCESS);
  
            // Add success feedback
            $(alternativeView).html(alternative.alternativeHintSuccess);
  
          } else {
            $(alternativeView).removeClass(this.base.CLASS_ERROR);
            $(alternativeView).removeClass(this.base.CLASS_SUCCESS);
            $(alternativeView).addClass(this.base.CLASS_ERROR);
  
            // Add error feedback
            $(alternativeView).html(alternative.alternativeHintError);
            this.setUserGotItRight(false);
          }
  
          $(alternativeView).removeClass(this.base.CLASS_HIDDEN);
        } else {
          // Add error feedback
          $(alternativeView).html(alternative.alternativeHintError);
          this.setUserGotItRight(false);
        }
      }

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to validateFinalAnswer " + err);
    }
  },

  submitQuestionTrueFalse: function () {
    try {
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      this.validateFinalAnswer();

      if (!this.userGotItRight()) {
        // Update the question status
        this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.WRONG);
        this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.WRONG, true, this.getSelectedAlternatives());

      } else {
        $(questionIdSelector + this.base.SELECTOR_TRY_AGAIN_BTN).css("opacity", "0.5");
        $(questionIdSelector + this.base.SELECTOR_SUBMIT_BTN).css("opacity", "0.5");

        // Update the question status
        this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.RIGHT);
        this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.RIGHT, true, this.getSelectedAlternatives());
      }

      // Force the user to click on the 'Try again' button to submit a new answer
      this.setIsAllowedToSelect(false);

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to submitQuestionTrueFalse " + err);
    }
  },

  validateSelectedAlternative: function (selectedAlternative) {
    try {
      // Get the selected alternative and option within the alternative
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      // Update the question status
      this.base.setQuestionStatusImg(questionIdSelector, this.base.QUESTION_STATUS.ANSWERING);
      this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.ANSWERING, true);

      $(questionIdSelector + this.base.SELECTOR_SUCCESFUL_ANSWER).removeClass(this.base.CLASS_HIDDEN);

      var selectedQuestion = -1;
      var selectedQuestionChoice = "";
      var alternative = $(selectedAlternative).parent().parent().parent();
      var idParts = [];

      if ($(alternative).hasClass(this.base.CLASS_ALTERNATIVE)) {
        idParts = $(alternative).attr("id").split("_");
      }

      selectedQuestion = Number(idParts[3]);

      if ($(selectedAlternative).parent().hasClass(this.base.T_OR_F.T)) {
        selectedQuestionChoice = this.base.T_OR_F.T;
      } else {
        selectedQuestionChoice = this.base.T_OR_F.F;
      }

      this.getSelectedAlternatives()[selectedQuestion] = selectedQuestionChoice;

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to validateSelectedAlternative " + err);
    }
  },

  resetQuestionStatus: function (questionIdSelector) {
    try {
      $(questionIdSelector + this.base.SELECTOR_IMG_PNG_QUESTION_STATUS).attr('src', "");

      // Update the question status
      this.getCallback()(this.getModel().unitId, this.getModel().questionId, this.base.QUESTION_STATUS.CLEAR, true);

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to resetQuestionStatus " + err);
    }
  },

  ensureSelectionRightAnswers: function () {
    try {
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);
      var alternativeSelector = this.base.SELECTOR_ALTERNATIVE_SELECTOR;

      for (var i = 0; i < this.getModel().alternatives.length; i++) {
        var alternativeSelectorView = $(questionIdSelector + alternativeSelector)[i];
        var alternative = this.getModel().alternatives[i];

        if (alternative != undefined) {

          if (alternative.isTrue) {
            $(alternativeSelectorView).find(this.base.SELECTOR_ALTERNATIVE_TRUE_INPUT).prop('checked', true);
          } else {
            $(alternativeSelectorView).find(this.base.SELECTOR_ALTERNATIVE_FALSE_INPUT).prop('checked', true);
          }
        }
      }

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to ensureSelectionRightAnswers " + err);
    }
  },

  hideHints: function () {
    try {
      var ctx = this;
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);
      var alternativesHints = $(questionIdSelector + this.base.SELECTOR_ALTERNATIVE_HINT);

      $(alternativesHints).each(function () {
        if (!$(this).hasClass(ctx.base.CLASS_HIDDEN)) {
          $(this).addClass(ctx.base.CLASS_HIDDEN);
        }
      });

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to hideHints " + err);
    }
  },

  hideWrongHints: function () {
    try {
      var ctx = this;
      var questionIdSelector = ctx.base.getQuestionIdSelector(ctx.getModel().unitNumber, ctx.getModel().questionNumber);
      var alternativesHints = $(questionIdSelector + ctx.base.SELECTOR_ALTERNATIVE_HINT);

      $(alternativesHints).each(function () {

        if (!$(this).hasClass(ctx.base.CLASS_HIDDEN) && $(this).hasClass(ctx.base.CLASS_WARNING)) {
          $(this).addClass(ctx.base.CLASS_HIDDEN);
        }
      });

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to hideWrongHints " + err);
    }
  },

  onClickAlternative: function (obj) {
    try {

      if (!this.userGotItRight()) {
        this.hideWrongHints();
        this.validateSelectedAlternative($(obj));
      } else {
        this.ensureSelectionRightAnswers();
      }

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to onClickAlternative " + err);
    }
  },

  revertClick: function() {
    try {
      // Build the selectors required to retrieve the Alternative HTML element of a True of False Question object
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);
      var alternativeSelector = this.base.SELECTOR_ALTERNATIVE_SELECTOR;

      // Get the alternatives that were previously selected
      var qAlternativesAnswersKeys = Object.keys(this.getSelectedAlternatives());

      for(var i = 0; i < qAlternativesAnswersKeys.length; i++) {
        qAlternativeAnswerKey = qAlternativesAnswersKeys[i];
        
        if (typeof(qAlternativeAnswerKey) !== "undefined") {
          var qAlternativeAnswer = this.getSelectedAlternatives()[qAlternativeAnswerKey];

          // Get the HTML object of the alternative
          var alternativeSelectorView = $(questionIdSelector + alternativeSelector)[i];
          
          if (typeof(alternativeSelectorView) != "undefined") {

            // Set the previously selected answer of each alternative of a True or False question
            if (qAlternativeAnswer === this.base.T_OR_F.T) {
              $(alternativeSelectorView).find(this.base.SELECTOR_ALTERNATIVE_TRUE_INPUT).prop('checked', true);
            } else {
              $(alternativeSelectorView).find(this.base.SELECTOR_ALTERNATIVE_FALSE_INPUT).prop('checked', true);
            }
          }
        }
      }

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to revertClick " + err);
    }
  },

  initEvents: function () {
    try {
      var ctx = this;
      var questionIdSelector = this.base.getQuestionIdSelector(this.getModel().unitNumber, this.getModel().questionNumber);

      $(questionIdSelector + this.base.SELECTOR_ALTERNATIVE_RADIO_BTN_LEFT_TF).on("click", function () {
        if (ctx.isAllowedToSelect()) {
          ctx.onClickAlternative($(this));
        } else {
          ctx.revertClick();
        }
      });

      $(questionIdSelector + this.base.SELECTOR_ALTERNATIVE_RADIO_BTN_RIGHT_TF).on("click", function () {
        if (ctx.isAllowedToSelect()) {
          ctx.onClickAlternative($(this));
        } else {
          ctx.revertClick();
        } 
      });

      $(questionIdSelector + this.base.SELECTOR_SUBMIT_BTN).on("click", function () {
        if (!ctx.userGotItRight()) {
          ctx.submitQuestionTrueFalse();
        }
      });

      $(questionIdSelector + this.base.SELECTOR_TRY_AGAIN_BTN).on("click", function () {
        if (!ctx.userGotItRight()) {
          ctx.resetQuestionStatus(questionIdSelector);
          ctx.hideHints();
          ctx.setIsAllowedToSelect(true);
          ctx.shuffleAlternatives();
          ctx.rebuild();

          if (ctx.getCallbackBtnTryAgain() != undefined) {
            ctx.getCallbackBtnTryAgain()();
          };
        }
      });

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to initEvents " + err);
    }
  },

  verifyStateForAnsweredQuestion: function () {
    try {
      var ctx = this;
      var questionIdSelector = ctx.base.getQuestionIdSelector(ctx.getModel().unitNumber, ctx.getModel().questionNumber);

      if (ctx.getModel().questionStatus === ctx.base.QUESTION_STATUS.RIGHT) {
        ctx.setUserGotItRight(true);
        ctx.ensureSelectionRightAnswers();
        ctx.setIsAllowedToSelect(false);

        var hintSelector = questionIdSelector + ctx.SELECTOR_ALTERNATIVE_HINT;
        var alternativesHints = $(hintSelector);

        $(alternativesHints).each(function () {
          $(this).removeClass(ctx.base.CLASS_WARNING);
          $(this).removeClass(ctx.base.CLASS_ERROR);
          $(this).addClass(ctx.base.CLASS_SUCCESS);
          $(this).removeClass(ctx.base.CLASS_HIDDEN);
        });

        $(questionIdSelector + ctx.base.SELECTOR_TRY_AGAIN_BTN).css("opacity", "0.5");
        $(questionIdSelector + ctx.base.SELECTOR_SUBMIT_BTN).css("opacity", "0.5");
      } else {
        ctx.setIsAllowedToSelect(true);
      }

      this.base.setQuestionStatusImg(questionIdSelector, ctx.getModel().questionStatus);

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to verifyStateForAnsweredQuestion " + err);
    }
  },

  adaptModelToLanguage: function (model) {
    try {
    
      if (model.lang != undefined) {

        switch (model.lang) {
  
          case "en":
            model.trueAlternativeSelectorLabel = "T";
            model.falseAlternativeSelectorLabel = "F";
            break;
  
          case "pt":
            model.trueAlternativeSelectorLabel = "V";
            model.falseAlternativeSelectorLabel = "F";
            break
  
          default:
            model.trueAlternativeSelectorLabel = "T";
            model.falseAlternativeSelectorLabel = "F";
            break;
        }
      }

    } catch (err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to adaptModelToLanguage " + err);
    }

    return model;
  },

  /**
   * Resort the alternatives of a question in a random way in order to make it more dificult to 
   * memorize the position of the right alternatives.
   * 
   * This function uses the 'Fisher–Yates Shuffle' algorithm, O(n), to make the shuffling process.
   * https://bost.ocks.org/mike/shuffle/
   */
  shuffleAlternatives: function() {
    try {

      if (this.getModel().shuffleAlternatives) {
        var items = this.getModel().alternatives;
        var m = items.length;
        var i = undefined;
        var t = undefined;
  
        // While there remain elements to shuffle…
        while (m) {
          // Pick a remaining element…
          i = Math.floor(Math.random() * m--);
  
          // And swap it with the current element.
          t = items[m];
          items[m] = items[i];
          items[i] = t;
        }
  
        this.getModel().alternatives = items;
      }
  
    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to shuffleAlternatives " + err);
    }
  },

  rebuild: function() {
    try {
      $(this.getTarget()).html("");

      var template = this.getTemplate();
      var questionHtml = Mustache.to_html(template, this.getModel());
      $(this.getTarget()).append(questionHtml);

      this.base.setQuestionStatusImg(this.getTarget(), this.getModel().questionStatus);
      this.initEvents();

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to rebuild " + err);
    }
  },

  build: function (tgt, model, callback, callbackBtnTryAgain) {
    try {

      if (tgt != undefined && model != undefined && callback != undefined) {
        model = this.adaptModelToLanguage(model);
        this.setTarget(tgt);
        this.setModel(model);
        this.setCallback(callback);
        this.setCallbackBtnTryAgain(callbackBtnTryAgain);
        this.rebuild();
        this.verifyStateForAnsweredQuestion();
      }

    } catch(err) {
      console.log("TrueOrFalseQuestion: Warning: Failed to build " + err);
    }
  }

};


/**
 * ListQuestionsStatus
 * The class ListQuestionsStatus allows to display a list of questions and their current status.
 * The status of a question is the same defined in the Question.js base class.
 * 
 * A sample of the expected data model for a TrueOrFalseQuestion object is:
   model: {
      groupQuestionsTitle: "Unit 1",
      questions: [
         {
            unitId: 1,
            questionId: 2,
            questionName: "Question 2",
            questionStatusImg: "/images/ico_right.svg"
         }
      ]
   }
*/
function ListQuestionsStatus() {
  this._initialized = false;
};

ListQuestionsStatus.prototype = {

  SELECTOR_QUESTION_LINK: " ul li .question-link",
  SELECTOR_QUESTION_STATUS_IMG: ".question-status-img",

  CLASS_TEXT_LIGHT: "text-light",
  CLASS_TEXT_DARK: "text-dark",

  base: jQuery.extend({}, new Question()),
  model: {},
  target: undefined,
  callback: undefined,

  /**
   * Getters and Setters
   */
  setModel: function (model) {
    this.model = model;
  },

  getModel: function () {
    return this.model;
  },

  setTarget: function (target) {
    this.target = target;
  },

  getTarget: function () {
    return this.target;
  },

  setCallback: function (callback) {
    this.callback = callback;
  },

  getCallback: function () {
    return this.callback;
  },

  /**
   * Other methods
   */
  getId: function (target) {
    try {
      return target.replace("#", "");
    } catch (err) {
      console.log("Failed to getId " + err);
    }
  },

  /**
   * Events
   */
  onQuestionSelected: function (rawQuestionId) {
    try {
      var unitNumber = -1;
      var questionNumber = -1;

      var questionUrlParts = rawQuestionId.split("_");
      var unitNumber = Number(questionUrlParts[3]);
      var questionNumber = Number(questionUrlParts[5]);

      if (this.getCallback() != undefined) {
        this.getCallback()(unitNumber, questionNumber);
      }
    } catch (err) {
      console.log("Failed to onQuestionSelected " + err);
    }
  },

  initEvents: function () {
    var ctx = this;

    $(this.getTarget() + this.SELECTOR_QUESTION_LINK).on("click", function () {
      var rawQuestionId = $(this).attr("id");
      ctx.onQuestionSelected(rawQuestionId);
    });
  },

  /**
   * Initialization
   */
  getTemplate: function () {
    return '<label>{{{groupQuestionsTitle}}}</label>' +
      '<ul>' +
      '{{#questions}}' +
      '<li>' +
      '<a id="' + this.getId(this.getTarget()) + '_unit_{{unitId}}_question_{{questionId}}" class="question-link">' +
      '<img src="{{questionStatusImg}}" class="question-status-img">' +
      '<span>{{questionName}}</span>' +
      '</a>' +
      '</li>' +
      '{{/questions}}' +
      '</ul>';
  },

  initTextColor: function() {
    try {
      var ctx = this;

      $(this.getTarget()).find(this.SELECTOR_QUESTION_STATUS_IMG).each(function() {
        var questionStatusImg = $(this).attr("src");
        
        if (questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_WRONG || 
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_WRONG ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_WRONG || 
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_SVG_WRONG || 
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_ANSWERING ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_ANSWERING ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_ANSWERING ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_SVG_ANSWERING ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_SUCCESS ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_SUCCESS ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_SUCCESS ||
            questionStatusImg === gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_SVG_SUCCESS) {
          $(this).parent().addClass(ctx.CLASS_TEXT_LIGHT);
        } else {
          $(this).parent().addClass(ctx.CLASS_TEXT_DARK);
        }
      });

    } catch (err) {
      console.log("ListQuestionsStatus warning: failed to initTextColor " + err);
    }
  },

  build: function (tgt, model, callback) {
    if (tgt != undefined && model != undefined) {
      this._initialized = true;

      this.setTarget(tgt);
      this.setModel(model);
      this.setCallback(callback);

      var template = this.getTemplate();
      var html = Mustache.to_html(template, this.getModel());

      $(this.getTarget()).append(html);
      this.initEvents();
      this.initTextColor();
    }
  }

};