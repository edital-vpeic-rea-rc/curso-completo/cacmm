
/**
 * colorfulTimeline.js handles the actions of the component ColorfulTimeline, 
 * which is a bar that contains an horizontal list of landmarks, when the user clicks on any of the landmarks,
 * a custom tooltip is displayed at the bottom or at the top of the selected item.
 * 
 * The placement of the tooltip dialog is made on the HTML part of the component to allow full customization.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ColorfulTimeline() {
	this._initialized = false;
}

ColorfulTimeline.prototype = {

    DEBUG_CTT: true,

    SELECTOR_BACKGROUND: " .ct-landmarks .ct-background",
    SELECTOR_TOOLTIP_SELECTORS_CONTAINER: " .ct-landmarks .selectors-container",
    SELECTOR_SELECTORS_CONTAINERS: " .selectors-container",
    SELECTOR_TOOLTIP_SELECTOR: " .ct-landmarks .selectors-container .outter-container .inner-container .ct-landmarks-container .ct-landmark-selector",
    SELECTOR_LANDMARK_POINTER: " .selected-landmark-pointer",
    SELECTOR_LANDMARK_CONTAINER: " .ct-landmarks .selectors-container .outter-container .inner-container .ct-landmarks-container",
    SELECTOR_LANDMARK_CONTAINER_1: " .ct-landmarks-container",
    SELECTOR_LANDMARK_CONTENT: " .ct-landmark-content",
    SELECTOR_SELECTED_LANDMARK_CONTENT_MOBILE: " .ct-selected-landmark-content-mobile",

    PLACEMENT_TOP: "top",
    PLACEMENT_BOTTOM: "bottom",

    CLASS_HIDDEN: "hidden",
    CLASS_SELECTED: "selected",

    DEFAULT_TOOLTIP_POINTER_Y: "0px",
    DEFAULT_TOOLTIP_POINTER_TOP: "-60px", 
    DEFAULT_TOOLTIP_POINTER_BOTTOM: "95px",
    DEFAULT_TOOLTIP_POINTER_BOTTOM_OVERFLOW: "95px",

    model : {
        bgImgSvg: "",
        bgImgPng: "",
        landmarks: [
            {
                id: "",
                contentContainerId: "",
                placement: "",
                label: ""
            }
        ],
        topTooltipsIds: [
            {
                id: "",
                label: "",
            }
        ],
        bottomTooltipsIds: [
            {
                id: "",
                label: "",
            }
        ]
    },

    target : undefined,
    prevTop : 0,

    /**
     * Getters and Setters
     */
    getSelectorId: function(selectorPosition) {
        return this.target.replace("#", "") + "_ct_s_" + selectorPosition;
    },

    getIdSelector: function(id) {
        return "#" + id;
    },

    /**
     * Events
     */
    closeAllTooltips: function() {
        try {
            for (var i = 0; i < this.model.landmarks.length; i++) {
                var landmark = this.model.landmarks[i];
                $(landmark.contentContainerId).addClass(this.CLASS_HIDDEN);
            }

            $(this.target + this.SELECTOR_TOOLTIP_SELECTOR).removeClass(this.CLASS_SELECTED);
            $(this.SELECTOR_LANDMARK_POINTER).addClass(this.CLASS_HIDDEN);

        } catch(err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to closeAllTooltips " + err);
            }
        }
    },

    openSelectedTooltip: function(selectedTooltipObj) {
        try {
            var tooltipId = $(selectedTooltipObj).attr("id");
            var selectedLandmark = undefined;
    
            // Find and open only the tooltip of the selected landmark
            this.closeAllTooltips();
            var landmarkXPos = -1;

            for (var i = 0; i < this.model.landmarks.length; i++) {
                var landmark = this.model.landmarks[i];
    
                if (landmark.id === tooltipId) {
                    selectedLandmark = landmark;
                    landmarkXPos = i;
                } else {
                    $(this.getIdSelector(landmark.id)).find(".ct-landmark-content").remove();
                    $(this.target + this.SELECTOR_SELECTED_LANDMARK_CONTENT_MOBILE).find(".ct-landmark-content").remove();
                }
            }

            // Build the content container of the selected landmark
            var contentContainer = '<div class="ct-landmark-content ';

            // Set the positioning of the selected landmark
            if (!gUi.isOnMobileViewMode()) { 

                if (selectedLandmark.placement === "top") {
                    contentContainer += "top ";
                } else {
                    contentContainer += "bottom ";
                }

                if (landmarkXPos < this.model.landmarks.length / 2) {
                    contentContainer += "left-side ";
                } else {
                    contentContainer += "right-side ";
                }
            }

            contentContainer += '">' + selectedLandmark.content + '</div>';
                
            if (gUi.isOnMobileViewMode()) {
                $(this.target + this.SELECTOR_SELECTED_LANDMARK_CONTENT_MOBILE).append(contentContainer);
            } else {
                // Insert the popup above or below the selected landmark pointer
                $(this.getIdSelector(selectedLandmark.id)).append(contentContainer);
            }

            // Add the selected class to the object
            $(this.target + this.SELECTOR_TOOLTIP_SELECTOR).removeClass(this.CLASS_SELECTED);
            $(selectedTooltipObj).addClass(this.CLASS_SELECTED);

        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to openSelectedTooltip: " + err);
            }
        }
    },

    openTooltipAtPositionN: function(position) {
        try {

            if (position != undefined) {

                if ($(this.target + this.SELECTOR_TOOLTIP_SELECTOR)[position] != undefined) {
                    $(this.target + this.SELECTOR_TOOLTIP_SELECTOR)[position].click();
                }
            }
        } catch(err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to openTooltipAtPositionN " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        $(this.target + this.SELECTOR_TOOLTIP_SELECTOR).on("click", function(){
            ctx.openSelectedTooltip($(this));
        });

        $(document).scroll( function(evt) {
            var currentTop = $(this).scrollTop();

            if (ctx.prevTop !== currentTop) {
                ctx.prevTop = currentTop;
                ctx.closeAllTooltips();
            }
        });
    },

    /**
     * Initialization
     */
    // UI
    getBgTemplate: function () {
        return '<object data="{{bgImgSvg}}" type="image/svg+xml" class="ct-background-img">' +
            '<img src="{{bgImgPng}}" class="ct-background-img">' +
            '</object>';
    },

    getSelectorsTemplate: function () {
        return '<div class="outter-container">' +
            '<div class="inner-container">' +
            '<div class="ct-landmarks-container">' +
            '{{#landmarks}}'+
            '<div id="{{id}}" class="ct-landmark-selector">' +
            '<span>{{{label}}}</span>' +
            '<div class="selected-landmark-pointer hidden"></div>' +
            '</div>' +
            '{{/landmarks}}' +
            '</div>' +
            '</div>' +
            '</div>';
    }, 

    applyBackgroundColorOnLandmarks: function() {
        try {
            for (var i = 0; i < this.model.landmarks.length; i++) {
                var landmark = this.model.landmarks[i];
                var startColor = "";
                var endColor = ""
    
                if (i < this.model.landmarks.length - 1) {
                    startColor = this.model.landmarks[i].bgColor;
                    endColor = this.model.landmarks[i + 1].bgColor;
    
                } else if (i > 0) {
                    startColor = this.model.landmarks[i - 1].bgColor;
                    endColor = this.model.landmarks[i].bgColor;
    
                } else {
                    startColor = this.model.landmarks[0].bgColor;
                    endColor = this.model.landmarks[0].bgColor;
                }
    
                $(this.getIdSelector(landmark.id)).css("background", startColor);
                $(this.getIdSelector(landmark.id)).css("background", "-webkit-linear-gradient(left, " + startColor + ", " + endColor + ")");
                $(this.getIdSelector(landmark.id)).css("background", "-o-linear-gradient(right, " + startColor + ", " + endColor + ")");
                $(this.getIdSelector(landmark.id)).css("background", "-moz-linear-gradient(right, " + startColor + ", " + endColor + ")");
                $(this.getIdSelector(landmark.id)).css("background", "linear-gradient(to right, " + startColor + ", " + endColor + ")");
            }
        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Failed to applyBackgroundColorOnLandmarks: " + err);
            }
        }
    },
    
    initUI: function() {
        try {
            var bgTemplate = this.getBgTemplate();
            var bgHtml = Mustache.to_html(bgTemplate, this.model);
            $(this.target + this.SELECTOR_BACKGROUND).html(bgHtml);
    
            var landmarkSelectorsTemplate = this.getSelectorsTemplate();
            var selectorsHtml = Mustache.to_html(landmarkSelectorsTemplate, this.model);
            $(this.target + this.SELECTOR_TOOLTIP_SELECTORS_CONTAINER).html(selectorsHtml);
    
            this.applyBackgroundColorOnLandmarks();
        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Failed to initUI: " + err);
            }
        }
    },

    // Data
    initData: function () {
        try {
            // Build the landmark objects related to the top tooltips
            var topTooltips = Object.assign({}, this.model.topTooltipsIds);
            var bottomTooltips = Object.assign({}, this.model.bottomTooltipsIds);
            this.model.landmarks = [];

            // Build the landmark bar selectors by alternating its related the data related to its content,
            // gathering information about one item from the top and one from the bottom.
            while (this.model.topTooltipsIds.length > 0 || this.model.bottomTooltipsIds.length > 0) {
                // Build a landmark based on information of a tooltip placed at the top
                var landmarkTop = {};
                landmarkTop.id = this.getSelectorId(this.model.landmarks.length);
                landmarkTop.placement = this.PLACEMENT_TOP;
                landmarkTop.contentContainerId = this.model.topTooltipsIds[0].id;
                landmarkTop.label = this.model.topTooltipsIds[0].label;
                landmarkTop.bgColor = this.model.topTooltipsIds[0].bgColor;

                if (landmarkTop.contentContainerId != undefined) {
                    landmarkTop.content = $(landmarkTop.contentContainerId).html();
                    $(landmarkTop.contentContainerId).remove();

                    this.model.landmarks.push(landmarkTop);
                }

                this.model.topTooltipsIds.splice(0, 1);

                // Build a landmark based on information of a tooltip placed at the bottom
                var landmarkBottom = {};
                landmarkBottom.id = this.getSelectorId(this.model.landmarks.length);
                landmarkBottom.placement = this.PLACEMENT_BOTTOM;
                landmarkBottom.contentContainerId = this.model.bottomTooltipsIds[0].id;
                landmarkBottom.label = this.model.bottomTooltipsIds[0].label;
                landmarkBottom.bgColor = this.model.bottomTooltipsIds[0].bgColor;

                if (landmarkBottom.contentContainerId != undefined) {
                    landmarkBottom.content = $(landmarkBottom.contentContainerId).html();
                    $(landmarkBottom.contentContainerId).remove();

                    this.model.landmarks.push(landmarkBottom);
                }

                this.model.bottomTooltipsIds.splice(0, 1);
            }

            // Rebuild the arrays that hold the ids of the top and bottom tooltip content containers
            topTooltipKeys = Object.keys(topTooltips);
            bottomTooltipKeys = Object.keys(bottomTooltips);

            for (var i = 0; i < topTooltipKeys.length; i++) {
                var key = topTooltipKeys[i];
                this.model.topTooltipsIds.push(topTooltips[key]);
            }

            for (var j = 0; j < bottomTooltipKeys.length; j++) {
                var key = bottomTooltipKeys[j];
                this.model.bottomTooltipsIds.push(bottomTooltips[key]);
            }

        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to initData: " + err);
            }
        }
    },

    build : function(tgt, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this.target = tgt;
                this.model = model;
    
                this.initData();
                this.initUI();
                this.initEvents();
            }
        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to build: " + err);
            }
        }
    }, 

    destroy: function() {
        try {
            // Remove events injected by the object
            $(this.target + this.SELECTOR_TOOLTIP_SELECTOR).off();
            
        } catch (err) {

            if (this.DEBUG_CTT) {
                console.log("ColorfulTimeline Warning: Failed to destroy: " + err);
            }
        }
    }

}