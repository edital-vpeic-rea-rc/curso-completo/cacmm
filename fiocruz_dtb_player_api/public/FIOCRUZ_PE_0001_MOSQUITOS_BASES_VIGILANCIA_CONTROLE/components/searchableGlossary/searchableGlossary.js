/**
 * searchableGlossary.js handles the actions of the component SearchableGlossary, which is an object 
 * that can be used to query for a search term via a search input or by clicking on one of the
 * letters of the alphabet, once a search is completed by any of the given methods, the search results 
 * are then presented inside a scrollable list.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Centro de Pesquisas Ageu Magalhães - Fiocruz Pernambuco.
 * @version 0.0.1
 * @requires jQuery
 * @requires Mustache
 * @license MIT
 */

function SearchableGlossary() {
	this._initialized = false;
}

SearchableGlossary.prototype = {

    DEBUG_SG : true,

    FORBIDDEN_SPECIAL_CHARS : {
        "À": "A",
        "Á": "A",
        "Â": "A",
        "Ã": "A",
        "Ä": "A",
        "Å": "A",
        "Æ": "A",
        "Ç": "C",
        "È": "E",
        "É": "E",
        "Ê": "E",
        "Ë": "E",
        "Ì": "I",
        "Í": "I",
        "Î": "I",
        "Ï": "I",
        "Ð": "D",
        "Ñ": "N",
        "Ò": "O",
        "Ó": "O",
        "Ô": "O",
        "Õ": "O",
        "Ö": "O",
        "Ø": "O",
        "Ù": "U",
        "Ú": "U",
        "Û": "U",
        "Ü": "U",
        "Ý": "Y",
        "Ŕ": "R",
        "Þ": "s",
        "ß": "B",
        "à": "a",
        "á": "a",
        "â": "a", 
        "ã": "a",
        "ä": "a",
        "å": "a",
        "æ": "a",
        "ç": "c",
        "è": "e",
        "é": "e",
        "ê": "e",
        "ë": "e",
        "ì": "i",
        "í": "i",
        "î": "i",
        "ï": "i",
        "ð": "o",
        "ñ": "n",
        "ò": "o",
        "ó": "o",
        "ô": "o",
        "õ": "o",
        "ö": "o",
        "ø": "o",
        "ù": "u",
        "ú": "u",
        "û": "u",
        "ü": "u",
        "ý": "y",
        "þ": "b",
        "ÿ": "y",
        "ŕ": "r"
    },

    // Selectors
    SELECTORS : {
        OUTTER_CONTAINER: ".glossary",
        SEARCH_INPUT: ".glossary .search-bar input",
        SEARCH_INPUT_ICO: ".glossary .search-bar img",
        CONTAINER_ALPHABET_SELECTORS: ".glossary .alphabet-selector p",
        CONTAINER_SEARCH_RESULTS: ".glossary .search-results",
        CONTAINER_SEARCH_RESULTS_ITEM: ".glossary .search-results .sr-item h3",
        CONTAINER_SEARCH_RESULTS_HIDDEN_ITEM: ".glossary .search-results .sr-item.hidden h3",
        CONTAINER_NO_SEARCH_RESULTS: ".glossary .item-not-found",
    },

    CLASSES : {
        CALL_ATTENTION_SEARCH: "call-user-attention-search",
        HIDDEN: "hidden",
        SELECTED: "selected"
    },

    // Main variables
    target : undefined,
    model : {
        searchPlaceHolder: "Search here ...",
        lblAlphabetSelectors: "Search by letter",
        lblItemNotFound: "Item not found",
        contentPerLetter: {
            A: {
                "A1": {
                    name: "A1",
                    htmlDescription: "Description of the item A1",
                    listImgs: [
                        ""
                    ]
                },
                "A2": {
                    name: "A2",
                    htmlDescription: "Description of the item A2",
                    listImgs: [
                        ""
                    ]
                }
            },
            B: {},
            C: {},
            D: {},
            E: {},
            F: {},
            G: {},
            H: {},
            I: {},
            J: {},
            K: {},
            L: {},
            M: {},
            N: {},
            O: {},
            P: {},
            Q: {},
            R: {},
            S: {},
            T: {},
            U: {},
            V: {},
            W: {},
            X: {},
            Y: {},
            Z: {}
        }
    },
    
    /**
     * Events
     */
    showSearchResultsContainer: function(show) {
        try {

            if (show) {
                $(this.SELECTORS.CONTAINER_SEARCH_RESULTS).removeClass(this.CLASSES.HIDDEN);
                $(this.SELECTORS.CONTAINER_NO_SEARCH_RESULTS).addClass(this.CLASSES.HIDDEN);
            } else {
                $(this.SELECTORS.CONTAINER_SEARCH_RESULTS).addClass(this.CLASSES.HIDDEN);
                $(this.SELECTORS.CONTAINER_NO_SEARCH_RESULTS).removeClass(this.CLASSES.HIDDEN);
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Failed to showSearchResultsContainer " + err);
            }
        }
    },

    callUsersAttentionToSearch: function() {
        try {
            var ctx = this;

            setTimeout(function () {
                $(ctx.target + ctx.SELECTORS.SEARCH_INPUT_ICO).addClass(ctx.CLASSES.CALL_ATTENTION_SEARCH);

                setTimeout(function () {
                    $(ctx.target + ctx.SELECTORS.SEARCH_INPUT_ICO).removeClass(ctx.CLASSES.CALL_ATTENTION_SEARCH);
                }, 1000);
            }, 2000);

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Failed to callUsersAttentionToSearch " + err);
            }
        }
    },

    getNewSearchResultItem: function (searchResultItemData) {
        try {
            return '<div class="sr-item">' +
                '<h3 class="title">' + searchResultItemData.name + '</h3>' +
                '<p class="description">' + searchResultItemData.htmlDescription + '</p>' +
                '</div>';

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: failed to getNewSsarchResultItem " + err);
            }
        }
    },

    performSearchByLetter: function (letter) {
        try {

            if (letter != undefined && letter !== "") {
                // Remove the current items inside the search results container
                $(this.target + this.SELECTORS.CONTAINER_SEARCH_RESULTS).html("");

                // Insert all the terms that start with the first letter of the user's search input, into the search results container
                var contentKeys = Object.keys(this.model.contentPerLetter[letter]);
    
                if (contentKeys.length > 0) {
                    this.showSearchResultsContainer(true);
    
                    for (var i = 0; i < contentKeys.length; i++) {
                        var key = contentKeys[i];
                        var content = this.model.contentPerLetter[letter][key];
                        var contentHtml = this.getNewSearchResultItem(content);
        
                        $(this.target + this.SELECTORS.CONTAINER_SEARCH_RESULTS).append(contentHtml);
                    }
                } else {
                    this.showSearchResultsContainer(false);
                }

                // Update the alphabetic selectors
                var ctx = this;
    
                $(ctx.target + ctx.SELECTORS.CONTAINER_ALPHABET_SELECTORS).each(function() {
                    $(this).parent().removeClass(ctx.CLASSES.SELECTED);
                });
    
                $(ctx.target + ctx.SELECTORS.CONTAINER_ALPHABET_SELECTORS).each(function() {
                    
                    if ($(this).html() == letter) {
                        $(this).parent().addClass(ctx.CLASSES.SELECTED);
                    }
                });
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Failed to performSearchByLetter " + err);
            }
        }
    },

    searchBySentence: function(letter, searchTerm) {
        try {

            if (letter != undefined && letter !== "") {
                // Remove the current items inside the search results container
                $(this.target + this.SELECTORS.CONTAINER_SEARCH_RESULTS).html("");

                // Insert all the terms that start with the first letter of the user's search input, into the search results container
                var letterKeys = Object.keys(this.model.contentPerLetter);

                if (letterKeys.length > 0) {
                    this.showSearchResultsContainer(true);

                    for (var i = 0; i < letterKeys.length; i++) {
                        var letterItems = Object.keys(this.model.contentPerLetter[letterKeys[i]]);
    
                        for (var j = 0; j < letterItems.length; j++) {
                            var cleanSearchableWord = this.getCleanSearchInput(letterItems[j]);
                            var cleanSearchTerm = this.getCleanSearchInput(searchTerm);
    
                            if (cleanSearchableWord.indexOf(cleanSearchTerm) !== -1) {
                                var key = letterItems[j];
                                var content = this.model.contentPerLetter[letterKeys[i]][key];
                                var contentHtml = this.getNewSearchResultItem(content);
                    
                                $(this.target + this.SELECTORS.CONTAINER_SEARCH_RESULTS).append(contentHtml);
                            }
                        }
                    }
                } else {
                    this.showSearchResultsContainer(false);
                }

                // Update the alphabetic selectors
                var ctx = this;
    
                $(ctx.target + ctx.SELECTORS.CONTAINER_ALPHABET_SELECTORS).each(function() {
                    $(this).parent().removeClass(ctx.CLASSES.SELECTED);
                });
    
                $(ctx.target + ctx.SELECTORS.CONTAINER_ALPHABET_SELECTORS).each(function() {
                    
                    if ($(this).html() == letter) {
                        $(this).parent().addClass(ctx.CLASSES.SELECTED);
                    }
                });
            }

        } catch (err) {
            console.log("Warning: Failed to searchBySentence " + err);
        }
    },

    searchByLetter: function(selectedObj) {
        try {
            var selectedLetter = $(selectedObj).html();
            this.performSearchByLetter(selectedLetter);

        } catch (err) {
            
            if (this.DEBUG_SG) {
                console.log("Warning: Failed to searchByLetter " + err);
            }
        }
    },

    filterResultsBySearchTerm: function(searchTerm) {
        try {
            var ctx = this;

            $(ctx.target + ctx.SELECTORS.CONTAINER_SEARCH_RESULTS_ITEM).each(function() {
                var str = $(this).html().toLowerCase().split(' ').join('_')
                searchTerm = searchTerm.toLowerCase().split(' ').join('_')

                if (str.indexOf(searchTerm) !== -1) {
                    $(this).parent().removeClass(ctx.CLASSES.HIDDEN);
                } else {
                    $(this).parent().addClass(ctx.CLASSES.HIDDEN);
                }
            });

            if ($(ctx.target + ctx.SELECTORS.CONTAINER_SEARCH_RESULTS_ITEM).length === 
                $(ctx.target + ctx.SELECTORS.CONTAINER_SEARCH_RESULTS_HIDDEN_ITEM).length) {
                this.showSearchResultsContainer(false);
            } else {
                this.showSearchResultsContainer(true);
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Failed to filterResultsBySearchTerm " + err);
            }
        }
    },

    getCleanSearchInput: function(sentence) {
        try {
            updatedSentence = "";
            
            for (i = 0; i < sentence.length; i++) {
                swap = false;

                if (this.FORBIDDEN_SPECIAL_CHARS[sentence.substr(i, 1)] != undefined) {
                    updatedSentence += this.FORBIDDEN_SPECIAL_CHARS[sentence.substr(i, 1)];
                } else {
                    updatedSentence += sentence.substr(i, 1);
                }
            }

            return updatedSentence;

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: failed to getCleanSearchInput " + err);
            }
        }
    },

    onChangedSearchInput: function(searchInputObj) {
        try {
            var searchedTerm = $(searchInputObj).val();

            if (searchedTerm != undefined) {

                if (searchedTerm != "") {
                    var cleanSentence = this.getCleanSearchInput(searchedTerm);
                    var letter = cleanSentence[0].toUpperCase();

                    if (cleanSentence.length < 2) {
                        this.performSearchByLetter(letter);
                    } else {
                        this.searchBySentence(letter, cleanSentence);
                    }
                    
                    this.filterResultsBySearchTerm(searchedTerm);
                    this.showSearchResultsContainer(true);
                    
                } else {
                    this.showSearchResultsContainer(false);
                    this.callUsersAttentionToSearch();
                }
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Failed to onChangedSearchInput " + err);
            }
        }
    },

    initEvents: function () {
        var ctx = this;

        $(ctx.target + ctx.SELECTORS.SEARCH_INPUT).on("input", function() {
            ctx.onChangedSearchInput(this);
        });
        $(ctx.target + ctx.SELECTORS.SEARCH_INPUT).on("propertychange", function() {
            ctx.onChangedSearchInput(this);
        });

        $(ctx.target + ctx.SELECTORS.CONTAINER_ALPHABET_SELECTORS).on("click", function() {
            ctx.searchByLetter(this);
        });
    },

    /**
     * Initialization
     */
    initUI: function () {
        try {
            this.performSearchByLetter("A");
            this.callUsersAttentionToSearch();

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not initUI " + err);
            }
        }
    },

    getLettersAlphabetSelector: function () {
        try {
            return '<div class="alphabet-selector-letter selected">' +
                '<p>A</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>B</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>C</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>D</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>E</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>F</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>G</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>H</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>I</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>J</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>K</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>L</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>M</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>N</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>O</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>P</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>Q</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>R</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>S</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>T</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>U</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>V</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>W</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>X</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>Y</p>' +
                '</div>' +
                '<div class="alphabet-selector-letter">' +
                '<p>Z</p>' +
                '</div>';

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: could not getLettersAlphabetSelector " + err);
            }
        }
    },

    getTemplate: function () {
        return '<div class="search-bar">' +
            '<input type="text" class="form-control" placeholder="{{searchPlaceHolder}}">' +
            '<img src="{{icoSearchInput}}"/>' +
            '</div>' +
            '<div class="alphabet-selector">' +
            '<p class="lbl-search-by-letter">{{lblAlphabetSelectors}}:</p>' +
            this.getLettersAlphabetSelector() +
            '</div>' +
            '<div class="search-results">' +
            '</div>' +
            '<div class="item-not-found hidden">' +
            '<p>{{lblItemNotFound}}</p>' +
            '</div>' +
            '<div class="placeholder-info">' +
            '<p>{{lblPlaceholderInfo}}</p>' +
            '</div>' +
            '<div class="alphabet-selector bottom">' +
            '<p class="lbl-search-by-letter">{{lblAlphabetSelectors}}:</p>' +
            this.getLettersAlphabetSelector() +
            '</div>';
    },

    buildSkeleton: function(tgt, model) {
        try {
            this.target = tgt;
            this.model = model;
            var html = Mustache.to_html(this.getTemplate(), this.model);
    
            $(this.target).html(html);

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Warning: Could not buildSkeleton.");
            }
        }
    },

    /**
     * Mandatory parameters:
     * @param tgt: The DOM element that is going to receive the HTML code of the object
     * @param model: The variable that carries the data model of the object
     */
    build : function(tgt, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this._initialized = true;

                this.buildSkeleton(tgt, model);
                this.initUI();
                this.initEvents();
            }

        } catch (err) {

            if (this.DEBUG_SG) {
                console.log("Failed to build SB object " + err);
            }
        }
    }

}