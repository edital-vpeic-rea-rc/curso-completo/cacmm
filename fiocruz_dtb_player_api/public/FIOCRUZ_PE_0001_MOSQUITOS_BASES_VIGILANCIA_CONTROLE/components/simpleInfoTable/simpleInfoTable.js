/**
 * simpleInfoTable.js simply displays information about a virus.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function SimpleInfoTable() {
	this._initialized = false;
}

SimpleInfoTable.prototype = {

    DEBUG_SIF: true,

    model : {
        infoItems : [
            {
                label: "",
                info: ""
            }
        ]
    },
    target : undefined,

    getTemplate: function () {
        return '<tbody>' +
            '{{#infoItems}}'+
            '<tr>' +
            '<td class="sit-label"><b>{{{label}}}</b></td>' +
            '<td class="sit-value">{{{info}}}</td>' +
            '</tr>' +
            '{{/infoItems}}'+
            '</tbody>';
    }, 

    build : function(tgt, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this.target = tgt;
                this.model = model;

                var template = this.getTemplate();
                var html = Mustache.to_html(template, this.model);
        
                $(this.target).html(html);
            }
        } catch(err) {

            if (this.DEBUG_SIF) {
                console.log("SimpleInfoTable Warning: Failed to build " + err);
            }
        }
    }

}