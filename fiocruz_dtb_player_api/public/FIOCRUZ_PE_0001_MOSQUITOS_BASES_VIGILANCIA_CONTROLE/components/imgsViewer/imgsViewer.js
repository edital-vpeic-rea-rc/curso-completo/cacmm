/**
 * imgsViewer.js displays a list of images in a container, the currently selected image is on 
 * focus at the center of the screen and the others can be viewed at the bottom, inside an horizontal bar.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function ImgsViewer() {
	this._initialized = false;
}

ImgsViewer.prototype = {

    DEGUG_IMGS_VIEWER: true,

    BTN_CLOSE_SELECTOR: " .toolbar-btn.btn-act-close .selector",
    BTN_TOGGLE_EXPAND_SELECTOR: " .toolbar-btn.btn-act-toggle-expand .selector",
    BTN_PREV_SELECTOR: " .fsm-imgv-btn-prev .selector",
    BTN_NEXT_SELECTOR: " .fsm-imgv-btn-next .selector",
    MAIN_IMG_SELECTOR: " .fsm-imgv-content .fsm-selected-img-png",
    MAIN_IMG_SVG_SELECTOR: " .fsm-imgv-content .fsm-selected-img-svg",
    THUMB_IMG_SELECTOR: " .fsm-footer .fsm-imgv-imgs-list img",

    HIDDEN_CLASS: "hidden",
    DISMISS_CLASS: "dismissed",
    SELECTED_CLASS: "selected",

    KEYBOARD : {
        left: 37,
        right: 39,
        esc: 27
    },

    model : {
        title: "",
        subTitle: "",
        selectedSlidePos: 0,
        urlSelectedImgPng: "",
        urlSelectedImgSvg: "",
        urlSvgIcoBtnClose: "",
        urlPngIcoBtnClose: "",
        urlSvgIcoBtnFullScreen: "",
        urlPngIcoBtnFullScreen: "",
        urlSvgPrevBtn: "",
        urlPngPrevBtn: "",
        urlSvgNextBtn: "",
        urlPngNextBtn: "",
        images : [
            {
                urlPng: "",
                urlSvg: "",
                selected: "selected",
            },
        ]
    },

    target : undefined,
    targetFsm : undefined,

    getFsmTemplate: function () {
        return '<div class="fsm-header">' +
            '<div class="fsm-imgv-title">' +
            '<h3>{{{title}}}</h3>' +
            '<small>{{{subTitle}}}</small>' +
            '</div>' +
            '<div class="fsm-imgv-toolbar">' +
            '<a class="toolbar-btn btn-act-close">' +
            '<object data="{{urlSvgIcoBtnClose}}" type="image/svg+xml" class="toolbar-btn-ico">' +
            '<img src="{{urlPngIcoBtnClose}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-body">' +
            '<div class="fsm-imgv-btn-prev">' +
            '<object data="{{urlSvgPrevBtn}}" type="image/svg+xml" class="fsm-body-btn-ico">' +
            '<img src="{{urlPngPrevBtn}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</div>' +
            '<div class="fsm-imgv-content">' +
            '<img src="{{urlSelectedImgPng}}" class="fsm-selected-img-png">' +
            '</div>' +
            '<div class="fsm-imgv-btn-next">' +
            '<object data="{{urlSvgNextBtn}}" type="image/svg+xml" class="fsm-body-btn-ico">' +
            '<img src="{{urlPngNextBtn}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-footer">' +
            '<div class="fsm-imgv-imgs-list">' +
            '{{#images}}' +
            '<img src="{{urlPng}}" class="{{selected}}" alt="{{slidePos}}">' +
            '{{/images}}' +
            '</div>' +
            '</div>';
    }, 

    getTemplate: function () {
        return '<div class="fsm-header">' +
            '<div class="fsm-imgv-title">' +
            '<h3>{{{title}}}</h3>' +
            '<small>{{{subTitle}}}</small>' +
            '</div>' +
            '<div class="fsm-imgv-toolbar">' +
            '<a class="toolbar-btn btn-act-toggle-expand">' +
            '<object data="{{urlSvgIcoBtnFullScreen}}" type="image/svg+xml" class="toolbar-btn-ico">' +
            '<img src="{{urlPngIcoBtnFullScreen}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-body">' +
            '<div class="fsm-imgv-btn-prev">' +
            '<object data="{{urlSvgPrevBtn}}" type="image/svg+xml" class="fsm-body-btn-ico">' +
            '<img src="{{urlPngPrevBtn}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</div>' +
            '<div class="fsm-imgv-content">' +
            '<img src="{{urlSelectedImgPng}}" class="fsm-selected-img-png">' +
            '</div>' +
            '<div class="fsm-imgv-btn-next">' +
            '<object data="{{urlSvgNextBtn}}" type="image/svg+xml" class="fsm-body-btn-ico">' +
            '<img src="{{urlPngNextBtn}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-footer">' +
            '<div class="fsm-imgv-imgs-list">' +
            '{{#images}}' +
            '<img src="{{urlPng}}" class="{{selected}}" alt="{{slidePos}}">' +
            '{{/images}}' +
            '</div>' +
            '</div>';
    }, 

    /**
     * Events
     */
    show: function() {
        gUi.hideHeaderEduRes();
        $(this.targetFsm).removeClass(this.DISMISS_CLASS);
        $(this.targetFsm).removeClass(this.HIDDEN_CLASS);
    },

    hide: function() {
        $(this.targetFsm).addClass(this.DISMISS_CLASS);

        setTimeout(function() {
            $(this.targetFsm).addClass(this.HIDDEN_CLASS);
        }, 500);
    },

    isShowing: function() {
        return !$(this.targetFsm).hasClass(this.HIDDEN_CLASS) && !$(this.targetFsm).hasClass(this.DISMISS_CLASS);
    },

    toggleFullScreenMode: function() {
        try {
            if (this.isShowing()) {
                this.hide();
            } else {
                this.show();
            }
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to toggleFullScreenMode object " + err);
            }
        }
    },

    updateSelectedSlide: function() {
        try {
            var ctx = this;

            // Get the urls of the selected slide from the data model
            var selectedImg = ctx.model.images[ctx.model.selectedSlidePos];

            if (selectedImg != undefined) {
                // Update the selected slide in the viewer
                $(ctx.target + ctx.MAIN_IMG_SELECTOR).attr("src", selectedImg.urlPng);
                $(ctx.target + ctx.MAIN_IMG_SVG_SELECTOR).attr("data", selectedImg.urlSvg);

                $(ctx.targetFsm + ctx.MAIN_IMG_SELECTOR).attr("src", selectedImg.urlPng);
                $(ctx.targetFsm + ctx.MAIN_IMG_SVG_SELECTOR).attr("data", selectedImg.urlSvg);
                
                // Get the selected position
                var selectedPos = ctx.model.selectedSlidePos;

                // Add the selected class to the thumbnail that is related to the selected 
                // slide on the main part of the component
                var count = 0;
                $(ctx.target + ctx.THUMB_IMG_SELECTOR).each(function() {
                    
                    if (count == selectedPos) {
                        $(this).addClass(ctx.SELECTED_CLASS);
                    } else {
                        $(this).removeClass(ctx.SELECTED_CLASS);
                    }

                    count++
                });

                // Add the selected class to the thumbnail that is related to the selected 
                // slide on the expanded part of the component
                count = 0;
                $(ctx.targetFsm + ctx.THUMB_IMG_SELECTOR).each(function() {
                    
                    if (count == selectedPos) {
                        $(this).addClass(ctx.SELECTED_CLASS);
                    } else {
                        $(this).removeClass(ctx.SELECTED_CLASS);
                    }

                    count++
                });
            }
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to updateSelectedSlide object " + err);
            }
        }
    },

    selectPreviousSlide: function() {
        try {
            if (this.model.selectedSlidePos > 0) {
                this.model.selectedSlidePos--;
            }

            this.updateSelectedSlide();
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to selectPreviousSlide object " + err);
            }
        }
    },

    selectNextSlide: function() {
        try {
            if (this.model.selectedSlidePos < this.model.images.length - 1) {
                this.model.selectedSlidePos++;
            }

            this.updateSelectedSlide();
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to selectNextSlide object " + err);
            }
        }
    },

    selectSlide: function(slide) {
        try {
            var slidePos = $(slide).attr("alt");
            
            if ((slidePos > -1) && (slidePos < this.model.images.length)) {
                this.model.selectedSlidePos = slidePos;
            }

            this.updateSelectedSlide();
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to selectSlide object " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        // Close and expand buttons
        $(ctx.target + ctx.BTN_TOGGLE_EXPAND_SELECTOR).on("click", function() {
            ctx.toggleFullScreenMode();
        });

        $(ctx.targetFsm + ctx.BTN_CLOSE_SELECTOR).on("click", function() {
            ctx.hide();
        });

        // Previous and next buttons
        $(ctx.target + ctx.BTN_PREV_SELECTOR).on("click", function() {
            ctx.selectPreviousSlide();
        });

        $(ctx.target + ctx.BTN_NEXT_SELECTOR).on("click", function() {
            ctx.selectNextSlide();
        });

        $(ctx.targetFsm + ctx.BTN_PREV_SELECTOR).on("click", function() {
            ctx.selectPreviousSlide();
        });

        $(ctx.targetFsm + ctx.BTN_NEXT_SELECTOR).on("click", function() {
            ctx.selectNextSlide();
        });

        // Thumbnail selection
        $(ctx.target + ctx.THUMB_IMG_SELECTOR).on("click", function() {
            ctx.selectSlide($(this));
        });

        $(ctx.targetFsm + ctx.THUMB_IMG_SELECTOR).on("click", function() {
            ctx.selectSlide($(this));
        });

        // Controlling the slides with the left and right keyboard direction arrows
        $(document).on('keyup.modal', function (event) {

            if (event.which == ctx.KEYBOARD.left) {
                ctx.selectPreviousSlide();

            } else if (event.which == ctx.KEYBOARD.right) {
                ctx.selectNextSlide();

            } else if (event.which == ctx.KEYBOARD.esc) {
                ctx.hide();
            }
        });

    },

    initModal: function() {
        this.hide();
    },

    build : function(tgt, tgtFsm, model) {
        try {
            this.target = tgt;
            this.targetFsm = tgtFsm;

            if (model != undefined) {
                this.model = model;

                // Build the Fsm HTML code
                var viewerTemplateFsm = this.getFsmTemplate();
                var viewerHtmlFsm = Mustache.to_html(viewerTemplateFsm, this.model);
                $(this.targetFsm).html(viewerHtmlFsm);
                this.initModal();
                
                // Build the regular component
                var viewerTemplate = this.getTemplate();
                var viewerHtml = Mustache.to_html(viewerTemplate, this.model);
                $(this.target).html(viewerHtml);
                
                // Init the events for both parts of the component
                this.initEvents();
            }

        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to build object " + err);
            }
        }
    },

    destroy: function() {
        try {
            // Remove html injected by the object
            $(this.target).html("");

            // Remove events injected by the object
            $(this.target + this.BTN_TOGGLE_EXPAND_SELECTOR).off();
            $(this.target + this.BTN_PREV_SELECTOR).off();
            $(this.target + this.BTN_NEXT_SELECTOR).off();
            $(this.target + this.THUMB_IMG_SELECTOR).off();
            $(this.targetFsm + this.BTN_CLOSE_SELECTOR).off();
            $(this.targetFsm + this.BTN_PREV_SELECTOR).off();
            $(this.targetFsm + this.BTN_NEXT_SELECTOR).off();
            $(this.targetFsm + this.THUMB_IMG_SELECTOR).off();
            
        } catch(err) {

            if (this.DEGUG_IMGS_VIEWER) {
                console.log("ImgsViewer Warning: Failed to destroy object " + err);
            }
        }
    }

}