/**
 * sidebar.js handles the actions of the component Sidebar, which is a navigational menu placed
 * at the left side of the page. It has a toggle event to show and hide the content of the menu
 * and these methods are triggered by a toggle button and by motion events (swipe) left and right
 * at the sidebar and at the containers of the page.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @requires gPages
 * @requires gImages
 * @license MIT
 */

function Sidebar() {
	this._initialized = false;
}

Sidebar.prototype = {

    SIDEBAR_SELECTOR : ".sidebar",
    SIDEBAR_CONTAINER_SELECTOR : ".sidebar-container",
    SIDEBAR_TOGGLE_CONTAINER_SELECTOR : ".sidebar-toggle, .sidebar-toggle-edge, .sidebar-toggle-img",
    SIDEBAR_TOGGLE_BTN_SELECTOR : ".sidebar-toggle, .sidebar-toggle-img",
    SIDEBAR_SECTION_SELECTABLE_ELEMENT : "label",
    SIDEBAR_ALL_TOGGLE_LIST_ITEM_SELECTOR : ".sidebar-container .selectable, .sidebar-container .sidebar-toggable-list-item, .sidebar-container .sidebar-toggable-list-item label",
    SIDEBAR_TOGGLE_LIST_ITEM_SELECTOR : ".sidebar-toggable-list-item label",
    SIDEBAR_TOGGLE_LIST_ITEM_ICON_SHOW_LIST_SELECTOR : ".icon-show-list",
    SIDEBAR_MENU_OPTION_SELECTOR : ".sidebar-container .selectable",
    SIDEBAR_MENU_MAIN_OPTIONS_SELECTOR : " .sidebar-main-items p",
    SIDEBAR_MENU_OPTION_MINIFIED_SELECTOR : ".sidebar.minified .sidebar-main-items ul li .sidebar-toggable-list-item img",
    SIDEBAR_BG_SELECTOR : ".sidebar-bg",
    SIDEBAR_FILTER_SELECTOR : ".filter",
    SIDEBAR_TOGGABLE_DESCRIPTION : ".sidebar-toggable-list-item.small-title",
    SIDEBAR_SELECTED_ITEM_SELECTOR : " .selected",

    CLASS_TOGGLE_SIDEBAR_MENU : "shrink-sidebar",
    CLASS_EXPAND_SIDEBAR : "expanded",
    CLASS_SELECTED_SIDEBAR_ITEM : "selected",
    CLASS_MINIFIED : "minified",
    CLASS_HIDDEN: "hidden",
    CLASS_FLIP: "flip",

    HIDDEN_LOCKED_STYLE : {
        height : "0px",
        width : "0px",
        display : "none"
    },

    model : {
        pageId : "",
        title : "",
        bgFilter : "",
        unitNumber : "",
        subTitle : "",
        summaryTitle : "",
        summaryItems : [],
        mainMenuItems : [],
        onItemSelectedCallback : undefined,
        isUnitGoalsEnabled : true
    },

    target : undefined,
    isShrinked : false,

    /**
     * Getters and Setters
     */
    getTemplate: function () {
        return '<div class="sidebar-bg sidebar-unit-{{unitNumber}}"></div>' +
            '<div class="sidebar">' +
            '<div class="sidebar-items unit-{{unitNumber}}">' +
            '<div id="sidebarSummary_{{pageId}}" class="container sidebar-summary">' +
            '<h1 id="sidebarTitle_{{pageId}}" class="title">{{title}}</h1>' +
            '<p id="sidebarSubTitle_{{pageId}}" class="sub-title">{{subTitle}}</p>' +
            '{{#isExtraTextVisible}}' + 
            '<div id="sidebarSummary_{{pageId}}" class="sidebar-toggable-list-item small-title">' +
            '<label>{{summaryTitle}} <span class="icon-show-list">+</span></label>' +
            '<ul>' +
            '{{#summaryItems}}' +
            '<li>{{{name}}}</li>' +
            '{{/summaryItems}}' +
            '</ul>' +
            '</div>' +
            '{{/isExtraTextVisible}}'  +
            '</div>' +
            '<div id="sidebarMainItems_{{pageId}}" class="container sidebar-main-items">' +
            '<ul>' +
            '{{#mainMenuItems}}' +
            '<li>' +
            '<div class="sidebar-toggable-list-item">' +
            '{{#hasSubItems}}' +
            '{{#itemSelected}}' +
            '<div class="menu-item">' +
            '<object type="image/svg+xml" data="{{{menuOptImgSelectedSvg}}}">' +
            '<img src="{{{menuOptImgSelected}}}">' +
            '</object>' +
            '<label class="selected">{{mainMenuName}} <span class="icon-show-list">+</span></label>' +
            '</div>' +
            '{{/itemSelected}}' +
            '{{^itemSelected}}' +
            '<div class="menu-item">' +
            '<object type="image/svg+xml" data="{{{menuOptImgSvg}}}">' +
            '<img src="{{{menuOptImg}}}">' +
            '</object>' +
            '<label pageId="{{id}}_{{pageId}}" class="{{selectableClass}}">{{mainMenuName}} <span class="icon-show-list">+</span></label>' +
            '<div pageId="{{id}}_{{pageId}}" class="{{selectableClass}} img-selector"></div>'+
            '</div>' +
            '{{/itemSelected}}' +
            '<div class="sub-items-contaner">' +
            '<ul>' +
            '{{#subItems}}' +
            '<li>' +
            '{{#subItemselected}}' +
            '<div class="menu-item">' +
            '<object type="image/svg+xml" data="{{{menuOptImgSelectedSvg}}}">' +
            '<img src="{{{menuOptImgSelected}}}">' +
            '</object>' +
            '<p pageId="{{id}}_{{pageId}}" class="selected {{selectableClass}}">{{subMenuName}}</p>' +
            '<div pageId="{{id}}_{{pageId}}" class="{{selectableClass}} img-selector"></div>'+
            '</div>' +
            '{{/subItemselected}}' +
            '{{^subItemselected}}' +
            '<div class="menu-item">' +
            '<object type="image/svg+xml" data="{{{menuOptImgSvg}}}">' +
            '<img src="{{{menuOptImg}}}">' +
            '</object>' +
            '<p pageId="{{id}}_{{pageId}}" class="{{selectableClass}}">{{subMenuName}}</p>' +
            '<div pageId="{{id}}_{{pageId}}" class="{{selectableClass}} img-selector"></div>'+
            '</div>' +
            '{{/subItemselected}}' +
            '</li>' +
            '{{/subItems}}' +
            '</ul>' +
            '</div>' +
            '{{/hasSubItems}}' +
            '{{^hasSubItems}}' +
            '{{#itemSelected}}' +
            '<div class="menu-item">' +
            '<object class="unit-{{id}} svg-img" type="image/svg+xml" data="{{{menuOptImgSelectedSvg}}}">' +
            '<img src="{{{menuOptImgSelected}}}">' +
            '</object>' +
            '<p pageId="{{id}}_{{pageId}}" class="selected {{selectableClass}}">{{mainMenuName}}</p>' +
            '<div pageId="{{id}}_{{pageId}}" class="{{selectableClass}} img-selector"></div>'+
            '</div>' +
            '{{/itemSelected}}' +
            '{{^itemSelected}}' +
            '<div class="menu-item">' +
            '<object type="image/svg+xml" data="{{{menuOptImgSvg}}}">' +
            '<img src="{{{menuOptImg}}}">' +
            '</object>' +
            '<p pageId="{{id}}_{{pageId}}" class="{{selectableClass}}">{{mainMenuName}}</p>' +
            '<div pageId="{{id}}_{{pageId}}" class="{{selectableClass}} img-selector"></div>'+
            '</div>' +
            '{{/itemSelected}}' +
            '{{/hasSubItems}}' +
            '</div>' +
            '</li>' +
            '{{/mainMenuItems}}' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>';
    },

    getSidebarToggleTemplate: function() {
        return '<object class="sidebar-toggle-img" data="images/sidebar_arrow_right.svg" type="image/svg+xml">'+
            '<img class="sidebar-toggle-img" src="images/sidebar_arrow_right.png"/>'+
            '</object>' +
            '<div class="sidebar-toggle-img-tap"></div>';
    },

    getSelectedSection : function() {
        try {
            return this.model.selectedSection;
        
        } catch (err) {
            console.log("Sidebar Warning: Failed to getSelectedSection " + err);

            return undefined;
        }
    },

    getSelectedSubSection : function () {
        try {
            return this.model.selectedSubSection;

        } catch (err) {
            console.log("Sidebar Warning: Failed to getSelectedSubSection " + err);

            return undefined;
        }
    },

    getSidebar : function() {
        try {
            return $(this.target);

        } catch (err) {
            console.log("Sidebar Warning: Failed to getSidebar " + err);

            return undefined;
        }
    },

    getToggableListItem : function(listItems) {
        try {
            var listItem = undefined;
            
            $(listItems).each(function() {
                listItem = $(this);
            });

            return listItem;

        } catch (err) {
            console.log("Sidebar Warning: Failed to getToggableListItem " + err);

            return undefined;
        }
    },

    /**
     * Events
     */
    lockHide : function() {
        try {
            $(this.target).css(this.HIDDEN_LOCKED_STYLE);
            $(this.target + " " + this.SIDEBAR_BG_SELECTOR).css(this.HIDDEN_LOCKED_STYLE);
            $(this.target + " " + this.SIDEBAR_FILTER_SELECTOR).css(this.HIDDEN_LOCKED_STYLE);
            $(this.target + " " + this.SIDEBAR_SELECTOR).css(this.HIDDEN_LOCKED_STYLE);
            $(this.SIDEBAR_TOGGLE_BTN_SELECTOR).css(this.HIDDEN_LOCKED_STYLE);

        } catch (err) {
            console.log("Sidebar Warning: Failed to lockHide " + err);
        }
    },

    toggleHeightMenuSection : function(listItem) {
        try {
            var sectionHeight = Number($(listItem).parent().css("height").replace("px",""));
        
            if (sectionHeight > 50) {
                $(listItem).parent().removeClass(this.CLASS_EXPAND_SIDEBAR);
                $(listItem).parent().find(this.SIDEBAR_TOGGLE_LIST_ITEM_ICON_SHOW_LIST_SELECTOR).text("+");
            } else {
                $(listItem).parent().addClass(this.CLASS_EXPAND_SIDEBAR);
                $(listItem).parent().find(this.SIDEBAR_TOGGLE_LIST_ITEM_ICON_SHOW_LIST_SELECTOR).text("-");
            }

        } catch (err) {
            console.log("Sidebar Warning: Failed to toggleHeightMenuSection " + err);
        }
    },

    getSidebarItemIdParts : function(selectedItem) {
        try {
            var selectedItemId = $(selectedItem).attr("pageId");
            var selectedItemIdParts = {
                "page" : "",
                "section" : -1,
                "subSection" : -1
            }
            
            var page = "";
            var section = "";
            var subSection = -1;

            if (selectedItemId != undefined) {
                var idParts1 = selectedItemId.split("_");

                if (idParts1 != undefined) {
                    var subSecPage = idParts1[0];
    
                    if (subSecPage != undefined) {
                        var idParts2 = subSecPage.split(".");
        
                        if (selectedItemId.indexOf(".") !== -1) {
                            page = idParts1[1];
                            section = Number(idParts2[0]);
                            subSection = idParts2[1];
                        } else {
                            page = idParts1[1];
                            section = Number(idParts1[0]);
                        }
            
                        selectedItemIdParts.page = page;
                        selectedItemIdParts.section = section;
                        selectedItemIdParts.subSection = subSection;
            
                        return selectedItemIdParts;
                    }
                }
            }

        } catch (err) {
            console.log("Sidebar Warning: Failed to getSidebarItemIdParts " + err);
        }
    },

    saveSelectedIndexes: function (selectedItem) {
        try {
            var idParts = this.getSidebarItemIdParts(selectedItem);

            if (idParts != undefined) {
                this.model.selectedPage = idParts.page;
                this.model.selectedSection = idParts.section;
                this.model.selectedSubSection = idParts.subSection;
            }

        } catch (err) {
            console.log("Sidebar Warning: Failed to saveSelectedIndexes " + err);
        }
    },

    changeFocusSelectedItem: function(selectedItem) {
        try {
            var ctx = this;
            var selectedPageId = selectedItem.replace("#", "");
            var selectableElementsSelectableItems = ' div[pageId="' + selectedPageId + '"], p[pageId="' + selectedPageId + '"]';

            // Deselect all items
            $(this.target + this.SIDEBAR_SELECTED_ITEM_SELECTOR).each(function() {
                $(this).removeClass(ctx.CLASS_SELECTED_SIDEBAR_ITEM);
                $(this).parent().parent().removeClass(ctx.CLASS_SELECTED_SIDEBAR_ITEM);
            });

            // Add the selected class to the selectable elements of the selected item
            $(this.target + selectableElementsSelectableItems).each(function() {
                $(this).addClass(ctx.CLASS_SELECTED_SIDEBAR_ITEM);
                $(this).parent().parent().addClass(ctx.CLASS_SELECTED_SIDEBAR_ITEM);
            });

            // Flip the selected item and ensure that the object and img elements of  
            // all items of the sidebar are going to behave similarly
            var itemNumber = Number(selectedItem.split("_")[0].replace("#", ""));
            var i = 1;

            $(this.SIDEBAR_MENU_OPTION_SELECTOR).parent().find("object").each(function() {
                $(this).removeClass(ctx.CLASS_FLIP);
                $(this).find("img").removeClass(ctx.CLASS_FLIP);

                if (i == itemNumber) {
                    $(this).addClass(ctx.CLASS_FLIP);
                    $(this).find("img").addClass(ctx.CLASS_FLIP);
                }

                i++;
            });

        } catch (err) {
            console.log("Sidebar Warning: Failed to changeFocusSelectedItem " + err);
        }
    },

    selectMenuItem : function(seletectedItem) {
        try {
            var selectedItemId = "#" + $(seletectedItem).attr("pageId");
            
            this.saveSelectedIndexes(seletectedItem);
            this.changeFocusSelectedItem(selectedItemId);

            if (this.model.onItemSelectedCallback != undefined) {
                this.model.onItemSelectedCallback();
            }

        } catch (err) {
            console.log("Sidebar Warning: Failed to selectMenuItem " + err);
        }
    },

    selectMenuItemN : function(numberMenuItem) {
        try {
            var menuItem = $(this.target + this.SIDEBAR_MENU_MAIN_OPTIONS_SELECTOR)[numberMenuItem - 1];
            this.selectMenuItem(menuItem);

        } catch (err) {
            console.log("Sidebar Warning: Failed to selectMenuItemN " + err);
        }
    },

    expandMinifiedSidebar : function() {
        try {
            this.isShrinked = false;

            $(this.SIDEBAR_SELECTOR).addClass(this.CLASS_EXPAND_SIDEBAR);
            $(this.SIDEBAR_SELECTOR).removeClass(this.CLASS_TOGGLE_SIDEBAR_MENU);
            
        } catch(err) {
            console.log("Sidebar Warning: Failed to expandMinifiedSidebar " + err);
        }
    },

    shrinkMinifiedSidebar : function() {
        try {
            this.isShrinked = true;

            $(this.SIDEBAR_SELECTOR).removeClass(this.CLASS_EXPAND_SIDEBAR);
            $(this.SIDEBAR_SELECTOR).addClass(this.CLASS_TOGGLE_SIDEBAR_MENU);

        } catch(err) {
            console.log("Sidebar Warning: Failed to shrinkMinifiedSidebar " + err);
        }
    },

    /**
     * Initialization
     */
    initEvents : function() {
        try {
            var ctx = this;

            // Select a new item on the menu
            $(this.target + this.SIDEBAR_MENU_OPTION_SELECTOR).on("click", function() {
                ctx.selectMenuItem(this);
            });

            // Toggle the height of a section of the menu
            $(this.target + " " + this.SIDEBAR_TOGGLE_LIST_ITEM_SELECTOR).on("click", function() {
                ctx.toggleHeightMenuSection($(this));
            });

        } catch(err) {
            console.log("Sidebar Warning: Failed to initEvents " + err);
        }
    },

    buildSidebarToggle: function() {
        try {
            var template = this.getSidebarToggleTemplate();
            var sidebarToggleHtml = Mustache.to_html(template);
    
            $(this.SIDEBAR_TOGGLE_CONTAINER_SELECTOR).each(function() {
                $(this).html(sidebarToggleHtml);
            });
    
        } catch(err) {
            console.log("Sidebar Warning: Failed to buildSidebarToggle " + err);
        }
    },

    customizeAppearance : function() {
        try {

            if (this.model.isMinified) {
                $(this.SIDEBAR_SELECTOR).addClass(this.CLASS_MINIFIED);
                $(this.SIDEBAR_TOGGABLE_DESCRIPTION).addClass(this.CLASS_HIDDEN);
            } else {
                $(this.SIDEBAR_SELECTOR).removeClass(this.CLASS_MINIFIED);
                $(this.SIDEBAR_TOGGABLE_DESCRIPTION).removeClass(this.CLASS_HIDDEN);
            }

            $(this.SIDEBAR_TOGGLE_BTN_SELECTOR).removeClass(this.CLASS_HIDDEN);

        } catch(err) {
            console.log("Sidebar Warning: Failed to customizeAppearance " + err);
        }
    },

    initAuxiliaryVariables: function(model) {
        try {

            if (model.isMinified) {
                model.isExtraTextVisible = false;
                model.isSidebarToggleBtnVisible = false;
            } else {
                model.isExtraTextVisible = true;
                model.isSidebarToggleBtnVisible = true;
            }
            
        } catch(err) {
            console.log("Sidebar Warning: Failed to initAuxiliaryVariables " + err);
        }
    },

    initMainVariables: function(tgt, model, callback) {
        try {
            this.model = model;
            this.target = tgt;
    
            if (callback != undefined) {
                this.model.onItemSelectedCallback = callback;
            }

        } catch(err) {
            console.log("Sidebar Warning: Failed to initMainVariables " + err);
        }
    },

    injectSidebarCodeIntoTarget: function() {
        try {
            var template = this.getTemplate();
            var sidebarHtml = Mustache.to_html(template, this.model);

            $(this.target).html(sidebarHtml);

        } catch(err) {
            console.log("Sidebar Warning: Failed to injectSidebarCodeIntoTarget " + err);
        }
    },

    initSelectedSidebarItem: function() {
        try {
            var sidebarItemId = $(this.target + this.SIDEBAR_MENU_OPTION_SELECTOR)[0];
            this.selectMenuItem(sidebarItemId);

        } catch(err) {
            console.log("Sidebar Warning: Failed to initSelectedSidebarItem " + err);
        }
    },

    build : function(tgt, model, callback) {
        
        try {

            if (tgt != undefined && model != undefined) {
                this.initAuxiliaryVariables(model);
                this.initMainVariables(tgt, model, callback);
                this.injectSidebarCodeIntoTarget();
                this.buildSidebarToggle();
                this.customizeAppearance();
                this.initEvents();
                this.initSelectedSidebarItem();   
            }

        } catch(err) {
            console.log("Sidebar Warning: Failed to build object " + err);
        }
    },

    /**
     * Destruction
     */
    reset : function() {

        try {
            this.model.pageId = "";
            this.model.title = "";
            this.model.bgFilter = "";
            this.model.unitNumber = "";
            this.model.subTitle = "";
            this.model.summaryTitle = "";
            this.model.summaryItems = [];
            this.model.mainMenuItems = [];
            this.model.onItemSelectedCallback = undefined;

        } catch(err) {
            console.log("Sidebar Warning: Failed to reset object " + err);
        }
    }

}