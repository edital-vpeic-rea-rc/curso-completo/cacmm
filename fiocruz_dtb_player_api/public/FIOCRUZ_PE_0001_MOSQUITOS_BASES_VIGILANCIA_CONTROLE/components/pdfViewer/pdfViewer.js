/**
 * pdfViewer.js displays an embed pdf within a container.
 * The pdf viewer used within the container is the native component provided by the user's browser.
 * The container can be maximized to improve the readability of the content.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

function PdfViewer() {
	this._initialized = false;
}

PdfViewer.prototype = {

    BTN_CLOSE_SELECTOR: " .toolbar-btn.btn-act-close .selector",
    BTN_TOGGLE_EXPAND_SELECTOR: " .toolbar-btn.btn-act-toggle-expand .selector",
    BODY_SELECTOR : "body",
    
    HIDDEN_CLASS: "hidden",
    DISMISS_CLASS: "dismissed",
    SELECTED_CLASS: "selected",

    DOWNLOADABLE_FILE_LINK: " .download-link",

    CSS_BODY_HIDDEN: {
        "overflow-y": "hidden"
    },
    CSS_BODY_SHOWING: {
        "overflow-y": "auto"
    },

    model : {
        title: "",
        fileType: "",
        subTitle: "",
        pdfFileUrl: "",
        initialPageNum: 0,
        urlSvgIcoBtnClose: "",
        urlPngIcoBtnClose: "",
        urlSvgIcoBtnFullScreen: "",
        urlPngIcoBtnFullScreen: ""
    },

    target : undefined,
    targetFsm : undefined,

    getFsmTemplate: function () {
        return '<div class="fsm-header">' +
            '<div class="fsm-pdfv-title">' +
            '<h3>{{{title}}}</h3>' +
            '<small>{{{subTitle}}}</small>' +
            '</div>' +
            '<div class="fsm-pdfv-toolbar">' +
            '<a class="toolbar-btn btn-act-close">' +
            '<object data="{{urlSvgIcoBtnClose}}" type="image/svg+xml" class="toolbar-btn-ico">' +
            '<img src="{{urlPngIcoBtnClose}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-body">' +
            '{{#showPdf}}' +
            '<object class="embed-pdf-viewer" height="100%" width="100%" type="application/pdf" data="{{pdfFileUrl}}#' +
            'view=Fit&'+
            'page={{initialPageNum}}"'+
            '>' +
            '<div class="link-unsupported-case">'+
            '<p>O seu browser não suporta o visualizador de PDF, faça o download clicando</p> '+
            '<a class="download-link"> neste link</a>'+
            '</div>'+
            '</object>'+
            '{{/showPdf}}' +
            '{{^showPdf}}' +
            '<div class="link-unsupported-case">'+
            '<p>O seu browser não suporta o visualizador de PDF, faça o download clicando</p> '+
            '<a class="download-link"> neste link</a>'+
            '</div>'+
            '{{/showPdf}}' +
            '</div>';
    },

    getTemplate: function () {
        return '<div class="fsm-header">' +
            '<div class="fsm-pdfv-title">' +
            '<h3>{{{title}}}</h3>' +
            '<small>{{{subTitle}}}</small>' +
            '</div>' +
            '<div class="fsm-pdfv-toolbar">' +
            '<a class="toolbar-btn btn-act-toggle-expand">' +
            '<object data="{{urlSvgIcoBtnFullScreen}}" type="image/svg+xml" class="toolbar-btn-ico">' +
            '<img src="{{urlPngIcoBtnFullScreen}}">' +
            '</object>' +
            '<div class="selector"></div>' +
            '</a>' +
            '</div>' +
            '</div>' +
            '<div class="fsm-body">' +
            '{{#showPdf}}' +
            '<object class="embed-pdf-viewer" height="100%" width="100%" type="application/pdf" data="{{pdfFileUrl}}#' +
            'view=Fit&'+
            'page={{initialPageNum}}"'+
            '>' +
            '<div class="link-unsupported-case">'+
            '<p>O seu browser não suporta o visualizador de PDF, faça o download clicando</p> '+
            '<a class="download-link"> neste link</a>'+
            '</div>'+
            '</object>'+
            '{{/showPdf}}' +
            '{{^showPdf}}' +
            '<div class="link-unsupported-case">'+
            '<p>O seu browser não suporta o visualizador de PDF, faça o download clicando</p> '+
            '<a class="download-link"> neste link</a>'+
            '</div>'+
            '{{/showPdf}}' +
            '</div>';
    },

    /**
     * Setters and Getters
     */
    setTitle: function(title) {
        this.model.title = title;
    },

    setSubTitle: function(subTitle) {
        this.model.subTitle = subTitle;
    }, 
    
    setPdfUrl: function(pdfUrl) {
        this.model.pdfFileUrl = pdfUrl;
    },

    setInitialPage: function(initialPage) {
        this.model.initialPageNum = initialPage;
    },

    /**
     * Events
     */     
    show: function() {
        gUi.hideHeaderEduRes();
        $(this.targetFsm).removeClass(this.DISMISS_CLASS);
        $(this.targetFsm).removeClass(this.HIDDEN_CLASS);
        $(this.BODY_SELECTOR).css(this.CSS_BODY_HIDDEN);
    },

    hide: function() {
        ctx = this;

        $(this.targetFsm).addClass(this.DISMISS_CLASS);

        setTimeout(function() {
            $(ctx.targetFsm).addClass(ctx.HIDDEN_CLASS);
            $(ctx.BODY_SELECTOR).css(ctx.CSS_BODY_SHOWING);
        }, 500);
    },

    isShowing: function() {
        return !$(this.targetFsm).hasClass(this.HIDDEN_CLASS) && !$(this.targetFsm).hasClass(this.DISMISS_CLASS);
    },

    toggleFullScreenMode: function() {
        try {
            
            if (this.isShowing()) {
                this.hide();
            } else {
                this.show();

                if (this.model.fileType != undefined && this.model.fileType != '' && 
                    this.model.title != undefined && this.model.title != '') {
                    gApiIntegration.setDownload(this.model.fileType, this.model.title);
                }
            }
        } catch (err) {
            console.log("Failed to toggleFullScreenMode");
        }
    },

    downloadFile: function() {
        if (this.isShowing()) {
            $(this.targetFsm + this.DOWNLOADABLE_FILE_LINK).attr({target: '_blank', href  : this.model.pdfFileUrl});
        } else {
            $(this.target + this.DOWNLOADABLE_FILE_LINK).attr({target: '_blank', href  : this.model.pdfFileUrl});
        }        
    },

    initEvents: function() {
        var ctx = this;

        // Close and expand buttons
        $(ctx.target + ctx.BTN_TOGGLE_EXPAND_SELECTOR).on("click", function() {
            ctx.toggleFullScreenMode();
        });

        $(ctx.targetFsm + ctx.BTN_CLOSE_SELECTOR).on("click", function() {
            ctx.hide();
        });

        // Start downloadable file link
        $(ctx.target + ctx.DOWNLOADABLE_FILE_LINK).on("click", function() {
            ctx.downloadFile()
        });

        $(ctx.targetFsm + ctx.DOWNLOADABLE_FILE_LINK).on("click", function() {
            ctx.downloadFile()
        });

    },

    initModal: function() {
        this.hide();
    },

    rebuild : function() {

        if (this.model != undefined && this.target != undefined && this.targetFsm != undefined) {

            if (gUi.isIPad() || gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {
                this.model.showPdf = false;
            } else {
                this.model.showPdf = true;
            }

            // Build the Fsm HTML code
            var viewerTemplateFsm = this.getFsmTemplate();
            var viewerHtmlFsm = Mustache.to_html(viewerTemplateFsm, this.model);
            $(this.targetFsm).html(viewerHtmlFsm);
            
            // Build the regular component
            var viewerTemplate = this.getTemplate();
            var viewerHtml = Mustache.to_html(viewerTemplate, this.model);
            $(this.target).html(viewerHtml);
            
            // Init the events for both parts of the component
            this.initEvents();
        }
    },

    build : function(tgt, tgtFsm, model) {
        this.target = tgt;
        this.targetFsm = tgtFsm;

        if (model != undefined) {
            this._initialized = true;
            this.model = model;

            if (gUi.isIPad() || gUi.isOnTabletViewMode() || gUi.isOnMobileViewMode()) {
                this.model.showPdf = false;
            } else {
                this.model.showPdf = true;
            }

            // Build the Fsm HTML code
            var viewerTemplateFsm = this.getFsmTemplate();
            var viewerHtmlFsm = Mustache.to_html(viewerTemplateFsm, this.model);
            $(this.targetFsm).html(viewerHtmlFsm);
            this.initModal();
            
            // Build the regular component
            var viewerTemplate = this.getTemplate();
            var viewerHtml = Mustache.to_html(viewerTemplate, this.model);
            $(this.target).html(viewerHtml);
            
            // Init the events for both parts of the component
            this.initEvents();
        }
    }

}