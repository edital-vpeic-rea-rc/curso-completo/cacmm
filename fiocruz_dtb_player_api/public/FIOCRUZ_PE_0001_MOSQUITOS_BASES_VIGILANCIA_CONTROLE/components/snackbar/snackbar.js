/**
 * snackBar.js Creates a small information box at the bottom of a component.
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires Mustache
 * @license MIT
 */

var SIMPLE_SNACKBAR_PARAMS = {

    SNACKBAR_POSITION: {
        TOP_LEFT: 0,
        TOP_RIGHT: 1,
        BOTTOM_RIGHT: 2,
        BOTTOM_LEFT: 3
    },
    TYPE: {
        INFORMATION: 0,
        CONFIRMATION: 1
    }

}

function SimpleSnackBar() {
	this._initialized = false;
}

SimpleSnackBar.prototype = {

    DEBUG_SSB : true,

    // Selectors
    SELECTORS : {
        OUTTER_CONTAINER: ".simple-snackbar .ssb-outter-container",
        MSG: ".simple-snackbar .ssb-outter-container .ssb-msg-container",
        ACTION_BUTTON: ".simple-snackbar .ssb-outter-container .ssb-action-container a"
    },

    CLASSES : {
        DISMISSED: "dismissed"
    },

    // Main variables
    target : undefined,
    model : {
        snackbarPosition: -1,
        type: -1,
        msg: "",
        textBtnDismiss: "",
        timeoutDismissMsg: 2000
    },
    
    // Getters and Setters
    getOutterContainer: function() {
        try {
            return this.target + this.SELECTORS.OUTTER_CONTAINER;
        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not getOutterContainer, " + err);
            }
        }
    },

    getMsgObject: function() {
        try {
            return $(this.target + this.SELECTORS.MSG);

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not getMsg, " + err);
            }
        }
    },

    setMsg: function(msg) {
        try {
            $(this.getMsgObject()).html(msg);

        } catch (err) {
            
            if (this.DEBUG_SSB) {
                console.log("Warning: could not setMsg, " + err);
            }
        }

        return "";
    },

    getActionBtnObject: function() {
        try {
            return $(this.target + this.SELECTORS.ACTION_BUTTON);

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not getActionBtnObject, " + err);
            }
        }
    },

    setActionBtnMsg: function(actionMsg) {
        try {

            if (actionMsg.length > 0) {
                $(this.getActionBtnObject()).html(actionMsg);

            } else if (this.DEBUG_SSB) {
                console.log("Warning: the text of this Snackbar action button cannot be empty");
            }

        } catch (err) {
            
            if (this.DEBUG_SSB) {
                console.log("Warning: could not setMsg, " + err);
            }
        }
    },

    setMsgTimeout: function(timeoutDismissMsg) {
        try {

            if (timeoutDismissMsg > 100) {
                this.model.timeoutDismissMsg = timeoutDismissMsg;
            }

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not set a timeout to the dismiss event of this Snackbar, " + err);
            }
        }
    },

    /**
     * Events
     */
    dismiss: function() {
        try {
            $(this.getOutterContainer()).addClass(this.CLASSES.DISMISSED);

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not dismiss this Snackbar, " + err);
            }
        }
    },

    show: function() {
        try {
            var ctx = this;

            // Show the Snackbar
            $(ctx.getOutterContainer()).removeClass(ctx.CLASSES.DISMISSED);

            // Dismiss it after a timeout
            setTimeout(function() {
                ctx.dismiss();
            }, ctx.model.timeoutDismissMsg);

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not show this Snackbar, " + err);
            }
        }
    },

    initEvents: function() {
        var ctx = this;

        $(ctx.target + ctx.SELECTORS.ACTION_BUTTON).on("click", function() {
            ctx.dismiss();
        });

    },

    /**
     * Initialization
     */ 

    initUI: function() {
        try {
            var ctx = this;

            setTimeout(function() {
                $(ctx.target).css("visibility", "visible");
            }, 500);

        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: could not initUI, " + err);
            }
        }
    },

    getTemplateMsgContainer: function () {
        return '<div class="ssb-msg-container">' +
            '{{{msg}}}' +
            '</div>';
    },

    getTemplateActionBtnContainer: function () {
        return '<div class="ssb-action-container">' +
            '<a href="" target="_blank">{{{textBtnDismiss}}}</a>' +
            '</div>';
    },

    getTemplateWithActionBtn: function () {
        return '<div class="ssb-outter-container dismissed">' +
            this.getTemplateMsgContainer() +
            this.getTemplateActionBtnContainer() +
            '</div>';
    },

    getSimpleTemplate: function () {
        return '<div class="ssb-outter-container dismissed">' +
            this.getTemplateMsgContainer() +
            '</div>';
    },

    buildSkeleton: function(tgt, model) {
        try {
            this.target = tgt;
            this.model = model;

            // Select the template
            var template = "";
            
            switch (this.model.type) {

                case SIMPLE_SNACKBAR_PARAMS.TYPE.INFORMATION: {
                    template = this.getSimpleTemplate();
                    break;
                }

                case SIMPLE_SNACKBAR_PARAMS.TYPE.CONFIRMATION: {
                    template = this.getTemplateWithActionBtn();
                    break;
                }

                default: {
                    
                    if (this.DEBUG_SSB) {
                        console.log("Warning: Invalid SimpleSnackBar type " + this.model.type + ", please update this value");
                    }
                }

            }

            // Build the HTML object
            var html = Mustache.to_html(template, this.model);
    
            $(this.target).html(html);

        } catch (err) {

            if (this.DEBUG_SSB) {
                console.log("Warning: Could not buildSkeleton.");
            }
        }
    },

    /**
     * Mandatory parameters:
     * @param tgt: The DOM element that is going to receive the HTML code of the object
     * @param model: The variable that carries the data model of the object
     */
    build : function(tgt, model) {
        try {

            if (tgt != undefined && model != undefined) {
                this._initialized = true;

                this.buildSkeleton(tgt, model);
                this.initUI();
                this.initEvents();
            }
        } catch(err) {

            if (this.DEBUG_SSB) {
                console.log("Failed to build SSB object " + err);
            }
        }
    }

}
