/**
 * about.js handles the UI and Logical events of the courseUnits.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @license MIT
 */

var SELECTED_TOP_MENU_OPTION = 1;

var PAGE_LOADER = "#componentPageLoaderAbout";
var PAGE_CONTAINER_SELECTOR = "#pageContainerAbout";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2About";
var SIDEBAR_SELECTOR = "#componentSidebarAbout";
var FOOTER_SELECTOR = "#componentFooterAbout";

var pageLoaderAbout = new PageLoader();
var headerTop2About = new HeaderTop2();
var sidebarAbout = new Sidebar();
var mainContainer = new MainContainer();
var footerAbout = new Footer();

/**
 * Initialization
 */
$(document).ready(function () {
   gPages.setCurrentPage(gPages.PAGE_ABOUT);
   initUI();
});

$(document).bind('pageinit', function () {
   mainContainer.initMobileEvents();
});

/**
 * UI
 */
function initUI() {
   gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
   gUi.setPageLoader(PAGE_LOADER, pageLoaderAbout, gMsgs.DEFAULT_LOADING_MESSAGE, "", function() {
      $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
      gUi.initEvents();
      initPageEvents();
   });

   initHeaderTop2();
   initSidebar();
   initMainContainer();
   initFooter();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
   try {
      headerTop2About.buildHeaderTop2(
         HEADER_TOP_2_SELECTOR,
         gImages.IMG_COURSE_LOGO,
         gPages.IDS.PAGE_ABOUT_ID,
         gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
         SELECTED_TOP_MENU_OPTION,
         -1
      );

      headerTop2About.initEvents();

   } catch (err) {

      if (unasus.pack.getDebug()) {
         console.log("Failed to init HeaderTop2 " + err);
      }
   }
}

function initSidebar() {
   try {
      var dataModel = sidebarAbout.model;
      dataModel.pageId = gPages.IDS.PAGE_ABOUT_ID;

      sidebarAbout.build(SIDEBAR_SELECTOR, dataModel, undefined);
      sidebarAbout.lockHide();
   } catch (err) {

      if (unasus.pack.getDebug()) {
         console.log("Failed to init Sidebar " + err);
      }
   }
}

function initMainContainer() {
   try {
      mainContainer.build(sidebarAbout);
      mainContainer.initGlogalEvents();
   } catch (err) {

      if (unasus.pack.getDebug()) {
         console.log("Failed to init MainContainer");
      }
   }
}

function initFooter() {
   try {
      gUi.setHasFixedFooter(false);
      footerAbout.useDefaultFooter(true);
      footerAbout.build(FOOTER_SELECTOR, undefined);
   } catch (err) {

      if (unasus.pack.getDebug()) {
         console.log("Failed to init Footer " + err);
      }
   }
}

/**
 * Page events
 */
function initPageEvents() {

   $(window).resize(function() {
      gUi.detectChangeLayoutReload();
   });
  
}