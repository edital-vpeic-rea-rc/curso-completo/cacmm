/**
 * courseUnitDetails1.js handles the UI and Logical events of the courseUnitDetails1.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Fiocruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jQuery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @requires ModalSimpleMessage
 * @requires Paginator
 * @license MIT
 */

// Globals
var TAG = "Cud1";
var UNIT_NUMBER = 1;
var SELECTED_TOP_MENU_OPTION = 4;

var SIDEBAR_MENU_SECTION_IDS = {
    summary: 1,
    problemSituation: 2,
    book: 3,
    material: 4,
    evaluation: 5
}
var SIDEBAR_MENU_SECTION_NAMES = {
    summary: "Síntese",
    problemSituation: "Situação Problema",
    book: "Livro",
    material: "Material Complementar",
    evaluation: "Avaliação"
}

/**
 * Selectors
 */
// Global selectors
var PAGE_LOADER = "#componentPageLoaderCud1";
var PAGE_CONTAINER_SELECTOR = "#pageContainerCud1";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Cud1";
var SIDEBAR_SELECTOR = "#componentSidebarCud1";
var FOOTER_SELECTOR = "#componentFooterCud1";
var PAGE_CONTENT_CONTAINER_SELECTORS = {
    summary: "#summaryContainerCud1",
    problemSituation: "#problemSituationContainerCud1",
    book: "#bookContainerCud1",
    material: "#materialContainerCud1",
    evaluation: "#evaluationContainerCud1"
}

var PAGE_CONTENT_TITLE_SELECTOR = ".page-title h3";
var MODAL_SIMPLE_MESSAGE_SELECTOR = "#componentModalSimpleMessageCud1";
var EVAL_FORM_PAGINATOR_SELECTOR = "#evalFormPaginatorCud1";
var VIDEO_DISPLAYER = "#componentVideoDisplayerCud1";

// Summary selectors
var SUMMARY_SUB_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex1Cud1",
    SUBSEC_2: "#cex2Cud1",
    SUBSEC_3: "#cex3Cud1"
}

// Problem Situation selectors
var FSM_PROBLEM_SITUATION_SELECTOR = "#componentProblemSituationCud1";
var FSM_MODAL_PROBLEM_SITUATION_SELECTOR = "#componentProblemSituationFSMsgCud1";

// Book selectors
var FSM_PDF_VIEWER_SELECTOR = "#componentPdfViewerCud1";
var FSM_PDF_VIEWER_MODAL_SELECTOR = "#componentPdfViewerFSMCud1";

// Complementary Materials selectors
var CM_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex4Cud1",
    SUBSEC_2: "#cex5Cud1",
    SUBSEC_3: "#cex6Cud1",
    SUBSEC_4: "#cex7Cud1",
    SUBSEC_5: "#cex8Cud1"
}
var CM_CS_HELTH_MINISTRY_SELECTOR = "#componentCmCsMinistryHealthCud1";
var CM_CS_RELATED_ARTICLES_SELECTOR = "#componentCmCsRelatedArticlesCud1";
var CM_CS_INFO_GRAPHIC_SELECTOR = "#componentCmCsInfoGraphicsCud1";
var CM_CS_VIDEOS_SELECTOR = "#componentCmVsCud1";
var CM_CS_WEB_CONTENT_SELECTOR = "#componentCmWebContentCud1";

// Evaluation selectors
var EVALUATION_FORM_SELECTOR = {
    PAGE_1: "#evalFormCud1Page1",
    PAGE_2: "#evalFormCud1Page2",
    PAGE_3: "#evalFormCud1Page3",
    PAGE_4: "#evalFormCud1Page4",
    PAGE_5: "#evalFormCud1Page5",
    PAGE_6: "#evalFormCud1Page6",
    PAGE_7: "#evalFormCud1Page7",
    PAGE_8: "#evalFormCud1Page8",
    PAGE_9: "#evalFormCud1Page9",
    PAGE_10: "#evalFormCud1Page10",
}
var PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM = "models_question_";
var PREFIX_PAGE_EVAL_FORM = "q_";

/**
 * Page objects
 */
// Global
var previousUIMode = undefined;

// Page components
var pageLoaderCud1 = new PageLoader();
var headerTop2Cud1 = new HeaderTop2();
var sidebarCud1 = new Sidebar();
var mainContainer = new MainContainer();
var footerCud1 = new Footer();
var modalSimpleMessageCud1 = new ModalSimpleMessage();

// Summary global items
var summaryModuleOverview = undefined;
var biologicCharacteristics = undefined;
var influenceFemaleNests = undefined;

// Problem situation items
var problemSituationImgViewerCud1 = undefined;

// Book section item
var pdfViewerCud1 = new PdfViewer();

// Complementary Materials
var ministryHealthSubSec = undefined;
var complMatRelatedArticlesCud1 = undefined;
var relatedArticlesSubSec = undefined;
var complMatInfoGraphicsCud1 = undefined;
var complMatVideosCud1 = undefined;
var videosSubSec = undefined;
var complMatWebContentCud1 = undefined;
var webContentSubSec = undefined;

// Evaluation
var EVAL_FORM_SET_SIZE = 10;
var EVAL_FORM_NUM_QUESTIONS_PER_SET = 2;

var evalFormPaginatorCud1 = undefined;
var selectedEvaluationPage = 1;
var previouslySelectedQuestions = {
    "q_1" : 0,
    "q_2" : 0,
    "q_3" : 0,
    "q_4" : 0,
    "q_5" : 0,
    "q_6" : 0,
    "q_7" : 0,
    "q_8" : 0,
    "q_9" : 0,
    "q_10" : 0
}
var question1Obj = new SingleAnswerQuestion();
var question2Obj = new SingleAnswerQuestion();
var question3Obj = new SingleAnswerQuestion();
var question4Obj = new SingleAnswerQuestion();
var question5Obj = new SingleAnswerQuestion();
var question6Obj = new SingleAnswerQuestion();
var question7Obj = new SingleAnswerQuestion();
var question8Obj = new SingleAnswerQuestion();
var question9Obj = new SingleAnswerQuestion();
var question10Obj = new SingleAnswerQuestion();
var modelsQuestions = {
    "models_question_1" : undefined,
    "models_question_2" : undefined,
    "models_question_3" : undefined,
    "models_question_4" : undefined,
    "models_question_5" : undefined,
    "models_question_6" : undefined,
    "models_question_7" : undefined,
    "models_question_8" : undefined,
    "models_question_9" : undefined,
    "models_question_10" : undefined
}

/**
 * Initialization
 */
$(document).ready(function () {
    gUi.initEvents();
    gPages.setCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_1);
    previousUIMode = gUi.isOnMobileViewMode();

    initData();
    initUI();
    initEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * Data
 */
function initData() {
    gData.loadUserCourse();
}

/**
 * UI
 */
function initUI() {
    // Init the UI of the base elements
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderCud1, gMsgs.DEFAULT_LOADING_MESSAGE, "", function () {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
        initPageSections();
        gUi.finishUiInit();
    });

    // Init the UI of the main components of the page
    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
    initPassedUnitModal();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Cud1.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_UNIT_DETAILS_1_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            1);

        headerTop2Cud1.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarCud1.model;
        dataModel.pageId = gPages.IDS.PAGE_COURSE_UNIT_DETAILS_1_ID;
        dataModel.title = "MÓDULO 1";
        dataModel.bgFilter = gUi.BLUE_FILTER;
        dataModel.unitNumber = "1";
        dataModel.subTitle = "Características biológicas, ecológicas e comportamentais";
        dataModel.summaryTitle = "Objetivos da unidade";
        dataModel.summaryItems = [
            {
                name: '<b>Objetivos</b>: Descrever os objetivos da unidade ....'
            },
            {
                name: '<b>Carga Horaria</b>: <span class="course-work-load">' + 10 + '</span> horas/aula'
            },
            {
                name: '<b>Recursos Educacionais</b>: texto de apoio produzido para o curso a partir de documentos do MS; vídeos com especialistas sobre o tema; biblioteca virtual.'
            },
            {
                name: '<b>Avaliação</b>: Atividades objetivas.'
            }
        ];
        dataModel.mainMenuItems = [
            {
                id: SIDEBAR_MENU_SECTION_IDS.summary,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.summary,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_SUMMARY, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_SUMMARY,
                hasSubItems: false,
                itemSelected: true,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.problemSituation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.problemSituation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.book,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.book,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_BOOK, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_BOOK, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_BOOK,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_BOOK,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.material,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.material,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.evaluation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.evaluation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_EVALUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_EVALUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            }
        ];
        dataModel.onItemSelectedCallback = onSidebarItemSelected;
        dataModel.isMinified = true;

        sidebarCud1.build(SIDEBAR_SELECTOR, dataModel, onSidebarItemSelected);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarCud1);
        mainContainer.initGlogalEvents();
        mainContainer.setSidebarToggleEventCallback(onSidebarToggle);

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        } else {
            mainContainer.expandSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init MainContainer " + err);
        }
    }
}

function onSidebarToggle(sidebarStatus) {
    // ..
}

function onSidebarItemSelected() {
    try {
        $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.book).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.material).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).addClass(gUi.CLASS_HIDDEN);

        resetSummaryPageSection();
        resetProblemSituationPageSection();
        resetBookPageSection();
        resetComplementaryMaterialsPageSection();        

        switch (sidebarCud1.getSelectedSection()) {

            case SIDEBAR_MENU_SECTION_IDS.summary: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.summary);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).removeClass(gUi.CLASS_HIDDEN);

                initSummaryPageSection();
                

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.problemSituation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.problemSituation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).removeClass(gUi.CLASS_HIDDEN);

                initProblemSituationPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.book: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.book);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.book).removeClass(gUi.CLASS_HIDDEN);

                pdfViewerCud1.setTitle("Capítulo da Unidade 1");
                pdfViewerCud1.setSubTitle(gFiles.PDF_COURSE_BOOK.TITLE);
                pdfViewerCud1.setPdfUrl(gFiles.PDF_COURSE_BOOK.URL);
                pdfViewerCud1.setInitialPage(1);
                pdfViewerCud1.rebuild();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.material: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.material);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.material).removeClass(gUi.CLASS_HIDDEN);

                initComplementaryMaterialsPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.evaluation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.evaluation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).removeClass(gUi.CLASS_HIDDEN);

                if (typeof (modalSimpleMessageCud1 !== "undefined") && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud1.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }

                break;
            }
        }

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to select Sidebar menu option " + err);
        }
    }
}

function initFooter() {
    try {
        gUi.setHasFixedFooter(false);
        footerCud1.useDefaultFooter(true);
        footerCud1.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Footer " + err);
        }
    }
}

function initPassedUnitModal() {
    var model = {
        img: gImages.ICO_GIF_UNIT_1_SUCCESS,
        title: gMsgs.MSG_CONGRATS,
        msg: gMsgs.MSG_FINISHED_UNIT_N + " " + UNIT_NUMBER,
        msgSupport: "",
        unitNumber: UNIT_NUMBER
    }
    modalSimpleMessageCud1.build(MODAL_SIMPLE_MESSAGE_SELECTOR, model);
}

/**
 * Page Sections
 */
function initPageSections() {
    resetSummaryPageSection();
    resetProblemSituationPageSection();
    resetBookPageSection();
    resetComplementaryMaterialsPageSection();

    initSummaryPageSection();
    initBookPageSection();
    initEvaluationSection();
}

/**
 * Page section: Summary
 */
function resetSummaryPageSection() {
    try {

        if (summaryModuleOverview != null && summaryModuleOverview != undefined) {
            summaryModuleOverview.destroy();
            summaryModuleOverview = null;

            biologicCharacteristics.destroy();
            biologicCharacteristics = null;

            influenceFemaleNests.destroy();
            influenceFemaleNests = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetSummaryPageSection " + err);
    }
}

function initSummaryPageSection() {
    // Subsection 1
    summaryModuleOverview = new ContentExpander();
    summaryModuleOverview.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Os mosquitos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 2
    biologicCharacteristics = new ContentExpander();
    biologicCharacteristics.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Características biológicas e ciclo de desenvolvimento",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    }, function () {
        console.log(TAG + "Callback method for the subsection 2 of the summary.");
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 3
    influenceFemaleNests = new ContentExpander();
    influenceFemaleNests.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_3, {
        title: "Aspectos que podem influenciar as fêmeas na escolha de criadouros",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    }, function () {
        console.log(TAG + "Callback method for the subsection 3 of the summary.");
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_3).removeClass(gUi.CLASS_HIDDEN);
}

/**
 * Page section: Problem situation
 */
function resetProblemSituationPageSection() {
    try {

        if (problemSituationImgViewerCud1 == null && problemSituationImgViewerCud1 != undefined) {
            problemSituationImgViewerCud1.destroy();
            problemSituationImgViewerCud1 = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetProblemSituationPageSection " + err);
    }
}

function initProblemSituationPageSection() {
    var PREFIX_PROBLEM_SITUATION_IMG = "images/unit_1_problem_situation_slide_";

    var model = {
        title: "Ainda tem larvas aqui? - Parte 1",
        subTitle: "<b>Objetivos:</b><br><ul><li>Conscientizar sobre a importância do trabalho dos agentes de endemias.</li><li>Dar noções sobre criadouros e resistência de larvas de mosquitos a inseticidas.</li><li>Fortalecer a importância da tríade serviço de saúde, pesquisa e população nas ações de redução populacional dos mosquitos no ambiente.</li></ul>",
        selectedSlidePos: 0,
        urlSelectedImgPng: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
        urlSelectedImgSvg: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
        urlSvgIcoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
        urlPngIcoBtnClose: gImages.ICO_PNG_CLOSE_BLACK,
        urlSvgIcoBtnFullScreen: gImages.ICO_SVG_FULL_SCREEN_BLACK,
        urlPngIcoBtnFullScreen: gImages.ICO_PNG_FULL_SCREEN_BLACK,
        urlSvgPrevBtn: gImages.ICO_SVG_ARROW_RIGHT_BLACK,
        urlPngPrevBtn: gImages.ICO_PNG_ARROW_RIGHT_BLACK,
        urlSvgNextBtn: gImages.ICO_SVG_ARROW_RIGHT_BLACK,
        urlPngNextBtn: gImages.ICO_PNG_ARROW_RIGHT_BLACK,
        images: [
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
                selected: "selected",
                slidePos: 0
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "2.png",
                selected: "",
                slidePos: 1
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "3.png",
                selected: "",
                slidePos: 2
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "4.png",
                selected: "",
                slidePos: 3
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "5.png",
                selected: "",
                slidePos: 4
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "6.png",
                selected: "",
                slidePos: 5
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "7.png",
                selected: "",
                slidePos: 6
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "8.png",
                selected: "",
                slidePos: 7
            }
        ]
    };

    problemSituationImgViewerCud1 = new ImgsViewer();
    problemSituationImgViewerCud1.build(FSM_PROBLEM_SITUATION_SELECTOR, FSM_MODAL_PROBLEM_SITUATION_SELECTOR, model);
}

/**
 * Page section: Book
 */
function resetBookPageSection() {
    try {

        if (pdfViewerCud1 != null && pdfViewerCud1 != undefined) {
            pdfViewerCud1.setTitle("Capítulo da Unidade 1");
            pdfViewerCud1.setSubTitle(gFiles.PDF_COURSE_BOOK_COVER.TITLE);
            pdfViewerCud1.setPdfUrl(gFiles.PDF_COURSE_BOOK_COVER.URL);
            pdfViewerCud1.setInitialPage(1);
            pdfViewerCud1.rebuild();
        }
    } catch(err) {
        console.log(TAG + " Failed to resetBookPageSection " + err);
    }
}

function initBookPageSection() {
    var model = {
        title: "Capítulo da Unidade 1",
        subTitle: gFiles.PDF_COURSE_BOOK.SUB_TITLE,
        pdfFileUrl: gFiles.PDF_COURSE_BOOK.URL,
        fileType: gFiles.FILE_TYPE.TEXT,
        initialPageNum: 1,
        urlSvgIcoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
        urlPngIcoBtnClose: gImages.ICO_PNG_CLOSE_BLACK,
        urlSvgIcoBtnFullScreen: gImages.ICO_SVG_FULL_SCREEN_BLACK,
        urlPngIcoBtnFullScreen: gImages.ICO_PNG_FULL_SCREEN_BLACK
    }

    pdfViewerCud1.build(FSM_PDF_VIEWER_SELECTOR, FSM_PDF_VIEWER_MODAL_SELECTOR, model);
}

/**
 * Page section: Complementary materials
 */
function resetComplementaryMaterialsPageSection() {
    try {

        if (ministryHealthSubSec != null && ministryHealthSubSec != undefined) {
            ministryHealthSubSec.destroy();
            ministryHealthSubSec = null;

            complMatRelatedArticlesCud1.destroy();
            complMatRelatedArticlesCud1 = null;

            relatedArticlesSubSec.destroy();
            relatedArticlesSubSec = null;

            complMatInfoGraphicsCud1.destroy();
            complMatInfoGraphicsCud1 = null;

            infoGraphicsSubSec.destroy();
            infoGraphicsSubSec = null;

            complMatVideosCud1.destroy();
            complMatVideosCud1 = null;

            videosSubSec.destroy();
            videosSubSec = null;

            complMatWebContentCud1.destroy();
            complMatWebContentCud1 = null;

            webContentSubSec.destroy();
            webContentSubSec = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetComplementaryMaterialsPageSection " + err);
    }
}

function initComplementaryMaterialsPageSection() {
    // Subsection 1 - Materials from the Ministry of Health
    var modelMinistryHealthMaterials = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gFiles.PDF_GUIA_VIGILANCIA.TITLE,
                author: gFiles.PDF_GUIA_VIGILANCIA.SUB_TITLE,
                fileUrl: gFiles.PDF_GUIA_VIGILANCIA.URL,
                internetUrl: gFiles.PDF_GUIA_VIGILANCIA.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: gImages.ICO_SVG_MS_BLACK,
                publisherIconPng: gImages.ICO_PNG_MS_BLACK,
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    };

    complMatHealthMinistryCud1 = new ContentSelector();
    complMatHealthMinistryCud1.build(CM_CS_HELTH_MINISTRY_SELECTOR, onReadableObjectSelected, modelMinistryHealthMaterials);

    ministryHealthSubSec = new ContentExpander();
    ministryHealthSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Materiais do Ministério da Saúde",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_1).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 2 - Related Articles
    var modelRelatedArticles = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gFiles.PDF_ART_REVISTA_SAUDE_PUBLICA.TITLE,
                author: gFiles.PDF_ART_REVISTA_SAUDE_PUBLICA.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_REVISTA_SAUDE_PUBLICA.URL,
                internetUrl: gFiles.PDF_ART_REVISTA_SAUDE_PUBLICA.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 1,
                title: gFiles.PDF_ART_PRINCIPAIS_MOSQUITOS_IMPORTANCIA_SANITARIA_BRASIL.TITLE,
                author: gFiles.PDF_ART_PRINCIPAIS_MOSQUITOS_IMPORTANCIA_SANITARIA_BRASIL.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_PRINCIPAIS_MOSQUITOS_IMPORTANCIA_SANITARIA_BRASIL.URL,
                internetUrl: gFiles.PDF_ART_PRINCIPAIS_MOSQUITOS_IMPORTANCIA_SANITARIA_BRASIL.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 2,
                title: gFiles.PDF_ART_LIST_MOSQUITOS_BRAZILIAN_STATE_PERNAMBUCO.TITLE,
                author: gFiles.PDF_ART_LIST_MOSQUITOS_BRAZILIAN_STATE_PERNAMBUCO.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_LIST_MOSQUITOS_BRAZILIAN_STATE_PERNAMBUCO.URL,
                internetUrl: gFiles.PDF_ART_LIST_MOSQUITOS_BRAZILIAN_STATE_PERNAMBUCO.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 3,
                title: gFiles.PDF_ART_CULICIDAE_FAUNA.TITLE,
                author: gFiles.PDF_ART_CULICIDAE_FAUNA.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_CULICIDAE_FAUNA.URL,
                internetUrl: gFiles.PDF_ART_CULICIDAE_FAUNA.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatRelatedArticlesCud1 = new ContentSelector();
    complMatRelatedArticlesCud1.build(CM_CS_RELATED_ARTICLES_SELECTOR, onReadableObjectSelected, modelRelatedArticles);

    relatedArticlesSubSec = new ContentExpander();
    relatedArticlesSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Artigos Relacionados",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_2).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 3 - Info Graphics
    var modelInfoGraphics = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gFiles.PDF_INFO_GRAF_PLACEHOLDER.TITLE,
                author: gFiles.PDF_INFO_GRAF_PLACEHOLDER.SUB_TITLE,
                fileUrl: gFiles.PDF_INFO_GRAF_PLACEHOLDER.URL,
                internetUrl: gFiles.PDF_INFO_GRAF_PLACEHOLDER.EXT_URL,
                fileType: gFiles.FILE_TYPE.OTHER,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_INFOGRAPHIC_WHITE,
                icoReadPng: gImages.ICO_PNG_INFOGRAPHIC_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatInfoGraphicsCud1 = new ContentSelector();
    complMatInfoGraphicsCud1.build(CM_CS_INFO_GRAPHIC_SELECTOR, onReadableObjectSelected, modelInfoGraphics);

    infoGraphicsSubSec = new ContentExpander();
    infoGraphicsSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_3, {
        title: "Infográficos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 4 - Videos
    var modelVideos = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        videosList: [
            {
                id: 0,
                title: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.TITLE,
                subTitle: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.SUB_TITLE,
                fileUrl: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.URL,
                internetUrl: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.EXT_URL,
                videoResolution: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.RESOLUTION,
                coverImg: gFiles.VIDEO_CONHECENDO_AEDES_TRANSMISSAO_ARBOVIRUS.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_DOWNLOAD_WHITE,
                imgPngVideoSrc: gImages.ICO_PNG_DOWNLOAD_WHITE,
                videoSrcText: "Disponível para download",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 1,
                title: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.TITLE,
                subTitle: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.SUB_TITLE,
                fileUrl: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.URL,
                internetUrl: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.EXT_URL,
                videoResolution: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.RESOLUTION,
                coverImg: gFiles.VIDEO_AEDES_E_ALBOPICTUS_AMEACA_TROPICOS.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 2,
                title: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.TITLE,
                subTitle: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.SUB_TITLE,
                fileUrl: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.URL,
                internetUrl: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.EXT_URL,
                videoResolution: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.RESOLUTION,
                coverImg: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            }
        ]
    }

    complMatVideosCud1 = new VideoSelector();
    complMatVideosCud1.build(CM_CS_VIDEOS_SELECTOR, onVideoObjectSelected, modelVideos);

    videosSubSec = new ContentExpander();
    videosSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_4, {
        title: "Vídeos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_4).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 5 - Web content
    var modelWebContent = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gExtResources.EXT_RESOURCE_PREVENCAO_COMBATE_DENGUE_CHIK_ZIKA_PMS.TITLE,
                author: gExtResources.EXT_RESOURCE_PREVENCAO_COMBATE_DENGUE_CHIK_ZIKA_PMS.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_PREVENCAO_COMBATE_DENGUE_CHIK_ZIKA_PMS.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 1,
                title: gExtResources.EXT_RESOURCE_MOSQUITOS_AMEACA_AR.TITLE,
                author: gExtResources.EXT_RESOURCE_MOSQUITOS_AMEACA_AR.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_MOSQUITOS_AMEACA_AR.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 2,
                title: gExtResources.EXT_RESOURCE_ANIMAIS_PERIGOSOS.TITLE,
                author: gExtResources.EXT_RESOURCE_ANIMAIS_PERIGOSOS.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_ANIMAIS_PERIGOSOS.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 3,
                title: gExtResources.EXT_RESOURCE_TRANSMISSAO_ZIKA_PERNILONGOS.TITLE,
                author: gExtResources.EXT_RESOURCE_TRANSMISSAO_ZIKA_PERNILONGOS.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_TRANSMISSAO_ZIKA_PERNILONGOS.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 4,
                title: gExtResources.EXT_RESOURCE_AEDES_VS_CULEX.TITLE,
                author: gExtResources.EXT_RESOURCE_AEDES_VS_CULEX.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_AEDES_VS_CULEX.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatWebContentCud1 = new ContentSelector();
    complMatWebContentCud1.build(CM_CS_WEB_CONTENT_SELECTOR, onWebObjectSelected, modelWebContent);

    webContentSubSec = new ContentExpander();
    webContentSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_5, {
        title: "Para Acessar na Web",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_5).removeClass(gUi.CLASS_HIDDEN);
}

function onReadableObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {

    if (fileUrl != undefined && fileUrl !== '') {
        pdfViewerCud1.setTitle("Materiais Complementares");

        if (title !== undefined) {
            pdfViewerCud1.setSubTitle(title);
        }

        if (fileUrl != undefined) {
            pdfViewerCud1.setPdfUrl(fileUrl);
        }

        if (initialPage != undefined) {
            pdfViewerCud1.setInitialPage(initialPage);
        }

        pdfViewerCud1.rebuild();
        pdfViewerCud1.show();
    }

    if (internetUrl != undefined && internetUrl !== '') {
        gData.onWebObjectSelected(internetUrl);
    }

    if (fileType != undefined && fileType != '' && title != undefined && title != '' && internetUrl === '') {
        gApiIntegration.setDownload(fileType, title);
    }
}

function onVideoObjectSelected(fileUrl, internetUrl, title, subTitle, videoResolution) {
    try {
        var videoContainerWidth = "100%";
        var selectedUrl = '';
        var localVideo = false;

        if (fileUrl != undefined && fileUrl !== '') {
            selectedUrl = fileUrl;
            localVideo = true;

            // Update the url of the video displayer and show it
            var complMatModalVideoCud1 = new ModalSimpleMessage();

            var model = {
                title: title,
                msg: subTitle,
                localVideo: localVideo,
                unitNumber: 1,
                videoContainerSize: videoContainerWidth,
                videoUrl: selectedUrl,
                videoResolution: videoResolution,
                msgSupport: '',
                videoSupportMsg: gMsgs.BROWSER_DOESNT_SUPPORT_THIS_VIDEO
            }

            complMatModalVideoCud1.build(VIDEO_DISPLAYER, model);
            complMatModalVideoCud1.show();

        } else if (internetUrl != undefined && internetUrl !== '') {
            gApiIntegration.setExternalLink(internetUrl);

            var win = window.open(internetUrl, '_blank');
            win.focus();

            console.log("internetUrl: " + internetUrl);

        }
    } catch (err) {
        console.log(TAG + " Failed to display video " + err);
    }
}

function onWebObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {
    gData.onWebObjectSelected(internetUrl);
}

/**
 * Page section: Evaluation
 */
function initEvaluationSection() {
    setTimeout(function() {
        // Build the questions' models randomly
        buildQuestionsModels();

        selectQuestionRandomly(1);
        selectQuestionRandomly(2);
        selectQuestionRandomly(3);
        selectQuestionRandomly(4);
        selectQuestionRandomly(5);
        selectQuestionRandomly(6);
        selectQuestionRandomly(7);
        selectQuestionRandomly(8);
        selectQuestionRandomly(9);
        selectQuestionRandomly(10);

        // Build the UI of the evaluation form
        evalFormPaginatorCud1 = new Paginator();
        evalFormPaginatorCud1.build(EVAL_FORM_PAGINATOR_SELECTOR, onEvaluationPageSelected);

    }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE / 2);
}

function buildQuestionsModels() {
    try {
        var Questions = jQuery.extend({}, new Question());
        var unit1 = gData.getCourse().units[0];

        // Build the questions of the evaluation form
        // First set of questions
        // Question 1
        var question1Model = unit1.unitQuestions[0];
        question1Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question1Model.staticAlternatives);
        var modelQuestion1 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question1Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 1,
            questionStatus: question1Model.questionStatus,
            enunciate: question1Model.enunciate,
            rightAlternative: question1Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question1Model.alternatives,
            staticAlternatives: question1Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 2
        var question2Model = unit1.unitQuestions[1];
        question2Model.alternatives = Questions.getAlphabetLetterForAlternatives(question2Model.alternatives, true);
        var modelQuestion2 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question2Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 2,
            questionStatus: question2Model.questionStatus,
            enunciate: question2Model.enunciate,
            rightAlternative: question2Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question2Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 3
        var question3Model = unit1.unitQuestions[2];
        question3Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question3Model.staticAlternatives);
        var modelQuestion3 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question3Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 3,
            questionStatus: question3Model.questionStatus,
            enunciate: question3Model.enunciate,
            rightAlternative: question3Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question3Model.alternatives,
            staticAlternatives: question3Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 4
        var question4Model = unit1.unitQuestions[3];
        question4Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question4Model.staticAlternatives);
        var modelQuestion4 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question4Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 4,
            questionStatus: question4Model.questionStatus,
            enunciate: question4Model.enunciate,
            rightAlternative: question4Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question4Model.alternatives,
            staticAlternatives: question4Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }
        
        // Question 5
        var question5Model = unit1.unitQuestions[4];
        question5Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question5Model.staticAlternatives);
        var modelQuestion5 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question5Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 5,
            questionStatus: question5Model.questionStatus,
            enunciate: question5Model.enunciate,
            rightAlternative: question5Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question5Model.alternatives,
            staticAlternatives: question5Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 6
        var question6Model = unit1.unitQuestions[5];
        question6Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question6Model.staticAlternatives);
        var modelQuestion6 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question6Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 6,
            questionStatus: question6Model.questionStatus,
            enunciate: question6Model.enunciate,
            rightAlternative: question6Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question6Model.alternatives,
            staticAlternatives: question6Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 7
        var question7Model = unit1.unitQuestions[6];
        question7Model.alternatives = Questions.getAlphabetLetterForAlternatives(question7Model.alternatives, true);
        var modelQuestion7 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question7Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 7,
            questionStatus: question7Model.questionStatus,
            enunciate: question7Model.enunciate,
            rightAlternative: question7Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question7Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 8
        var question8Model = unit1.unitQuestions[7];
        question8Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question8Model.staticAlternatives);
        var modelQuestion8 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question8Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 8,
            questionStatus: question8Model.questionStatus,
            enunciate: question8Model.enunciate,
            rightAlternative: question8Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question8Model.alternatives,
            staticAlternatives: question8Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 9
        var question9Model = unit1.unitQuestions[8];
        question9Model.alternatives = Questions.getAlphabetLetterForAlternatives(question9Model.alternatives, true);
        var modelQuestion9 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question9Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 9,
            questionStatus: question9Model.questionStatus,
            enunciate: question9Model.enunciate,
            rightAlternative: question9Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question9Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }
        
        // Question 10
        var question10Model = unit1.unitQuestions[9];
        question10Model.alternatives = Questions.getAlphabetLetterForAlternatives(question10Model.alternatives, true);
        var modelQuestion10 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question10Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 10,
            questionStatus: question10Model.questionStatus,
            enunciate: question10Model.enunciate,
            rightAlternative: question10Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question10Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Second set of questions
        // Question 11
        var question11Model = unit1.unitQuestions[10];
        question11Model.alternatives = Questions.getAlphabetLetterForAlternatives(question11Model.alternatives, true);
        var modelQuestion11 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question11Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 1,
            questionStatus: question11Model.questionStatus,
            enunciate: question11Model.enunciate,
            rightAlternative: question11Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question11Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 12
        var question12Model = unit1.unitQuestions[11];
        question12Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question12Model.staticAlternatives);
        var modelQuestion12 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question12Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 2,
            questionStatus: question12Model.questionStatus,
            enunciate: question12Model.enunciate,
            rightAlternative: question12Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question12Model.alternatives,
            staticAlternatives: question12Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 13
        var question13Model = unit1.unitQuestions[12];
        question13Model.alternatives = Questions.getAlphabetLetterForAlternatives(question13Model.alternatives, true);
        var modelQuestion13 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question13Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 3,
            questionStatus: question13Model.questionStatus,
            enunciate: question13Model.enunciate,
            rightAlternative: question13Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question13Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 14
        var question14Model = unit1.unitQuestions[13];
        question14Model.alternatives = Questions.getAlphabetLetterForAlternatives(question14Model.alternatives, true);
        var modelQuestion14 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question14Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 4,
            questionStatus: question14Model.questionStatus,
            enunciate: question14Model.enunciate,
            rightAlternative: question14Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question14Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }
        
        // Question 15
        var question15Model = unit1.unitQuestions[14];
        question15Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question15Model.staticAlternatives);
        var modelQuestion15 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question15Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 5,
            questionStatus: question15Model.questionStatus,
            enunciate: question15Model.enunciate,
            rightAlternative: question15Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question15Model.alternatives,
            staticAlternatives: question15Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 16
        var question16Model = unit1.unitQuestions[15];
        question16Model.alternatives = Questions.getAlphabetLetterForAlternatives(question16Model.alternatives, true);
        var modelQuestion16 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question16Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 6,
            questionStatus: question16Model.questionStatus,
            enunciate: question16Model.enunciate,
            rightAlternative: question16Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question16Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 17
        var question17Model = unit1.unitQuestions[16];
        question17Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question17Model.staticAlternatives);
        var modelQuestion17 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question17Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 7,
            questionStatus: question17Model.questionStatus,
            enunciate: question17Model.enunciate,
            rightAlternative: question17Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question17Model.alternatives,
            staticAlternatives: question17Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 18
        var question18Model = unit1.unitQuestions[17];
        question18Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question18Model.staticAlternatives);
        var modelQuestion18 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question18Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 8,
            questionStatus: question18Model.questionStatus,
            enunciate: question18Model.enunciate,
            rightAlternative: question18Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question18Model.alternatives,
            staticAlternatives: question18Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 19
        var question19Model = unit1.unitQuestions[18];
        question19Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question19Model.staticAlternatives);
        var modelQuestion19 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question19Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 9,
            questionStatus: question19Model.questionStatus,
            enunciate: question19Model.enunciate,
            rightAlternative: question19Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question19Model.alternatives,
            staticAlternatives: question19Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 20
        var question20Model = unit1.unitQuestions[19];
        question20Model.alternatives = Questions.getAlphabetLetterForAlternatives(question20Model.alternatives, true);
        var modelQuestion20 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit1.unitId,
            questionId: question20Model.questionId,
            unitNumber: unit1.unitId,
            questionNumber: 10,
            questionStatus: question20Model.questionStatus,
            enunciate: question20Model.enunciate,
            rightAlternative: question20Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question20Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Build randomly the questions of the evaludation form
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "1"] = [modelQuestion1, modelQuestion11];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "2"] = [modelQuestion2, modelQuestion12];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "3"] = [modelQuestion3, modelQuestion13];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "4"] = [modelQuestion4, modelQuestion14];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "5"] = [modelQuestion5, modelQuestion15];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "6"] = [modelQuestion6, modelQuestion16];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "7"] = [modelQuestion7, modelQuestion17];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "8"] = [modelQuestion8, modelQuestion18];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "9"] = [modelQuestion9, modelQuestion19];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "10"] = [modelQuestion10, modelQuestion20];

        // Ensure that if one question of the possible questions of a page of the evaluation form
        // has the status "right", all the other questions of that same page will have the same status
        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelsQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];
            var statusQuestionNRigth = false;

            for (var j = 0; j < modelsQuestion.length; j++) {

                if (modelsQuestion[j].questionStatus == gData.QUESTION_STATUS.RIGHT) {
                    statusQuestionNRigth = true;
                }
            }

            if (statusQuestionNRigth) {
                
                for (var j = 0; j < modelsQuestion.length; j++) {
                    modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i][j].questionStatus = gData.QUESTION_STATUS.RIGHT;
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to buildQuestionsModels " + err);
    }
}

function selectQuestionRandomly(questionNumber) {
    try {
        var modelsSelectedQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + questionNumber];
        var selectedModel = modelsSelectedQuestion[getRandomNumberForQuestion(questionNumber)];
        
        switch (questionNumber) {

            case 1:
                question1Obj.build(EVALUATION_FORM_SELECTOR.PAGE_1, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
    
            case 2:
                question2Obj.build(EVALUATION_FORM_SELECTOR.PAGE_2, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
            
            case 3:
                question3Obj.build(EVALUATION_FORM_SELECTOR.PAGE_3, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
            
            case 4:
                question4Obj.build(EVALUATION_FORM_SELECTOR.PAGE_4, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
        
            case 5:
                question5Obj.build(EVALUATION_FORM_SELECTOR.PAGE_5, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 6:
                question6Obj.build(EVALUATION_FORM_SELECTOR.PAGE_6, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 7:
                question7Obj.build(EVALUATION_FORM_SELECTOR.PAGE_7, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 8:
                question8Obj.build(EVALUATION_FORM_SELECTOR.PAGE_8, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 9:
                question9Obj.build(EVALUATION_FORM_SELECTOR.PAGE_9, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 10:
                question10Obj.build(EVALUATION_FORM_SELECTOR.PAGE_10, 
                                    selectedModel, 
                                    onQuestionAlternativeSelected, 
                                    onClickBtnTryAgain);
                break;
        }

    } catch (err) {
        console.log(TAG + " Failed to selectQuestionRandmonly " + err);
    }
}

function getRandomNumberForQuestion(questionNumber) {
    try {
        var selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);

        while (previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] === selectedQuestion) {
            selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);
        }

        previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] = selectedQuestion;

        return selectedQuestion;

    } catch (err) {
        console.log(TAG + " Failed to getRandomNumberForQuestion: " + err);
    }
}

function onEvaluationPageSelected(pageNumber) {
    try {
        // Verify if the user has completed the current module
        selectedEvaluationPage = pageNumber;

        if (selectedEvaluationPage === EVAL_FORM_SET_SIZE && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
            modalSimpleMessageCud1.verifyConditionsAndDisplayModal(UNIT_NUMBER);
        }

        // Rebuild the selected question updating it randomly
        selectQuestionRandomly(pageNumber);

    } catch (err) {
        console.log(TAG + " Failed to save the index of the selected evaluation page " + err);
    }
}

function onQuestionAlternativeSelected(unitId, questionId, questionStatus, submit, selectedAlternative) {
    try {
        alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit);
        verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit)

    } catch (err) {
        console.log(TAG + " Failed to save the status of a question " + err);
    }
}

function onClickBtnTryAgain() {
    try {
        selectQuestionRandomly(selectedEvaluationPage);

    } catch (err) {
        console.log(TAG + " Failed to onClickBtnTryAgain " + err);
    }
}

/**
 * Update the status of all possible questions of the selected "page / question"  
 */
function alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit) {
    try {
        var selectedPageEvaluationForm = -1;

        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];

            for (var j = 0; j < modelQuestions.length; j++) {
                
                if (modelQuestions[j].questionId === questionId) {
                    selectedPageEvaluationForm = i;
                    break;
                }
            }
        }

        if (selectedPageEvaluationForm !== -1) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + selectedPageEvaluationForm];

            for (var j = 0; j < modelQuestions.length; j++) {
                modelQuestions[j].questionStatus = questionStatus;

                if (modelQuestions[j].questionId === questionId) {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        selectedAlternative);
                } else {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        "-1");
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to alignStatusQuestionOfPageEvalForm " + err);
    }
}

function verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit) {
    try {

        if (typeof (questionStatus) !== "undefined" && typeof (submit) !== "undefined") {

            if (questionStatus !== gData.QUESTION_STATUS.CLEAR) {

                if (selectedEvaluationPage === EVAL_FORM_SET_SIZE) {

                    if (questionStatus !== gData.QUESTION_STATUS.ANSWERING) {
                        modalSimpleMessageCud1.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                    }

                } else if (gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud1.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }
            }
        }

    } catch(err) {
        console.log(TAG + " Failed to verifyUserCompletedModuleAndDisplayModal " + err);
    }
}

/**
 * Events
 * Detect events that must be triggered after the UI of the page is built
 */
function initEvents() {
    
    $(window).resize(function() {
        gUi.detectChangeLayoutReload();
    });

    detectLinkSelectedQuestion();
}

/**
 * Verify if the link of a question was selected in the previous page, if it was, 
 * select the evaluation form section of the current page and display the selected question.
 */
function detectLinkSelectedQuestion() {
    try {

        // Verify if a User has clicked on the link of a question in the previous page
        if (gData.hasSelectedQuestionLink()) {
            sidebarCud1.selectMenuItemN(SIDEBAR_MENU_SECTION_IDS.evaluation);

            setTimeout(function () {
                // Show the selected question of the evaluation form
                var questionLink = gData.getSelectedQuestionLink();
                evalFormPaginatorCud1.navigateToPageN(questionLink.questionNumber);

                // Reset the session variable selectedQuestionLink
                gData.setSelectedQuestionLink(undefined, undefined);

            }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE);
        }
    } catch (err) {
        console.log(TAG + " Failed to detectSelectedQuestionLink " + err);
    }
}