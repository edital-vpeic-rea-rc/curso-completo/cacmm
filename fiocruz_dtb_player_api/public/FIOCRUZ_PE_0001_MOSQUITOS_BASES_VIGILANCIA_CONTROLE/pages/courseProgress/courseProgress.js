/**
 * courseProgress.js handles the UI and Logical events of the courseProgress.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @requires SimpleMessageModal
 * @license MIT
 */

var SELECTED_TOP_MENU_OPTION = 3;

// Selectors
var PAGE_LOADER = "#componentPageLoaderProgress";
var PAGE_CONTAINER_SELECTOR = "#pageContainerProgress";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Progress";
var SIDEBAR_SELECTOR = "#componentSidebarProgress";
var FOOTER_SELECTOR = "#componentFooterProgress";
var QUESTION_STATUS_IMG_SELECTOR = " ul li a img";

var USERNAME_SELECTOR = "#username";
var SUMMARIZED_INFO_SELECTOR = ".summarized-info";
var DETAILED_INFO_SELECTOR = ".detailed-info";
var COURSE_PERCENTAGE_SELECTOR = ".course-percentage";
var CONTENT_MSG_SELECTOR = ".content-msg";
var IS_APPROVED_SELECTOR = ".is-approved";
var COURSE_INIT_SELECTOR = "#courseInitDate";
var PROGRESS_PUZZLE_COURSE_COMPLETED_IMG_SELECTOR = "#progressPuzzle_complete";
var UNIT_QUESTIONS_SELECTOR_PREFIX = "#unitQuestionStatus_";
var UNIT_PROGRESS_PUZZLE_SELECTOR_PREFIX = "#progressPuzzle_";
var SELECTORS_QUESTION_STATUS_IMGS = {
    unit_1_incomplete: "#unitQuestionStatus_1 .img-status .incomplete",
    unit_1_complete: "#unitQuestionStatus_1 .img-status .complete",
    unit_2_incomplete: "#unitQuestionStatus_2 .img-status .incomplete",
    unit_2_complete: "#unitQuestionStatus_2 .img-status .complete",
    unit_3_incomplete: "#unitQuestionStatus_3 .img-status .incomplete",
    unit_3_complete: "#unitQuestionStatus_3 .img-status .complete"
}
var SELECTORS_MODULES_LINES = {
    unit_1: "#unitQuestionStatus_1 .line",
    unit_2: "#unitQuestionStatus_2 .line"
}
var SELECTOR_DISMISS_FINAL_MSG = ".is-approved .final-msg, .is-approved .btn-close, .is-approved .selectable";
var CONFIRMATION_DIALOG_SELECTOR = "#progressConfirmationDialog";

var CLASS_FLIP_SHOW = "flipShow";
var CLASS_FLIP_HIDE = "flipHide";
var CLASS_FILL_LINE = "filled";
var CLASS_DISMISS_FINAL_MSG = "dismiss";

// HTML Components
var pageLoaderProgress = new PageLoader();
var sidebarProgress = new Sidebar();
var headerTop2Progress = new HeaderTop2();
var mainContainer = new MainContainer();
var footerProgress = new Footer();
var confirmationDialog = new ConfirmationDialog();

/**
 * Initialization
 */
$(document).ready(function () {
    gPages.setCurrentPage(gPages.PAGE_COURSE_PROGRESS);
    initUI();
    loadData();
    gUi.initEvents();
    initPageEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * UI
 */
function initUI() {
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderProgress, gMsgs.DEFAULT_LOADING_MESSAGE, "", function() {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
    });

    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Progress.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_PROGRESS_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            -1);

        headerTop2Progress.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarProgress.model;
        dataModel.pageId = gPages.IDS.PAGE_PROGRESS_ID;
        dataModel.title = "ANDAMENTO";
        dataModel.bgFilter = gUi.PURPLE_FILTER;
        dataModel.unitNumber = "progress";
        dataModel.subTitle = "";
        dataModel.summaryTitle = "";
        dataModel.summaryItems = [];
        dataModel.mainMenuItems = [];

        sidebarProgress.build(SIDEBAR_SELECTOR, dataModel, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarProgress);
        mainContainer.initGlogalEvents();
        mainContainer.shrinkSidebar();

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init MainContainer");
        }
    }
}

function initFooter() {
    try {
        footerProgress.useDefaultFooter(true);
        footerProgress.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init Footer " + err);
        }
    }
}

/**
 * Data Loading
 */
function loadData() {
    gData.loadUserCourse();

    setTimeout(function () {
        loadCourseStartingDate();
        loadCoursePercentage();

        setTimeout(function () {
            loadQuestionsStatus();
    
            setTimeout(function () {
    
                if (getNumberFinishedUnits() > 0) {
                    loadUnitsStatusScenes();

                    var scrollTopSpeedDelta = 400;

                    if (gUi.isOnMobileViewMode()) {
                        scrollTopSpeedDelta = 1000;
                    }

                    $(gUi.HTML_BODY_SELECTOR).animate({
                        scrollTop: scrollTopSpeedDelta
                    }, scrollTopSpeedDelta * 2, function() {

                        if (gData.hasUserFinishedCourse()) {

                            setTimeout(function() {

                                $(gUi.HTML_BODY_SELECTOR).animate({
                                    scrollTop: -scrollTopSpeedDelta
                                }, function() {
                                    showConclusionScene();
                                });
                            }, scrollTopSpeedDelta);
                        }
                    });
                }
            }, 3000);
        }, 1500);
    }, 1500);
}

function getNumberFinishedUnits() {
    var numFinishedUnits = 0;

    for (var i = 1; i <= gData.getCourse().units.length; i++) {

        if (gData.hasUserFinishedUnit(i)) {
            numFinishedUnits++;
        }
    }

    return numFinishedUnits;
}

function loadCoursePercentage() {
    var percentage = gData.getCourseStatus().percentage;

    try {

        if (percentage != undefined) {
            $(DETAILED_INFO_SELECTOR).removeClass(gUi.CLASS_FADE_OUT);
            $(DETAILED_INFO_SELECTOR).addClass(gUi.CLASS_FADE_IN);

            $(COURSE_PERCENTAGE_SELECTOR).html(percentage + "%");
            $(COURSE_PERCENTAGE_SELECTOR).addClass(gUi.CLASS_BIGGER);
        } else {
            $(COURSE_INIT_SELECTOR).parent().html(gMsgs.MSG_FAILED_LOAD_COURSE_STATUS);
            $(COURSE_PERCENTAGE_SELECTOR).parent().parent().html("");
        }
        
    } catch (err) {

        if (unasus.pack.getDebug() && err != undefined) {
            console.log("Failed to get the course percentage " + err);
        }
    }
}

function loadCourseStartingDate() {
    try {
        var username = gData.getUsername();
        var date = new Date(gData.getPPUInitDate());
        var formattedDate = gUtils.getPrettyDate(date);

        $(SUMMARIZED_INFO_SELECTOR).removeClass(gUi.CLASS_FADE_OUT);
        $(SUMMARIZED_INFO_SELECTOR).addClass(gUi.CLASS_FADE_IN);

        if (formattedDate == undefined || formattedDate.indexOf('NaN') >= 0) {
            $(COURSE_INIT_SELECTOR).parent().html("");

            var i = 0;
            $(CONTENT_MSG_SELECTOR + " small").each(function() {

                if (i == 0) {
                    $(this).html(gMsgs.MSG_FAILED_LOAD_COURSE_STATUS);
                } else {
                    $(this).html("");
                }

                i++;
            });
        } else {
            $(USERNAME_SELECTOR).html(username + ", ");
            $(COURSE_INIT_SELECTOR).html(formattedDate + ".");
        }

    } catch (err) {

        if (unasus.pack.getDebug() && err != undefined) {
            console.log("Failed to get the course start date " + err);
        }
    }
}

/** 
 * Create the data model of the objects that are going to display the 
 * list of questions of each unit
 */
function loadQuestionsStatus() {
    try {

        if (typeof(gData.getCourse()) !== "undefined") {

            if (typeof(gData.getCourse().units) !== "undefined") {

                for (var i = 1; i <= gData.getCourse().units.length; i++) {
                    var target = UNIT_QUESTIONS_SELECTOR_PREFIX + i;
                    var model = gData.getUnitQuestionListModel(i);
                    
                    for (var j = 0; j < model.questions.length; j++) {
                        var questionName = model.questions[j].questionName.replace("Questão", "");
                        model.questions[j].questionName = questionName;

                        var questionStatusImg = model.questions[j].questionStatusImg;
                        
                        if (questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_WRONG || 
                            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_WRONG) {
                            model.questions[j].questionStatusImg = gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_WRONG;
                        }

                        if (questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_ANSWERING ||
                            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_ANSWERING) {
                            model.questions[j].questionStatusImg = gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_ANSWERING;
                        }

                        if (questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_PNG_SUCCESS ||
                            questionStatusImg === gImages.QUESTION_STATUS_IMAGES.ICO_SVG_SUCCESS) {
                            model.questions[j].questionStatusImg = gImages.QUESTION_STATUS_IMAGES_NO_SIGNS.ICO_PNG_SUCCESS;
                        }
                    }

                    if (typeof(model) !== "undefined") {
                        var questionsList = new ListQuestionsStatus();
                        questionsList.build(target, model, onQuestionInListClicked);
                    } else {
                        $(target).html("Houve uma falha ao carregar o status das questões da Unidade " + i);
                    }
                }
            }
        }
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to get the the status of a question " + err);
        }
    }
}

function onQuestionInListClicked(unitNumber, questionNumber) {
    
    try {

        if (unitNumber !== -1 && questionNumber != -1) {
            gData.setSelectedQuestionLink(unitNumber, questionNumber);

            switch (unitNumber) {

                case 1:
                    gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_1);

                    break;

                case 2:
            
                    if (gData.hasUserFinishedUnit(1)) {
                        gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_2);
                    } else {
                        showConfirmationDialog();
                    }
        
                    break;
        
                case 3:
                    
                    if (gData.hasUserFinishedUnit(1) && gData.hasUserFinishedUnit(2)) {
                        gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_3);
                    } else {
                        showConfirmationDialog();
                    }
                    
                    break;
            }
        } else {
            console.log("Failed to get unit and question number within initEvents.");
        }
    } catch (err) {
        console.log("Failed to execute onQuestionInListClicked " + err);
    }
}

function showConfirmationDialog() {
    try {
        this.model = {
            headerMsg: "Calma ...",
            icoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
            mainMsg: "Para ter acesso a este módulo é necessário concluir o anterior.",
            actionMsg: "ir para",
            cancelBtnText: gData.prevModuleInfo.pageName,
            confirmBtnText: gData.nextModuleInfo.pageName,
            callbackCancelAction: navigateToPreviousModule,
            callbackConfirmAction: navigateToNextModule
        }
        confirmationDialog.rebuild(this.CONFIRMATION_DIALOG_SELECTOR, this.model);
        confirmationDialog.show(true);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init initConfirmationDialog " + err);
        }
    }
}

function navigateToPreviousModule() {
    try {
        gData.setSelectedQuestionLink(undefined, undefined);
        gPages.updateCurrentPage(gData.prevModuleInfo.pageId);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init navigateToPreviousModule " + err);
        }
    }
}

function navigateToNextModule() {
    try {
        gData.setSelectedQuestionLink(undefined, undefined);
        gPages.updateCurrentPage(gData.nextModuleInfo.pageId);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init navigateToNextModule " + err);
        }
    }
}

function loadUnitsStatusScenes() {
    try {
        finishedCourse = true;

        for (var i = 1; i <= gData.getCourse().units.length; i++) {

            if (gData.hasUserFinishedUnit(i)) {
                showCompletedSceneForUnit(i);
            } else {
                finishedCourse = false;
            }
        }
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to get the units status " + err);
        }
    }
}

function showCompletedSceneForUnit(unityNumber) {
    try {

        switch (unityNumber) {

            case 1: {
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_1_incomplete).addClass(CLASS_FLIP_HIDE);
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_1_complete).addClass(CLASS_FLIP_SHOW);

                setTimeout(function() {
                    $(SELECTORS_MODULES_LINES.unit_1).addClass(CLASS_FILL_LINE);
                }, 500);

                break;
            }

            case 2: {
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_2_incomplete).addClass(CLASS_FLIP_HIDE);
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_2_complete).addClass(CLASS_FLIP_SHOW);

                setTimeout(function() {
                    $(SELECTORS_MODULES_LINES.unit_2).addClass(CLASS_FILL_LINE);
                }, 1000);

                break;
            }

            case 3: {
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_3_incomplete).addClass(CLASS_FLIP_HIDE);
                $(SELECTORS_QUESTION_STATUS_IMGS.unit_3_complete).addClass(CLASS_FLIP_SHOW);
                break;
            }
        }

    } catch (err) {
        console.log("Warning: Failed to showCompletedSceneForUnit " + err);
    }
}

function showConclusionScene() {
    $(IS_APPROVED_SELECTOR).find(".title").html(gMsgs.MSG_YOU_WERE_APPROVED);
    $(IS_APPROVED_SELECTOR).find(".msg").html(gMsgs.MSG_YOU_FINISHED_EDUCATIONAL_RESOURCE);

    $(IS_APPROVED_SELECTOR).each(function () {
        $(this).removeClass(gUi.CLASS_FADE_OUT);
        $(this).addClass(gUi.CLASS_FADE_IN);
    });

    for (var i = 1; i <= 4; i++) {
        $(UNIT_PROGRESS_PUZZLE_SELECTOR_PREFIX + i).addClass(gUi.CLASS_FADE_OUT);
    }

    $(PROGRESS_PUZZLE_COURSE_COMPLETED_IMG_SELECTOR).removeClass(gUi.CLASS_HIDDEN);
}

/**
 * Page events
 */
function initPageEvents() {

    $(window).resize(function() {
        gUi.detectChangeLayoutReload();
    });

    $(SELECTOR_DISMISS_FINAL_MSG).on("click", function() {
        $(IS_APPROVED_SELECTOR).addClass(CLASS_DISMISS_FINAL_MSG);
    });

}