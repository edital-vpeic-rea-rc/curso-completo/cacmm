/**
 * courseUnitDetails3.js handles the UI and Logical events of the courseUnitDetails2.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Fiocruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jQuery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @requires ModalSimpleMessage
 * @requires Paginator
 * @license MIT
 */

// Globals
var TAG = "Cud3";
var UNIT_NUMBER = 3;
var SELECTED_TOP_MENU_OPTION = 4;

var SIDEBAR_MENU_SECTION_IDS = {
    summary: 1,
    problemSituation: 2,
    book: 3,
    material: 4,
    evaluation: 5
}
var SIDEBAR_MENU_SECTION_NAMES = {
    summary: "Síntese",
    problemSituation: "Situação Problema",
    book: "Livro",
    material: "Material Complementar",
    evaluation: "Avaliação"
}

/**
 * Selectors
 */
// Global selectors
var PAGE_LOADER = "#componentPageLoaderCud3";
var PAGE_CONTAINER_SELECTOR = "#pageContainerCud3";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Cud3";
var SIDEBAR_SELECTOR = "#componentSidebarCud3";
var FOOTER_SELECTOR = "#componentFooterCud3";
var PAGE_CONTENT_CONTAINER_SELECTORS = {
    summary: "#summaryContainerCud3",
    problemSituation: "#problemSituationContainerCud3",
    book: "#bookContainerCud3",
    material: "#materialContainerCud3",
    evaluation: "#evaluationContainerCud3"
}

var PAGE_CONTENT_TITLE_SELECTOR = ".page-title h3";
var MODAL_SIMPLE_MESSAGE_SELECTOR = "#componentModalSimpleMessageCud3";
var EVAL_FORM_PAGINATOR_SELECTOR = "#evalFormPaginatorCud3";
var VIDEO_DISPLAYER = "#componentVideoDisplayerCud3";

// Summary selectors
var SUMMARY_SUB_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex1Cud3",
    SUBSEC_2: "#cex2Cud3",
    MAGNIFIER: "#stepsModMosquitos"
}

// Problem Situation selectors
var PROBLEM_SITUATION_VIDEO_SELECTOR = "#componentVideoProblemSituationCud3 video";

// Book selectors
var FSM_PDF_VIEWER_SELECTOR = "#componentPdfViewerCud3";
var FSM_PDF_VIEWER_MODAL_SELECTOR = "#componentPdfViewerFSMCud3";

// Complementary Materials selectors
var CM_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex4Cud3",
    SUBSEC_2: "#cex5Cud3",
    SUBSEC_3: "#cex6Cud3",
    SUBSEC_4: "#cex7Cud3",
    SUBSEC_5: "#cex8Cud3"
}
var CM_CS_HELTH_MINISTRY_SELECTOR = "#componentCmCsMinistryHealthCud3";
var CM_CS_RELATED_ARTICLES_SELECTOR = "#componentCmCsRelatedArticlesCud3";
var CM_CS_INFO_GRAPHIC_SELECTOR = "#componentCmCsInfoGraphicsCud3";
var CM_CS_VIDEOS_SELECTOR = "#componentCmVsCud3";
var CM_CS_WEB_CONTENT_SELECTOR = "#componentCmWebContentCud3";

// Evaluation selectors
var EVALUATION_FORM_SELECTOR = {
    PAGE_1: "#evalFormCud3Page1",
    PAGE_2: "#evalFormCud3Page2",
    PAGE_3: "#evalFormCud3Page3",
    PAGE_4: "#evalFormCud3Page4"
}
var PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM = "models_question_";
var PREFIX_PAGE_EVAL_FORM = "q_";

/**
 * Page objects
 */
// Global
var previousUIMode = undefined;

// Page components
var pageLoaderCud3 = new PageLoader();
var headerTop2Cud3 = new HeaderTop2();
var sidebarCud3 = new Sidebar();
var mainContainer = new MainContainer();
var footerCud3 = new Footer();
var modalSimpleMessageCud3 = new ModalSimpleMessage();
var imgMagnifier = new ImgMagnifier();

// Summary global items
var summaryModuleOverview = undefined;
var summaryModuleDna = undefined;

// Problem situation items
var problemSituationVideoUrl = "videos/dra_leda_agentes_saude_pilar_fundamental_540p.mp4";

// Book section item
var pdfViewerCud3 = new PdfViewer();

// Complementary Materials
var complMatHealthMinistryCud3 = undefined;
var ministryHealthSubSec = undefined;
var complMatRelatedArticlesCud3 = undefined;
var relatedArticlesSubSec = undefined;
var complMatInfoGraphicsCud3 = undefined;
var complMatVideosCud3 = undefined;
var videosSubSec = undefined;
var complMatWebContentCud3 = undefined;
var webContentSubSec = undefined;

// Evaluation
var EVAL_FORM_SET_SIZE = 4;
var EVAL_FORM_NUM_QUESTIONS_PER_SET = 2;

var evalFormPaginatorCud3 = undefined;
var selectedEvaluationPage = 1;
var previouslySelectedQuestions = {
    "q_1" : 0,
    "q_2" : 0,
    "q_3" : 0,
    "q_4" : 0
}
var question1Obj = new SingleAnswerQuestion();
var question2Obj = new SingleAnswerQuestion();
var question3Obj = new SingleAnswerQuestion();
var question4Obj = new SingleAnswerQuestion();
var modelsQuestions = {
    "models_question_1" : undefined,
    "models_question_2" : undefined,
    "models_question_3" : undefined,
    "models_question_4" : undefined
}

/**
 * Initialization
 */
$(document).ready(function () {
    gUi.initEvents();
    gPages.setCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_3);
    previousUIMode = gUi.isOnMobileViewMode();

    initData();
    initUI();
    initEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * Data
 */
function initData() {
    gData.loadUserCourse();
}

/**
 * UI
 */
function initUI() {
    // Init the UI of the base elements
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderCud3, gMsgs.DEFAULT_LOADING_MESSAGE, "", function () {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
        initPageSections();
        gUi.finishUiInit();
    });

    // Init the UI of the main components of the page
    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
    initPassedUnitModal();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Cud3.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_UNIT_DETAILS_3_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            1);

        headerTop2Cud3.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarCud3.model;
        dataModel.pageId = gPages.IDS.PAGE_COURSE_UNIT_DETAILS_3_ID;
        dataModel.title = "MÓDULO 3";
        dataModel.bgFilter = gUi.BLUE_FILTER;
        dataModel.unitNumber = "3";
        dataModel.subTitle = "Controle de mosquitos";
        dataModel.summaryTitle = "Objetivos da unidade";
        dataModel.summaryItems = [
            {
                name: '<b>Objetivos</b>: ...'
            },
            {
                name: '<b>Carga Horaria</b>: <span class="course-work-load">' + 10 + '</span> horas/aula'
            },
            {
                name: '<b>Recursos Educacionais</b>: texto de apoio produzido para o curso a partir de documentos do MS; vídeos com especialistas sobre o tema; biblioteca virtual.'
            },
            {
                name: '<b>Avaliação</b>: Atividades objetivas.'
            }
        ];
        dataModel.mainMenuItems = [
            {
                id: SIDEBAR_MENU_SECTION_IDS.summary,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.summary,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_SUMMARY, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_SUMMARY,
                hasSubItems: false,
                itemSelected: true,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.problemSituation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.problemSituation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.book,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.book,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_BOOK, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_BOOK, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_BOOK,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_BOOK,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.material,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.material,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.evaluation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.evaluation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_EVALUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_EVALUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            }
        ];
        dataModel.onItemSelectedCallback = onSidebarItemSelected;
        dataModel.isMinified = true;

        sidebarCud3.build(SIDEBAR_SELECTOR, dataModel, onSidebarItemSelected);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarCud3);
        mainContainer.initGlogalEvents();
        mainContainer.setSidebarToggleEventCallback(onSidebarToggle);

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        } else {
            mainContainer.expandSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init MainContainer " + err);
        }
    }
}

function onSidebarToggle(sidebarStatus) {
    // ..
}

function onSidebarItemSelected() {
    try {
        $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.book).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.material).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).addClass(gUi.CLASS_HIDDEN);

        resetSummaryPageSection();
        resetProblemSituationPageSection();
        resetBookPageSection();
        resetComplementaryMaterialsPageSection();

        switch (sidebarCud3.getSelectedSection()) {

            case SIDEBAR_MENU_SECTION_IDS.summary: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.summary);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).removeClass(gUi.CLASS_HIDDEN);

                initSummaryPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.problemSituation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.problemSituation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).removeClass(gUi.CLASS_HIDDEN);

                initProblemSituationPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.book: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.book);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.book).removeClass(gUi.CLASS_HIDDEN);

                pdfViewerCud3.setTitle("Capítulo da Unidade 3");
                pdfViewerCud3.setSubTitle(gFiles.PDF_COURSE_BOOK.TITLE);
                pdfViewerCud3.setPdfUrl(gFiles.PDF_COURSE_BOOK.URL);
                pdfViewerCud3.setInitialPage(83);
                pdfViewerCud3.rebuild();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.material: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.material);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.material).removeClass(gUi.CLASS_HIDDEN);

                initComplementaryMaterialsPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.evaluation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.evaluation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).removeClass(gUi.CLASS_HIDDEN);

                if (typeof (modalSimpleMessageCud3 !== "undefined") && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud3.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }

                break;
            }
        }

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to select Sidebar menu option " + err);
        }
    }
}

function initFooter() {
    try {
        gUi.setHasFixedFooter(false);
        footerCud3.useDefaultFooter(true);
        footerCud3.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Footer " + err);
        }
    }
}

function initPassedUnitModal() {
    var model = {
        img: gImages.ICO_GIF_UNIT_3_SUCCESS,
        title: gMsgs.MSG_CONGRATS,
        msg: gMsgs.MSG_FINISHED_UNIT_N + " " + UNIT_NUMBER,
        msgSupport: "",
        unitNumber: UNIT_NUMBER
    }
    modalSimpleMessageCud3.build(MODAL_SIMPLE_MESSAGE_SELECTOR, model);
}

/**
 * Page Sections
 */
function initPageSections() {
    resetSummaryPageSection();
    resetProblemSituationPageSection();
    resetBookPageSection();
    resetComplementaryMaterialsPageSection();

    initSummaryPageSection();
    initBookPageSection();
    initEvaluationSection();
}

/**
 * Page section: Summary
 */
function resetSummaryPageSection() {
    try {

        if (summaryModuleOverview != null && summaryModuleOverview != undefined) {
            summaryModuleOverview.destroy();
            summaryModuleOverview = null;

            summaryModuleDna.destroy();
            summaryModuleDna = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetSummaryPageSection " + err);
    }
}

function initSummaryPageSection() {
    // Subsection 1
    summaryModuleOverview = new ContentExpander();
    summaryModuleOverview.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Manipulação de mosquitos como estratégia de controle",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 2
    summaryModuleDna = new ContentExpander();
    summaryModuleDna.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Exemplo simplificado das etapas de modificação de mosquitos através da Transgenia",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2).removeClass(gUi.CLASS_HIDDEN);
}

/**
 * Page section: Problem situation
 */
function resetProblemSituationPageSection() {
    try {
        $(PROBLEM_SITUATION_VIDEO_SELECTOR).attr("src", problemSituationVideoUrl);
    } catch(err) {
        console.log(TAG + " Failed to resetProblemSituationPageSection " + err);
    }
}

function initProblemSituationPageSection() {
    $(PROBLEM_SITUATION_VIDEO_SELECTOR).attr("src", problemSituationVideoUrl);
}

/**
 * Page section: Book
 */
function resetBookPageSection() {
    try {

        if (pdfViewerCud3 != null && pdfViewerCud3 != undefined) {
            pdfViewerCud3.setTitle("Capítulo da Unidade 3");
            pdfViewerCud3.setSubTitle(gFiles.PDF_COURSE_BOOK.TITLE);
            pdfViewerCud3.setPdfUrl(gFiles.PDF_COURSE_BOOK.URL);
            pdfViewerCud3.setInitialPage(1);
            pdfViewerCud3.rebuild();
        }
    } catch(err) {
        console.log(TAG + " Failed to resetBookPageSection " + err);
    }
}

function initBookPageSection() {
    var model = {
        title: "Capítulo da Unidade 3",
        subTitle: gFiles.PDF_COURSE_BOOK_COVER.SUB_TITLE,
        pdfFileUrl: gFiles.PDF_COURSE_BOOK_COVER.URL,
        fileType: gFiles.FILE_TYPE.TEXT,
        initialPageNum: 1,
        urlSvgIcoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
        urlPngIcoBtnClose: gImages.ICO_PNG_CLOSE_BLACK,
        urlSvgIcoBtnFullScreen: gImages.ICO_SVG_FULL_SCREEN_BLACK,
        urlPngIcoBtnFullScreen: gImages.ICO_PNG_FULL_SCREEN_BLACK
    }

    pdfViewerCud3.build(FSM_PDF_VIEWER_SELECTOR, FSM_PDF_VIEWER_MODAL_SELECTOR, model);
}

/**
 * Page section: Complementary materials
 */
function resetComplementaryMaterialsPageSection() {
    try {

        if (complMatHealthMinistryCud3 != null && complMatHealthMinistryCud3 != undefined) {
            complMatHealthMinistryCud3.destroy();
            complMatHealthMinistryCud3 = null;

            ministryHealthSubSec.destroy();
            ministryHealthSubSec = null;

            complMatRelatedArticlesCud3.destroy();
            complMatRelatedArticlesCud3 = null;

            relatedArticlesSubSec.destroy();
            relatedArticlesSubSec = null;

            complMatInfoGraphicsCud3.destroy();
            complMatInfoGraphicsCud3 = null;

            infoGraphicsSubSec.destroy();
            infoGraphicsSubSec = null;

            complMatVideosCud3.destroy();
            complMatVideosCud3 = null;

            videosSubSec.destroy();
            videosSubSec = null;

            complMatWebContentCud3.destroy();
            complMatWebContentCud3 = null;

            webContentSubSec.destroy();
            webContentSubSec = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetComplementaryMaterialsPageSection " + err);
    }
}

function initComplementaryMaterialsPageSection() {
    // Subsection 1 - Materials from the Ministry of Health
    var modelMinistryHealthMaterials = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: []
    };

    complMatHealthMinistryCud3 = new ContentSelector();
    complMatHealthMinistryCud3.build(CM_CS_HELTH_MINISTRY_SELECTOR, onReadableObjectSelected, modelMinistryHealthMaterials);

    ministryHealthSubSec = new ContentExpander();
    ministryHealthSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Materiais do Ministério da Saúde",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 2 - Related Articles
    var modelRelatedArticles = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: []
    }

    complMatRelatedArticlesCud3 = new ContentSelector();
    complMatRelatedArticlesCud3.build(CM_CS_RELATED_ARTICLES_SELECTOR, onReadableObjectSelected, modelRelatedArticles);

    relatedArticlesSubSec = new ContentExpander();
    relatedArticlesSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Artigos Relacionados",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 3 - Info Graphics
    var modelInfoGraphics = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: []
    }

    complMatInfoGraphicsCud3 = new ContentSelector();
    complMatInfoGraphicsCud3.build(CM_CS_INFO_GRAPHIC_SELECTOR, onReadableObjectSelected, modelInfoGraphics);

    infoGraphicsSubSec = new ContentExpander();
    infoGraphicsSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_3, {
        title: "Infográficos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 4 - Videos
    var modelVideos = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        videosList: [
            {
                id: 0,
                title: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.TITLE,
                subTitle: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.SUB_TITLE,
                fileUrl: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.URL,
                internetUrl: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.EXT_URL,
                videoResolution: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.RESOLUTION,
                coverImg: gFiles.VIDEO_MUNDO_MACRO_MICRO_AEDES.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 1,
                title: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.TITLE,
                subTitle: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.SUB_TITLE,
                fileUrl: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.URL,
                internetUrl: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.EXT_URL,
                videoResolution: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.RESOLUTION,
                coverImg: gFiles.VIDEO_DESENVOLVIMENTO_TEC_COMBATER_AEDES.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 2,
                title: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.TITLE,
                subTitle: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.SUB_TITLE,
                fileUrl: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.URL,
                internetUrl: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.EXT_URL,
                videoResolution: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.RESOLUTION,
                coverImg: gFiles.VIDEO_MICROINJECAO_WOLBACHIA.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 3,
                title: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.TITLE,
                subTitle: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.SUB_TITLE,
                fileUrl: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.URL,
                internetUrl: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.EXT_URL,
                videoResolution: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.RESOLUTION,
                coverImg: gFiles.VIDEO_WMP_BRASIL_COMO_TRABALHAMOS.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 4,
                title: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.TITLE,
                subTitle: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.SUB_TITLE,
                fileUrl: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.URL,
                internetUrl: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.EXT_URL,
                videoResolution: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.RESOLUTION,
                coverImg: gFiles.VIDEO_WMP_BRASIL_COMPLEXO_ALEMAO.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 5,
                title: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.TITLE,
                subTitle: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.SUB_TITLE,
                fileUrl: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.URL,
                internetUrl: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.EXT_URL,
                videoResolution: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.RESOLUTION,
                coverImg: gFiles.VIDEO_G1_NOTICIA_RECIFE_INAUGURA_CENTRO_MOSQUITOS.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            },
            {
                id: 6,
                title: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.TITLE,
                subTitle: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.SUB_TITLE,
                fileUrl: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.URL,
                internetUrl: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.EXT_URL,
                videoResolution: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.RESOLUTION,
                coverImg: gFiles.VIDEO_OPINIAO_PERNAMBUCO_AVANCO_ZIKA.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            }
        ]
    }

    complMatVideosCud3 = new VideoSelector();
    complMatVideosCud3.build(CM_CS_VIDEOS_SELECTOR, onVideoObjectSelected, modelVideos);

    videosSubSec = new ContentExpander();
    videosSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_4, {
        title: "Vídeos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_4).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 5 - Web content
    var modelWebContent = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.TITLE,
                author: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 1,
                title: gExtResources.EXT_RESOURCE_FIOCRUZ_PROJETO_TESTE_CONTROLE_AEDES.TITLE,
                author: gExtResources.EXT_RESOURCE_FIOCRUZ_PROJETO_TESTE_CONTROLE_AEDES.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_FIOCRUZ_PROJETO_TESTE_CONTROLE_AEDES.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 2,
                title: gExtResources.EXT_RESOURCE_CANAL_SAUDE_UM_SIMPLES_MOSQUITO.TITLE,
                author: gExtResources.EXT_RESOURCE_CANAL_SAUDE_UM_SIMPLES_MOSQUITO.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_CANAL_SAUDE_UM_SIMPLES_MOSQUITO.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 3,
                title: gExtResources.EXT_RESOURCE_NOTICIAS_RECENTES_WMP_BRASIL_METODO_WOLBACHIA.TITLE,
                author: gExtResources.EXT_RESOURCE_NOTICIAS_RECENTES_WMP_BRASIL_METODO_WOLBACHIA.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_NOTICIAS_RECENTES_WMP_BRASIL_METODO_WOLBACHIA.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatWebContentCud3 = new ContentSelector();
    complMatWebContentCud3.build(CM_CS_WEB_CONTENT_SELECTOR, onWebObjectSelected, modelWebContent);

    webContentSubSec = new ContentExpander();
    webContentSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_5, {
        title: "Para Acessar na Web",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_5).removeClass(gUi.CLASS_HIDDEN);
}

function onReadableObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {

    if (fileUrl != undefined && fileUrl !== '') {
        pdfViewerCud3.setTitle("Materiais Complementares");

        if (title !== undefined) {
            pdfViewerCud3.setSubTitle(title);
        }

        if (fileUrl != undefined) {
            pdfViewerCud3.setPdfUrl(fileUrl);
        }

        if (initialPage != undefined) {
            pdfViewerCud3.setInitialPage(initialPage);
        }

        pdfViewerCud3.rebuild();
        pdfViewerCud3.show();
    }

    if (internetUrl != undefined && internetUrl !== '') {
        gData.onWebObjectSelected(internetUrl);
    }

    if (fileType != undefined && fileType != '' && title != undefined && title != '' && internetUrl === '') {
        gApiIntegration.setDownload(fileType, title);
    }
}

function onVideoObjectSelected(fileUrl, internetUrl, title, subTitle, videoResolution) {
    try {
        var videoContainerWidth = "100%";
        var selectedUrl = '';
        var localVideo = false;

        if (fileUrl != undefined && fileUrl !== '') {
            selectedUrl = fileUrl;
            localVideo = true;

            // Update the url of the video displayer and show it
            var complMatModalVideoCud3 = new ModalSimpleMessage();

            var model = {
                title: title,
                msg: subTitle,
                localVideo: localVideo,
                unitNumber: 2,
                videoContainerSize: videoContainerWidth,
                videoUrl: selectedUrl,
                videoResolution: videoResolution,
                msgSupport: '',
                videoSupportMsg: gMsgs.BROWSER_DOESNT_SUPPORT_THIS_VIDEO
            }

            complMatModalVideoCud3.build(VIDEO_DISPLAYER, model);
            complMatModalVideoCud3.show();

        } else if (internetUrl != undefined && internetUrl !== '') {
            gApiIntegration.setExternalLink(internetUrl);

            var win = window.open(internetUrl, '_blank');
            win.focus();

            console.log("internetUrl: " + internetUrl);

        }
    } catch (err) {
        console.log(TAG + " Failed to display video " + err);
    }
}

function onWebObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {
    gData.onWebObjectSelected(internetUrl);
}

/**
 * Page section: Evaluation
 */
function initEvaluationSection() {
    setTimeout(function() {
        // Build the questions' models randomly
        buildQuestionsModels();

        selectQuestionRandomly(1);
        selectQuestionRandomly(2);
        selectQuestionRandomly(3);
        selectQuestionRandomly(4);

        // Build the UI of the evaluation form
        evalFormPaginatorCud3 = new Paginator();
        evalFormPaginatorCud3.build(EVAL_FORM_PAGINATOR_SELECTOR, onEvaluationPageSelected);

    }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE / 2);
}

function buildQuestionsModels() {
    try {
        var Questions = jQuery.extend({}, new Question());
        var unit3 = gData.getCourse().units[2];

        // Build the questions of the evaluation form
        // First set of questions
        // Question 1
        var question1Model = unit3.unitQuestions[0];
        question1Model.alternatives = Questions.getAlphabetLetterForAlternatives(question1Model.alternatives, true);
        var modelQuestion1 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question1Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 1,
            questionStatus: question1Model.questionStatus,
            enunciate: question1Model.enunciate,
            rightAlternative: question1Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question1Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 2
        var question2Model = unit3.unitQuestions[1];
        question2Model.alternatives = Questions.getAlphabetLetterForAlternatives(question2Model.alternatives, true);
        var modelQuestion2 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question2Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 2,
            questionStatus: question2Model.questionStatus,
            enunciate: question2Model.enunciate,
            rightAlternative: question2Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question2Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 3
        var question3Model = unit3.unitQuestions[2];
        question3Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question3Model.staticAlternatives);
        var modelQuestion3 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question3Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 3,
            questionStatus: question3Model.questionStatus,
            enunciate: question3Model.enunciate,
            rightAlternative: question3Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question3Model.staticAlternatives,
            alternatives: question3Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 4
        var question4Model = unit3.unitQuestions[3];
        question4Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question4Model.staticAlternatives);
        var modelQuestion4 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question4Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 4,
            questionStatus: question4Model.questionStatus,
            enunciate: question4Model.enunciate,
            rightAlternative: question4Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question4Model.staticAlternatives,
            alternatives: question4Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }
        
        // Second set of questions
        // Question 5
        var question5Model = unit3.unitQuestions[4];
        question5Model.alternatives = Questions.getAlphabetLetterForAlternatives(question5Model.alternatives, true);
        var modelQuestion5 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question5Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 1,
            questionStatus: question5Model.questionStatus,
            enunciate: question5Model.enunciate,
            rightAlternative: question5Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question5Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 6
        var question6Model = unit3.unitQuestions[5];
        question6Model.alternatives = Questions.getAlphabetLetterForAlternatives(question6Model.alternatives, true);
        var modelQuestion6 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question6Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 2,
            questionStatus: question6Model.questionStatus,
            enunciate: question6Model.enunciate,
            rightAlternative: question6Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question6Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 7
        var question7Model = unit3.unitQuestions[6];
        question7Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question7Model.staticAlternatives);
        var modelQuestion7 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question7Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 3,
            questionStatus: question7Model.questionStatus,
            enunciate: question7Model.enunciate,
            rightAlternative: question7Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question7Model.staticAlternatives,
            alternatives: question7Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 8
        var question8Model = unit3.unitQuestions[7];
        question8Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question8Model.staticAlternatives);
        var modelQuestion8 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit3.unitId,
            questionId: question8Model.questionId,
            unitNumber: unit3.unitId,
            questionNumber: 4,
            questionStatus: question8Model.questionStatus,
            enunciate: question8Model.enunciate,
            rightAlternative: question8Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question8Model.staticAlternatives,
            alternatives: question8Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Build randomly the questions of the evaludation form
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "1"] = [modelQuestion1, modelQuestion5];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "2"] = [modelQuestion2, modelQuestion6];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "3"] = [modelQuestion3, modelQuestion7];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "4"] = [modelQuestion4, modelQuestion8];

        // Ensure that if one question of the possible questions of a page of the evaluation form
        // has the status "right", all the other questions of that same page will have the same status
        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelsQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];
            var statusQuestionNRigth = false;

            for (var j = 0; j < modelsQuestion.length; j++) {

                if (modelsQuestion[j].questionStatus == gData.QUESTION_STATUS.RIGHT) {
                    statusQuestionNRigth = true;
                    break;
                }
            }

            if (statusQuestionNRigth) {
                
                for (var j = 0; j < modelsQuestion.length; j++) {
                    modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i][j].questionStatus = gData.QUESTION_STATUS.RIGHT;
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to buildQuestionsModels " + err);
    }
}

function selectQuestionRandomly(questionNumber) {
    try {
        var modelsSelectedQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + questionNumber];
        var selectedModel = modelsSelectedQuestion[getRandomNumberForQuestion(questionNumber)];
        
        switch (questionNumber) {

            case 1:
                question1Obj.build(EVALUATION_FORM_SELECTOR.PAGE_1, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected,
                                   onClickBtnTryAgain);
                break;
    
            case 2:
                question2Obj.build(EVALUATION_FORM_SELECTOR.PAGE_2, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected,
                                   onClickBtnTryAgain);
                break;
            
            case 3:
                question3Obj.build(EVALUATION_FORM_SELECTOR.PAGE_3, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected,
                                   onClickBtnTryAgain);
                break;
            
            case 4:
                question4Obj.build(EVALUATION_FORM_SELECTOR.PAGE_4, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected,
                                   onClickBtnTryAgain);
                break;
        }

    } catch (err) {
        console.log(TAG + " Failed to selectQuestionRandmonly " + err);
    }
}

function getRandomNumberForQuestion(questionNumber) {
    try {
        var selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);

        while (previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] === selectedQuestion) {
            selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);
        }

        previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] = selectedQuestion;

        return selectedQuestion;

    } catch (err) {
        console.log(TAG + " Failed to getRandomNumberForQuestion: " + err);
    }
}

function onEvaluationPageSelected(pageNumber) {
    try {
        // Verify if the user has completed the current module
        selectedEvaluationPage = pageNumber;

        if (selectedEvaluationPage === EVAL_FORM_SET_SIZE && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
            modalSimpleMessageCud3.verifyConditionsAndDisplayModal(UNIT_NUMBER);
        }

        // Rebuild the selected question updating it randomly
        selectQuestionRandomly(pageNumber);

    } catch (err) {
        console.log(TAG + " Failed to save the index of the selected evaluation page " + err);
    }
}

function onQuestionAlternativeSelected(unitId, questionId, questionStatus, submit, selectedAlternative) {
    try {
        alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit);
        verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit)

    } catch (err) {
        console.log(TAG + " Failed to save the status of a question " + err);
    }
}

function onClickBtnTryAgain() {
    try {
        selectQuestionRandomly(selectedEvaluationPage);

    } catch (err) {
        console.log(TAG + " Failed to onClickBtnTryAgain " + err);
    }
}

/**
 * Update the status of all possible questions of the selected "page / question"  
 */
function alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit) {
    try {
        var selectedPageEvaluationForm = -1;

        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];

            for (var j = 0; j < modelQuestions.length; j++) {
                
                if (modelQuestions[j].questionId === questionId) {
                    selectedPageEvaluationForm = i;
                    break;
                }
            }
        }

        if (selectedPageEvaluationForm !== -1) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + selectedPageEvaluationForm];

            for (var j = 0; j < modelQuestions.length; j++) {
                modelQuestions[j].questionStatus = questionStatus;

                if (modelQuestions[j].questionId === questionId) {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        selectedAlternative);
                } else {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        "-1");
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to alignStatusQuestionOfPageEvalForm " + err);
    }
}

function verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit) {
    try {

        if (typeof (questionStatus) !== "undefined" && typeof (submit) !== "undefined") {

            if (questionStatus !== gData.QUESTION_STATUS.CLEAR) {

                if (selectedEvaluationPage === EVAL_FORM_SET_SIZE) {

                    if (questionStatus !== gData.QUESTION_STATUS.ANSWERING) {
                        modalSimpleMessageCud3.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                    }

                } else if (gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud3.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }
            }
        }

    } catch(err) {
        console.log(TAG + " Failed to verifyUserCompletedModuleAndDisplayModal " + err);
    }
}

/**
 * Events
 * Detect events that must be triggered after the UI of the page is built
 */
function initEvents() {
    
    $(window).resize(function() {
        gUi.detectChangeLayoutReload();
    });

    detectLinkSelectedQuestion();
}

/**
 * Verify if the link of a question was selected in the previous page, if it was, 
 * select the evaluation form section of the current page and display the selected question.
 */
function detectLinkSelectedQuestion() {
    try {

        // Verify if a User has clicked on the link of a question in the previous page
        if (gData.hasSelectedQuestionLink()) {
            sidebarCud3.selectMenuItemN(SIDEBAR_MENU_SECTION_IDS.evaluation);

            setTimeout(function () {
                // Show the selected question of the evaluation form
                var questionLink = gData.getSelectedQuestionLink();
                evalFormPaginatorCud3.navigateToPageN(questionLink.questionNumber - 20);

                // Reset the session variable selectedQuestionLink
                gData.setSelectedQuestionLink(undefined, undefined);

            }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE);
        }
    } catch (err) {
        console.log(TAG + " Failed to detectSelectedQuestionLink " + err);
    }
}