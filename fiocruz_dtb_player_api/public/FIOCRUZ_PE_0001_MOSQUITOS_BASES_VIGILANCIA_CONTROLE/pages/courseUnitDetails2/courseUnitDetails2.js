/**
 * courseUnitDetails2.js handles the UI and Logical events of the courseUnitDetails2.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Fiocruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jQuery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @requires ModalSimpleMessage
 * @requires Paginator
 * @license MIT
 */

// Globals
var TAG = "Cud2";
var UNIT_NUMBER = 2;
var SELECTED_TOP_MENU_OPTION = 4;

var SIDEBAR_MENU_SECTION_IDS = {
    summary: 1,
    problemSituation: 2,
    book: 3,
    material: 4,
    evaluation: 5
}
var SIDEBAR_MENU_SECTION_NAMES = {
    summary: "Síntese",
    problemSituation: "Situação Problema",
    book: "Livro",
    material: "Material Complementar",
    evaluation: "Avaliação"
}

/**
 * Selectors
 */
// Global selectors
var PAGE_LOADER = "#componentPageLoaderCud2";
var PAGE_CONTAINER_SELECTOR = "#pageContainerCud2";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Cud2";
var SIDEBAR_SELECTOR = "#componentSidebarCud2";
var FOOTER_SELECTOR = "#componentFooterCud2";
var PAGE_CONTENT_CONTAINER_SELECTORS = {
    summary: "#summaryContainerCud2",
    problemSituation: "#problemSituationContainerCud2",
    book: "#bookContainerCud2",
    material: "#materialContainerCud2",
    evaluation: "#evaluationContainerCud2"
}

var PAGE_CONTENT_TITLE_SELECTOR = ".page-title h3";
var MODAL_SIMPLE_MESSAGE_SELECTOR = "#componentModalSimpleMessageCud2";
var EVAL_FORM_PAGINATOR_SELECTOR = "#evalFormPaginatorCud2";
var VIDEO_DISPLAYER = "#componentVideoDisplayerCud2";

// Summary selectors
var SUMMARY_SUB_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex1Cud2",
    SUBSEC_2: "#cex2Cud2",
    SUBSEC_3: "#cex3Cud2"
}

// Problem Situation selectors
var FSM_PROBLEM_SITUATION_SELECTOR = "#componentProblemSituationCud2";
var FSM_MODAL_PROBLEM_SITUATION_SELECTOR = "#componentProblemSituationFSMsgCud2";

// Book selectors
var FSM_PDF_VIEWER_SELECTOR = "#componentPdfViewerCud2";
var FSM_PDF_VIEWER_MODAL_SELECTOR = "#componentPdfViewerFSMCud2";

// Complementary Materials selectors
var CM_SECTIONS_SELECTORS = {
    SUBSEC_1: "#cex4Cud2",
    SUBSEC_2: "#cex5Cud2",
    SUBSEC_3: "#cex6Cud2",
    SUBSEC_4: "#cex7Cud2",
    SUBSEC_5: "#cex8Cud2"
}
var CM_CS_HELTH_MINISTRY_SELECTOR = "#componentCmCsMinistryHealthCud2";
var CM_CS_RELATED_ARTICLES_SELECTOR = "#componentCmCsRelatedArticlesCud2";
var CM_CS_INFO_GRAPHIC_SELECTOR = "#componentCmCsInfoGraphicsCud2";
var CM_CS_VIDEOS_SELECTOR = "#componentCmVsCud2";
var CM_CS_WEB_CONTENT_SELECTOR = "#componentCmWebContentCud2";

// Evaluation selectors
var EVALUATION_FORM_SELECTOR = {
    PAGE_1: "#evalFormCud2Page1",
    PAGE_2: "#evalFormCud2Page2",
    PAGE_3: "#evalFormCud2Page3",
    PAGE_4: "#evalFormCud2Page4",
    PAGE_5: "#evalFormCud2Page5",
    PAGE_6: "#evalFormCud2Page6",
    PAGE_7: "#evalFormCud2Page7",
    PAGE_8: "#evalFormCud2Page8",
    PAGE_9: "#evalFormCud2Page9",
    PAGE_10: "#evalFormCud2Page10"
}
var PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM = "models_question_";
var PREFIX_PAGE_EVAL_FORM = "q_";

/**
 * Page objects
 */
// Global
var previousUIMode = undefined;

// Page components
var pageLoaderCud2 = new PageLoader();
var headerTop2Cud2 = new HeaderTop2();
var sidebarCud2 = new Sidebar();
var mainContainer = new MainContainer();
var footerCud2 = new Footer();
var modalSimpleMessageCud2 = new ModalSimpleMessage();

// Summary global items
var summaryModuleOverview = undefined;
var mosquitoControllingTools = undefined;
var environmentalActions = undefined;

// Problem situation items
var problemSituationImgViewerCud2 = undefined;

// Book section item
var pdfViewerCud2 = new PdfViewer();

// Complementary Materials
var complMatHealthMinistryCud2 = undefined;
var ministryHealthSubSec = undefined;
var complMatRelatedArticlesCud2 = undefined;
var relatedArticlesSubSec = undefined;
var complMatInfoGraphicsCud2 = undefined;
var complMatVideosCud2 = undefined;
var videosSubSec = undefined;
var complMatWebContentCud2 = undefined;
var webContentSubSec = undefined;

// Evaluation
var EVAL_FORM_SET_SIZE = 10;
var EVAL_FORM_NUM_QUESTIONS_PER_SET = 2;

var evalFormPaginatorCud2 = undefined;
var selectedEvaluationPage = 1;
var previouslySelectedQuestions = {
    "q_1" : 0,
    "q_2" : 0,
    "q_3" : 0,
    "q_4" : 0,
    "q_5" : 0,
    "q_6" : 0,
    "q_7" : 0,
    "q_8" : 0,
    "q_9" : 0,
    "q_10" : 0
}
var question1Obj = new SingleAnswerQuestion();
var question2Obj = new SingleAnswerQuestion();
var question3Obj = new SingleAnswerQuestion();
var question4Obj = new SingleAnswerQuestion();
var question5Obj = new SingleAnswerQuestion();
var question6Obj = new SingleAnswerQuestion();
var question7Obj = new SingleAnswerQuestion();
var question8Obj = new SingleAnswerQuestion();
var question9Obj = new SingleAnswerQuestion();
var question10Obj = new SingleAnswerQuestion();
var modelsQuestions = {
    "models_question_1" : undefined,
    "models_question_2" : undefined,
    "models_question_3" : undefined,
    "models_question_4" : undefined,
    "models_question_5" : undefined,
    "models_question_6" : undefined,
    "models_question_7" : undefined,
    "models_question_8" : undefined,
    "models_question_9" : undefined,
    "models_question_10" : undefined
}

/**
 * Initialization
 */
$(document).ready(function () {
    gUi.initEvents();
    gPages.setCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_2);
    previousUIMode = gUi.isOnMobileViewMode();

    initData();
    initUI();
    initEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * Data
 */
function initData() {
    gData.loadUserCourse();
}

/**
 * UI
 */
function initUI() {
    // Init the UI of the base elements
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderCud2, gMsgs.DEFAULT_LOADING_MESSAGE, "", function () {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
        initPageSections();
        gUi.finishUiInit();
    });

    // Init the UI of the main components of the page
    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
    initPassedUnitModal();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Cud2.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_UNIT_DETAILS_2_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            1);

        headerTop2Cud2.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarCud2.model;
        dataModel.pageId = gPages.IDS.PAGE_COURSE_UNIT_DETAILS_2_ID;
        dataModel.title = "MÓDULO 2";
        dataModel.bgFilter = gUi.BLUE_FILTER;
        dataModel.unitNumber = "2";
        dataModel.subTitle = "Controle de mosquitos";
        dataModel.summaryTitle = "Objetivos da unidade";
        dataModel.summaryItems = [
            {
                name: '<b>Objetivos</b>: ...'
            },
            {
                name: '<b>Carga Horaria</b>: <span class="course-work-load">' + 10 + '</span> horas/aula'
            },
            {
                name: '<b>Recursos Educacionais</b>: texto de apoio produzido para o curso a partir de documentos do MS; vídeos com especialistas sobre o tema; biblioteca virtual.'
            },
            {
                name: '<b>Avaliação</b>: Atividades objetivas.'
            }
        ];
        dataModel.mainMenuItems = [
            {
                id: SIDEBAR_MENU_SECTION_IDS.summary,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.summary,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_SUMMARY, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_SUMMARY,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_SUMMARY,
                hasSubItems: false,
                itemSelected: true,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.problemSituation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.problemSituation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_PROBLEM_SITUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_PROBLEM_SITUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.book,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.book,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_BOOK, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_BOOK, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_BOOK,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_BOOK,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.material,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.material,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_COMPLEMENTARY_MATERIALS,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            },
            {
                id: SIDEBAR_MENU_SECTION_IDS.evaluation,
                mainMenuName: SIDEBAR_MENU_SECTION_NAMES.evaluation,
                menuOptImgSelectedSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION, 
                menuOptImgSelected: gImages.ICO_PNG_SIDEBAR_EVALUATION, 
                menuOptImgSvg: gImages.ICO_SVG_SIDEBAR_EVALUATION,
                menuOptImg: gImages.ICO_PNG_SIDEBAR_EVALUATION,
                hasSubItems: false,
                itemSelected: false,
                selectableClass: gUi.CLASS_SELECTABLE,
                subItems: []
            }
        ];
        dataModel.onItemSelectedCallback = onSidebarItemSelected;
        dataModel.isMinified = true;

        sidebarCud2.build(SIDEBAR_SELECTOR, dataModel, onSidebarItemSelected);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarCud2);
        mainContainer.initGlogalEvents();
        mainContainer.setSidebarToggleEventCallback(onSidebarToggle);

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        } else {
            mainContainer.expandSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init MainContainer " + err);
        }
    }
}

function onSidebarToggle(sidebarStatus) {
    // ..
}

function onSidebarItemSelected() {
    try {
        $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.book).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.material).addClass(gUi.CLASS_HIDDEN);
        $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).addClass(gUi.CLASS_HIDDEN);

        resetSummaryPageSection();
        resetProblemSituationPageSection();
        resetBookPageSection();
        resetComplementaryMaterialsPageSection();        

        switch (sidebarCud2.getSelectedSection()) {

            case SIDEBAR_MENU_SECTION_IDS.summary: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.summary);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.summary).removeClass(gUi.CLASS_HIDDEN);

                initSummaryPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.problemSituation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.problemSituation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.problemSituation).removeClass(gUi.CLASS_HIDDEN);

                initProblemSituationPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.book: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.book);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.book).removeClass(gUi.CLASS_HIDDEN);

                pdfViewerCud2.setTitle("Capítulo da Unidade 2");
                pdfViewerCud2.setSubTitle(gFiles.PDF_COURSE_BOOK.TITLE);
                pdfViewerCud2.setPdfUrl(gFiles.PDF_COURSE_BOOK.URL);
                pdfViewerCud2.setInitialPage(41);
                pdfViewerCud2.rebuild();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.material: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.material);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.material).removeClass(gUi.CLASS_HIDDEN);

                initComplementaryMaterialsPageSection();

                break;
            }

            case SIDEBAR_MENU_SECTION_IDS.evaluation: {
                $(PAGE_CONTENT_TITLE_SELECTOR).html(SIDEBAR_MENU_SECTION_NAMES.evaluation);
                $(PAGE_CONTENT_CONTAINER_SELECTORS.evaluation).removeClass(gUi.CLASS_HIDDEN);

                if (typeof (modalSimpleMessageCud2 !== "undefined") && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud2.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }

                break;
            }
        }

        if (gUi.isOnMobileViewMode() || gUi.isOnTabletViewMode()) {
            mainContainer.shrinkSidebar();
        }

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to select Sidebar menu option " + err);
        }
    }
}

function initFooter() {
    try {
        gUi.setHasFixedFooter(false);
        footerCud2.useDefaultFooter(true);
        footerCud2.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log(TAG + " Failed to init Footer " + err);
        }
    }
}

function initPassedUnitModal() {
    var model = {
        img: gImages.ICO_GIF_UNIT_2_SUCCESS,
        title: gMsgs.MSG_CONGRATS,
        msg: gMsgs.MSG_FINISHED_UNIT_N + " " + UNIT_NUMBER,
        msgSupport: "",
        unitNumber: UNIT_NUMBER
    }
    modalSimpleMessageCud2.build(MODAL_SIMPLE_MESSAGE_SELECTOR, model);
}

/**
 * Page Sections
 */
function initPageSections() {
    resetSummaryPageSection();
    resetProblemSituationPageSection();
    resetBookPageSection();
    resetComplementaryMaterialsPageSection();

    initSummaryPageSection();
    initBookPageSection();
    initEvaluationSection();
}

/**
 * Page section: Summary
 */
function resetSummaryPageSection() {
    try {

        if (summaryModuleOverview != null && summaryModuleOverview != undefined) {
            summaryModuleOverview.destroy();
            summaryModuleOverview = null;

            mosquitoControllingTools.destroy();
            mosquitoControllingTools = null;

            environmentalActions.destroy();
            environmentalActions = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetSummaryPageSection " + err);
    }
}

function initSummaryPageSection() {
    // Subsection 1
    summaryModuleOverview = new ContentExpander();
    summaryModuleOverview.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Controle de mosquitos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_1).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 2
    mosquitoControllingTools = new ContentExpander();
    mosquitoControllingTools.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Ferramentas para o controle de mosquitos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_2).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 3
    environmentalActions = new ContentExpander();
    environmentalActions.build(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_3, {
        title: "Ações ambientais",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(SUMMARY_SUB_SECTIONS_SELECTORS.SUBSEC_3).removeClass(gUi.CLASS_HIDDEN);
}

/**
 * Page section: Problem situation
 */
function resetProblemSituationPageSection() {
    try {

        if (problemSituationImgViewerCud2 == null && problemSituationImgViewerCud2 != undefined) {
            problemSituationImgViewerCud2.destroy();
            problemSituationImgViewerCud2 = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetProblemSituationPageSection " + err);
    }
}

function initProblemSituationPageSection() {
    var PREFIX_PROBLEM_SITUATION_IMG = "images/unit_2_problem_situation_slide_";

    var model = {
        title: "Ainda tem larvas aqui? - Parte 2",
        subTitle: "<b>Objetivos:</b><br><ul><li>Conscientizar sobre a importância do trabalho dos agentes de endemias.</li><li>Dar noções sobre criadouros e resistência de larvas de mosquitos a inseticidas.</li><li>Fortalecer a importância da tríade serviço de saúde, pesquisa e população nas ações de redução populacional dos mosquitos no ambiente.</li></ul>",
        selectedSlidePos: 0,
        urlSelectedImgPng: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
        urlSelectedImgSvg: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
        urlSvgIcoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
        urlPngIcoBtnClose: gImages.ICO_PNG_CLOSE_BLACK,
        urlSvgIcoBtnFullScreen: gImages.ICO_SVG_FULL_SCREEN_BLACK,
        urlPngIcoBtnFullScreen: gImages.ICO_PNG_FULL_SCREEN_BLACK,
        urlSvgPrevBtn: gImages.ICO_SVG_ARROW_RIGHT_BLACK,
        urlPngPrevBtn: gImages.ICO_PNG_ARROW_RIGHT_BLACK,
        urlSvgNextBtn: gImages.ICO_SVG_ARROW_RIGHT_BLACK,
        urlPngNextBtn: gImages.ICO_PNG_ARROW_RIGHT_BLACK,
        images: [
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "1.png",
                selected: "selected",
                slidePos: 0
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "2.png",
                selected: "",
                slidePos: 1
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "3.png",
                selected: "",
                slidePos: 2
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "4.png",
                selected: "",
                slidePos: 3
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "5.png",
                selected: "",
                slidePos: 4
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "6.png",
                selected: "",
                slidePos: 5
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "7.png",
                selected: "",
                slidePos: 6
            },
            {
                urlSvg: "",
                urlPng: PREFIX_PROBLEM_SITUATION_IMG + "8.png",
                selected: "",
                slidePos: 7
            }
        ]
    };

    problemSituationImgViewerCud2 = new ImgsViewer();
    problemSituationImgViewerCud2.build(FSM_PROBLEM_SITUATION_SELECTOR, FSM_MODAL_PROBLEM_SITUATION_SELECTOR, model);
}

/**
 * Page section: Book
 */
function resetBookPageSection() {
    try {

        if (pdfViewerCud2 != null && pdfViewerCud2 != undefined) {
            pdfViewerCud2.setTitle("Capítulo da Unidade 2");
            pdfViewerCud2.setSubTitle(gFiles.PDF_COURSE_BOOK.TITLE);
            pdfViewerCud2.setPdfUrl(gFiles.PDF_COURSE_BOOK.URL);
            pdfViewerCud2.setInitialPage(1);
            pdfViewerCud2.rebuild();
        }
    } catch(err) {
        console.log(TAG + " Failed to resetBookPageSection " + err);
    }
}

function initBookPageSection() {
    var model = {
        title: "Capítulo da Unidade 2",
        subTitle: gFiles.PDF_COURSE_BOOK_COVER.SUB_TITLE,
        pdfFileUrl: gFiles.PDF_COURSE_BOOK_COVER.URL,
        fileType: gFiles.FILE_TYPE.TEXT,
        initialPageNum: 1,
        urlSvgIcoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
        urlPngIcoBtnClose: gImages.ICO_PNG_CLOSE_BLACK,
        urlSvgIcoBtnFullScreen: gImages.ICO_SVG_FULL_SCREEN_BLACK,
        urlPngIcoBtnFullScreen: gImages.ICO_PNG_FULL_SCREEN_BLACK
    }

    pdfViewerCud2.build(FSM_PDF_VIEWER_SELECTOR, FSM_PDF_VIEWER_MODAL_SELECTOR, model);
}

/**
 * Page section: Complementary materials
 */
function resetComplementaryMaterialsPageSection() {
    try {

        if (complMatHealthMinistryCud2 != null && complMatHealthMinistryCud2 != undefined) {
            complMatHealthMinistryCud2.destroy();
            complMatHealthMinistryCud2 = null;

            ministryHealthSubSec.destroy();
            ministryHealthSubSec = null;

            complMatRelatedArticlesCud2.destroy();
            complMatRelatedArticlesCud2 = null;

            relatedArticlesSubSec.destroy();
            relatedArticlesSubSec = null;

            complMatInfoGraphicsCud2.destroy();
            complMatInfoGraphicsCud2 = null;

            infoGraphicsSubSec.destroy();
            infoGraphicsSubSec = null;

            complMatVideosCud2.destroy();
            complMatVideosCud2 = null;

            videosSubSec.destroy();
            videosSubSec = null;

            complMatWebContentCud2.destroy();
            complMatWebContentCud2 = null;

            webContentSubSec.destroy();
            webContentSubSec = null;
        }

    } catch(err) {
        console.log(TAG + " Failed to resetComplementaryMaterialsPageSection " + err);
    }
}

function initComplementaryMaterialsPageSection() {
    // Subsection 2 - Materials from the Ministry of Health
    var modelMinistryHealthMaterials = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: []
    };

    complMatHealthMinistryCud2 = new ContentSelector();
    complMatHealthMinistryCud2.build(CM_CS_HELTH_MINISTRY_SELECTOR, onReadableObjectSelected, modelMinistryHealthMaterials);

    ministryHealthSubSec = new ContentExpander();
    ministryHealthSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_1, {
        title: "Materiais do Ministério da Saúde",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 2 - Related Articles
    var modelRelatedArticles = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gFiles.PDF_ART_VECTOR_BORNE_DISEASES_GUEDES_2010.TITLE,
                author: gFiles.PDF_ART_VECTOR_BORNE_DISEASES_GUEDES_2010.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_VECTOR_BORNE_DISEASES_GUEDES_2010.URL,
                internetUrl: gFiles.PDF_ART_VECTOR_BORNE_DISEASES_GUEDES_2010.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 1,
                title: gFiles.PDF_ART_PARASITES_VECTORS_2013_ARAUJO.TITLE,
                author: gFiles.PDF_ART_PARASITES_VECTORS_2013_ARAUJO.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_PARASITES_VECTORS_2013_ARAUJO.URL,
                internetUrl: gFiles.PDF_ART_PARASITES_VECTORS_2013_ARAUJO.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 2,
                title: gFiles.PDF_ART_DEVELOPING_NEW_APPROACHES_PREVENTING_AEDES.TITLE,
                author: gFiles.PDF_ART_DEVELOPING_NEW_APPROACHES_PREVENTING_AEDES.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_DEVELOPING_NEW_APPROACHES_PREVENTING_AEDES.URL,
                internetUrl: gFiles.PDF_ART_DEVELOPING_NEW_APPROACHES_PREVENTING_AEDES.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 3,
                title: gFiles.PDF_ART_CHARACTERIZATION_SPACIAL_TEMPORAL_DYNAMICS_DENGUE.TITLE,
                author: gFiles.PDF_ART_CHARACTERIZATION_SPACIAL_TEMPORAL_DYNAMICS_DENGUE.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_CHARACTERIZATION_SPACIAL_TEMPORAL_DYNAMICS_DENGUE.URL,
                internetUrl: gFiles.PDF_ART_CHARACTERIZATION_SPACIAL_TEMPORAL_DYNAMICS_DENGUE.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 4,
                title: gFiles.PDF_ART_DENGUE_BULLETIN_2003_SANTOS.TITLE,
                author: gFiles.PDF_ART_DENGUE_BULLETIN_2003_SANTOS.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_DENGUE_BULLETIN_2003_SANTOS.URL,
                internetUrl: gFiles.PDF_ART_DENGUE_BULLETIN_2003_SANTOS.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            },
            {
                id: 5,
                title: gFiles.PDF_ART_VECTOR_SURVEILLANCE_DENGUE.TITLE,
                author: gFiles.PDF_ART_VECTOR_SURVEILLANCE_DENGUE.SUB_TITLE,
                fileUrl: gFiles.PDF_ART_VECTOR_SURVEILLANCE_DENGUE.URL,
                internetUrl: gFiles.PDF_ART_VECTOR_SURVEILLANCE_DENGUE.EXT_URL,
                fileType: gFiles.FILE_TYPE.TEXT,
                initialPage: 1,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: gImages.ICO_SVG_READ_WHITE,
                icoReadPng: gImages.ICO_PNG_READ_WHITE,
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatRelatedArticlesCud2 = new ContentSelector();
    complMatRelatedArticlesCud2.build(CM_CS_RELATED_ARTICLES_SELECTOR, onReadableObjectSelected, modelRelatedArticles);

    relatedArticlesSubSec = new ContentExpander();
    relatedArticlesSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_2, {
        title: "Artigos Relacionados",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_2).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 3 - Info Graphics
    var modelInfoGraphics = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: []
    }

    complMatInfoGraphicsCud2 = new ContentSelector();
    complMatInfoGraphicsCud2.build(CM_CS_INFO_GRAPHIC_SELECTOR, onReadableObjectSelected, modelInfoGraphics);

    infoGraphicsSubSec = new ContentExpander();
    infoGraphicsSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_3, {
        title: "Infográficos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    // Subsection 4 - Videos
    var modelVideos = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        videosList: [
            {
                id: 0,
                title: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.TITLE,
                subTitle: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.SUB_TITLE,
                fileUrl: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.URL,
                internetUrl: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.EXT_URL,
                videoResolution: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.RESOLUTION,
                coverImg: gFiles.VIDEO_AEDES_SIMPLES_MOSQUITO.COVER,
                imgSvgVideoSrc: gImages.ICO_SVG_NEED_INTERNET,
                imgPngVideoSrc: gImages.ICO_PNG_NEED_INTERNET,
                videoSrcText: "Precisa de internet",
                imgSvgFilterIcon: gImages.ICO_SVG_PLAY_ARROW_WHITE,
                imgPngFilterIcon: gImages.ICO_PNG_PLAY_ARROW_WHITE,
                filterText: "Assista"
            }
        ]
    }

    complMatVideosCud2 = new VideoSelector();
    complMatVideosCud2.build(CM_CS_VIDEOS_SELECTOR, onVideoObjectSelected, modelVideos);

    videosSubSec = new ContentExpander();
    videosSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_4, {
        title: "Vídeos",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });
    $(CM_SECTIONS_SELECTORS.SUBSEC_4).removeClass(gUi.CLASS_HIDDEN);

    // Subsection 5 - Web content
    var modelWebContent = {
        hasHeader: false,
        hasFooter: false,
        title: "",
        footerNote: "",
        contentList: [
            {
                id: 0,
                title: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.TITLE,
                author: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.SUB_TITLE,
                fileUrl: "",
                internetUrl: gExtResources.EXT_RESOURCE_GLOBO_TECNICA_ESTERILIZACAO_AEDES.URL,
                publisherIconSvg: "",
                publisherIconPng: "",
                icoReadSvg: "",
                icoReadPng: "",
                icoInternetSvg: gImages.ICO_SVG_NEED_INTERNET,
                icoInternetPng: gImages.ICO_SVG_NEED_INTERNET
            }
        ]
    }

    complMatWebContentCud2 = new ContentSelector();
    complMatWebContentCud2.build(CM_CS_WEB_CONTENT_SELECTOR, onWebObjectSelected, modelWebContent);

    webContentSubSec = new ContentExpander();
    webContentSubSec.build(CM_SECTIONS_SELECTORS.SUBSEC_5, {
        title: "Para Acessar na Web",
        icon_svg: gImages.ICO_SVG_ARROW_DOWN_WHITE
    });

    $(CM_SECTIONS_SELECTORS.SUBSEC_5).removeClass(gUi.CLASS_HIDDEN);
}

function onReadableObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {

    if (fileUrl != undefined && fileUrl !== '') {
        pdfViewerCud2.setTitle("Materiais Complementares");

        if (title !== undefined) {
            pdfViewerCud2.setSubTitle(title);
        }

        if (fileUrl != undefined) {
            pdfViewerCud2.setPdfUrl(fileUrl);
        }

        if (initialPage != undefined) {
            pdfViewerCud2.setInitialPage(initialPage);
        }

        pdfViewerCud2.rebuild();
        pdfViewerCud2.show();
    }

    if (internetUrl != undefined && internetUrl !== '') {
        gData.onWebObjectSelected(internetUrl);
    }

    if (fileType != undefined && fileType != '' && title != undefined && title != '' && internetUrl === '') {
        gApiIntegration.setDownload(fileType, title);
    }
}

function onVideoObjectSelected(fileUrl, internetUrl, title, subTitle, videoResolution) {
    try {
        var videoContainerWidth = "100%";
        var selectedUrl = '';
        var localVideo = false;

        if (fileUrl != undefined && fileUrl !== '') {
            selectedUrl = fileUrl;
            localVideo = true;

            // Update the url of the video displayer and show it
            var complMatModalVideoCud2 = new ModalSimpleMessage();

            var model = {
                title: title,
                msg: subTitle,
                localVideo: localVideo,
                unitNumber: 2,
                videoContainerSize: videoContainerWidth,
                videoUrl: selectedUrl,
                videoResolution: videoResolution,
                msgSupport: '',
                videoSupportMsg: gMsgs.BROWSER_DOESNT_SUPPORT_THIS_VIDEO
            }

            complMatModalVideoCud2.build(VIDEO_DISPLAYER, model);
            complMatModalVideoCud2.show();

        } else if (internetUrl != undefined && internetUrl !== '') {
            gApiIntegration.setExternalLink(internetUrl);

            var win = window.open(internetUrl, '_blank');
            win.focus();

            console.log("internetUrl: " + internetUrl);

        }
    } catch (err) {
        console.log(TAG + " Failed to display video " + err);
    }
}

function onWebObjectSelected(fileUrl, internetUrl, title, initialPage, fileType) {
    gData.onWebObjectSelected(internetUrl);
}

/**
 * Page section: Evaluation
 */
function initEvaluationSection() {
    setTimeout(function() {
        // Build the questions' models randomly
        buildQuestionsModels();

        selectQuestionRandomly(1);
        selectQuestionRandomly(2);
        selectQuestionRandomly(3);
        selectQuestionRandomly(4);
        selectQuestionRandomly(5);
        selectQuestionRandomly(6);
        selectQuestionRandomly(7);
        selectQuestionRandomly(8);
        selectQuestionRandomly(9);
        selectQuestionRandomly(10);

        // Build the UI of the evaluation form
        evalFormPaginatorCud2 = new Paginator();
        evalFormPaginatorCud2.build(EVAL_FORM_PAGINATOR_SELECTOR, onEvaluationPageSelected);

    }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE / 2);
}

function buildQuestionsModels() {
    try {
        var Questions = jQuery.extend({}, new Question());
        var unit2 = gData.getCourse().units[1];

        // Build the questions of the evaluation form
        // First set of questions
        // Question 1
        var question1Model = unit2.unitQuestions[0];
        question1Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question1Model.staticAlternatives);
        var modelQuestion1 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question1Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 1,
            questionStatus: question1Model.questionStatus,
            enunciate: question1Model.enunciate,
            rightAlternative: question1Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            alternatives: question1Model.alternatives,
            staticAlternatives: question1Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 2
        var question2Model = unit2.unitQuestions[1];
        question2Model.alternatives = Questions.getAlphabetLetterForAlternatives(question2Model.alternatives, true);
        var modelQuestion2 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question2Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 2,
            questionStatus: question2Model.questionStatus,
            enunciate: question2Model.enunciate,
            rightAlternative: question2Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question2Model.alternatives,
            staticAlternatives: question2Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 3
        var question3Model = unit2.unitQuestions[2];
        question3Model.alternatives = Questions.getAlphabetLetterForAlternatives(question3Model.alternatives, true);
        var modelQuestion3 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question3Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 3,
            questionStatus: question3Model.questionStatus,
            enunciate: question3Model.enunciate,
            rightAlternative: question3Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question3Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 4
        var question4Model = unit2.unitQuestions[3];
        question4Model.alternatives = Questions.getAlphabetLetterForAlternatives(question4Model.alternatives, true);
        var modelQuestion4 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question4Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 4,
            questionStatus: question4Model.questionStatus,
            enunciate: question4Model.enunciate,
            rightAlternative: question4Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question4Model.alternatives,
            staticAlternatives: question4Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }
        
        // Question 5
        var question5Model = unit2.unitQuestions[4];
        question5Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question5Model.staticAlternatives);
        var modelQuestion5 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question5Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 5,
            questionStatus: question5Model.questionStatus,
            enunciate: question5Model.enunciate,
            rightAlternative: question5Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question5Model.staticAlternatives,
            alternatives: question5Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 6
        var question6Model = unit2.unitQuestions[5];
        question6Model.alternatives = Questions.getAlphabetLetterForAlternatives(question6Model.alternatives, true);
        var modelQuestion6 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question6Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 6,
            questionStatus: question6Model.questionStatus,
            enunciate: question6Model.enunciate,
            rightAlternative: question6Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question6Model.alternatives,
            staticAlternatives: question6Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 7
        var question7Model = unit2.unitQuestions[6];
        question7Model.alternatives = Questions.getAlphabetLetterForAlternatives(question7Model.alternatives, true);
        var modelQuestion7 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question7Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 7,
            questionStatus: question7Model.questionStatus,
            enunciate: question7Model.enunciate,
            rightAlternative: question7Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question7Model.alternatives,
            staticAlternatives: question7Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 8
        var question8Model = unit2.unitQuestions[7];
        question8Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question8Model.staticAlternatives);
        var modelQuestion8 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question8Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 8,
            questionStatus: question8Model.questionStatus,
            enunciate: question8Model.enunciate,
            rightAlternative: question8Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question8Model.staticAlternatives,
            alternatives: question8Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 9
        var question9Model = unit2.unitQuestions[8];
        question9Model.alternatives = Questions.getAlphabetLetterForAlternatives(question9Model.alternatives, true);
        var modelQuestion9 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question9Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 9,
            questionStatus: question9Model.questionStatus,
            enunciate: question9Model.enunciate,
            rightAlternative: question9Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question9Model.alternatives,
            staticAlternatives: question9Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }
        
        // Question 10
        var question10Model = unit2.unitQuestions[9];
        question10Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question10Model.staticAlternatives);
        var modelQuestion10 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question10Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 10,
            questionStatus: question10Model.questionStatus,
            enunciate: question10Model.enunciate,
            rightAlternative: question10Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question10Model.staticAlternatives,
            alternatives: question10Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Second set of questions
        // Question 11
        var question11Model = unit2.unitQuestions[10];
        question11Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question11Model.staticAlternatives);
        var modelQuestion11 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question11Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 1,
            questionStatus: question11Model.questionStatus,
            enunciate: question11Model.enunciate,
            rightAlternative: question11Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question11Model.staticAlternatives,
            alternatives: question11Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 12
        var question12Model = unit2.unitQuestions[11];
        question12Model.alternatives = Questions.getAlphabetLetterForAlternatives(question12Model.alternatives, true);
        var modelQuestion12 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question12Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 2,
            questionStatus: question12Model.questionStatus,
            enunciate: question12Model.enunciate,
            rightAlternative: question12Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question12Model.alternatives,
            staticAlternatives: question12Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 13
        var question13Model = unit2.unitQuestions[12];
        question13Model.alternatives = Questions.getAlphabetLetterForAlternatives(question13Model.alternatives, true);
        var modelQuestion13 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question13Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 3,
            questionStatus: question13Model.questionStatus,
            enunciate: question13Model.enunciate,
            rightAlternative: question13Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question13Model.alternatives,
            staticAlternatives: question13Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 14
        var question14Model = unit2.unitQuestions[13];
        question14Model.alternatives = Questions.getAlphabetLetterForAlternatives(question14Model.alternatives, true);
        var modelQuestion14 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question14Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 4,
            questionStatus: question14Model.questionStatus,
            enunciate: question14Model.enunciate,
            rightAlternative: question14Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question14Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }
        
        // Question 15
        var question15Model = unit2.unitQuestions[14];
        question15Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question15Model.staticAlternatives);
        var modelQuestion15 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question15Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 5,
            questionStatus: question15Model.questionStatus,
            enunciate: question15Model.enunciate,
            rightAlternative: question15Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question15Model.staticAlternatives,
            alternatives: question15Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 16
        var question16Model = unit2.unitQuestions[15];
        question16Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question16Model.staticAlternatives);
        var modelQuestion16 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question16Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 6,
            questionStatus: question16Model.questionStatus,
            enunciate: question16Model.enunciate,
            rightAlternative: question16Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question16Model.staticAlternatives,
            alternatives: question16Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 17
        var question17Model = unit2.unitQuestions[16];
        question17Model.alternatives = Questions.getAlphabetLetterForAlternatives(question17Model.alternatives, true);
        var modelQuestion17 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question17Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 7,
            questionStatus: question17Model.questionStatus,
            enunciate: question17Model.enunciate,
            rightAlternative: question17Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question17Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 18
        var question18Model = unit2.unitQuestions[17];
        question18Model.staticAlternatives = Questions.addRomanAlgarismsForAlternatives(question18Model.staticAlternatives);
        var modelQuestion18 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question18Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 8,
            questionStatus: question18Model.questionStatus,
            enunciate: question18Model.enunciate,
            rightAlternative: question18Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: true,
            staticAlternatives: question18Model.staticAlternatives,
            alternatives: question18Model.alternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ROMANIZE,
            shuffleAlternatives: false
        }

        // Question 19
        var question19Model = unit2.unitQuestions[18];
        question19Model.alternatives = Questions.getAlphabetLetterForAlternatives(question19Model.alternatives, true);
        var modelQuestion19 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question19Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 9,
            questionStatus: question19Model.questionStatus,
            enunciate: question19Model.enunciate,
            rightAlternative: question19Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question19Model.alternatives,
            staticAlternatives: question19Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Question 20
        var question20Model = unit2.unitQuestions[19];
        question20Model.alternatives = Questions.getAlphabetLetterForAlternatives(question20Model.alternatives, true);
        var modelQuestion20 = {
            lang: gData.LANG.PT,
            btnTryAgainText: gMsgs.DEFAULT_LBL_BTN_TRY_AGAIN,
            btnSubmitText: gMsgs.DEFAULT_LBL_BTN_SUBMIT_ANSWER,
            unitId: unit2.unitId,
            questionId: question20Model.questionId,
            unitNumber: unit2.unitId,
            questionNumber: 10,
            questionStatus: question20Model.questionStatus,
            enunciate: question20Model.enunciate,
            rightAlternative: question20Model.rightAlternativeId,
            questionErrorMsg: gData.DEFAULT_MSG_WRONG_QUESTION_ANSWER,
            hasStaticAlternatives: false,
            alternatives: question20Model.alternatives,
            staticAlternatives: question20Model.staticAlternatives,
            alternativeLetterTransformation: Questions.ALTERNATIVE_LETTERS_TRANSFORMATION.ALPHABET,
            shuffleAlternatives: false
        }

        // Build randomly the questions of the evaludation form
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "1"] = [modelQuestion1, modelQuestion11];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "2"] = [modelQuestion2, modelQuestion12];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "3"] = [modelQuestion3, modelQuestion13];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "4"] = [modelQuestion4, modelQuestion14];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "5"] = [modelQuestion5, modelQuestion15];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "6"] = [modelQuestion6, modelQuestion16];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "7"] = [modelQuestion7, modelQuestion17];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "8"] = [modelQuestion8, modelQuestion18];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "9"] = [modelQuestion9, modelQuestion19];
        modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + "10"] = [modelQuestion10, modelQuestion20];

        // Ensure that if one question of the possible questions of a page of the evaluation form
        // has the status "right", all the other questions of that same page will have the same status
        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelsQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];
            var statusQuestionNRigth = false;

            for (var j = 0; j < modelsQuestion.length; j++) {

                if (modelsQuestion[j].questionStatus == gData.QUESTION_STATUS.RIGHT) {
                    statusQuestionNRigth = true;
                    break;
                }
            }

            if (statusQuestionNRigth) {
                
                for (var j = 0; j < modelsQuestion.length; j++) {
                    modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i][j].questionStatus = gData.QUESTION_STATUS.RIGHT;
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to buildQuestionsModels " + err);
    }
}

function selectQuestionRandomly(questionNumber) {
    try {
        var modelsSelectedQuestion = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + questionNumber];
        var selectedModel = modelsSelectedQuestion[getRandomNumberForQuestion(questionNumber)];
        
        switch (questionNumber) {

            case 1:
                question1Obj.build(EVALUATION_FORM_SELECTOR.PAGE_1, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
    
            case 2:
                question2Obj.build(EVALUATION_FORM_SELECTOR.PAGE_2, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
            
            case 3:
                question3Obj.build(EVALUATION_FORM_SELECTOR.PAGE_3, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 4:
                question4Obj.build(EVALUATION_FORM_SELECTOR.PAGE_4, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;
        
            case 5:
                question5Obj.build(EVALUATION_FORM_SELECTOR.PAGE_5, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 6:
                question6Obj.build(EVALUATION_FORM_SELECTOR.PAGE_6, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 7:
                question7Obj.build(EVALUATION_FORM_SELECTOR.PAGE_7, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 8:
                question8Obj.build(EVALUATION_FORM_SELECTOR.PAGE_8, 
                                   selectedModel, 
                                   onQuestionAlternativeSelected, 
                                   onClickBtnTryAgain);
                break;

            case 9:
                question9Obj.build(EVALUATION_FORM_SELECTOR.PAGE_9, 
                                    selectedModel, 
                                    onQuestionAlternativeSelected, 
                                    onClickBtnTryAgain);
                break;

            case 10:
                question10Obj.build(EVALUATION_FORM_SELECTOR.PAGE_10, 
                                    selectedModel, 
                                    onQuestionAlternativeSelected, 
                                    onClickBtnTryAgain);
                break;
        }

    } catch (err) {
        console.log(TAG + " Failed to selectQuestionRandmonly " + err);
    }
}

function getRandomNumberForQuestion(questionNumber) {
    try {
        var selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);

        while (previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] === selectedQuestion) {
            selectedQuestion = Math.floor(Math.random() * EVAL_FORM_NUM_QUESTIONS_PER_SET);
        }

        previouslySelectedQuestions[PREFIX_PAGE_EVAL_FORM + questionNumber] = selectedQuestion;

        return selectedQuestion;

    } catch (err) {
        console.log(TAG + " Failed to getRandomNumberForQuestion: " + err);
    }
}

function onEvaluationPageSelected(pageNumber) {
    try {
        // Verify if the user has completed the current module
        selectedEvaluationPage = pageNumber;

        if (selectedEvaluationPage === EVAL_FORM_SET_SIZE && gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
            modalSimpleMessageCud2.verifyConditionsAndDisplayModal(UNIT_NUMBER);
        }

        // Rebuild the selected question updating it randomly
        selectQuestionRandomly(pageNumber);

    } catch (err) {
        console.log(TAG + " Failed to save the index of the selected evaluation page " + err);
    }
}

function onQuestionAlternativeSelected(unitId, questionId, questionStatus, submit, selectedAlternative) {
    try {
        alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit);
        verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit)

    } catch (err) {
        console.log(TAG + " Failed to save the status of a question " + err);
    }
}

function onClickBtnTryAgain() {
    try {
        selectQuestionRandomly(selectedEvaluationPage);

    } catch (err) {
        console.log(TAG + " Failed to onClickBtnTryAgain " + err);
    }
}

/**
 * Update the status of all possible questions of the selected "page / question"  
 */
function alignStatusQuestionsOfPageEvalForm(unitId, questionId, questionStatus, selectedAlternative, submit) {
    try {
        var selectedPageEvaluationForm = -1;

        for (var i = 1; i <= EVAL_FORM_SET_SIZE; i++) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + i];

            for (var j = 0; j < modelQuestions.length; j++) {
                
                if (modelQuestions[j].questionId === questionId) {
                    selectedPageEvaluationForm = i;
                    break;
                }
            }
        }

        if (selectedPageEvaluationForm !== -1) {
            var modelQuestions = modelsQuestions[PREFIX_MODEL_QUESTION_OF_PAGE_EVAL_FORM + selectedPageEvaluationForm];

            for (var j = 0; j < modelQuestions.length; j++) {
                modelQuestions[j].questionStatus = questionStatus;

                if (modelQuestions[j].questionId === questionId) {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        selectedAlternative);
                } else {
                    gData.saveQuestionStatus(
                        unitId, 
                        modelQuestions[j].questionId, 
                        modelQuestions[j].questionStatus, 
                        submit, 
                        "-1");
                }
            }
        }

    } catch (err) {
        console.log(TAG + " Failed to alignStatusQuestionOfPageEvalForm " + err);
    }
}

function verifyUserCompletedModuleAndDisplayModal(selectedEvaluationPage, questionStatus, submit) {
    try {

        if (typeof (questionStatus) !== "undefined" && typeof (submit) !== "undefined") {

            if (questionStatus !== gData.QUESTION_STATUS.CLEAR) {

                if (selectedEvaluationPage === EVAL_FORM_SET_SIZE) {

                    if (questionStatus !== gData.QUESTION_STATUS.ANSWERING) {
                        modalSimpleMessageCud2.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                    }

                } else if (gData.hasUserCompletelyFinishedUnit(UNIT_NUMBER)) {
                    modalSimpleMessageCud2.verifyConditionsAndDisplayModal(UNIT_NUMBER);
                }
            }
        }

    } catch(err) {
        console.log(TAG + " Failed to verifyUserCompletedModuleAndDisplayModal " + err);
    }
}

/**
 * Events
 * Detect events that must be triggered after the UI of the page is built
 */
function initEvents() {
    
    $(window).resize(function() {
        gUi.detectChangeLayoutReload();
    });

    detectLinkSelectedQuestion();
}

/**
 * Verify if the link of a question was selected in the previous page, if it was, 
 * select the evaluation form section of the current page and display the selected question.
 */
function detectLinkSelectedQuestion() {
    try {

        // Verify if a User has clicked on the link of a question in the previous page
        if (gData.hasSelectedQuestionLink()) {
            sidebarCud2.selectMenuItemN(SIDEBAR_MENU_SECTION_IDS.evaluation);

            setTimeout(function () {
                // Show the selected question of the evaluation form
                var questionLink = gData.getSelectedQuestionLink();
                evalFormPaginatorCud2.navigateToPageN(questionLink.questionNumber - EVAL_FORM_SET_SIZE);

                // Reset the session variable selectedQuestionLink
                gData.setSelectedQuestionLink(undefined, undefined);

            }, gUi.TIMEOUT_DISPLAY_QUESTION_AFTER_LINK_CLICKED_ANOTHER_PAGE);
        }
    } catch (err) {
        console.log(TAG + " Failed to detectSelectedQuestionLink " + err);
    }
}