/**
 * glossary.js handles the UI and Logical events of the glossary.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @requires SimpleMessageModal
 * @license MIT
 */

var SELECTED_TOP_MENU_OPTION = 2;

// Selectors
var PAGE_LOADER = "#componentPageLoaderGlossary";
var PAGE_CONTAINER_SELECTOR = "#pageContainerGlossary";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Glossary";
var SIDEBAR_SELECTOR = "#componentSidebarGlossary";
var FOOTER_SELECTOR = "#componentFooterGlossary";
var GLOSSARY_SELECTOR = "#componentGlossary";

// HTML Components
var pageLoaderGlossary = new PageLoader();
var sidebarGlossary = new Sidebar();
var headerTop2Glossary = new HeaderTop2();
var mainContainer = new MainContainer();
var footerGlossary = new Footer();
var glossary = new SearchableGlossary();

/**
 * Initialization
 */
$(document).ready(function () {
    gPages.setCurrentPage(gPages.PAGE_COURSE_GLOSSARY_ID);
    initUI();
    gUi.initEvents();
    initPageEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * UI
 */
function initUI() {
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderGlossary, gMsgs.DEFAULT_LOADING_MESSAGE, "", function() {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
    });

    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
    initGlossary();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Glossary.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_GLOSSARY_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            -1);

        headerTop2Glossary.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarGlossary.model;
        dataModel.pageId = gPages.IDS.PAGE_COURSE_GLOSSARY_ID;
        dataModel.title = "GLOSSÁRIO";
        dataModel.bgFilter = gUi.PURPLE_FILTER;
        dataModel.unitNumber = "Glossary";
        dataModel.subTitle = "";
        dataModel.summaryTitle = "";
        dataModel.summaryItems = [];
        dataModel.mainMenuItems = [];

        sidebarGlossary.build(SIDEBAR_SELECTOR, dataModel, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarGlossary);
        mainContainer.initGlogalEvents();
        mainContainer.shrinkSidebar();

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to init MainContainer");
        }
    }
}

function initFooter() {
    try {
        footerGlossary.useDefaultFooter(true);
        footerGlossary.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to init Footer " + err);
        }
    }
}

function initGlossary() {
    try {
        glossary.build(GLOSSARY_SELECTOR, gUtils.getGlossaryInfo());

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to initGlossary " + err);
        }
    }
}

/**
 * Event
 */
function initPageEvents() {
    try {

        $(window).resize(function() {
            gUi.detectChangeLayoutReload();
        });

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Warning: Failed to initPageEvents");
        }
    }
}