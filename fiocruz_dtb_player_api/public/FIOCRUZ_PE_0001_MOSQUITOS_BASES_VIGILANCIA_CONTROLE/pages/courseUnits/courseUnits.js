/**
 * courseUnits.js handles the UI and Logical events of the courseUnits.html page
 * 
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright FioCruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jquery
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @requires HeaderTop2
 * @requires Sidebar
 * @requires MainContainer
 * @requires Footer
 * @license MIT
 */

var SELECTED_TOP_MENU_OPTION = 4;
var MAIN_MENU_IDS = {
    UNIT_1: "menuOptUnit1",
    UNIT_2: "menuOptUnit2",
    UNIT_3: "menuOptUnit3"
}

var PAGE_LOADER = "#componentPageLoaderUnits";
var PAGE_CONTAINER_SELECTOR = "#pageContainerUnits";
var HEADER_TOP_2_SELECTOR = "#componentHeaderTop2Units";
var SIDEBAR_SELECTOR = "#componentSidebarUnits";
var MAIN_MENU_SELECTORS = {
    UNIT_1: "#menuOptUnit1",
    UNIT_2: "#menuOptUnit2",
    UNIT_3: "#menuOptUnit3"
}
var FOOTER_SELECTOR = "#componentFooterUnits";
var CONFIRMATION_DIALOG_SELECTOR = "#unitsConfirmationDialog";

var FULL_SCREEN_MENU_OPTION_SELECTOR = ".full-screen-menu .module .menu-opt";
var FULL_SCREEN_MENU_FILTER_SELECTOR = ".full-screen-menu .module .filter, .full-screen-menu .module .menu-opt";
var PREFIX_SELECTOR_FILTER_OF_MENU_OPTION = ".full-screen-menu .module .filter.menu-opt-";
var PREFIX_ID_MENU_OPT = "courseUnitsMenuOptUnit_";
var FULL_SCREEN_MSG_SELECTOR = "#fullScreenMsgU";
var SELECTOR_BTN_START = ".full-screen-msg .fsm-body .btn-start";

var CLASS_DARK_FILTER = "dark-filter";
var CLASS_HOVERED = "hovered";

var TIMEOUT_INIT_MENU_OPTIONS_HEIGHT = 3000;

var pageLoaderUnits = new PageLoader();
var headerTop2Units = new HeaderTop2();
var sidebarUnits = new Sidebar();
var mainContainer = new MainContainer();
var footerUnits = new Footer();
var fullScreenDialog = new FullScreenDialog();
var confirmationDialog = new ConfirmationDialog();

/**
 * Initialization
 */
$(document).ready(function () {
    gPages.setCurrentPage(gPages.PAGE_COURSE_UNITS);
    initData();
    initUI();
    gUi.initEvents();
    initPageEvents();
});

$(document).bind('pageinit', function () {
    mainContainer.initMobileEvents();
});

/**
 * Data
 */
function initData() {
    gData.loadUserCourse();
}

/**
 * UI
 */
function initUI() {
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gUi.setPageLoader(PAGE_LOADER, pageLoaderUnits, gMsgs.DEFAULT_LOADING_MESSAGE, "", function () {
        $(gUi.getPageContainerSelector()).removeClass(gUi.CLASS_HIDDEN);
        initFullScreenDialog();
        startPresentation();
    });

    initHeaderTop2();
    initSidebar();
    initMainContainer();
    initFooter();
}

/**
 * Page Structure
 */
function initHeaderTop2() {
    try {
        headerTop2Units.buildHeaderTop2(
            HEADER_TOP_2_SELECTOR,
            gImages.IMG_COURSE_LOGO,
            gPages.IDS.PAGE_COURSE_UNITS_ID,
            gMsgs.APP_PAGE_TITLE_WITH_NEW_LINE,
            SELECTED_TOP_MENU_OPTION,
            -1);

        headerTop2Units.initEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init HeaderTop2 " + err);
        }
    }
}

function initSidebar() {
    try {
        var dataModel = sidebarUnits.model;
        dataModel.pageId = gPages.IDS.PAGE_COURSE_UNITS_ID;

        sidebarUnits.build(SIDEBAR_SELECTOR, dataModel, undefined);
        sidebarUnits.lockHide();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init Sidebar " + err);
        }
    }
}

function initMainContainer() {
    try {
        mainContainer.build(sidebarUnits);
        mainContainer.initGlogalEvents();
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init MainContainer");
        }
    }
}

function initFooter() {
    try {
        gUi.setHasFixedFooter(false);
        footerUnits.useDefaultFooter(true);
        footerUnits.build(FOOTER_SELECTOR, undefined);
    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init Footer " + err);
        }
    }
}

function initFullScreenDialog() {
    try {
        var model = {
            title: '',
            msg: '',
            imgMsg: gImages.IMG_COURSE_BRAND,
            extraMsg: '',
            hasCloseBtn: false,
            hasConfirmBtn: true,
            icoStartImg: gImages.ICO_SVG_PLAY_ARROW_WHITE
        };

        fullScreenDialog.build(FULL_SCREEN_MSG_SELECTOR, model, onBtnConfirmClicked);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init initFullScreenDialog " + err);
        }
    }
}

function startPresentation() {

    if (!gData.hasPresentedWelcomeDialog()) {
        setTimeout(function () {
            $(SELECTOR_BTN_START).addClass(CLASS_HOVERED);

            setTimeout(function () {
                $(SELECTOR_BTN_START).removeClass(CLASS_HOVERED);
            }, 600);
        }, 1500);

        setTimeout(function () {
            $(SELECTOR_BTN_START).addClass(CLASS_HOVERED);

            setTimeout(function () {
                $(SELECTOR_BTN_START).removeClass(CLASS_HOVERED);
            }, 600);
        }, 2600);

        setTimeout(function () {
            $(SELECTOR_BTN_START).addClass(CLASS_HOVERED);

            setTimeout(function () {
                $(SELECTOR_BTN_START).removeClass(CLASS_HOVERED);
            }, 600);
        }, 3700);

        fullScreenDialog.show();
    }
}

/**
 * Page Events
 */
function onBtnConfirmClicked() {
    gData.setHasPresentedWelcomeDialog(true);
    fullScreenDialog.dismiss();

    setTimeout(function () {
        startPresentation();
    }, 1000);
}

function initPageEvents() {

    $(window).resize(function() {
        gUi.detectChangeLayoutReload();
    })

    $(FULL_SCREEN_MENU_FILTER_SELECTOR).on("click", function () {
        onFullScreenMenuItemSelected($(this));
    });

}

function onFullScreenMenuItemSelected(menuItem) {
    var selectedOptIdParts = $(menuItem).attr("id").split("_");
    var selectedItemId = Number(selectedOptIdParts[1]);

    switch (selectedItemId) {

        case 1:
            gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_1);

            break;

        case 2:
            
            if (gData.hasUserFinishedUnit(1)) {
                gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_2);
            } else {
                showConfirmationDialog();
            }

            break;

        case 3:
            
            if (gData.hasUserFinishedUnit(1) && gData.hasUserFinishedUnit(2)) {
                gPages.updateCurrentPage(gPages.PAGE_COURSE_UNIT_DETAILS_3);
            } else {
                showConfirmationDialog();
            }
            
            break;
    }
}

function showConfirmationDialog() {
    try {
        this.model = {
            headerMsg: "Calma ...",
            icoBtnClose: gImages.ICO_SVG_CLOSE_BLACK,
            mainMsg: "Para ter acesso a este módulo é necessário concluir o anterior.",
            actionMsg: "ir para",
            cancelBtnText: gData.prevModuleInfo.pageName,
            confirmBtnText: gData.nextModuleInfo.pageName,
            callbackCancelAction: navigateToPreviousModule,
            callbackConfirmAction: navigateToNextModule
        }
        confirmationDialog.rebuild(this.CONFIRMATION_DIALOG_SELECTOR, this.model);
        confirmationDialog.show(true);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init initConfirmationDialog " + err);
        }
    }
}

function navigateToPreviousModule() {
    try {
        gPages.updateCurrentPage(gData.prevModuleInfo.pageId);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init navigateToPreviousModule " + err);
        }
    }
}

function navigateToNextModule() {
    try {
        gPages.updateCurrentPage(gData.nextModuleInfo.pageId);

    } catch (err) {

        if (unasus.pack.getDebug()) {
            console.log("Failed to init navigateToNextModule " + err);
        }
    }
}

function addFilters(selectedMenuOpt) {

    for (var i = 1; i <= 3; i++) {
        var id = $(selectedMenuOpt).attr("id");

        if (id != undefined) {
            selectedMenuOptNumber = Number(id.replace(PREFIX_ID_MENU_OPT, ""));

            if (selectedMenuOptNumber === i) {
                $(PREFIX_ID_MENU_OPT + i).removeClass(CLASS_DARK_FILTER);
            } else {
                $(PREFIX_ID_MENU_OPT + i).addClass(CLASS_DARK_FILTER);
            }
        }
    }
}

function removeFilters() {
    $(FULL_SCREEN_MENU_FILTER_SELECTOR).each(function () {
        $(this).removeClass(CLASS_DARK_FILTER);
    });
}
