/**
 * @author Diego M. Silva - (diegomsilva.com)
 * @copyright Fiocruz EaD - Centro de Pesquisas Ageu Magalhães
 * @version 0.0.1
 * @requires jQuery
 * @requires gApi
 * @requires gUi
 * @requires gMsgs
 * @requires gPages
 * @requires gData
 * @license MIT
 */

var PAGE_CONTAINER_SELECTOR = "#pageContainerIndex";

/**
 * Initialization
 */
$(document).ready(function () {
    gUi.setPageContainer(PAGE_CONTAINER_SELECTOR);
    gData.loadViewedClinicalCases();
    gData.loadUserCourse();
    gPages.updateCurrentPage(gPages.PAGE_COURSE_UNITS);
});