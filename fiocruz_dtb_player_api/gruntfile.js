module.exports = function (grunt) {

    var PATH_BASE_DIR = "public/FIOCRUZ_PE_0001_MOSQUITOS_BASES_VIGILANCIA_CONTROLE/";
    var PATH_GLOBALS = PATH_BASE_DIR + "globals/";
    var PATH_COMPONENTS = PATH_BASE_DIR + "components/";
    var PATH_COMPONENTS_DIST_BUNDLES = PATH_COMPONENTS + "bundles/";
    var PATH_PAGES = PATH_BASE_DIR + "pages/";

    var POSTFIX_CSS = ".css";
    var POSTFIX_MIN_CSS = ".min" + POSTFIX_CSS;
    var POSTFIX_BUNDLE_CSS = ".bundle" + POSTFIX_MIN_CSS;
    var POSTFIX_JS = ".js";
    var POSTFIX_MIN_JS = ".min" + POSTFIX_JS;
    var POSTFIX_BUNDLE_JS = ".bundle" + POSTFIX_MIN_JS;

    /**
     * Globals
     */
    // Minified files
    var gData = {
        MIN_JS: PATH_GLOBALS + "gData" + POSTFIX_MIN_JS
    };
    var gUnasus = {
        MIN_JS: PATH_GLOBALS + "gUnasusApiIntegration" + POSTFIX_MIN_JS
    };
    var gUtils = {
        MIN_JS: PATH_GLOBALS + "gUtils" + POSTFIX_MIN_JS
    };
    var gStyles = {
        MIN_CSS: PATH_GLOBALS + "gStyles" + POSTFIX_MIN_CSS
    };
    
    // Bundle of minified files
    var globalsBundle = {
        BUNDLE_CSS: PATH_GLOBALS + "globals" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_GLOBALS + "globals" + POSTFIX_BUNDLE_JS
    };

    /**
     * Components
     */
    // Basic components' minified files
    var headerTop2Component = {
        MIN_CSS: PATH_COMPONENTS + "headerTop2/headerTop2" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "headerTop2/headerTop2" + POSTFIX_MIN_JS
    };
    var mainContainerComponent = {
        MIN_CSS: PATH_COMPONENTS + "mainContainer/mainContainer" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "mainContainer/mainContainer" + POSTFIX_MIN_JS
    };
    var sidebarComponent = {
        MIN_CSS: PATH_COMPONENTS + "sidebar/sidebar" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "sidebar/sidebar" + POSTFIX_MIN_JS
    };
    var footerComponent = {
        MIN_CSS: PATH_COMPONENTS + "footer/footer" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "footer/footer" + POSTFIX_MIN_JS
    };
    var pageLoaderComponent = {
        MIN_CSS: PATH_COMPONENTS + "pageLoader/pageLoader" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "pageLoader/pageLoader" + POSTFIX_MIN_JS
    };

    // Acessory components' minified files, used by the bundles of the unitDetailsN.html pages
    var contentExpanderComponent = {
        MIN_CSS: PATH_COMPONENTS + "contentExpander/contentExpander" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "contentExpander/contentExpander" + POSTFIX_MIN_JS
    };
    var paginatorComponent = {
        MIN_CSS: PATH_COMPONENTS + "paginator/paginator" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "paginator/paginator" + POSTFIX_MIN_JS
    };
    var questionComponent = {
        MIN_CSS: PATH_COMPONENTS + "question/question" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "question/question" + POSTFIX_MIN_JS
    };
    var imgsViewerComponent = {
        MIN_CSS: PATH_COMPONENTS + "imgsViewer/imgsViewer" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "imgsViewer/imgsViewer" + POSTFIX_MIN_JS
    };
    var pdfViewerComponent = {
        MIN_CSS: PATH_COMPONENTS + "pdfViewer/pdfViewer" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "pdfViewer/pdfViewer" + POSTFIX_MIN_JS
    };
    var contentSelectorComponent = {
        MIN_CSS: PATH_COMPONENTS + "contentSelector/contentSelector" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "contentSelector/contentSelector" + POSTFIX_MIN_JS
    };
    var videoSelectorComponent = {
        MIN_CSS: PATH_COMPONENTS + "videoSelector/videoSelector" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "videoSelector/videoSelector" + POSTFIX_MIN_JS
    };
    var fullScreenMsgComponent = {
        MIN_CSS: PATH_COMPONENTS + "fullScreenMsg/fullScreenMsg" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "fullScreenMsg/fullScreenMsg" + POSTFIX_MIN_JS
    };
    var modalSimpleMessageComponent = {
        MIN_CSS: PATH_COMPONENTS + "modalSimpleMessage/modalSimpleMessage" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "modalSimpleMessage/modalSimpleMessage" + POSTFIX_MIN_JS
    };
    var confirmationDialogComponent = {
        MIN_CSS: PATH_COMPONENTS + "confirmationDialog/confirmationDialog" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "confirmationDialog/confirmationDialog" + POSTFIX_MIN_JS
    };
    
    // Other components' minified files
    var basicInfoCardComponent = {
        MIN_CSS: PATH_COMPONENTS + "basicInfoCard/basicInfoCard" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "basicInfoCard/basicInfoCard" + POSTFIX_MIN_JS
    };
    var simpleTooltipComponent = {
        MIN_CSS: PATH_COMPONENTS + "simpleTooltip/simpleTooltip" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "simpleTooltip/simpleTooltip" + POSTFIX_MIN_JS
    };
    var simpleGraphComponent = {
        MIN_CSS: PATH_COMPONENTS + "simpleGraph/simpleGraph" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "simpleGraph/simpleGraph" + POSTFIX_MIN_JS
    };
    var snackbarComponent = {
        MIN_CSS: PATH_COMPONENTS + "snackbar/snackbar" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "snackbar/snackbar" + POSTFIX_MIN_JS
    };
    var parallaxComponent = {
        MIN_CSS: PATH_COMPONENTS + "parallax/parallax" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "parallax/parallax" + POSTFIX_MIN_JS
    };
    var verticalInfoCardComponent = {
        MIN_CSS: PATH_COMPONENTS + "verticalInfoCard/verticalInfoCard" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "verticalInfoCard/verticalInfoCard" + POSTFIX_MIN_JS
    };
    var horizontalTabbedBoxComponent = {
        MIN_CSS: PATH_COMPONENTS + "horizontalTabbedBox/horizontalTabbedBox" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "horizontalTabbedBox/horizontalTabbedBox" + POSTFIX_MIN_JS
    };
    var congenitalZikaVirusComponent = {
        MIN_CSS: PATH_COMPONENTS + "congenitalZikaVirus/congenitalZikaVirus" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "congenitalZikaVirus/congenitalZikaVirus" + POSTFIX_MIN_JS
    };
    var timelineWithSlidesComponent = {
        MIN_CSS: PATH_COMPONENTS + "timelineWithSlides/timelineWithSlides" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "timelineWithSlides/timelineWithSlides" + POSTFIX_MIN_JS
    };
    var colorfulTimelineComponent = {
        MIN_CSS: PATH_COMPONENTS + "colorfulTimeline/colorfulTimeline" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "colorfulTimeline/colorfulTimeline" + POSTFIX_MIN_JS
    };
    var tabbedBoxComponent = {
        MIN_CSS: PATH_COMPONENTS + "tabbedBox/tabbedBox" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "tabbedBox/tabbedBox" + POSTFIX_MIN_JS
    };
    var simpleInfoTableComponent = {
        MIN_CSS: PATH_COMPONENTS + "simpleInfoTable/simpleInfoTable" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "simpleInfoTable/simpleInfoTable" + POSTFIX_MIN_JS
    };
    var searchableGlossaryComponent = {
        MIN_CSS: PATH_COMPONENTS + "searchableGlossary/searchableGlossary" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "searchableGlossary/searchableGlossary" + POSTFIX_MIN_JS
    };
    var imgMagnifier = {
        MIN_CSS: PATH_COMPONENTS + "imgMagnifier/imgMagnifier" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_COMPONENTS + "imgMagnifier/imgMagnifier" + POSTFIX_MIN_JS
    };

    // Bundles
    var basicComponentsBundle = {
        BUNDLE_CSS: PATH_COMPONENTS_DIST_BUNDLES + "basicComponents" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_COMPONENTS_DIST_BUNDLES + "basicComponents" + POSTFIX_BUNDLE_JS,
    };

    var accessoryComponentsBundle = {
        BUNDLE_CSS: PATH_COMPONENTS_DIST_BUNDLES + "accessoryComponents" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_COMPONENTS_DIST_BUNDLES + "accessoryComponents" + POSTFIX_BUNDLE_JS,
    };

    /**
     * Pages
     */
    var indexPage = {
        MIN_CSS: PATH_PAGES + "index/index" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "index/index" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "index/index" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "index/index" + POSTFIX_BUNDLE_JS
    };
    
    var courseUnitsPage = {
        MIN_CSS: PATH_PAGES + "courseUnits/courseUnits" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "courseUnits/courseUnits" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "courseUnits/courseUnits" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "courseUnits/courseUnits" + POSTFIX_BUNDLE_JS
    };

    var courseProgressPage = {
        MIN_CSS: PATH_PAGES + "courseProgress/courseProgress" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "courseProgress/courseProgress" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "courseProgress/courseProgress" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "courseProgress/courseProgress" + POSTFIX_BUNDLE_JS
    };

    var courseGlossaryPage = {
        MIN_CSS: PATH_PAGES + "glossary/glossary" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "glossary/glossary" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "glossary/glossary" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "glossary/glossary" + POSTFIX_BUNDLE_JS
    };
    
    var aboutPage = {
        MIN_CSS: PATH_PAGES + "about/about" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "about/about" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "about/about" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "about/about" + POSTFIX_BUNDLE_JS
    };

    var courseUnitDetails1Page = {
        MIN_CSS: PATH_PAGES + "courseUnitDetails1/courseUnitDetails1" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "courseUnitDetails1/courseUnitDetails1" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "courseUnitDetails1/courseUnitDetails1" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "courseUnitDetails1/courseUnitDetails1" + POSTFIX_BUNDLE_JS
    };
    
    var courseUnitDetails2Page = {
        MIN_CSS: PATH_PAGES + "courseUnitDetails2/courseUnitDetails2" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "courseUnitDetails2/courseUnitDetails2" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "courseUnitDetails2/courseUnitDetails2" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "courseUnitDetails2/courseUnitDetails2" + POSTFIX_BUNDLE_JS
    };

    var courseUnitDetails3Page = {
        MIN_CSS: PATH_PAGES + "courseUnitDetails3/courseUnitDetails3" + POSTFIX_MIN_CSS,
        MIN_JS: PATH_PAGES + "courseUnitDetails3/courseUnitDetails3" + POSTFIX_MIN_JS,
        BUNDLE_CSS: PATH_PAGES + "courseUnitDetails3/courseUnitDetails3" + POSTFIX_BUNDLE_CSS,
        BUNDLE_JS: PATH_PAGES + "courseUnitDetails3/courseUnitDetails3" + POSTFIX_BUNDLE_JS
    };

    /**
     * Tasks configuration
     */
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            css: {
                files: [
                    PATH_BASE_DIR + "**/*" + POSTFIX_CSS,
                    PATH_BASE_DIR + "**/**/*" + POSTFIX_CSS,
                    PATH_BASE_DIR + "**/**/**/*" + POSTFIX_CSS,
                    "!" + PATH_BASE_DIR + "**/*" + POSTFIX_MIN_CSS,
                    "!" + PATH_BASE_DIR + "**/**/*" + POSTFIX_MIN_CSS,
                    "!" + PATH_BASE_DIR + "**/**/**/*" + POSTFIX_MIN_CSS,
                    "!" + PATH_BASE_DIR + "**/*" + POSTFIX_BUNDLE_CSS,
                    "!" + PATH_BASE_DIR + "**/**/*" + POSTFIX_BUNDLE_CSS,
                    "!" + PATH_BASE_DIR + "**/**/**/*" + POSTFIX_BUNDLE_CSS
                ],
                tasks: [
                    "cssmin",
                    "concat"
                ]
            },
            js: {
                files: [
                    PATH_BASE_DIR + "**/*" + POSTFIX_JS,
                    PATH_BASE_DIR + "**/**/*" + POSTFIX_JS,
                    PATH_BASE_DIR + "**/**/**/*" + POSTFIX_JS,
                    "!" + PATH_BASE_DIR + "**/*" + POSTFIX_MIN_JS,
                    "!" + PATH_BASE_DIR + "**/**/*" + POSTFIX_MIN_JS,
                    "!" + PATH_BASE_DIR + "**/**/**/*" + POSTFIX_MIN_JS,
                    "!" + PATH_BASE_DIR + "**/*" + POSTFIX_BUNDLE_JS,
                    "!" + PATH_BASE_DIR + "**/**/*" + POSTFIX_BUNDLE_JS,
                    "!" + PATH_BASE_DIR + "**/**/**/*" + POSTFIX_BUNDLE_JS
                ],
                tasks: [
                    "uglify",
                    "concat"
                ]
            }
        },

        /**
         * Minify all the CSS files of the app
         */
        cssmin: {
            target: {
                files: [

                    /**
                     * Minify the CSS files created for the app and that are shared by all the pages
                     */
                    {
                        expand: true,
                        cwd: PATH_GLOBALS,
                        src: [
                            "*" + POSTFIX_CSS, 
                            "!*" + POSTFIX_MIN_CSS,
                            "!*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_GLOBALS,
                        ext: POSTFIX_MIN_CSS
                    },

                    /**
                     * Minify the CSS file of each component of the UI
                     */
                    {
                        expand: true,
                        cwd: PATH_COMPONENTS,
                        src: [
                            "**/*" + POSTFIX_CSS, 
                            "!**/*" + POSTFIX_MIN_CSS,
                            "!**/*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_COMPONENTS,
                        ext: POSTFIX_MIN_CSS
                    },

                    /**
                     * Minify the CSS file of each page of the app
                     */
                    {
                        expand: true,
                        cwd: PATH_PAGES,
                        src: [
                            "**/*" + POSTFIX_CSS, 
                            "!**/*" + POSTFIX_MIN_CSS,
                            "!**/*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_PAGES,
                        ext: POSTFIX_MIN_CSS
                    }
                ]
            }
        },

        /**
         * Minify all the JS files of the app
         */
        uglify: {

            /**
             * Minify the JS files shared by all the pages of the app
             */
            globalsJs: {
                options: {
                    manage: false,
                    mangle: false
                },
                files: [
                    {
                        expand: true,
                        cwd: PATH_GLOBALS,
                        src: [
                            "*" + POSTFIX_JS,
                            "!*" + POSTFIX_MIN_JS,
                            "!*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_GLOBALS,
                        ext: POSTFIX_MIN_JS
                    }
                ]
            },

            /**
             * Minify the JS file of each component of the UI
             */
            componentsJs: {
                options: {
                    manage: false,
                    mangle: false
                },
                files: [
                    {
                        expand: true,
                        cwd: PATH_COMPONENTS,
                        src: [
                            "**/*" + POSTFIX_JS,
                            "!**/*" + POSTFIX_MIN_JS,
                            "!**/*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_COMPONENTS,
                        ext: POSTFIX_MIN_JS
                    }
                ]
            },

            /**
             * Minify the JS file of each page of the app
             */
            pagesJs: {
                options: {
                    manage: false,
                    mangle: false
                },
                files: [
                    {
                        expand: true,
                        cwd: PATH_PAGES,
                        src: [
                            "**/*" + POSTFIX_JS,
                            "!**/*" + POSTFIX_MIN_JS,
                            "!**/*" + POSTFIX_BUNDLE_JS
                        ],
                        dest: PATH_PAGES,
                        ext: POSTFIX_MIN_JS
                    }
                ]
            }
        },

        /**
         * Join the minified CSS and JS files into packages global and component packages, these packages
         * are them merged together to fetch the bundle.min.css and bundle.min.js files to each page
         * of the app.
         */
        concat: {
            options: {
                separator: "/* New file */",
                stripBanners: true,
                banner: "/* <%= pkg.name %> = v<%= pkg.version %> = " + "<%= grunt.template.today('yyyy-mm-dd') %> */"
            },

            /**
             * Create bundle files with all the global minified CSS and JS files
             */
            globalCss: {
                src: [
                    gStyles.MIN_CSS
                ],
                dest: globalsBundle.BUNDLE_CSS
            },
            globalJs: {
                src: [
                    gUnasus.MIN_JS,
                    gUtils.MIN_JS,
                    gData.MIN_JS
                ],
                dest: globalsBundle.BUNDLE_JS
            },

            /**
             * Create bundle files with the minified CSS and JS files of all the basic components
             */
            basicComponentsCss: {
                src: [
                    headerTop2Component.MIN_CSS,
                    mainContainerComponent.MIN_CSS,
                    sidebarComponent.MIN_CSS,
                    footerComponent.MIN_CSS,
                    pageLoaderComponent.MIN_CSS,
                ],
                dest: basicComponentsBundle.BUNDLE_CSS
            },
            basicComponentsJs: {
                src: [
                    headerTop2Component.MIN_JS,
                    mainContainerComponent.MIN_JS,
                    sidebarComponent.MIN_JS,
                    footerComponent.MIN_JS,
                    pageLoaderComponent.MIN_JS,
                ],
                dest: basicComponentsBundle.BUNDLE_JS
            },

            /**
             * Create bundle files with the minified CSS and JS files of all the accessory components, used mainly by the 'UnitDetailsN' pages
             */
            accessoryComponentsCss: {
                src: [
                    contentExpanderComponent.MIN_CSS,
                    paginatorComponent.MIN_CSS,
                    questionComponent.MIN_CSS,
                    imgsViewerComponent.MIN_CSS,
                    pdfViewerComponent.MIN_CSS,
                    contentSelectorComponent.MIN_CSS,
                    videoSelectorComponent.MIN_CSS,
                    fullScreenMsgComponent.MIN_CSS,
                    modalSimpleMessageComponent.MIN_CSS,
                    confirmationDialogComponent.MIN_CSS,
                    searchableGlossaryComponent.MIN_CSS,
                    imgMagnifier.MIN_CSS
                ],
                dest: accessoryComponentsBundle.BUNDLE_CSS
            },
            accessoryComponentsJs: {
                src: [
                    contentExpanderComponent.MIN_JS,
                    paginatorComponent.MIN_JS,
                    questionComponent.MIN_JS,
                    imgsViewerComponent.MIN_JS,
                    pdfViewerComponent.MIN_JS,
                    contentSelectorComponent.MIN_JS,
                    videoSelectorComponent.MIN_JS,
                    fullScreenMsgComponent.MIN_JS,
                    modalSimpleMessageComponent.MIN_JS,
                    confirmationDialogComponent.MIN_JS,
                    searchableGlossaryComponent.MIN_JS,
                    imgMagnifier.MIN_JS
                ],
                dest: accessoryComponentsBundle.BUNDLE_JS
            },

            /**
             * Create bundle CSS and JS files for each page of the app
             */
            // Index
            pageBundleIndexCss: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    indexPage.MIN_CSS
                ],
                dest: indexPage.BUNDLE_CSS
            },
            pageBundleIndexJs: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    indexPage.MIN_JS
                ],
                dest: indexPage.BUNDLE_JS
            },

            // CourseUnits
            pageBundleCourseUnitsCss: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    fullScreenMsgComponent.MIN_CSS,
                    confirmationDialogComponent.MIN_CSS,
                    courseUnitsPage.MIN_CSS
                ],
                dest: courseUnitsPage.BUNDLE_CSS
            },
            pageBundleCourseUnitsJs: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    fullScreenMsgComponent.MIN_JS,
                    confirmationDialogComponent.MIN_JS,
                    courseUnitsPage.MIN_JS
                ],
                dest: courseUnitsPage.BUNDLE_JS
            },

            // CourseProgress
            pageBundleCourseProgressCss: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    questionComponent.MIN_CSS,
                    modalSimpleMessageComponent.MIN_CSS,
                    confirmationDialogComponent.MIN_CSS,
                    courseProgressPage.MIN_CSS
                ],
                dest: courseProgressPage.BUNDLE_CSS
            },
            pageBundleCourseProgressJs: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    questionComponent.MIN_JS,
                    modalSimpleMessageComponent.MIN_JS,
                    confirmationDialogComponent.MIN_JS,
                    courseProgressPage.MIN_JS
                ],
                dest: courseProgressPage.BUNDLE_JS
            },

            // CourseGlossaary
            pageBundleCourseGlossaryCss: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    modalSimpleMessageComponent.MIN_CSS,
                    searchableGlossaryComponent.MIN_CSS,
                    courseGlossaryPage.MIN_CSS
                ],
                dest: courseGlossaryPage.BUNDLE_CSS
            },
            pageBundleCourseGlossaryJs: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    modalSimpleMessageComponent.MIN_JS,
                    searchableGlossaryComponent.MIN_JS,
                    courseGlossaryPage.MIN_JS
                ],
                dest: courseGlossaryPage.BUNDLE_JS
            },            

            // About
            pageBundleAboutCss: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    aboutPage.MIN_CSS
                ],
                dest: aboutPage.BUNDLE_CSS
            },
            pageBundleAboutJs: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    aboutPage.MIN_JS
                ],
                dest: aboutPage.BUNDLE_JS
            },

            // CourseUnitDetails1
            pageBundleCourseUnitDetails1Css: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    accessoryComponentsBundle.BUNDLE_CSS,
                    tabbedBoxComponent.MIN_CSS,
                    simpleInfoTableComponent.MIN_CSS,
                    courseUnitDetails1Page.MIN_CSS
                ],
                dest: courseUnitDetails1Page.BUNDLE_CSS
            },
            pageBundleCourseUnitDetails1Js: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    accessoryComponentsBundle.BUNDLE_JS,
                    tabbedBoxComponent.MIN_JS,
                    simpleInfoTableComponent.MIN_JS,
                    courseUnitDetails1Page.MIN_JS
                ],
                dest: courseUnitDetails1Page.BUNDLE_JS
            },

            // CourseUnitDetails2
            pageBundleCourseUnitDetails2Css: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    accessoryComponentsBundle.BUNDLE_CSS,
                    tabbedBoxComponent.MIN_CSS,
                    simpleInfoTableComponent.MIN_CSS,
                    courseUnitDetails2Page.MIN_CSS
                ],
                dest: courseUnitDetails2Page.BUNDLE_CSS
            },
            pageBundleCourseUnitDetails2Js: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    accessoryComponentsBundle.BUNDLE_JS,
                    tabbedBoxComponent.MIN_JS,
                    simpleInfoTableComponent.MIN_JS,
                    courseUnitDetails2Page.MIN_JS
                ],
                dest: courseUnitDetails2Page.BUNDLE_JS
            },

            // CourseUnitDetails3
            pageBundleCourseUnitDetails3Css: {
                src: [
                    basicComponentsBundle.BUNDLE_CSS,
                    accessoryComponentsBundle.BUNDLE_CSS,
                    tabbedBoxComponent.MIN_CSS,
                    simpleInfoTableComponent.MIN_CSS,
                    courseUnitDetails3Page.MIN_CSS
                ],
                dest: courseUnitDetails3Page.BUNDLE_CSS
            },
            pageBundleCourseUnitDetails3Js: {
                src: [
                    basicComponentsBundle.BUNDLE_JS,
                    accessoryComponentsBundle.BUNDLE_JS,
                    tabbedBoxComponent.MIN_JS,
                    simpleInfoTableComponent.MIN_JS,
                    courseUnitDetails3Page.MIN_JS
                ],
                dest: courseUnitDetails3Page.BUNDLE_JS
            }

        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['cssmin', 'uglify', 'concat']);

};