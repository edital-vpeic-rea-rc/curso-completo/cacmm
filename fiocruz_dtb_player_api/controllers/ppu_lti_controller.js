/**
 * This controller is responsible for making the bridge between the PPU application that is being served
 * by the Provider in an LTI architecture and its Consumer.
 */

const TAG = "ppu_lti_controller";
const FLAG_FIOCRUZ_DTB_PLAYER_PARAMS = "//InitialFiocruzDTBPlayerParams**GO**HERE";
const MSG_FAILED_INIT_PPU_LTI = "Failed to initializeExternalLTIModule";
const MSG_COULD_NOT_INIT_PPU_LTI = "Não foi possível inicializar o PPU";
const MSG_FAILED_SET_DATA = "Failed to setData";
const MSG_FAILED_SIMPLY_UPDATE_DATA = "Failed to simplyUpdateStudentsData";
const MSG_FAILED_TRY_UPDATE_STUDENT_GRADE = "Failed to tryUpdateStudentsGrade";
const MSG_FAILED_GET_DATA = "Failed to getData";
const MSG_FAILED_GET_USER = "Failed to getUser";

const fs = require('fs');
const lti = require("ims-lti");
const uuid = require("uuid4");

const settings = require("../settings/secret");
const logHelper = require("../helpers/log_helper");
const idHelper = require("../helpers/id_helper");
const objHelper = require("../helpers/obj_helper");
const dateHelper = require("../helpers/date_helper");
const ltiConsumerDbManager = require("../persistence/lti_consumer_db_manager");
const studentDbManager = require("../persistence/student_db_manager");
const studentDataDbManager = require("../persistence/student_data_db_manager");
const sessionManager = require("../persistence/nosql_manager");

exports.displayPPU = function (req, res) {
    var html = fs.readFileSync('public/player.html');
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(html);
}

exports.initializeExternalLTIModule = function (req, res) {
    var moodleData = new lti.Provider(settings.lti_auth.KEY, settings.lti_auth.SHARED_SECRET);
    moodleData.valid_request(req, (err, isValid) => {

        if (!isValid) {
            res.send(logHelper.getMsgWithErr(TAG, "Invalid request", err));
            return;
        }

        // Upsert LTIConsumer on database
        ltiConsumerDbManager.upsertLTIConsumer(
            moodleData.body.context_title,
            moodleData.body.lis_outcome_service_url,
            moodleData.body.context_label,
            moodleData.body.resource_link_title,
            moodleData.body.tool_consumer_instance_name,
            moodleData.body.lti_version,
            dateHelper.getTimestampNow(),
            dateHelper.getTimezone()
        ).then(function(results) {
            
            if (results) {

                // Get LTIConsumer from database
                ltiConsumerDbManager.getLTIConsumer(
                    moodleData.body.context_title,
                    moodleData.body.lis_outcome_service_url
                ).then(function(ltiConsumer) {

                    if (ltiConsumer != undefined) {
                        
                        // Upsert Student on database
                        studentDbManager.upsertStudent(
                            ltiConsumer.lti_consumer_id,
                            moodleData.body.lis_person_contact_email_primary,
                            moodleData.body.ext_user_username,
                            moodleData.body.lis_person_name_full,
                            moodleData.body.user_id,
                            moodleData.body.roles[0],
                            dateHelper.getTimestampNow(),
                            dateHelper.getTimezone()
                        ).then(function(results) {

                            if (results) {

                                // Get Student from database
                                studentDbManager.getStudent(
                                    ltiConsumer.lti_consumer_id,
                                    moodleData.body.lis_person_contact_email_primary
                                ).then(function(student) {

                                    if (student != undefined) {

                                        // Store the user's moodle data on the session varibles
                                        var sessionID = uuid();

                                        sessionManager.setSessionVariable(
                                            sessionID,
                                            moodleData
                                        ).then(function() {

                                            // Update the FiocruzDTB PPU Player API's client with the basic 
                                            // settings necessary to perform future requests.
                                            var html = fs.readFileSync('public/player.html');
                                            var html = html.toString().replace(FLAG_FIOCRUZ_DTB_PLAYER_PARAMS,
                                                `
                                                var FiocruzDTBPlayerParams = {
                                                    sessionID: "${sessionID}",
                                                    PPUID: "${ltiConsumer.lti_consumer_id}",
                                                    userUID: "${student.user_email}",
                                                    returnUrl: "${ltiConsumer.course_url}"
                                                };
                                                `
                                            );
            
                                            res.setHeader("Content-Type", "text/html");
                                            res.send(html);
                                            // ./Update the FiocruzDTB PPU Player API's client with the basic 

                                        }, function(err) {
                                            logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                                            res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                                        });
                                        // ./Store the user's moodle data on the session varibles

                                    } else {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, "Student is undefined");
                                        res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                                    }
                                }, function(err) {
                                    logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                                    res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                                });
                                // ./Get Student from database

                            } else {
                                logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                                res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                            }
                        }, function(err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                            res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                        });
                        // ./Upsert Student on database

                    } else {
                        logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                        res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                    }
                }, function(err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                    res.send(MSG_COULD_NOT_INIT_PPU_LTI);
                });
                // ./Get LTIConsumer from database

            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
                res.send(MSG_COULD_NOT_INIT_PPU_LTI);
            }
        }, function (err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_INIT_PPU_LTI, err);
            res.send(MSG_COULD_NOT_INIT_PPU_LTI);
        });
        // ./Upsert LTIConsumer on database
    });
}

exports.getUser = function (req, res) {

    if (logHelper.isDebugEnabled()) {
        logHelper.logMsg(TAG, `\n\n${TAG}/GetUser/`);
        logHelper.logMsg(TAG, `user UID: ${req.params.userUID}`);
        logHelper.logMsg(TAG, `PPU request UUID: ${req.params.ppuid}`);
    }

    // Get student from the database
    studentDbManager.getStudent(
        req.params.ppuid,
        req.params.userUID
    ).then(function(student) {
        
        if (student != undefined) {
            var user = {};
        
            if (student.full_name != undefined) {
                user.RealName = student.full_name;
            }
    
            if (student.user_id != undefined) {
                user.Id = student.user_id;
            }
    
            if (student.user_email != undefined) {
                user.Email = student.user_email;
            }
    
            res.send(user);

        } else {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_USER, "");
            res.send({});
        }
    }, function(err) {
        logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_USER, err);
        res.send({});
    });
    // ./Get student from the database
}

exports.getData = function (req, res) {

    if (logHelper.isDebugEnabled()) {
        logHelper.logMsg(TAG, `\n\n${TAG}/GetData/`);
        logHelper.logMsg(TAG, `user UID: ${req.params.userUID}`);
        logHelper.logMsg(TAG, `PPU request UUID: ${req.params.ppuid}`);
    }

    try {
        // Get the student from the database
        studentDbManager.getStudent(
            req.params.ppuid,
            req.params.userUID
        ).then(function(student) {
            var storage = JSON.parse(req.body["Storage"]);

            if (student != undefined && storage != undefined) {
                var key = undefined;
                    
                if (storage.Key != undefined) {
                    
                    // Get the student's data from the database
                    key = idHelper.getUserDataKeyForDB(storage.Key);

                    studentDataDbManager.getStudentData(
                        student.student_id,
                        key,
                    ).then(function(dataItem) {

                        // Return the student's data from the database
                        if (dataItem != undefined) {

                            if (logHelper.isDebugEnabled()) {
                                logHelper.logMsg(TAG, `storage.Key: ${key}`);
                                logHelper.logMsg(TAG, `dataItem: ${JSON.stringify(dataItem)}`);
                            }
    
                            res.send(JSON.stringify(dataItem.data_value));
                        } else {
                            res.send(JSON.stringify({}));
                        }
                        // ./Return the student's data from the database

                    }, function(err) {
                        logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_DATA, "Failed to getStudentData");
                        res.send(JSON.stringify({}));
                    });
                    // ./Get the student's data from the database

                } else {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_DATA, "storage.Key is undefined");
                    res.send(JSON.stringify({}));
                }
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_DATA, "student or storage is undefined");
                res.send(JSON.stringify({}));
            }
        }, function(err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_DATA, err);
            res.send(JSON.stringify({}));
        });
        // ./Get the student from the database

    } catch (err) {
        logHelper.logMsgWithErr(TAG, MSG_FAILED_GET_DATA, err);
        res.send(JSON.stringify({}));
    }
}

exports.setData = function (req, res) {

    if (logHelper.isDebugEnabled()) {
        logHelper.logMsg(TAG, `\n\n${TAG}/SetData/`);
        logHelper.logMsg(TAG, `user UID: ${req.params.userUID}`);
        logHelper.logMsg(TAG, `PPU request UUID: ${req.params.ppuid}`);
        logHelper.logMsg(TAG, `sessionID: ${req.params.sessionid}`);
    }

    try {
        // Get the student from the database
        studentDbManager.getStudent(
            req.params.ppuid,
            req.params.userUID
        ).then(function(student) {

            // Parse the new data
            try {
                var storage = req.body["Storage"];

                if (objHelper.isJsonString(storage)) {
                    storage = JSON.parse(storage);
                }

                if (student != undefined && storage != undefined) {
    
                    if (storage.Key != undefined && 
                        storage.UTC != undefined && 
                        storage.Timezone != undefined && 
                        storage.Value != undefined) {
    
                        var key = idHelper.getUserDataKeyForDB(storage.Key);
    
                        if (key === idHelper.userDataTypes.STATUS && storage.Value.LTIvalue != undefined) {
    
                            // Update the StudentData object and try to update the student's grade
                            tryUpdateStudentGrade(
                                key, 
                                student.student_id, 
                                req.params.sessionid, 
                                storage,
                                student.user_email
                            )
                            .then(function(results) {
                                res.send(results);
                            }, function(err) {
                                logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, "Invalid input params");
                                res.send(false);
                            });
                            // ./Update the StudentData object and try to update the student's grade

                        } else {
    
                            // Update the StudentData object
                            simplyUpdateStudentsData(
                                key, 
                                student.student_id, 
                                storage,
                                student.user_email
                            )
                            .then(function(results) {
                                res.send(results);
                            }, function(err) {
                                logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, "Invalid input params");
                                res.send(false);
                            });
                            // ./Update the StudentData object

                        }
                    } else {
                        logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, "Invalid input params");
                        res.send(false);
                    }
                } else {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, "Student of storage variable is undefined");
                    res.send(false);
                }
            } catch (err) {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, err);
                res.send(false);
            }
            // ./Parse the new data

        }, function(err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, err);
            res.send(false);
        });
        // ./Get the student from the database

    } catch (err) {
        logHelper.logMsgWithErr(TAG, MSG_FAILED_SET_DATA, err);
        res.send(false);
    }
}

function tryUpdateStudentGrade(key, studentId, sessionid, storage, userEmail) {

    return new Promise(function(resolve, reject) {

        if (logHelper.isDebugEnabled()) {
            logHelper.logMsg(TAG, `\ntryUpdateStudentGrade, key: ${key}`);
            logHelper.logMsg(TAG, `storage ${JSON.stringify(storage)}`);
        }

        // Get student's data from database
        studentDataDbManager.getStudentData(
            studentId,
            key
        ).then(function(studentData) {
            var upsertStudentGrade = false;

            if (studentData == undefined) {
                upsertStudentGrade = true;
            } else {
                studentData = studentData.data_value;

                if (storage.Value.LTIvalue >= studentData.LTIvalue && 
                    storage.Value.percentage > studentData.percentage) {
                    upsertStudentGrade = true;   
                }
            }

            if (upsertStudentGrade) {

                // Upsert the student's data on the database
                studentDataDbManager.upsertStudentData(
                    studentId,
                    userEmail,
                    key,
                    storage.Value,
                    storage.UTC,
                    storage.Timezone
                ).then(function(results) {

                    if (results) {
                        
                        // Update the student's grade on Moodle  
                        try {

                            try {
                                sessionManager.getSessionVariable(
                                    sessionid
                                ).then(function(session) {
                                    var updatedGrade = storage.Value.LTIvalue;

                                    if (updatedGrade > 1) {
                                        updatedGrade = 1;
                                    } else if (updatedGrade < 0) {
                                        updatedGrade = 0;
                                    }

                                    if (logHelper.isDebugEnabled()) {
                                        logHelper.logMsg(TAG, `updatedGrade: ${updatedGrade}`);
                                    }

                                    try {
                                        session.outcome_service.send_replace_result(updatedGrade, (err, isValid) => {
                                                                            
                                            if (err != undefined) {
                                                logHelper.logMsgWithErr(TAG, "Failed to tryUpdateStudentsGrade", err);
                                            }

                                            if (isValid) {
                                                logHelper.logMsg(TAG, "Student's grade has been updated on LTI Customer");
                                                resolve(true);
                                            } else {
                                                logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, "Invalid update on the LTI Customer");
                                                reject(false);
                                            }
                                        }, function(err) {
                                            logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
                                            reject(false);
                                        });
                                    } catch (err) {
                                        logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
                                        reject(false);
                                    }                            
                                }, function(err) {
                                    logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
                                    reject(false);
                                });
                            } catch (err) {
                                logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, "Failed to get session variable");
                                reject(false);
                            }    
                        } catch (err) {
                            logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
                            reject(false);
                        }
                        // ./Update the student's grade on Moodle 

                    } else {
                        logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, "Failed to upsertStudentData");
                        reject(false);    
                    }
                }, function(err) {
                    logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
                    reject(false);
                });
                // ./Upsert the student's data on the database

            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, "LTIValue doens't meet the criteria");
                reject(false);
            }
        }, function(err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_TRY_UPDATE_STUDENT_GRADE, err);
            reject(false);
        });
        // ./Get student's data from database
    });
}

function simplyUpdateStudentsData(key, studentId, storage, studentEmail) {

    return new Promise(function(resolve, reject) {

        if (logHelper.isDebugEnabled()) {
            logHelper.logMsg(TAG, `\nsimplyUpdateStudentsData, key: ${key}`);
            logHelper.logMsg(TAG, `storage ${JSON.stringify(storage)}`);
        }

        studentDataDbManager.upsertStudentData(
            studentId,
            studentEmail,
            key,
            storage.Value,
            storage.UTC,
            storage.Timezone
        ).then(function(results) {

            if (results) {
                resolve(true);
            } else {
                logHelper.logMsgWithErr(TAG, MSG_FAILED_SIMPLY_UPDATE_DATA, "Failed to execute action on Database");
                reject(false);    
            }
        }, function(err) {
            logHelper.logMsgWithErr(TAG, MSG_FAILED_SIMPLY_UPDATE_DATA, err);
            reject(false);
        });
    });
}