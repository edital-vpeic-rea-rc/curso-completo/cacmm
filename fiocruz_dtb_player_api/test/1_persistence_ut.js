/**
 * This file aims to test all the controllers of the system.
 * 
 * About the dependencies
 * Mocha - Describe, run and build tests
 * Chai - Check values within the tests
 */

const expect = require("chai").expect;

const publicSettings = require("../settings/public");
const logHelper = require("../helpers/log_helper");
const sqlManager = require("../persistence/sql_manager");
const ltiConsumerDbManager = require("../persistence/lti_consumer_db_manager");
const studentDbManager = require("../persistence/student_db_manager");
const studentDataDbManager = require("../persistence/student_data_db_manager");

const TAG = "1_persistence_ut";
const TEST_CRUD_LTI_CONSUMER = "CRUD LTI_Consumer";
const TEST_CRUD_STUDENT = "CRUD Student";
const TEST_CRUD_STUDENT_DATA = "CRUD StudentData";

publicSettings.register_logs_remote_db = false;
sqlManager.setIsTesting(true);

var handleTestFailure = function(testFlag, err) {
    logHelper.logMsgWithErr(TAG, testFlag, err);
    expect(false).to.be.equal(true);
    done();
}

describe("Persistence", function() {
    
    describe(TEST_CRUD_LTI_CONSUMER, function() {
        this.timeout(40000);

        it("Should upsert, find and delete LTI_Consumers", function(done) {
            // Upsert LTIConsumer 1
            ltiConsumerDbManager.upsertLTIConsumer(
                "LTI Test Course 1",
                "http://localhost:8888/moodle38/mod/lti/service1.php",
                "LTI Course 1",
                "Mosquitos: Bases da Vigilância e Controle - HTML 1",
                "Moodle 3.8 1",
                "LTI-1p0 1",
                "2019-06-23 11:36:57.001-0300",
                -180
            ).then(function(results) {
                expect(results).to.be.equal(true);
                
                // Upsert LTIConsumer 2
                ltiConsumerDbManager.upsertLTIConsumer(
                    "LTI Test Course 2",
                    "http://localhost:8888/moodle38/mod/lti/service2.php",
                    "LTI Course 2",
                    "Mosquitos: Bases da Vigilância e Controle - HTML 2",
                    "Moodle 3.8 2",
                    "LTI-1p0 2",
                    "2019-06-23 11:36:57.001-0300",
                    -180
                ).then(function(results) {
                    expect(results).to.be.equal(true);

                    // Get all LTIConsumers
                    ltiConsumerDbManager.getLTIConsumers()
                    .then(function(allLtiConsumers) {
                        expect(allLtiConsumers).to.be.an('array');
                        expect(allLtiConsumers.length).to.be.equal(2);

                        // Get LTIConsumer 2
                        ltiConsumerDbManager.getLTIConsumer(
                            "LTI Test Course 2",
                            "http://localhost:8888/moodle38/mod/lti/service2.php"
                        ).then(function(ltiConsumer2) {
                            expect(ltiConsumer2.course_name).to.be.equal("LTI Test Course 2");

                            // Delete LTIConsumer 1
                            ltiConsumerDbManager.deleteLTIConsumer(
                                "LTI Test Course 1",
                                "http://localhost:8888/moodle38/mod/lti/service1.php"
                            ).then(function(results) {
                                expect(results).to.be.equal(true);

                                // Delete LTIConsumer 2
                                ltiConsumerDbManager.deleteLTIConsumer(
                                    "LTI Test Course 2",
                                    "http://localhost:8888/moodle38/mod/lti/service2.php"
                                ).then(function(results) {
                                    expect(results).to.be.equal(true);
                                    
                                    // Get all LTIConsumers and get an empty array
                                    ltiConsumerDbManager.getLTIConsumers()
                                    .then(function(allLtiConsumers) {
                                        expect(allLtiConsumers.length).to.be.equal(0);
                                        done();

                                    }, function(err) {
                                        handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                                    });
                                }, function(err) {
                                    handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                                });
                            }, function(err) {
                                handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                            });
                        }, function(err) {
                            handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                        });
                    }, function(err) {
                        handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                    });
                }, function(err) {
                    handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
                });
            }, function(err) {
                handleTestFailure(TEST_CRUD_LTI_CONSUMER, err);
            });
        });
    });

    describe(TEST_CRUD_STUDENT, function() {
        this.timeout(80000);

        it("Should upsert, find and delete Student", function(done) {

            // Upsert LTIConsumer 1
            ltiConsumerDbManager.upsertLTIConsumer(
                "LTI Test Course 1",
                "http://localhost:8888/moodle38/mod/lti/service1.php",
                "LTI Course 1",
                "Mosquitos: Bases da Vigilância e Controle - HTML 1",
                "Moodle 3.8 1",
                "LTI-1p0 1",
                "2019-06-23 11:36:57.001-0300",
                -180
            ).then(function(results) {
                expect(results).to.be.equal(true);

                // Get LTIConsumer 1
                ltiConsumerDbManager.getLTIConsumer(
                    "LTI Test Course 1",
                    "http://localhost:8888/moodle38/mod/lti/service1.php"
                ).then(function(ltiConsumer1) {
                    expect(ltiConsumer1.course_name).to.be.equal("LTI Test Course 1");

                    // Upsert Student 1
                    studentDbManager.upsertStudent(
                        ltiConsumer1.lti_consumer_id,
                        "user.email.1@gmail.com",
                        "User 1",
                        "User Number 1",
                        "4",
                        "Learner",
                        "2019-06-23 11:36:57.001-0300",
                        -180
                    ).then(function(results) {
                        expect(results).to.be.equal(true);
                        
                        // Upsert Student 2
                        studentDbManager.upsertStudent(
                            ltiConsumer1.lti_consumer_id,
                            "user.email.2@gmail.com",
                            "User 2",
                            "User Number 2",
                            "8",
                            "Learner",
                            "2019-06-23 11:36:57.001-0300",
                            -180
                        ).then(function(results) {
                            expect(results).to.be.equal(true);

                            // Get all Students
                            studentDbManager.getStudents()
                            .then(function(allStudents) {
                                expect(allStudents).to.be.an('array');
                                expect(allStudents.length).to.be.equal(2);

                                // Get Student 2
                                studentDbManager.getStudent(
                                    ltiConsumer1.lti_consumer_id,
                                    "user.email.2@gmail.com"
                                ).then(function(student) {
                                    expect(student.user_email).to.be.equal("user.email.2@gmail.com");

                                    // Delete Student 1
                                    studentDbManager.deleteStudent(
                                        ltiConsumer1.lti_consumer_id,
                                        "user.email.1@gmail.com"
                                    ).then(function(results) {
                                        expect(results).to.be.equal(true);

                                        // Delete Student 2
                                        studentDbManager.deleteStudent(
                                            ltiConsumer1.lti_consumer_id,
                                            "user.email.2@gmail.com"
                                        ).then(function(results) {
                                            expect(results).to.be.equal(true);
                                            
                                            // Get all Students and get an empty array
                                            studentDbManager.getStudents()
                                            .then(function(allStudents) {
                                                expect(allStudents.length).to.be.equal(0);

                                                // Delete LTIConsumer 1
                                                ltiConsumerDbManager.deleteLTIConsumer(
                                                    "LTI Test Course 1",
                                                    "http://localhost:8888/moodle38/mod/lti/service1.php",
                                                ).then(function(results) {
                                                    expect(results).to.be.equal(true);
                                                    done();

                                                }, function(err) {
                                                    handleTestFailure(TEST_CRUD_STUDENT, err);
                                                });
                                            }, function(err) {
                                                handleTestFailure(TEST_CRUD_STUDENT, err);
                                            });
                                        }, function(err) {
                                            handleTestFailure(TEST_CRUD_STUDENT, err);
                                        });
                                    }, function(err) {
                                        handleTestFailure(TEST_CRUD_STUDENT, err);
                                    });
                                }, function(err) {
                                    handleTestFailure(TEST_CRUD_STUDENT, err);
                                });
                            }, function(err) {
                                handleTestFailure(TEST_CRUD_STUDENT, err);
                            });
                        }, function(err) {
                            handleTestFailure(TEST_CRUD_STUDENT, err);
                        });
                    }, function(err) {
                        handleTestFailure(TEST_CRUD_STUDENT, err);
                    });
                }, function(err) {
                    handleTestFailure(TEST_CRUD_STUDENT, err);
                });
            }, function(err) {
                handleTestFailure(TEST_CRUD_STUDENT, err);
            });
        });
    });

    describe(TEST_CRUD_STUDENT_DATA, function() {
        this.timeout(120000);

        it("Should upsert, find and delete StudentData", function(done) {

            // Upsert LTIConsumer 1
            ltiConsumerDbManager.upsertLTIConsumer(
                "LTI Test Course 1",
                "http://localhost:8888/moodle38/mod/lti/service1.php",
                "LTI Course 1",
                "Mosquitos: Bases da Vigilância e Controle - HTML 1",
                "Moodle 3.8 1",
                "LTI-1p0 1",
                "2019-06-23 11:36:57.001-0300",
                -180
            ).then(function(results) {
                expect(results).to.be.equal(true);

                // Get LTIConsumer 1
                ltiConsumerDbManager.getLTIConsumer(
                    "LTI Test Course 1",
                    "http://localhost:8888/moodle38/mod/lti/service1.php",
                ).then(function(ltiConsumer1) {
                    expect(ltiConsumer1.course_name).to.be.equal("LTI Test Course 1");

                    // Upsert Student 1
                    studentDbManager.upsertStudent(
                        ltiConsumer1.lti_consumer_id,
                        "user.email.1@gmail.com",
                        "User 1",
                        "User Number 1",
                        "4",
                        "Learner",
                        "2019-06-23 11:36:57.001-0300",
                        -180
                    ).then(function(results) {
                        expect(results).to.be.equal(true);
                        
                        // Get Student 1
                        studentDbManager.getStudent(
                            ltiConsumer1.lti_consumer_id,
                            "user.email.1@gmail.com",
                        ).then(function(student1) {
                            expect(student1.user_email).to.be.equal("user.email.1@gmail.com");

                            // Upsert StudentData 1
                            studentDataDbManager.upsertStudentData(
                                student1.student_id, 
                                student1.user_email,
                                "NAVIGATION",
                                `{
                                    "item": "NAVIGATION",
                                    "utc": "2019-06-23 11:36:57.127-0300",
                                    "timezone": -180,
                                    "value": {
                                        "previous": "index.html",
                                        "current": "courseUnits.html"
                                    }
                                }`,
                                "2019-06-23 11:36:57.001-0300",
                                -180
                            ).then(function(results) {
                                expect(results).to.be.equal(true);
                                
                                // Upsert StudentData 2
                                studentDataDbManager.upsertStudentData(
                                    student1.student_id, 
                                    student1.user_email,
                                    "STATUS",
                                    `{
                                        "item": "STATUS",
                                        "utc": "2019-06-23 11:36:57.041-0300",
                                        "timezone": -180,
                                        "value": {
                                            "status": "attended",
                                            "percentage": 0,
                                            "LTIvalue": 0,
                                            "U1_val": 0,
                                            "U2_val": 0,
                                            "U3_val": 0,
                                            "q1_status": "",
                                            "q2_status": "",
                                            "q3_status": "",
                                            "q4_status": "",
                                            "q5_status": "",
                                            "q6_status": "",
                                            "q7_status": "",
                                            "q8_status": "",
                                            "q9_status": "",
                                            "q10_status": "",
                                            "q11_status": "",
                                            "q12_status": "",
                                            "q14_status": "",
                                            "q13_status": "",
                                            "q15_status": "",
                                            "q16_status": "",
                                            "q17_status": "",
                                            "q18_status": "",
                                            "q19_status": "",
                                            "q20_status": "",
                                            "q21_status": "",
                                            "q22_status": "",
                                            "q23_status": "",
                                            "q24_status": ""
                                        }
                                    }
                                    `,
                                    "2019-06-23 11:36:59.001-0300",
                                    -180
                                ).then(function(results) {
                                    expect(results).to.be.equal(true);

                                    // Get all StudentData objects of the Student 1
                                    studentDataDbManager.getAllStudentData(
                                        student1.student_id
                                    ).then(function(allStudent1Data) {
                                        expect(allStudent1Data).to.be.an('array');
                                        expect(allStudent1Data.length).to.be.equal(2);

                                        // Get specific StudentData object
                                        studentDataDbManager.getStudentData(
                                            student1.student_id,
                                            "NAVIGATION",
                                        ).then(function(student1DataObj) {
                                            expect(student1DataObj.data_key).to.be.equal("NAVIGATION");

                                            // Delete StudentData 1
                                            studentDataDbManager.deleteStudentData(
                                                student1.student_id,
                                                "NAVIGATION",
                                            ).then(function(results) {
                                                expect(results).to.be.equal(true);

                                                // Delete StudentData 2
                                                studentDataDbManager.deleteStudentData(
                                                    student1.student_id,
                                                    "STATUS",
                                                ).then(function(results) {
                                                    expect(results).to.be.equal(true);
                                                    
                                                    // Get all StudentData objects of the Student 1 and get an empty array
                                                    studentDataDbManager.getAllStudentData(
                                                        student1.student_id
                                                    )
                                                    .then(function(allStudent1Data) {
                                                        expect(allStudent1Data.length).to.be.equal(0);
                                                        
                                                        // Delete Student 2
                                                        studentDbManager.deleteStudent(
                                                            ltiConsumer1.lti_consumer_id,
                                                            "user.email.1@gmail.com"
                                                        ).then(function(results) {
                                                            expect(results).to.be.equal(true);

                                                            // Delete LTIConsumer 1
                                                            ltiConsumerDbManager.deleteLTIConsumer(
                                                                "LTI Test Course 1",
                                                                "http://localhost:8888/moodle38/mod/lti/service1.php",
                                                            ).then(function(results) {
                                                                expect(results).to.be.equal(true);
                                                                done();

                                                            }, function(err) {
                                                                handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                                            });
                                                        }, function(err) {
                                                            handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                                        });
                                                    }, function(err) {
                                                        handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                                    });
                                                }, function(err) {
                                                    handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                                });
                                            }, function(err) {
                                                handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                            });
                                        }, function(err) {
                                            handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                        });
                                    }, function(err) {
                                        handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                    });
                                }, function(err) {
                                    handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                                });
                            }, function(err) {
                                handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                            });
                        }, function(err) {
                            handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                        });
                    }, function(err) {
                        handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                    });
                }, function(err) {
                    handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
                });
            }, function(err) {
                handleTestFailure(TEST_CRUD_STUDENT_DATA, err);
            });
        });
    });

});