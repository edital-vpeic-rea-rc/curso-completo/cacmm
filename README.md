[PPU]: <https://dev.unasus.gov.br/PUBLICO/PPU>
[JQuery - versão, 1.11.1]: <https://code.jquery.com/>
[Bootstrap - versão, 3.3.7]: <https://www.bootstrapcdn.com/legacy/bootstrap/>
[Mustache]: <https://mustache.github.io/>
[Grunt]: <https://gruntjs.com/>
[LTI]: <https://www.imsglobal.org/activity/learning-tools-interoperability>
[MySQL]: <https://dev.mysql.com/downloads/mysql/5.7.html>
[banco de dados não relacional]: <https://firebase.google.com/docs/admin/setup>
[Docker]: <https://www.docker.com/>
[IMS Global]: <https://www.imsglobal.org/activity/learning-tools-interoperability>
[Git]: <https://git-scm.com/>

[Branch master - REA: Mosquitos Bases da Vigilância e Controle]: <https://gitlab.com/edital-vpeic-rea-rc/curso-completo/cacmm?nav_source=navbar>
[Hostgator]: <https://www.hostgator.com.br/>
[Filezilla]: <https://filezilla-project.org/>
[Ambiente de testes do REA: Mosquitos Bases da Vigilância e Controle]: <http://www.mosquitos.eadfiocruzpe.com.br/>
[NodeJS]: <https://nodejs.org/en/>

[![N|Solid](http://www.mosquitos.eadfiocruzpe.com.br/FIOCRUZ_PE_0001_MOSQUITOS_BASES_VIGILANCIA_CONTROLE/images/img_course_brand.png)](http://www.mosquitos.eadfiocruzpe.com.br/)

# Sobre

O Recurso Educacional Aberto - REA, Mosquitos: Bases da Vigilância e Controle, foi criado com base no padrão [PPU], utilizando HTML5, CSS3 e Javascript. As seguintes bibliotecas foram utilizadas na construção do projeto:

* [JQuery - versão, 1.11.1] - Utilizada para a manipulação do DOM.
* [Bootstrap - versão, 3.3.7] - Utilizada para tornar o layout responsivo.
* [Mustache] - Utilizada para construir os templates HTML dos componentes customizados criados com HTML5, JS e CSS3 para o curso.

A padronização PPU é uma forma de empacotar uma aplicação HTML5, CSS3 e Javascript garantindo os princípios de um Recurso Educacional Aberto REA, de modo que ela se conecte a uma API, que por sua vez implementa o padrão [LTI], criado pela [IMS Global] para conectar recursos educacionais a sistemas de gestão do aprendizado - Learning Management System LMS, como o Moodle.

## Quais são as dependências deste projeto?

A principal dependência deste projeto é uma API escrita em [NodeJS], que faz a ponte entre a aplicação no padrão PPU e o LMS, utilizando o padrão [LTI]. Esta API utiliza um banco de dados relacional [MySQL], para a gestão dos dados dos LMS clientes, dos usuários e de seus dados. Ela também utiliza um [banco de dados não relacional], para gerenciar todas as requisições relevantes dos usuários feitas para a API. 

A API FiocruzDTB PPU Player deve ser hospedada no mesmo servidor da applicação PPU. O script que conecta a aplicação PPU à API é o "se_unasus_player_api.js", ele deve ficar junto com o pacote da aplicação, que por sua vez deve ficar dentro da pasta public do servidor NodeJs. Abaixo segue um esquema que demonstra esta organização:

```
    README.md
    DOCKERFILE
    ..
    ..
    fiocruz_dtb_player_api/
        index.js
        gruntfile.js
        node_modules/
        ..
        ..
        public/
            se_unasus_player_api.js
            player.html
            ..
            ..
            APLICACAO_FORMATADA_COMO_PPU/
                se_unasus_pack.js
                se_unasus_pack.json
                capa.png
                index.html
                ..
                ..
```

Para encapsular esta estrutura e assegurar as dependências da API e do PPU, a ferramenta [Docker] foi utilizada no projeto.

### O que devo fazer para testar este projeto?

Antes de iniciar o processo de instalação, assista a este vídeo: https://drive.google.com/file/d/1g8_OYxYCH5S_ukwkROdHZFio2InExkn8/view?usp=sharing

### 0 - Instale as dependências do projeto (LAMP e Docker)

### 0.1 - Crie os bancos de dados relacionais de testes e de produção.

OBS: Um banco de dados MySQL de produção já está definido no arquivo de configuração do projeto, portanto fica a cargo da equipe encarregada pela instalação deste recurso criar ou não criar outro banco de dados, o projeto já está pronto para uso em seu estado atual, porém é aconselhável criar um novo banco em um servidor que esteja adequado para a carga de requisições esperada para o curso. 

O novo banco de dados MySQL pode ser criado no próprio servidor da API ou em outro servidor, eu pessoalmente acredito que seja melhor criá-lo em um servidor dedicado para o banco de dados, pois esta API foi criada de forma modular e portanto, permite que várias instâncias estejam se comunicando diretamente com um único banco de dados, ou seja, seria possível ter N instâncias desta API funcionando e escrevendo no mesmo banco de dados, o que tornaria o sistema mais escalável e a manutenção dos dados mais fácil.

Porém, como o intuito do projeto é que ele seja o mais independente possível, abaixo seguem algumas dicas para a criação do banco de dados no próprio servidor da API. 

OBS: A dica abaixo considera que o servidor está funcionando com o sistema operacional Ubuntu 16.4.

- Instale no servidor a stack LAMP - Linux Apache MySQL PHP.
    https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

Após a instalação, você terá acesso ao servidor de banco de dados MySQL de preferência a uma versão igual ou superior à v5.7.23 e também ao phpMyAdmin, a ferramenta gráfica para o gerenciamento do servidor MySQL.

- Crie um novo usuário para o servidor do banco de dados.
- Crie um novo banco de dados de produção para o projeto.
- Crie um novo banco de dados de testes para o projeto.
- Selecione o banco de dados de produção e então a aba SQL, copie os comandos abaixo, não se esquecendo de substituir o nome do usuário user_name e o nome do banco de dados bd_name no fim dos comandos abaixo:

```
    CREATE TABLE LTI_Consumer
    (
    lti_consumer_id        int           NOT NULL AUTO_INCREMENT,
    course_name            varchar(1000) NOT NULL,
    course_url             varchar(1000) NOT NULL,
    course_short_name      varchar(255)  NULL,
    external_resource_name varchar(1000) NULL,
    lti_consumer_tool_name varchar(255)  NULL,
    lti_version            varchar(100)  NULL,
    created_at             varchar(100)  NULL,
    updated_at             varchar(100)  NULL,
    timezone               integer       NULL,

    PRIMARY KEY(lti_consumer_id)
    );

    CREATE TABLE Student
    (
    student_id      int          NOT NULL AUTO_INCREMENT,
    lti_consumer_id int          NOT NULL,   
    user_email      varchar(255) NOT NULL,
    user_name       varchar(150) NOT NULL,
    full_name       varchar(300) NULL,
    user_id         varchar(100) NOT NULL,
    role            varchar(100) NOT NULL,
    created_at      varchar(100) NULL,
    updated_at      varchar(100) NULL,
    timezone        integer      NULL,

    PRIMARY KEY (student_id),
    FOREIGN KEY (lti_consumer_id) REFERENCES LTI_Consumer(lti_consumer_id)
    );

    CREATE TABLE Student_Data
    (
    student_data_id int          NOT NULL AUTO_INCREMENT,
    student_id      int          NOT NULL,
    data_key        varchar(500) NOT NULL,
    data_value      text         NOT NULL,
    created_at      varchar(100) NULL,
    updated_at      varchar(100) NULL,
    timezone        integer      NULL,

    PRIMARY KEY (student_data_id),
    FOREIGN KEY (student_id) REFERENCES Student(student_id)
    );

    GRANT ALL PRIVILEGES ON bd_name.* TO 'user_name'@'localhost'
```
- Clique no botão "OK", e então as tabelas do banco de dados serão criadas e o usuário terá acesso para modificar o banco.
- Repita o procedimento no banco de dados de testes, novamente não se esqueça de atualizar os valores no último comando.
- Anote as seguintes informações para os bancos de dados de produção e testes:
```
    host     : // exemplo:   '127.0.0.1',
    port     : // exemplo:   8889,
    database : // exemplo:   'rea_ppu_mosquitos_vetores_test',
    user     : // exemplo:   'rea_ppu_admin',
    password : // exemplo:   '1234qwer'
```

### 0.2 - Instale o Docker

O [Docker] é uma ferramenta utilizada para encapsular o sistema operacional e dependências de uma aplicação, eliminando o problema de dependências conflintantes em diferentes ambientes de execução. Ele foi utilizado nesse projeto para encapsular a API FiocruzDTB PPU Player, portanto vamos à instalação do [Docker].

- Siga o passo a passo contido neste excelente tutorial da DigitalOcean:
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

### 0.3 - Instale o Git

O [Git] é uma ferramenta utilizada para fazer a gestão do código fonte e é através dela que vamos obter o código do projeto da branch master de forma rápida e prática.

- Siga o passo a passo deste tutorial:
https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-16-04

### 1 - Obtenha o código do projeto em sua branch master 

Faça o download ou clone o projeto em sua branch master: [Branch master - REA: Mosquitos Bases da Vigilância e Controle]. 

### 2 - Atualize manualmente na API, os arquivos de configuração não públicos enviados por email

Atualize o conteúdo do arquivo abaixo com o texto enviado por email:

```
    README.md
    DOCKERFILE
    ..
    ..
    fiocruz_dtb_player_api/
        index.js
        gruntfile.js
        node_modules/
        settings/
            secret.js <<<=== Atualize o conteúdo deste arquivo
        ..
        ..
        public/
            se_unasus_player_api.js
            player.html
            ..
            ..
            APLICACAO_FORMATADA_COMO_PPU/
                se_unasus_pack.js
                se_unasus_pack.json
                capa.png
                index.html
                ..
                ..
```

Atualize os valores das variáveis abaixo, com as informações dos bancos de dados relacionais criados anteriormente para o projeto

    - relational_db_connection
    - relational_db_connection_test
	
**NOTA:**  

Se você não está utilizando o docker, após clonar o projeto é necessário rodar o comando `npm install` para instalar as dependências do projeto.  

### 3 - Execute o projeto em um novo container do Docker

- Navegue até a pasta raiz do projeto, a que contém o Dockerfile, e execute o seguinte comando para criar uma imagem da API FiocruzDBT PPU Player.
```
docker image build -t diegosilva776/fiocruz_dtb_player_api:1.0.0 .
```
- Confirme que a imagem foi criada corretamente
```
docker image ls
```
- Execute a imagem criada anteriormente em modo de testes. O comando abaixo executará um container permitindo a visualização de seus logs e portanto os testes unitários da camada de persistência da aplicação.
```
docker run -a STDOUT -p 5000:5000  diegosilva776/fiocruz_dtb_player_api:1.0.0
```
- Após o teste, exclua o container digitanto o comando abaixo:
```
crt + c
```
- Agora execute a image em um container do Docker como um serviço. O comando abaixo executa a imagem criada anteriormente em um container do docker como um serviço, conecta a porta 5000 do container à porta 5000 do servidor e garante que o container sempre seja reiniciado automaticamente em caso de falhas no servidor ou do próprio container.
```
docker run -dit --restart unless-stopped -p 5000:5000 diegosilva776/fiocruz_dtb_player_api:1.0.0 
```
- Para ver o log do container em execução, execute o comando abaixo e copie o id do container
```
docker container ls
```
- Agora execute o log utilizando o id do container
```
docker logs -f id_do_container
```

### 4 - Configure a conexão entre o LMS, o Moodle neste caso, e o servidor que está executando a API

- Abra o arquivo secret.js e copie os valores da variável lti_auth: KEY e SHARED_SECRET.
- Crie um novo recurso externo no curso do Moodle onde se deseja adicionar este PPU.
- Abra a página de configurações do recurso externo e na seção 'General', preencha as seguintes opções:
    - Tool URL: http(s)://url_do_servidor_utilizado:5000/ext_module
    - Launch Container: New window
    - Consumer Key: o valor de KEY
    - Shared secret: o valor de SHARED_SECRET
- Na seção 'Grade':
    - Type: point
    - Maximum grade: 1
    - Grade to pass: 1 

### 5 - Em caso de dúvidas

- Não hesite em entrar em contato: 
    - diego.marcolino.silva@gmail.com
    - producao.eadpe@gmail.com

### Extra - Teste o projeto localmente utilizando o Firefox, independente da API

Este tipo de testes pode ser útil se você quiser avaliar somente o lado cliente da aplicação, ou seja, a aplicação no formato PPU.

Para testar o projeto localmente no Firefox, descompacte a pasta que você baixou ou clonou da branch master,navegue até a pasta indicada abaixo e clique com o botão direito sobre o arquivo player.html e então selecione "Abrir com > Firefox":

```
    README.md
    DOCKERFILE
    ..
    ..
    fiocruz_dtb_player_api/
        index.js
        gruntfile.js
        node_modules/
        settings/
        ..
        ..
        public/
            se_unasus_player_api.js
            player.html  <<<=== Clique com o botão direito neste arquivo.
            ..
            ..
            APLICACAO_FORMATADA_COMO_PPU/
                se_unasus_pack.js
                se_unasus_pack.json
                capa.png
                index.html
                ..
                ..
```

Para instalar somente o projeto do PPU em um servidor web, adquira uma conta de hospedagem simples no [Hostgator] ou em um serviço similar, se conecte com sua conta FTP do serviço adquirido através do [Filezilla], que é uma solução para a transferência de arquivos de sua máquina para um servidor através do procolo FTP, após realizar a conexão com seu servidor utilizando suas credenciais de acesso, faça a transferência dos arquivos do curso para uma pasta dentro do servidor. O resultado deste procedimento deve ser similar a este: [Ambiente de testes do REA: Mosquitos Bases da Vigilância e Controle]