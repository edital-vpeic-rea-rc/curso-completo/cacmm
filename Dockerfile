FROM node:latest

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY ./fiocruz_dtb_player_api/package.json /usr/src/app

RUN npm install

COPY ./fiocruz_dtb_player_api /usr/src/app

EXPOSE 5000

CMD ["npm", "start"]